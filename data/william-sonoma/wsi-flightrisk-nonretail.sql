select *
from (
	select *
	from (
		select
			p2.employee as employee
			, date_trunc('month', beg_date) as month
			, coalesce(title, 'Unknown') as title
			, coalesce(departmentname, 'Unknown') as departmentname
			, coalesce(exempt, 'Unknown') as exempt
			, coalesce(fte, 1) as fte
			, coalesce(points, 0) as points
			, coalesce(eth_desc, 'Unknown') as eth_desc
			, coalesce(job_cat, 'Unknown') as job_cat
			, coalesce(generation, 'Unknown') as generation
			, coalesce(sex, 'Unknown') as sex
			, coalesce(payrate, 0) as payrate
			, coalesce(workcity, 'Unknown') as workcity
			, coalesce(workstate, 'Unknown') as workstate
			, coalesce(workcountry, 'Unknown') as workcountry
			, coalesce(level1, 'Unknown') as level1
			, coalesce(level2, 'Unknown') as level2
			, coalesce(schedule, 'Unknown') as schedule
			, coalesce(workschedule, 'Unknown') as workschedule
			, coalesce(perfrating, 0) as perfrating
			, coalesce(grade, 'Unknown') as grade
			, round((division_test(((least(termdate::date, end_date::date, now()::date) - coalesce(pjobdate::date, servicedate::date))+1), 365)::numeric),1) as hrjobten
			, row_number()
				over (
					partition by employee
					order by beg_date desc, end_date desc
				) as transaction_seq
			, case when empstatus like 'T%' then 1 else 0 end as turn
		from eff_wsi_employee_phase2_new p2
		where level1 NOT IN ('Retail', 'Global Retail')
	) allemps
	where
		transaction_seq = 1
) base
	inner join (
		select employee, empstatus
		from (
			select
				employee
				, empstatus
				, row_number()
					over (
						partition by employee
						order by beg_date desc, end_date desc
					) as transaction_seq
			from eff_wsi_employee_phase2_new p2
			where empstatus not like 'T%'
		) laststatus
		where transaction_seq = 1
	) statuses
		on base.employee = statuses.employee	
		
	inner join (
		select
			trans.employee
			, count(type) filter (where type = 'Promotions') as num_promotions
			, count(type) filter (where type = 'Demotions') as num_demotions
			, count(type) filter (where type = 'Laterals') as num_laterals
			, count(type) filter (where type = 'Leave of Absence') as num_leaves
			, count(type) filter (where type = 'Location Change') as num_loc_changes
			, count(type) filter (where type = 'Job Change') as num_job_changes
			, count(type) filter (where type = 'Department Change') as num_dept_changes
		from vw_hrtrans_type_p2 trans
		group by trans.employee
	) movement
		on movement.employee::bigint = base.employee::bigint
		