webpackJsonp([6],{

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	__webpack_require__.p = absUrl('assets/js/');
	
	var dash = __webpack_require__(1);
	__webpack_require__(1737);
	__webpack_require__(1758);
	__webpack_require__(1737);
	__webpack_require__(1704);
	__webpack_require__(1760);
	__webpack_require__(1763);
	__webpack_require__(1764);
	__webpack_require__(1775);
	$(function () {
	
	    $.fn.initDoctrineDatepickers = function () {
	        var lst = this.find('.doctrine-form-datepicker');
	        lst.datetimepicker({
	            dateFormat: getPageData('userDateFormat'),
	            showOn: "both",
	            buttonText: '<i class="fas fa-calendar-alt"></i>',
	            showAnim: 'slideDown',
	            changeMonth: true,
	            changeYear: true,
	            autoclose: true,
	            multidate: false,
	            keyboardNavigation: false,
	            pickTime: false
	        });
	    };
	
	    // Initialize datepickers
	    try {
	        var lst = $('.datepicker-target, .doctrine-form-datepicker, .datepicker');
	        lst.datetimepicker({
	            dateFormat: getPageData('userDateFormat'),
	            buttonText: '<i class="icon-calendar"></i>',
	            showAnim: 'slideDown',
	            changeMonth: true,
	            changeYear: true,
	            autoclose: true,
	            multidate: false,
	            forceParse: false,
	            keyboardNavigation: false,
	            pickTime: false
	        });
	    } catch (e) {}
	
	    var maxHeaderOffsetDefault = 55;
	    var maxheaderoffset = maxHeaderOffsetDefault;
	    var screen_small = 767;
	
	    function viewport() {
	        var e = window,
	            a = 'inner';
	        if (!('innerWidth' in window)) {
	            a = 'client';
	            e = document.documentElement || document.body;
	        }
	        return { width: e[a + 'Width'], height: e[a + 'Height'] };
	    }
	
	    function updateMaxHeaderOffset() {
	        if (viewport().width <= screen_small) {
	            maxheaderoffset = 0;
	        } else {
	            maxheaderoffset = maxHeaderOffsetDefault;
	        }
	    }
	
	    var offsetScroll = function offsetScroll(event) {
	        updateMaxHeaderOffset();
	        if (window.pageYOffset > maxheaderoffset) {
	            $('#dash-header').css('top', -maxheaderoffset + "px");
	        } else {
	            $('#dash-header').css('top', -window.pageYOffset + "px");
	        }
	    };
	    $(window).scroll(offsetScroll);
	    setTimeout(function () {
	        $(window).scroll();
	    }, 100);
	
	    setTimeout(function () {
	        return $(".dash-container").css('margin-top', $("#dash-header").height());
	    }, 200);
	});

/***/ }),

/***/ 1758:
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;/*
	//! version : 3.1.3
	=========================================================
	bootstrap-datetimepicker.js
	https://github.com/Eonasdan/bootstrap-datetimepicker
	=========================================================
	The MIT License (MIT)
	
	Copyright (c) 2014 Jonathan Peterson
	
	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:
	
	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.
	
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
	*/
	;(function (root, factory) {
	    'use strict';
	    if (true) {
	        // AMD is used - Register as an anonymous module.
	        !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(31), __webpack_require__(1759)], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	    } else if (typeof exports === 'object') {
	        factory(require('jquery'), require('moment'));
	    }
	    else {
	        // Neither AMD or CommonJS used. Use global variables.
	        if (!jQuery) {
	            throw new Error('bootstrap-datetimepicker requires jQuery to be loaded first');
	        }
	        if (!moment) {
	            throw new Error('bootstrap-datetimepicker requires moment.js to be loaded first');
	        }
	        factory(root.jQuery, moment);
	    }
	}(this, function ($, moment) {
	    'use strict';
	    if (typeof moment === 'undefined') {
	        throw new Error('momentjs is required');
	    }
	
	    var dpgId = 0,
	
	    DateTimePicker = function (element, options) {
	        var defaults = $.fn.datetimepicker.defaults,
	
	            icons = {
	                time: 'glyphicon glyphicon-time',
	                date: 'glyphicon glyphicon-calendar',
	                up: 'glyphicon glyphicon-chevron-up',
	                down: 'glyphicon glyphicon-chevron-down'
	            },
	
	            picker = this,
	            errored = false,
	            dDate,
	
	        init = function () {
	            var icon = false, localeData, rInterval;
	            picker.options = $.extend({}, defaults, options);
	            picker.options.icons = $.extend({}, icons, picker.options.icons);
	
	            picker.element = $(element);
	
	            dataToOptions();
	
	            if (!(picker.options.pickTime || picker.options.pickDate)) {
	                throw new Error('Must choose at least one picker');
	            }
	
	            picker.id = dpgId++;
	            moment.locale(picker.options.language);
	            picker.date = moment();
	            picker.unset = false;
	            picker.isInput = picker.element.is('input');
	            picker.component = false;
	
	            if (picker.element.hasClass('input-group')) {
	                if (picker.element.find('.datepickerbutton').size() === 0) {//in case there is more then one 'input-group-addon' Issue #48
	                    picker.component = picker.element.find('[class^="input-group-"]');
	                }
	                else {
	                    picker.component = picker.element.find('.datepickerbutton');
	                }
	            }
	            picker.format = picker.options.format;
	
	            localeData = moment().localeData();
	
	            if (!picker.format) {
	                picker.format = (picker.options.pickDate ? localeData.longDateFormat('L') : '');
	                if (picker.options.pickDate && picker.options.pickTime) {
	                    picker.format += ' ';
	                }
	                picker.format += (picker.options.pickTime ? localeData.longDateFormat('LT') : '');
	                if (picker.options.useSeconds) {
	                    if (localeData.longDateFormat('LT').indexOf(' A') !== -1) {
	                        picker.format = picker.format.split(' A')[0] + ':ss A';
	                    }
	                    else {
	                        picker.format += ':ss';
	                    }
	                }
	            }
	            picker.use24hours = (picker.format.toLowerCase().indexOf('a') < 0 && picker.format.indexOf('h') < 0);
	
	            if (picker.component) {
	                icon = picker.component.find('span');
	            }
	
	            if (picker.options.pickTime) {
	                if (icon) {
	                    icon.addClass(picker.options.icons.time);
	                }
	            }
	            if (picker.options.pickDate) {
	                if (icon) {
	                    icon.removeClass(picker.options.icons.time);
	                    icon.addClass(picker.options.icons.date);
	                }
	            }
	
	            picker.options.widgetParent =
	                typeof picker.options.widgetParent === 'string' && picker.options.widgetParent ||
	                picker.element.parents().filter(function () {
	                    return 'scroll' === $(this).css('overflow-y');
	                }).get(0) ||
	                'body';
	
	            picker.widget = $(getTemplate()).appendTo(picker.options.widgetParent);
	
	            picker.minViewMode = picker.options.minViewMode || 0;
	            if (typeof picker.minViewMode === 'string') {
	                switch (picker.minViewMode) {
	                    case 'months':
	                        picker.minViewMode = 1;
	                        break;
	                    case 'years':
	                        picker.minViewMode = 2;
	                        break;
	                    default:
	                        picker.minViewMode = 0;
	                        break;
	                }
	            }
	            picker.viewMode = picker.options.viewMode || 0;
	            if (typeof picker.viewMode === 'string') {
	                switch (picker.viewMode) {
	                    case 'months':
	                        picker.viewMode = 1;
	                        break;
	                    case 'years':
	                        picker.viewMode = 2;
	                        break;
	                    default:
	                        picker.viewMode = 0;
	                        break;
	                }
	            }
	
	            picker.viewMode = Math.max(picker.viewMode, picker.minViewMode);
	
	            picker.options.disabledDates = indexGivenDates(picker.options.disabledDates);
	            picker.options.enabledDates = indexGivenDates(picker.options.enabledDates);
	
	            picker.startViewMode = picker.viewMode;
	            picker.setMinDate(picker.options.minDate);
	            picker.setMaxDate(picker.options.maxDate);
	            fillDow();
	            fillMonths();
	            fillHours();
	            fillMinutes();
	            fillSeconds();
	            update();
	            showMode();
	            if (!getPickerInput().prop('disabled')) {
	                attachDatePickerEvents();
	            }
	            if (picker.options.defaultDate !== '' && getPickerInput().val() === '') {
	                picker.setValue(picker.options.defaultDate);
	            }
	            if (picker.options.minuteStepping !== 1) {
	                rInterval = picker.options.minuteStepping;
	                picker.date.minutes((Math.round(picker.date.minutes() / rInterval) * rInterval) % 60).seconds(0);
	            }
	        },
	
	        getPickerInput = function () {
	            var input;
	
	            if (picker.isInput) {
	                return picker.element;
	            }
	            input = picker.element.find('.datepickerinput');
	            if (input.size() === 0) {
	                input = picker.element.find('input');
	            }
	            else if (!input.is('input')) {
	                throw new Error('CSS class "datepickerinput" cannot be applied to non input element');
	            }
	            return input;
	        },
	
	        dataToOptions = function () {
	            var eData;
	            if (picker.element.is('input')) {
	                eData = picker.element.data();
	            }
	            else {
	                eData = picker.element.find('input').data();
	            }
	            if (eData.dateFormat !== undefined) {
	                picker.options.format = eData.dateFormat;
	            }
	            if (eData.datePickdate !== undefined) {
	                picker.options.pickDate = eData.datePickdate;
	            }
	            if (eData.datePicktime !== undefined) {
	                picker.options.pickTime = eData.datePicktime;
	            }
	            if (eData.dateUseminutes !== undefined) {
	                picker.options.useMinutes = eData.dateUseminutes;
	            }
	            if (eData.dateUseseconds !== undefined) {
	                picker.options.useSeconds = eData.dateUseseconds;
	            }
	            if (eData.dateUsecurrent !== undefined) {
	                picker.options.useCurrent = eData.dateUsecurrent;
	            }
	            if (eData.calendarWeeks !== undefined) {
	                picker.options.calendarWeeks = eData.calendarWeeks;
	            }
	            if (eData.dateMinutestepping !== undefined) {
	                picker.options.minuteStepping = eData.dateMinutestepping;
	            }
	            if (eData.dateMindate !== undefined) {
	                picker.options.minDate = eData.dateMindate;
	            }
	            if (eData.dateMaxdate !== undefined) {
	                picker.options.maxDate = eData.dateMaxdate;
	            }
	            if (eData.dateShowtoday !== undefined) {
	                picker.options.showToday = eData.dateShowtoday;
	            }
	            if (eData.dateCollapse !== undefined) {
	                picker.options.collapse = eData.dateCollapse;
	            }
	            if (eData.dateLanguage !== undefined) {
	                picker.options.language = eData.dateLanguage;
	            }
	            if (eData.dateDefaultdate !== undefined) {
	                picker.options.defaultDate = eData.dateDefaultdate;
	            }
	            if (eData.dateDisableddates !== undefined) {
	                picker.options.disabledDates = eData.dateDisableddates;
	            }
	            if (eData.dateEnableddates !== undefined) {
	                picker.options.enabledDates = eData.dateEnableddates;
	            }
	            if (eData.dateIcons !== undefined) {
	                picker.options.icons = eData.dateIcons;
	            }
	            if (eData.dateUsestrict !== undefined) {
	                picker.options.useStrict = eData.dateUsestrict;
	            }
	            if (eData.dateDirection !== undefined) {
	                picker.options.direction = eData.dateDirection;
	            }
	            if (eData.dateSidebyside !== undefined) {
	                picker.options.sideBySide = eData.dateSidebyside;
	            }
	            if (eData.dateDaysofweekdisabled !== undefined) {
	                picker.options.daysOfWeekDisabled = eData.dateDaysofweekdisabled;
	            }
	        },
	
	        place = function () {
	            var position = 'absolute',
	                offset = picker.component ? picker.component.offset() : picker.element.offset(),
	                $window = $(window),
	                placePosition;
	
	            picker.width = picker.component ? picker.component.outerWidth() : picker.element.outerWidth();
	            offset.top = offset.top + picker.element.outerHeight();
	
	            if (picker.options.direction === 'up') {
	                placePosition = 'top';
	            } else if (picker.options.direction === 'bottom') {
	                placePosition = 'bottom';
	            } else if (picker.options.direction === 'auto') {
	                if (offset.top + picker.widget.height() > $window.height() + $window.scrollTop() && picker.widget.height() + picker.element.outerHeight() < offset.top) {
	                    placePosition = 'top';
	                } else {
	                    placePosition = 'bottom';
	                }
	            }
	            if (placePosition === 'top') {
	                offset.bottom = $window.height() - offset.top + picker.element.outerHeight() + 3;
	                picker.widget.addClass('top').removeClass('bottom');
	            } else {
	                offset.top += 1;
	                picker.widget.addClass('bottom').removeClass('top');
	            }
	
	            if (picker.options.width !== undefined) {
	                picker.widget.width(picker.options.width);
	            }
	
	            if (picker.options.orientation === 'left') {
	                picker.widget.addClass('left-oriented');
	                offset.left = offset.left - picker.widget.width() + 20;
	            }
	
	            if (isInFixed()) {
	                position = 'fixed';
	                offset.top -= $window.scrollTop();
	                offset.left -= $window.scrollLeft();
	            }
	
	            if ($window.width() < offset.left + picker.widget.outerWidth()) {
	                offset.right = $window.width() - offset.left - picker.width;
	                offset.left = 'auto';
	                picker.widget.addClass('pull-right');
	            } else {
	                offset.right = 'auto';
	                picker.widget.removeClass('pull-right');
	            }
	
	            if (placePosition === 'top') {
	                picker.widget.css({
	                    position: position,
	                    bottom: offset.bottom,
	                    top: 'auto',
	                    left: offset.left,
	                    right: offset.right
	                });
	            } else {
	                picker.widget.css({
	                    position: position,
	                    top: offset.top,
	                    bottom: 'auto',
	                    left: offset.left,
	                    right: offset.right
	                });
	            }
	        },
	
	        notifyChange = function (oldDate, eventType) {
	            if (moment(picker.date).isSame(moment(oldDate)) && !errored) {
	                return;
	            }
	            errored = false;
	            picker.element.trigger({
	                type: 'dp.change',
	                date: moment(picker.date),
	                oldDate: moment(oldDate)
	            });
	
	            if (eventType !== 'change') {
	                picker.element.change();
	            }
	        },
	
	        notifyError = function (date) {
	            errored = true;
	            picker.element.trigger({
	                type: 'dp.error',
	                date: moment(date, picker.format, picker.options.useStrict)
	            });
	        },
	
	        update = function (newDate) {
	            moment.locale(picker.options.language);
	            var dateStr = newDate;
	            if (!dateStr) {
	                dateStr = getPickerInput().val();
	                if (dateStr) {
	                    picker.date = moment(dateStr, picker.format, picker.options.useStrict);
	                }
	                if (!picker.date) {
	                    picker.date = moment();
	                }
	            }
	            picker.viewDate = moment(picker.date).startOf('month');
	            fillDate();
	            fillTime();
	        },
	
	        fillDow = function () {
	            moment.locale(picker.options.language);
	            var html = $('<tr>'), weekdaysMin = moment.weekdaysMin(), i;
	            if (picker.options.calendarWeeks === true) {
	                html.append('<th class="cw">#</th>');
	            }
	            if (moment().localeData()._week.dow === 0) { // starts on Sunday
	                for (i = 0; i < 7; i++) {
	                    html.append('<th class="dow">' + weekdaysMin[i] + '</th>');
	                }
	            } else {
	                for (i = 1; i < 8; i++) {
	                    if (i === 7) {
	                        html.append('<th class="dow">' + weekdaysMin[0] + '</th>');
	                    } else {
	                        html.append('<th class="dow">' + weekdaysMin[i] + '</th>');
	                    }
	                }
	            }
	            picker.widget.find('.datepicker-days thead').append(html);
	        },
	
	        fillMonths = function () {
	            moment.locale(picker.options.language);
	            var html = '', i, monthsShort = moment.monthsShort();
	            for (i = 0; i < 12; i++) {
	                html += '<span class="month">' + monthsShort[i] + '</span>';
	            }
	            picker.widget.find('.datepicker-months td').append(html);
	        },
	
	        fillDate = function () {
	            if (!picker.options.pickDate) {
	                return;
	            }
	            moment.locale(picker.options.language);
	            var year = picker.viewDate.year(),
	                month = picker.viewDate.month(),
	                startYear = picker.options.minDate.year(),
	                startMonth = picker.options.minDate.month(),
	                endYear = picker.options.maxDate.year(),
	                endMonth = picker.options.maxDate.month(),
	                currentDate,
	                prevMonth, nextMonth, html = [], row, clsName, i, days, yearCont, currentYear, months = moment.months();
	
	            picker.widget.find('.datepicker-days').find('.disabled').removeClass('disabled');
	            picker.widget.find('.datepicker-months').find('.disabled').removeClass('disabled');
	            picker.widget.find('.datepicker-years').find('.disabled').removeClass('disabled');
	
	            picker.widget.find('.datepicker-days th:eq(1)').text(
	                months[month] + ' ' + year);
	
	            prevMonth = moment(picker.viewDate, picker.format, picker.options.useStrict).subtract(1, 'months');
	            days = prevMonth.daysInMonth();
	            prevMonth.date(days).startOf('week');
	            if ((year === startYear && month <= startMonth) || year < startYear) {
	                picker.widget.find('.datepicker-days th:eq(0)').addClass('disabled');
	            }
	            if ((year === endYear && month >= endMonth) || year > endYear) {
	                picker.widget.find('.datepicker-days th:eq(2)').addClass('disabled');
	            }
	
	            nextMonth = moment(prevMonth).add(42, 'd');
	            while (prevMonth.isBefore(nextMonth)) {
	                if (prevMonth.weekday() === moment().startOf('week').weekday()) {
	                    row = $('<tr>');
	                    html.push(row);
	                    if (picker.options.calendarWeeks === true) {
	                        row.append('<td class="cw">' + prevMonth.week() + '</td>');
	                    }
	                }
	                clsName = '';
	                if (prevMonth.year() < year || (prevMonth.year() === year && prevMonth.month() < month)) {
	                    clsName += ' old';
	                } else if (prevMonth.year() > year || (prevMonth.year() === year && prevMonth.month() > month)) {
	                    clsName += ' new';
	                }
	                if (prevMonth.isSame(moment({y: picker.date.year(), M: picker.date.month(), d: picker.date.date()}))) {
	                    clsName += ' active';
	                }
	                if (isInDisableDates(prevMonth, 'day') || !isInEnableDates(prevMonth)) {
	                    clsName += ' disabled';
	                }
	                if (picker.options.showToday === true) {
	                    if (prevMonth.isSame(moment(), 'day')) {
	                        clsName += ' today';
	                    }
	                }
	                if (picker.options.daysOfWeekDisabled) {
	                    for (i = 0; i < picker.options.daysOfWeekDisabled.length; i++) {
	                        if (prevMonth.day() === picker.options.daysOfWeekDisabled[i]) {
	                            clsName += ' disabled';
	                            break;
	                        }
	                    }
	                }
	                row.append('<td class="day' + clsName + '">' + prevMonth.date() + '</td>');
	
	                currentDate = prevMonth.date();
	                prevMonth.add(1, 'd');
	
	                if (currentDate === prevMonth.date()) {
	                    prevMonth.add(1, 'd');
	                }
	            }
	            picker.widget.find('.datepicker-days tbody').empty().append(html);
	            currentYear = picker.date.year();
	            months = picker.widget.find('.datepicker-months').find('th:eq(1)').text(year).end().find('span').removeClass('active');
	            if (currentYear === year) {
	                months.eq(picker.date.month()).addClass('active');
	            }
	            if (year - 1 < startYear) {
	                picker.widget.find('.datepicker-months th:eq(0)').addClass('disabled');
	            }
	            if (year + 1 > endYear) {
	                picker.widget.find('.datepicker-months th:eq(2)').addClass('disabled');
	            }
	            for (i = 0; i < 12; i++) {
	                if ((year === startYear && startMonth > i) || (year < startYear)) {
	                    $(months[i]).addClass('disabled');
	                } else if ((year === endYear && endMonth < i) || (year > endYear)) {
	                    $(months[i]).addClass('disabled');
	                }
	            }
	
	            html = '';
	            year = parseInt(year / 10, 10) * 10;
	            yearCont = picker.widget.find('.datepicker-years').find(
	                'th:eq(1)').text(year + '-' + (year + 9)).parents('table').find('td');
	            picker.widget.find('.datepicker-years').find('th').removeClass('disabled');
	            if (startYear > year) {
	                picker.widget.find('.datepicker-years').find('th:eq(0)').addClass('disabled');
	            }
	            if (endYear < year + 9) {
	                picker.widget.find('.datepicker-years').find('th:eq(2)').addClass('disabled');
	            }
	            year -= 1;
	            for (i = -1; i < 11; i++) {
	                html += '<span class="year' + (i === -1 || i === 10 ? ' old' : '') + (currentYear === year ? ' active' : '') + ((year < startYear || year > endYear) ? ' disabled' : '') + '">' + year + '</span>';
	                year += 1;
	            }
	            yearCont.html(html);
	        },
	
	        fillHours = function () {
	            moment.locale(picker.options.language);
	            var table = picker.widget.find('.timepicker .timepicker-hours table'), html = '', current, i, j;
	            table.parent().hide();
	            if (picker.use24hours) {
	                current = 0;
	                for (i = 0; i < 6; i += 1) {
	                    html += '<tr>';
	                    for (j = 0; j < 4; j += 1) {
	                        html += '<td class="hour">' + padLeft(current.toString()) + '</td>';
	                        current++;
	                    }
	                    html += '</tr>';
	                }
	            }
	            else {
	                current = 1;
	                for (i = 0; i < 3; i += 1) {
	                    html += '<tr>';
	                    for (j = 0; j < 4; j += 1) {
	                        html += '<td class="hour">' + padLeft(current.toString()) + '</td>';
	                        current++;
	                    }
	                    html += '</tr>';
	                }
	            }
	            table.html(html);
	        },
	
	        fillMinutes = function () {
	            var table = picker.widget.find('.timepicker .timepicker-minutes table'), html = '', current = 0, i, j, step = picker.options.minuteStepping;
	            table.parent().hide();
	            if (step === 1)  {
	                step = 5;
	            }
	            for (i = 0; i < Math.ceil(60 / step / 4) ; i++) {
	                html += '<tr>';
	                for (j = 0; j < 4; j += 1) {
	                    if (current < 60) {
	                        html += '<td class="minute">' + padLeft(current.toString()) + '</td>';
	                        current += step;
	                    } else {
	                        html += '<td></td>';
	                    }
	                }
	                html += '</tr>';
	            }
	            table.html(html);
	        },
	
	        fillSeconds = function () {
	            var table = picker.widget.find('.timepicker .timepicker-seconds table'), html = '', current = 0, i, j;
	            table.parent().hide();
	            for (i = 0; i < 3; i++) {
	                html += '<tr>';
	                for (j = 0; j < 4; j += 1) {
	                    html += '<td class="second">' + padLeft(current.toString()) + '</td>';
	                    current += 5;
	                }
	                html += '</tr>';
	            }
	            table.html(html);
	        },
	
	        fillTime = function () {
	            if (!picker.date) {
	                return;
	            }
	            var timeComponents = picker.widget.find('.timepicker span[data-time-component]'),
	                hour = picker.date.hours(),
	                period = picker.date.format('A');
	            if (!picker.use24hours) {
	                if (hour === 0) {
	                    hour = 12;
	                } else if (hour !== 12) {
	                    hour = hour % 12;
	                }
	                picker.widget.find('.timepicker [data-action=togglePeriod]').text(period);
	            }
	            timeComponents.filter('[data-time-component=hours]').text(padLeft(hour));
	            timeComponents.filter('[data-time-component=minutes]').text(padLeft(picker.date.minutes()));
	            timeComponents.filter('[data-time-component=seconds]').text(padLeft(picker.date.second()));
	        },
	
	        click = function (e) {
	            e.stopPropagation();
	            e.preventDefault();
	            picker.unset = false;
	            var target = $(e.target).closest('span, td, th'), month, year, step, day, oldDate = moment(picker.date);
	            if (target.length === 1) {
	                if (!target.is('.disabled')) {
	                    switch (target[0].nodeName.toLowerCase()) {
	                        case 'th':
	                            switch (target[0].className) {
	                                case 'picker-switch':
	                                    showMode(1);
	                                    break;
	                                case 'prev':
	                                case 'next':
	                                    step = dpGlobal.modes[picker.viewMode].navStep;
	                                    if (target[0].className === 'prev') {
	                                        step = step * -1;
	                                    }
	                                    picker.viewDate.add(step, dpGlobal.modes[picker.viewMode].navFnc);
	                                    fillDate();
	                                    break;
	                            }
	                            break;
	                        case 'span':
	                            if (target.is('.month')) {
	                                month = target.parent().find('span').index(target);
	                                picker.viewDate.month(month);
	                            } else {
	                                year = parseInt(target.text(), 10) || 0;
	                                picker.viewDate.year(year);
	                            }
	                            if (picker.viewMode === picker.minViewMode) {
	                                picker.date = moment({
	                                    y: picker.viewDate.year(),
	                                    M: picker.viewDate.month(),
	                                    d: picker.viewDate.date(),
	                                    h: picker.date.hours(),
	                                    m: picker.date.minutes(),
	                                    s: picker.date.seconds()
	                                });
	                                set();
	                                notifyChange(oldDate, e.type);
	                            }
	                            showMode(-1);
	                            fillDate();
	                            break;
	                        case 'td':
	                            if (target.is('.day')) {
	                                day = parseInt(target.text(), 10) || 1;
	                                month = picker.viewDate.month();
	                                year = picker.viewDate.year();
	                                if (target.is('.old')) {
	                                    if (month === 0) {
	                                        month = 11;
	                                        year -= 1;
	                                    } else {
	                                        month -= 1;
	                                    }
	                                } else if (target.is('.new')) {
	                                    if (month === 11) {
	                                        month = 0;
	                                        year += 1;
	                                    } else {
	                                        month += 1;
	                                    }
	                                }
	                                picker.date = moment({
	                                    y: year,
	                                    M: month,
	                                    d: day,
	                                    h: picker.date.hours(),
	                                    m: picker.date.minutes(),
	                                    s: picker.date.seconds()
	                                }
	                                );
	                                picker.viewDate = moment({
	                                    y: year, M: month, d: Math.min(28, day)
	                                });
	                                fillDate();
	                                set();
	                                notifyChange(oldDate, e.type);
	                            }
	                            break;
	                    }
	                }
	            }
	        },
	
	        actions = {
	            incrementHours: function () {
	                checkDate('add', 'hours', 1);
	            },
	
	            incrementMinutes: function () {
	                checkDate('add', 'minutes', picker.options.minuteStepping);
	            },
	
	            incrementSeconds: function () {
	                checkDate('add', 'seconds', 1);
	            },
	
	            decrementHours: function () {
	                checkDate('subtract', 'hours', 1);
	            },
	
	            decrementMinutes: function () {
	                checkDate('subtract', 'minutes', picker.options.minuteStepping);
	            },
	
	            decrementSeconds: function () {
	                checkDate('subtract', 'seconds', 1);
	            },
	
	            togglePeriod: function () {
	                var hour = picker.date.hours();
	                if (hour >= 12) {
	                    hour -= 12;
	                } else {
	                    hour += 12;
	                }
	                picker.date.hours(hour);
	            },
	
	            showPicker: function () {
	                picker.widget.find('.timepicker > div:not(.timepicker-picker)').hide();
	                picker.widget.find('.timepicker .timepicker-picker').show();
	            },
	
	            showHours: function () {
	                picker.widget.find('.timepicker .timepicker-picker').hide();
	                picker.widget.find('.timepicker .timepicker-hours').show();
	            },
	
	            showMinutes: function () {
	                picker.widget.find('.timepicker .timepicker-picker').hide();
	                picker.widget.find('.timepicker .timepicker-minutes').show();
	            },
	
	            showSeconds: function () {
	                picker.widget.find('.timepicker .timepicker-picker').hide();
	                picker.widget.find('.timepicker .timepicker-seconds').show();
	            },
	
	            selectHour: function (e) {
	                var hour = parseInt($(e.target).text(), 10);
	                if (!picker.use24hours) {
	                    if (picker.date.hours() >= 12) {
	                        if (hour !== 12) {
	                            hour += 12;
	                        }
	                    } else {
	                        if (hour === 12) {
	                            hour = 0;
	                        }
	                    }
	                }
	                picker.date.hours(hour);
	                actions.showPicker.call(picker);
	            },
	
	            selectMinute: function (e) {
	                picker.date.minutes(parseInt($(e.target).text(), 10));
	                actions.showPicker.call(picker);
	            },
	
	            selectSecond: function (e) {
	                picker.date.seconds(parseInt($(e.target).text(), 10));
	                actions.showPicker.call(picker);
	            }
	        },
	
	        doAction = function (e) {
	            var oldDate = moment(picker.date),
	                action = $(e.currentTarget).data('action'),
	                rv = actions[action].apply(picker, arguments);
	            stopEvent(e);
	            if (!picker.date) {
	                picker.date = moment({y: 1970});
	            }
	            set();
	            fillTime();
	            notifyChange(oldDate, e.type);
	            return rv;
	        },
	
	        stopEvent = function (e) {
	            e.stopPropagation();
	            e.preventDefault();
	        },
	
	        keydown = function (e) {
	            if (e.keyCode === 27) { // allow escape to hide picker
	                picker.hide();
	            }
	        },
	
	        change = function (e) {
	            moment.locale(picker.options.language);
	            var input = $(e.target), oldDate = moment(picker.date), newDate = moment(input.val(), picker.format, picker.options.useStrict);
	            if (newDate.isValid() && !isInDisableDates(newDate) && isInEnableDates(newDate)) {
	                update();
	                picker.setValue(newDate);
	                notifyChange(oldDate, e.type);
	                set();
	            }
	            else {
	                picker.viewDate = oldDate;
	                picker.unset = true;
	                notifyChange(oldDate, e.type);
	                notifyError(newDate);
	            }
	        },
	
	        showMode = function (dir) {
	            if (dir) {
	                picker.viewMode = Math.max(picker.minViewMode, Math.min(2, picker.viewMode + dir));
	            }
	            picker.widget.find('.datepicker > div').hide().filter('.datepicker-' + dpGlobal.modes[picker.viewMode].clsName).show();
	        },
	
	        attachDatePickerEvents = function () {
	            var $this, $parent, expanded, closed, collapseData;
	            picker.widget.on('click', '.datepicker *', $.proxy(click, this)); // this handles date picker clicks
	            picker.widget.on('click', '[data-action]', $.proxy(doAction, this)); // this handles time picker clicks
	            picker.widget.on('mousedown', $.proxy(stopEvent, this));
	            picker.element.on('keydown', $.proxy(keydown, this));
	            if (picker.options.pickDate && picker.options.pickTime) {
	                picker.widget.on('click.togglePicker', '.accordion-toggle', function (e) {
	                    e.stopPropagation();
	                    $this = $(this);
	                    $parent = $this.closest('ul');
	                    expanded = $parent.find('.in');
	                    closed = $parent.find('.collapse:not(.in)');
	
	                    if (expanded && expanded.length) {
	                        collapseData = expanded.data('collapse');
	                        if (collapseData && collapseData.transitioning) {
	                            return;
	                        }
	                        expanded.collapse('hide');
	                        closed.collapse('show');
	                        $this.find('span').toggleClass(picker.options.icons.time + ' ' + picker.options.icons.date);
	                        if (picker.component) {
	                            picker.component.find('span').toggleClass(picker.options.icons.time + ' ' + picker.options.icons.date);
	                        }
	                    }
	                });
	            }
	            if (picker.isInput) {
	                picker.element.on({
	                    'click': $.proxy(picker.show, this),
	                    'focus': $.proxy(picker.show, this),
	                    'change': $.proxy(change, this),
	                    'blur': $.proxy(picker.hide, this)
	                });
	            } else {
	                picker.element.on({
	                    'change': $.proxy(change, this)
	                }, 'input');
	                if (picker.component) {
	                    picker.component.on('click', $.proxy(picker.show, this));
	                    picker.component.on('mousedown', $.proxy(stopEvent, this));
	                } else {
	                    picker.element.on('click', $.proxy(picker.show, this));
	                }
	            }
	        },
	
	        attachDatePickerGlobalEvents = function () {
	            $(window).on(
	                'resize.datetimepicker' + picker.id, $.proxy(place, this));
	            if (!picker.isInput) {
	                $(document).on(
	                    'mousedown.datetimepicker' + picker.id, $.proxy(picker.hide, this));
	            }
	        },
	
	        detachDatePickerEvents = function () {
	            picker.widget.off('click', '.datepicker *', picker.click);
	            picker.widget.off('click', '[data-action]');
	            picker.widget.off('mousedown', picker.stopEvent);
	            if (picker.options.pickDate && picker.options.pickTime) {
	                picker.widget.off('click.togglePicker');
	            }
	            if (picker.isInput) {
	                picker.element.off({
	                    'focus': picker.show,
	                    'change': change,
	                    'click': picker.show,
	                    'blur' : picker.hide
	                });
	            } else {
	                picker.element.off({
	                    'change': change
	                }, 'input');
	                if (picker.component) {
	                    picker.component.off('click', picker.show);
	                    picker.component.off('mousedown', picker.stopEvent);
	                } else {
	                    picker.element.off('click', picker.show);
	                }
	            }
	        },
	
	        detachDatePickerGlobalEvents = function () {
	            $(window).off('resize.datetimepicker' + picker.id);
	            if (!picker.isInput) {
	                $(document).off('mousedown.datetimepicker' + picker.id);
	            }
	        },
	
	        isInFixed = function () {
	            if (picker.element) {
	                var parents = picker.element.parents(), inFixed = false, i;
	                for (i = 0; i < parents.length; i++) {
	                    if ($(parents[i]).css('position') === 'fixed') {
	                        inFixed = true;
	                        break;
	                    }
	                }
	                return inFixed;
	            } else {
	                return false;
	            }
	        },
	
	        set = function () {
	            moment.locale(picker.options.language);
	            var formatted = '';
	            if (!picker.unset) {
	                formatted = moment(picker.date).format(picker.format);
	            }
	            getPickerInput().val(formatted);
	            picker.element.data('date', formatted);
	            if (!picker.options.pickTime) {
	                picker.hide();
	            }
	        },
	
	        checkDate = function (direction, unit, amount) {
	            moment.locale(picker.options.language);
	            var newDate;
	            if (direction === 'add') {
	                newDate = moment(picker.date);
	                if (newDate.hours() === 23) {
	                    newDate.add(amount, unit);
	                }
	                newDate.add(amount, unit);
	            }
	            else {
	                newDate = moment(picker.date).subtract(amount, unit);
	            }
	            if (isInDisableDates(moment(newDate.subtract(amount, unit))) || isInDisableDates(newDate)) {
	                notifyError(newDate.format(picker.format));
	                return;
	            }
	
	            if (direction === 'add') {
	                picker.date.add(amount, unit);
	            }
	            else {
	                picker.date.subtract(amount, unit);
	            }
	            picker.unset = false;
	        },
	
	        isInDisableDates = function (date, timeUnit) {
	            moment.locale(picker.options.language);
	            var maxDate = moment(picker.options.maxDate, picker.format, picker.options.useStrict),
	                minDate = moment(picker.options.minDate, picker.format, picker.options.useStrict);
	
	            if (timeUnit) {
	                maxDate = maxDate.endOf(timeUnit);
	                minDate = minDate.startOf(timeUnit);
	            }
	
	            if (date.isAfter(maxDate) || date.isBefore(minDate)) {
	                return true;
	            }
	            if (picker.options.disabledDates === false) {
	                return false;
	            }
	            return picker.options.disabledDates[date.format('YYYY-MM-DD')] === true;
	        },
	        isInEnableDates = function (date) {
	            moment.locale(picker.options.language);
	            if (picker.options.enabledDates === false) {
	                return true;
	            }
	            return picker.options.enabledDates[date.format('YYYY-MM-DD')] === true;
	        },
	
	        indexGivenDates = function (givenDatesArray) {
	            // Store given enabledDates and disabledDates as keys.
	            // This way we can check their existence in O(1) time instead of looping through whole array.
	            // (for example: picker.options.enabledDates['2014-02-27'] === true)
	            var givenDatesIndexed = {}, givenDatesCount = 0, i;
	            for (i = 0; i < givenDatesArray.length; i++) {
	                if (moment.isMoment(givenDatesArray[i]) || givenDatesArray[i] instanceof Date) {
	                    dDate = moment(givenDatesArray[i]);
	                } else {
	                    dDate = moment(givenDatesArray[i], picker.format, picker.options.useStrict);
	                }
	                if (dDate.isValid()) {
	                    givenDatesIndexed[dDate.format('YYYY-MM-DD')] = true;
	                    givenDatesCount++;
	                }
	            }
	            if (givenDatesCount > 0) {
	                return givenDatesIndexed;
	            }
	            return false;
	        },
	
	        padLeft = function (string) {
	            string = string.toString();
	            if (string.length >= 2) {
	                return string;
	            }
	            return '0' + string;
	        },
	
	        getTemplate = function () {
	            var
	                headTemplate =
	                        '<thead>' +
	                            '<tr>' +
	                                '<th class="prev">&lsaquo;</th><th colspan="' + (picker.options.calendarWeeks ? '6' : '5') + '" class="picker-switch"></th><th class="next">&rsaquo;</th>' +
	                            '</tr>' +
	                        '</thead>',
	                contTemplate =
	                        '<tbody><tr><td colspan="' + (picker.options.calendarWeeks ? '8' : '7') + '"></td></tr></tbody>',
	                template = '<div class="datepicker-days">' +
	                    '<table class="table-condensed">' + headTemplate + '<tbody></tbody></table>' +
	                '</div>' +
	                '<div class="datepicker-months">' +
	                    '<table class="table-condensed">' + headTemplate + contTemplate + '</table>' +
	                '</div>' +
	                '<div class="datepicker-years">' +
	                    '<table class="table-condensed">' + headTemplate + contTemplate + '</table>' +
	                '</div>',
	                ret = '';
	            if (picker.options.pickDate && picker.options.pickTime) {
	                ret = '<div class="bootstrap-datetimepicker-widget' + (picker.options.sideBySide ? ' timepicker-sbs' : '') + (picker.use24hours ? ' usetwentyfour' : '') + ' dropdown-menu" style="z-index:9999 !important;">';
	                if (picker.options.sideBySide) {
	                    ret += '<div class="row">' +
	                       '<div class="col-sm-6 datepicker">' + template + '</div>' +
	                       '<div class="col-sm-6 timepicker">' + tpGlobal.getTemplate() + '</div>' +
	                     '</div>';
	                } else {
	                    ret += '<ul class="list-unstyled">' +
	                        '<li' + (picker.options.collapse ? ' class="collapse in"' : '') + '>' +
	                            '<div class="datepicker">' + template + '</div>' +
	                        '</li>' +
	                        '<li class="picker-switch accordion-toggle"><a class="btn" style="width:100%"><span class="' + picker.options.icons.time + '"></span></a></li>' +
	                        '<li' + (picker.options.collapse ? ' class="collapse"' : '') + '>' +
	                            '<div class="timepicker">' + tpGlobal.getTemplate() + '</div>' +
	                        '</li>' +
	                   '</ul>';
	                }
	                ret += '</div>';
	                return ret;
	            }
	            if (picker.options.pickTime) {
	                return (
	                    '<div class="bootstrap-datetimepicker-widget dropdown-menu">' +
	                        '<div class="timepicker">' + tpGlobal.getTemplate() + '</div>' +
	                    '</div>'
	                );
	            }
	            return (
	                '<div class="bootstrap-datetimepicker-widget dropdown-menu">' +
	                    '<div class="datepicker">' + template + '</div>' +
	                '</div>'
	            );
	        },
	
	        dpGlobal = {
	            modes: [
	                {
	                    clsName: 'days',
	                    navFnc: 'month',
	                    navStep: 1
	                },
	                {
	                    clsName: 'months',
	                    navFnc: 'year',
	                    navStep: 1
	                },
	                {
	                    clsName: 'years',
	                    navFnc: 'year',
	                    navStep: 10
	                }
	            ]
	        },
	
	        tpGlobal = {
	            hourTemplate: '<span data-action="showHours"   data-time-component="hours"   class="timepicker-hour"></span>',
	            minuteTemplate: '<span data-action="showMinutes" data-time-component="minutes" class="timepicker-minute"></span>',
	            secondTemplate: '<span data-action="showSeconds"  data-time-component="seconds" class="timepicker-second"></span>'
	        };
	
	        tpGlobal.getTemplate = function () {
	            return (
	                '<div class="timepicker-picker">' +
	                    '<table class="table-condensed">' +
	                        '<tr>' +
	                            '<td><a href="#" class="btn" data-action="incrementHours"><span class="' + picker.options.icons.up + '"></span></a></td>' +
	                            '<td class="separator"></td>' +
	                            '<td>' + (picker.options.useMinutes ? '<a href="#" class="btn" data-action="incrementMinutes"><span class="' + picker.options.icons.up + '"></span></a>' : '') + '</td>' +
	                            (picker.options.useSeconds ?
	                                '<td class="separator"></td><td><a href="#" class="btn" data-action="incrementSeconds"><span class="' + picker.options.icons.up + '"></span></a></td>' : '') +
	                            (picker.use24hours ? '' : '<td class="separator"></td>') +
	                        '</tr>' +
	                        '<tr>' +
	                            '<td>' + tpGlobal.hourTemplate + '</td> ' +
	                            '<td class="separator">:</td>' +
	                            '<td>' + (picker.options.useMinutes ? tpGlobal.minuteTemplate : '<span class="timepicker-minute">00</span>') + '</td> ' +
	                            (picker.options.useSeconds ?
	                                '<td class="separator">:</td><td>' + tpGlobal.secondTemplate + '</td>' : '') +
	                            (picker.use24hours ? '' : '<td class="separator"></td>' +
	                            '<td><button type="button" class="btn btn-primary" data-action="togglePeriod"></button></td>') +
	                        '</tr>' +
	                        '<tr>' +
	                            '<td><a href="#" class="btn" data-action="decrementHours"><span class="' + picker.options.icons.down + '"></span></a></td>' +
	                            '<td class="separator"></td>' +
	                            '<td>' + (picker.options.useMinutes ? '<a href="#" class="btn" data-action="decrementMinutes"><span class="' + picker.options.icons.down + '"></span></a>' : '') + '</td>' +
	                            (picker.options.useSeconds ?
	                                '<td class="separator"></td><td><a href="#" class="btn" data-action="decrementSeconds"><span class="' + picker.options.icons.down + '"></span></a></td>' : '') +
	                            (picker.use24hours ? '' : '<td class="separator"></td>') +
	                        '</tr>' +
	                    '</table>' +
	                '</div>' +
	                '<div class="timepicker-hours" data-action="selectHour">' +
	                    '<table class="table-condensed"></table>' +
	                '</div>' +
	                '<div class="timepicker-minutes" data-action="selectMinute">' +
	                    '<table class="table-condensed"></table>' +
	                '</div>' +
	                (picker.options.useSeconds ?
	                    '<div class="timepicker-seconds" data-action="selectSecond"><table class="table-condensed"></table></div>' : '')
	            );
	        };
	
	        picker.destroy = function () {
	            detachDatePickerEvents();
	            detachDatePickerGlobalEvents();
	            picker.widget.remove();
	            picker.element.removeData('DateTimePicker');
	            if (picker.component) {
	                picker.component.removeData('DateTimePicker');
	            }
	        };
	
	        picker.show = function (e) {
	            if (getPickerInput().prop('disabled')) {
	                return;
	            }
	            if (picker.options.useCurrent) {
	                if (getPickerInput().val() === '') {
	                    if (picker.options.minuteStepping !== 1) {
	                        var mDate = moment(),
	                        rInterval = picker.options.minuteStepping;
	                        mDate.minutes((Math.round(mDate.minutes() / rInterval) * rInterval) % 60).seconds(0);
	                        picker.setValue(mDate.format(picker.format));
	                    } else {
	                        picker.setValue(moment().format(picker.format));
	                    }
	                    notifyChange('', e.type);
	                }
	            }
	            // if this is a click event on the input field and picker is already open don't hide it
	            if (e && e.type === 'click' && picker.isInput && picker.widget.hasClass('picker-open')) {
	                return;
	            }
	            if (picker.widget.hasClass('picker-open')) {
	                picker.widget.hide();
	                picker.widget.removeClass('picker-open');
	            }
	            else {
	                picker.widget.show();
	                picker.widget.addClass('picker-open');
	            }
	            picker.height = picker.component ? picker.component.outerHeight() : picker.element.outerHeight();
	            place();
	            picker.element.trigger({
	                type: 'dp.show',
	                date: moment(picker.date)
	            });
	            attachDatePickerGlobalEvents();
	            if (e) {
	                stopEvent(e);
	            }
	        };
	
	        picker.disable = function () {
	            var input = getPickerInput();
	            if (input.prop('disabled')) {
	                return;
	            }
	            input.prop('disabled', true);
	            detachDatePickerEvents();
	        };
	
	        picker.enable = function () {
	            var input = getPickerInput();
	            if (!input.prop('disabled')) {
	                return;
	            }
	            input.prop('disabled', false);
	            attachDatePickerEvents();
	        };
	
	        picker.hide = function () {
	            // Ignore event if in the middle of a picker transition
	            var collapse = picker.widget.find('.collapse'), i, collapseData;
	            for (i = 0; i < collapse.length; i++) {
	                collapseData = collapse.eq(i).data('collapse');
	                if (collapseData && collapseData.transitioning) {
	                    return;
	                }
	            }
	            picker.widget.hide();
	            picker.widget.removeClass('picker-open');
	            picker.viewMode = picker.startViewMode;
	            showMode();
	            picker.element.trigger({
	                type: 'dp.hide',
	                date: moment(picker.date)
	            });
	            detachDatePickerGlobalEvents();
	        };
	
	        picker.setValue = function (newDate) {
	            moment.locale(picker.options.language);
	            if (!newDate) {
	                picker.unset = true;
	                set();
	            } else {
	                picker.unset = false;
	            }
	            if (!moment.isMoment(newDate)) {
	                newDate = (newDate instanceof Date) ? moment(newDate) : moment(newDate, picker.format, picker.options.useStrict);
	            } else {
	                newDate = newDate.locale(picker.options.language);
	            }
	            if (newDate.isValid()) {
	                picker.date = newDate;
	                set();
	                picker.viewDate = moment({y: picker.date.year(), M: picker.date.month()});
	                fillDate();
	                fillTime();
	            }
	            else {
	                notifyError(newDate);
	            }
	        };
	
	        picker.getDate = function () {
	            if (picker.unset) {
	                return null;
	            }
	            return moment(picker.date);
	        };
	
	        picker.setDate = function (date) {
	            var oldDate = moment(picker.date);
	            if (!date) {
	                picker.setValue(null);
	            } else {
	                picker.setValue(date);
	            }
	            notifyChange(oldDate, 'function');
	        };
	
	        picker.setDisabledDates = function (dates) {
	            picker.options.disabledDates = indexGivenDates(dates);
	            if (picker.viewDate) {
	                update();
	            }
	        };
	
	        picker.setEnabledDates = function (dates) {
	            picker.options.enabledDates = indexGivenDates(dates);
	            if (picker.viewDate) {
	                update();
	            }
	        };
	
	        picker.setMaxDate = function (date) {
	            if (date === undefined) {
	                return;
	            }
	            if (moment.isMoment(date) || date instanceof Date) {
	                picker.options.maxDate = moment(date);
	            } else {
	                picker.options.maxDate = moment(date, picker.format, picker.options.useStrict);
	            }
	            if (picker.viewDate) {
	                update();
	            }
	        };
	
	        picker.setMinDate = function (date) {
	            if (date === undefined) {
	                return;
	            }
	            if (moment.isMoment(date) || date instanceof Date) {
	                picker.options.minDate = moment(date);
	            } else {
	                picker.options.minDate = moment(date, picker.format, picker.options.useStrict);
	            }
	            if (picker.viewDate) {
	                update();
	            }
	        };
	
	        init();
	    };
	
	    $.fn.datetimepicker = function (options) {
	        return this.each(function () {
	            var $this = $(this),
	                data = $this.data('DateTimePicker');
	            if (!data) {
	                $this.data('DateTimePicker', new DateTimePicker(this, options));
	            }
	        });
	    };
	
	    $.fn.datetimepicker.defaults = {
	        format: false,
	        pickDate: true,
	        pickTime: true,
	        useMinutes: true,
	        useSeconds: false,
	        useCurrent: true,
	        calendarWeeks: false,
	        minuteStepping: 1,
	        minDate: moment({y: 1900}),
	        maxDate: moment().add(100, 'y'),
	        showToday: true,
	        collapse: true,
	        language: moment.locale(),
	        defaultDate: '',
	        disabledDates: false,
	        enabledDates: false,
	        icons: {},
	        useStrict: false,
	        direction: 'auto',
	        sideBySide: false,
	        daysOfWeekDisabled: [],
	        widgetParent: false
	    };
	}));


/***/ }),

/***/ 1759:
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;/* WEBPACK VAR INJECTION */(function(global, module) {//! moment.js
	//! version : 2.8.4
	//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
	//! license : MIT
	//! momentjs.com
	
	(function (undefined) {
	    /************************************
	        Constants
	    ************************************/
	
	    var moment,
	        VERSION = '2.8.4',
	        // the global-scope this is NOT the global object in Node.js
	        globalScope = typeof global !== 'undefined' ? global : this,
	        oldGlobalMoment,
	        round = Math.round,
	        hasOwnProperty = Object.prototype.hasOwnProperty,
	        i,
	
	        YEAR = 0,
	        MONTH = 1,
	        DATE = 2,
	        HOUR = 3,
	        MINUTE = 4,
	        SECOND = 5,
	        MILLISECOND = 6,
	
	        // internal storage for locale config files
	        locales = {},
	
	        // extra moment internal properties (plugins register props here)
	        momentProperties = [],
	
	        // check for nodeJS
	        hasModule = (typeof module !== 'undefined' && module && module.exports),
	
	        // ASP.NET json date format regex
	        aspNetJsonRegex = /^\/?Date\((\-?\d+)/i,
	        aspNetTimeSpanJsonRegex = /(\-)?(?:(\d*)\.)?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?)?/,
	
	        // from http://docs.closure-library.googlecode.com/git/closure_goog_date_date.js.source.html
	        // somewhat more in line with 4.4.3.2 2004 spec, but allows decimal anywhere
	        isoDurationRegex = /^(-)?P(?:(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?|([0-9,.]*)W)$/,
	
	        // format tokens
	        formattingTokens = /(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Q|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,4}|x|X|zz?|ZZ?|.)/g,
	        localFormattingTokens = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g,
	
	        // parsing token regexes
	        parseTokenOneOrTwoDigits = /\d\d?/, // 0 - 99
	        parseTokenOneToThreeDigits = /\d{1,3}/, // 0 - 999
	        parseTokenOneToFourDigits = /\d{1,4}/, // 0 - 9999
	        parseTokenOneToSixDigits = /[+\-]?\d{1,6}/, // -999,999 - 999,999
	        parseTokenDigits = /\d+/, // nonzero number of digits
	        parseTokenWord = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i, // any word (or two) characters or numbers including two/three word month in arabic.
	        parseTokenTimezone = /Z|[\+\-]\d\d:?\d\d/gi, // +00:00 -00:00 +0000 -0000 or Z
	        parseTokenT = /T/i, // T (ISO separator)
	        parseTokenOffsetMs = /[\+\-]?\d+/, // 1234567890123
	        parseTokenTimestampMs = /[\+\-]?\d+(\.\d{1,3})?/, // 123456789 123456789.123
	
	        //strict parsing regexes
	        parseTokenOneDigit = /\d/, // 0 - 9
	        parseTokenTwoDigits = /\d\d/, // 00 - 99
	        parseTokenThreeDigits = /\d{3}/, // 000 - 999
	        parseTokenFourDigits = /\d{4}/, // 0000 - 9999
	        parseTokenSixDigits = /[+-]?\d{6}/, // -999,999 - 999,999
	        parseTokenSignedNumber = /[+-]?\d+/, // -inf - inf
	
	        // iso 8601 regex
	        // 0000-00-00 0000-W00 or 0000-W00-0 + T + 00 or 00:00 or 00:00:00 or 00:00:00.000 + +00:00 or +0000 or +00)
	        isoRegex = /^\s*(?:[+-]\d{6}|\d{4})-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/,
	
	        isoFormat = 'YYYY-MM-DDTHH:mm:ssZ',
	
	        isoDates = [
	            ['YYYYYY-MM-DD', /[+-]\d{6}-\d{2}-\d{2}/],
	            ['YYYY-MM-DD', /\d{4}-\d{2}-\d{2}/],
	            ['GGGG-[W]WW-E', /\d{4}-W\d{2}-\d/],
	            ['GGGG-[W]WW', /\d{4}-W\d{2}/],
	            ['YYYY-DDD', /\d{4}-\d{3}/]
	        ],
	
	        // iso time formats and regexes
	        isoTimes = [
	            ['HH:mm:ss.SSSS', /(T| )\d\d:\d\d:\d\d\.\d+/],
	            ['HH:mm:ss', /(T| )\d\d:\d\d:\d\d/],
	            ['HH:mm', /(T| )\d\d:\d\d/],
	            ['HH', /(T| )\d\d/]
	        ],
	
	        // timezone chunker '+10:00' > ['10', '00'] or '-1530' > ['-15', '30']
	        parseTimezoneChunker = /([\+\-]|\d\d)/gi,
	
	        // getter and setter names
	        proxyGettersAndSetters = 'Date|Hours|Minutes|Seconds|Milliseconds'.split('|'),
	        unitMillisecondFactors = {
	            'Milliseconds' : 1,
	            'Seconds' : 1e3,
	            'Minutes' : 6e4,
	            'Hours' : 36e5,
	            'Days' : 864e5,
	            'Months' : 2592e6,
	            'Years' : 31536e6
	        },
	
	        unitAliases = {
	            ms : 'millisecond',
	            s : 'second',
	            m : 'minute',
	            h : 'hour',
	            d : 'day',
	            D : 'date',
	            w : 'week',
	            W : 'isoWeek',
	            M : 'month',
	            Q : 'quarter',
	            y : 'year',
	            DDD : 'dayOfYear',
	            e : 'weekday',
	            E : 'isoWeekday',
	            gg: 'weekYear',
	            GG: 'isoWeekYear'
	        },
	
	        camelFunctions = {
	            dayofyear : 'dayOfYear',
	            isoweekday : 'isoWeekday',
	            isoweek : 'isoWeek',
	            weekyear : 'weekYear',
	            isoweekyear : 'isoWeekYear'
	        },
	
	        // format function strings
	        formatFunctions = {},
	
	        // default relative time thresholds
	        relativeTimeThresholds = {
	            s: 45,  // seconds to minute
	            m: 45,  // minutes to hour
	            h: 22,  // hours to day
	            d: 26,  // days to month
	            M: 11   // months to year
	        },
	
	        // tokens to ordinalize and pad
	        ordinalizeTokens = 'DDD w W M D d'.split(' '),
	        paddedTokens = 'M D H h m s w W'.split(' '),
	
	        formatTokenFunctions = {
	            M    : function () {
	                return this.month() + 1;
	            },
	            MMM  : function (format) {
	                return this.localeData().monthsShort(this, format);
	            },
	            MMMM : function (format) {
	                return this.localeData().months(this, format);
	            },
	            D    : function () {
	                return this.date();
	            },
	            DDD  : function () {
	                return this.dayOfYear();
	            },
	            d    : function () {
	                return this.day();
	            },
	            dd   : function (format) {
	                return this.localeData().weekdaysMin(this, format);
	            },
	            ddd  : function (format) {
	                return this.localeData().weekdaysShort(this, format);
	            },
	            dddd : function (format) {
	                return this.localeData().weekdays(this, format);
	            },
	            w    : function () {
	                return this.week();
	            },
	            W    : function () {
	                return this.isoWeek();
	            },
	            YY   : function () {
	                return leftZeroFill(this.year() % 100, 2);
	            },
	            YYYY : function () {
	                return leftZeroFill(this.year(), 4);
	            },
	            YYYYY : function () {
	                return leftZeroFill(this.year(), 5);
	            },
	            YYYYYY : function () {
	                var y = this.year(), sign = y >= 0 ? '+' : '-';
	                return sign + leftZeroFill(Math.abs(y), 6);
	            },
	            gg   : function () {
	                return leftZeroFill(this.weekYear() % 100, 2);
	            },
	            gggg : function () {
	                return leftZeroFill(this.weekYear(), 4);
	            },
	            ggggg : function () {
	                return leftZeroFill(this.weekYear(), 5);
	            },
	            GG   : function () {
	                return leftZeroFill(this.isoWeekYear() % 100, 2);
	            },
	            GGGG : function () {
	                return leftZeroFill(this.isoWeekYear(), 4);
	            },
	            GGGGG : function () {
	                return leftZeroFill(this.isoWeekYear(), 5);
	            },
	            e : function () {
	                return this.weekday();
	            },
	            E : function () {
	                return this.isoWeekday();
	            },
	            a    : function () {
	                return this.localeData().meridiem(this.hours(), this.minutes(), true);
	            },
	            A    : function () {
	                return this.localeData().meridiem(this.hours(), this.minutes(), false);
	            },
	            H    : function () {
	                return this.hours();
	            },
	            h    : function () {
	                return this.hours() % 12 || 12;
	            },
	            m    : function () {
	                return this.minutes();
	            },
	            s    : function () {
	                return this.seconds();
	            },
	            S    : function () {
	                return toInt(this.milliseconds() / 100);
	            },
	            SS   : function () {
	                return leftZeroFill(toInt(this.milliseconds() / 10), 2);
	            },
	            SSS  : function () {
	                return leftZeroFill(this.milliseconds(), 3);
	            },
	            SSSS : function () {
	                return leftZeroFill(this.milliseconds(), 3);
	            },
	            Z    : function () {
	                var a = -this.zone(),
	                    b = '+';
	                if (a < 0) {
	                    a = -a;
	                    b = '-';
	                }
	                return b + leftZeroFill(toInt(a / 60), 2) + ':' + leftZeroFill(toInt(a) % 60, 2);
	            },
	            ZZ   : function () {
	                var a = -this.zone(),
	                    b = '+';
	                if (a < 0) {
	                    a = -a;
	                    b = '-';
	                }
	                return b + leftZeroFill(toInt(a / 60), 2) + leftZeroFill(toInt(a) % 60, 2);
	            },
	            z : function () {
	                return this.zoneAbbr();
	            },
	            zz : function () {
	                return this.zoneName();
	            },
	            x    : function () {
	                return this.valueOf();
	            },
	            X    : function () {
	                return this.unix();
	            },
	            Q : function () {
	                return this.quarter();
	            }
	        },
	
	        deprecations = {},
	
	        lists = ['months', 'monthsShort', 'weekdays', 'weekdaysShort', 'weekdaysMin'];
	
	    // Pick the first defined of two or three arguments. dfl comes from
	    // default.
	    function dfl(a, b, c) {
	        switch (arguments.length) {
	            case 2: return a != null ? a : b;
	            case 3: return a != null ? a : b != null ? b : c;
	            default: throw new Error('Implement me');
	        }
	    }
	
	    function hasOwnProp(a, b) {
	        return hasOwnProperty.call(a, b);
	    }
	
	    function defaultParsingFlags() {
	        // We need to deep clone this object, and es5 standard is not very
	        // helpful.
	        return {
	            empty : false,
	            unusedTokens : [],
	            unusedInput : [],
	            overflow : -2,
	            charsLeftOver : 0,
	            nullInput : false,
	            invalidMonth : null,
	            invalidFormat : false,
	            userInvalidated : false,
	            iso: false
	        };
	    }
	
	    function printMsg(msg) {
	        if (moment.suppressDeprecationWarnings === false &&
	                typeof console !== 'undefined' && console.warn) {
	            console.warn('Deprecation warning: ' + msg);
	        }
	    }
	
	    function deprecate(msg, fn) {
	        var firstTime = true;
	        return extend(function () {
	            if (firstTime) {
	                printMsg(msg);
	                firstTime = false;
	            }
	            return fn.apply(this, arguments);
	        }, fn);
	    }
	
	    function deprecateSimple(name, msg) {
	        if (!deprecations[name]) {
	            printMsg(msg);
	            deprecations[name] = true;
	        }
	    }
	
	    function padToken(func, count) {
	        return function (a) {
	            return leftZeroFill(func.call(this, a), count);
	        };
	    }
	    function ordinalizeToken(func, period) {
	        return function (a) {
	            return this.localeData().ordinal(func.call(this, a), period);
	        };
	    }
	
	    while (ordinalizeTokens.length) {
	        i = ordinalizeTokens.pop();
	        formatTokenFunctions[i + 'o'] = ordinalizeToken(formatTokenFunctions[i], i);
	    }
	    while (paddedTokens.length) {
	        i = paddedTokens.pop();
	        formatTokenFunctions[i + i] = padToken(formatTokenFunctions[i], 2);
	    }
	    formatTokenFunctions.DDDD = padToken(formatTokenFunctions.DDD, 3);
	
	
	    /************************************
	        Constructors
	    ************************************/
	
	    function Locale() {
	    }
	
	    // Moment prototype object
	    function Moment(config, skipOverflow) {
	        if (skipOverflow !== false) {
	            checkOverflow(config);
	        }
	        copyConfig(this, config);
	        this._d = new Date(+config._d);
	    }
	
	    // Duration Constructor
	    function Duration(duration) {
	        var normalizedInput = normalizeObjectUnits(duration),
	            years = normalizedInput.year || 0,
	            quarters = normalizedInput.quarter || 0,
	            months = normalizedInput.month || 0,
	            weeks = normalizedInput.week || 0,
	            days = normalizedInput.day || 0,
	            hours = normalizedInput.hour || 0,
	            minutes = normalizedInput.minute || 0,
	            seconds = normalizedInput.second || 0,
	            milliseconds = normalizedInput.millisecond || 0;
	
	        // representation for dateAddRemove
	        this._milliseconds = +milliseconds +
	            seconds * 1e3 + // 1000
	            minutes * 6e4 + // 1000 * 60
	            hours * 36e5; // 1000 * 60 * 60
	        // Because of dateAddRemove treats 24 hours as different from a
	        // day when working around DST, we need to store them separately
	        this._days = +days +
	            weeks * 7;
	        // It is impossible translate months into days without knowing
	        // which months you are are talking about, so we have to store
	        // it separately.
	        this._months = +months +
	            quarters * 3 +
	            years * 12;
	
	        this._data = {};
	
	        this._locale = moment.localeData();
	
	        this._bubble();
	    }
	
	    /************************************
	        Helpers
	    ************************************/
	
	
	    function extend(a, b) {
	        for (var i in b) {
	            if (hasOwnProp(b, i)) {
	                a[i] = b[i];
	            }
	        }
	
	        if (hasOwnProp(b, 'toString')) {
	            a.toString = b.toString;
	        }
	
	        if (hasOwnProp(b, 'valueOf')) {
	            a.valueOf = b.valueOf;
	        }
	
	        return a;
	    }
	
	    function copyConfig(to, from) {
	        var i, prop, val;
	
	        if (typeof from._isAMomentObject !== 'undefined') {
	            to._isAMomentObject = from._isAMomentObject;
	        }
	        if (typeof from._i !== 'undefined') {
	            to._i = from._i;
	        }
	        if (typeof from._f !== 'undefined') {
	            to._f = from._f;
	        }
	        if (typeof from._l !== 'undefined') {
	            to._l = from._l;
	        }
	        if (typeof from._strict !== 'undefined') {
	            to._strict = from._strict;
	        }
	        if (typeof from._tzm !== 'undefined') {
	            to._tzm = from._tzm;
	        }
	        if (typeof from._isUTC !== 'undefined') {
	            to._isUTC = from._isUTC;
	        }
	        if (typeof from._offset !== 'undefined') {
	            to._offset = from._offset;
	        }
	        if (typeof from._pf !== 'undefined') {
	            to._pf = from._pf;
	        }
	        if (typeof from._locale !== 'undefined') {
	            to._locale = from._locale;
	        }
	
	        if (momentProperties.length > 0) {
	            for (i in momentProperties) {
	                prop = momentProperties[i];
	                val = from[prop];
	                if (typeof val !== 'undefined') {
	                    to[prop] = val;
	                }
	            }
	        }
	
	        return to;
	    }
	
	    function absRound(number) {
	        if (number < 0) {
	            return Math.ceil(number);
	        } else {
	            return Math.floor(number);
	        }
	    }
	
	    // left zero fill a number
	    // see http://jsperf.com/left-zero-filling for performance comparison
	    function leftZeroFill(number, targetLength, forceSign) {
	        var output = '' + Math.abs(number),
	            sign = number >= 0;
	
	        while (output.length < targetLength) {
	            output = '0' + output;
	        }
	        return (sign ? (forceSign ? '+' : '') : '-') + output;
	    }
	
	    function positiveMomentsDifference(base, other) {
	        var res = {milliseconds: 0, months: 0};
	
	        res.months = other.month() - base.month() +
	            (other.year() - base.year()) * 12;
	        if (base.clone().add(res.months, 'M').isAfter(other)) {
	            --res.months;
	        }
	
	        res.milliseconds = +other - +(base.clone().add(res.months, 'M'));
	
	        return res;
	    }
	
	    function momentsDifference(base, other) {
	        var res;
	        other = makeAs(other, base);
	        if (base.isBefore(other)) {
	            res = positiveMomentsDifference(base, other);
	        } else {
	            res = positiveMomentsDifference(other, base);
	            res.milliseconds = -res.milliseconds;
	            res.months = -res.months;
	        }
	
	        return res;
	    }
	
	    // TODO: remove 'name' arg after deprecation is removed
	    function createAdder(direction, name) {
	        return function (val, period) {
	            var dur, tmp;
	            //invert the arguments, but complain about it
	            if (period !== null && !isNaN(+period)) {
	                deprecateSimple(name, 'moment().' + name  + '(period, number) is deprecated. Please use moment().' + name + '(number, period).');
	                tmp = val; val = period; period = tmp;
	            }
	
	            val = typeof val === 'string' ? +val : val;
	            dur = moment.duration(val, period);
	            addOrSubtractDurationFromMoment(this, dur, direction);
	            return this;
	        };
	    }
	
	    function addOrSubtractDurationFromMoment(mom, duration, isAdding, updateOffset) {
	        var milliseconds = duration._milliseconds,
	            days = duration._days,
	            months = duration._months;
	        updateOffset = updateOffset == null ? true : updateOffset;
	
	        if (milliseconds) {
	            mom._d.setTime(+mom._d + milliseconds * isAdding);
	        }
	        if (days) {
	            rawSetter(mom, 'Date', rawGetter(mom, 'Date') + days * isAdding);
	        }
	        if (months) {
	            rawMonthSetter(mom, rawGetter(mom, 'Month') + months * isAdding);
	        }
	        if (updateOffset) {
	            moment.updateOffset(mom, days || months);
	        }
	    }
	
	    // check if is an array
	    function isArray(input) {
	        return Object.prototype.toString.call(input) === '[object Array]';
	    }
	
	    function isDate(input) {
	        return Object.prototype.toString.call(input) === '[object Date]' ||
	            input instanceof Date;
	    }
	
	    // compare two arrays, return the number of differences
	    function compareArrays(array1, array2, dontConvert) {
	        var len = Math.min(array1.length, array2.length),
	            lengthDiff = Math.abs(array1.length - array2.length),
	            diffs = 0,
	            i;
	        for (i = 0; i < len; i++) {
	            if ((dontConvert && array1[i] !== array2[i]) ||
	                (!dontConvert && toInt(array1[i]) !== toInt(array2[i]))) {
	                diffs++;
	            }
	        }
	        return diffs + lengthDiff;
	    }
	
	    function normalizeUnits(units) {
	        if (units) {
	            var lowered = units.toLowerCase().replace(/(.)s$/, '$1');
	            units = unitAliases[units] || camelFunctions[lowered] || lowered;
	        }
	        return units;
	    }
	
	    function normalizeObjectUnits(inputObject) {
	        var normalizedInput = {},
	            normalizedProp,
	            prop;
	
	        for (prop in inputObject) {
	            if (hasOwnProp(inputObject, prop)) {
	                normalizedProp = normalizeUnits(prop);
	                if (normalizedProp) {
	                    normalizedInput[normalizedProp] = inputObject[prop];
	                }
	            }
	        }
	
	        return normalizedInput;
	    }
	
	    function makeList(field) {
	        var count, setter;
	
	        if (field.indexOf('week') === 0) {
	            count = 7;
	            setter = 'day';
	        }
	        else if (field.indexOf('month') === 0) {
	            count = 12;
	            setter = 'month';
	        }
	        else {
	            return;
	        }
	
	        moment[field] = function (format, index) {
	            var i, getter,
	                method = moment._locale[field],
	                results = [];
	
	            if (typeof format === 'number') {
	                index = format;
	                format = undefined;
	            }
	
	            getter = function (i) {
	                var m = moment().utc().set(setter, i);
	                return method.call(moment._locale, m, format || '');
	            };
	
	            if (index != null) {
	                return getter(index);
	            }
	            else {
	                for (i = 0; i < count; i++) {
	                    results.push(getter(i));
	                }
	                return results;
	            }
	        };
	    }
	
	    function toInt(argumentForCoercion) {
	        var coercedNumber = +argumentForCoercion,
	            value = 0;
	
	        if (coercedNumber !== 0 && isFinite(coercedNumber)) {
	            if (coercedNumber >= 0) {
	                value = Math.floor(coercedNumber);
	            } else {
	                value = Math.ceil(coercedNumber);
	            }
	        }
	
	        return value;
	    }
	
	    function daysInMonth(year, month) {
	        return new Date(Date.UTC(year, month + 1, 0)).getUTCDate();
	    }
	
	    function weeksInYear(year, dow, doy) {
	        return weekOfYear(moment([year, 11, 31 + dow - doy]), dow, doy).week;
	    }
	
	    function daysInYear(year) {
	        return isLeapYear(year) ? 366 : 365;
	    }
	
	    function isLeapYear(year) {
	        return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;
	    }
	
	    function checkOverflow(m) {
	        var overflow;
	        if (m._a && m._pf.overflow === -2) {
	            overflow =
	                m._a[MONTH] < 0 || m._a[MONTH] > 11 ? MONTH :
	                m._a[DATE] < 1 || m._a[DATE] > daysInMonth(m._a[YEAR], m._a[MONTH]) ? DATE :
	                m._a[HOUR] < 0 || m._a[HOUR] > 24 ||
	                    (m._a[HOUR] === 24 && (m._a[MINUTE] !== 0 ||
	                                           m._a[SECOND] !== 0 ||
	                                           m._a[MILLISECOND] !== 0)) ? HOUR :
	                m._a[MINUTE] < 0 || m._a[MINUTE] > 59 ? MINUTE :
	                m._a[SECOND] < 0 || m._a[SECOND] > 59 ? SECOND :
	                m._a[MILLISECOND] < 0 || m._a[MILLISECOND] > 999 ? MILLISECOND :
	                -1;
	
	            if (m._pf._overflowDayOfYear && (overflow < YEAR || overflow > DATE)) {
	                overflow = DATE;
	            }
	
	            m._pf.overflow = overflow;
	        }
	    }
	
	    function isValid(m) {
	        if (m._isValid == null) {
	            m._isValid = !isNaN(m._d.getTime()) &&
	                m._pf.overflow < 0 &&
	                !m._pf.empty &&
	                !m._pf.invalidMonth &&
	                !m._pf.nullInput &&
	                !m._pf.invalidFormat &&
	                !m._pf.userInvalidated;
	
	            if (m._strict) {
	                m._isValid = m._isValid &&
	                    m._pf.charsLeftOver === 0 &&
	                    m._pf.unusedTokens.length === 0 &&
	                    m._pf.bigHour === undefined;
	            }
	        }
	        return m._isValid;
	    }
	
	    function normalizeLocale(key) {
	        return key ? key.toLowerCase().replace('_', '-') : key;
	    }
	
	    // pick the locale from the array
	    // try ['en-au', 'en-gb'] as 'en-au', 'en-gb', 'en', as in move through the list trying each
	    // substring from most specific to least, but move to the next array item if it's a more specific variant than the current root
	    function chooseLocale(names) {
	        var i = 0, j, next, locale, split;
	
	        while (i < names.length) {
	            split = normalizeLocale(names[i]).split('-');
	            j = split.length;
	            next = normalizeLocale(names[i + 1]);
	            next = next ? next.split('-') : null;
	            while (j > 0) {
	                locale = loadLocale(split.slice(0, j).join('-'));
	                if (locale) {
	                    return locale;
	                }
	                if (next && next.length >= j && compareArrays(split, next, true) >= j - 1) {
	                    //the next array item is better than a shallower substring of this one
	                    break;
	                }
	                j--;
	            }
	            i++;
	        }
	        return null;
	    }
	
	    function loadLocale(name) {
	        var oldLocale = null;
	        if (!locales[name] && hasModule) {
	            try {
	                oldLocale = moment.locale();
	                !(function webpackMissingModule() { var e = new Error("Cannot find module \"./locale\""); e.code = 'MODULE_NOT_FOUND'; throw e; }());
	                // because defineLocale currently also sets the global locale, we want to undo that for lazy loaded locales
	                moment.locale(oldLocale);
	            } catch (e) { }
	        }
	        return locales[name];
	    }
	
	    // Return a moment from input, that is local/utc/zone equivalent to model.
	    function makeAs(input, model) {
	        var res, diff;
	        if (model._isUTC) {
	            res = model.clone();
	            diff = (moment.isMoment(input) || isDate(input) ?
	                    +input : +moment(input)) - (+res);
	            // Use low-level api, because this fn is low-level api.
	            res._d.setTime(+res._d + diff);
	            moment.updateOffset(res, false);
	            return res;
	        } else {
	            return moment(input).local();
	        }
	    }
	
	    /************************************
	        Locale
	    ************************************/
	
	
	    extend(Locale.prototype, {
	
	        set : function (config) {
	            var prop, i;
	            for (i in config) {
	                prop = config[i];
	                if (typeof prop === 'function') {
	                    this[i] = prop;
	                } else {
	                    this['_' + i] = prop;
	                }
	            }
	            // Lenient ordinal parsing accepts just a number in addition to
	            // number + (possibly) stuff coming from _ordinalParseLenient.
	            this._ordinalParseLenient = new RegExp(this._ordinalParse.source + '|' + /\d{1,2}/.source);
	        },
	
	        _months : 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_'),
	        months : function (m) {
	            return this._months[m.month()];
	        },
	
	        _monthsShort : 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_'),
	        monthsShort : function (m) {
	            return this._monthsShort[m.month()];
	        },
	
	        monthsParse : function (monthName, format, strict) {
	            var i, mom, regex;
	
	            if (!this._monthsParse) {
	                this._monthsParse = [];
	                this._longMonthsParse = [];
	                this._shortMonthsParse = [];
	            }
	
	            for (i = 0; i < 12; i++) {
	                // make the regex if we don't have it already
	                mom = moment.utc([2000, i]);
	                if (strict && !this._longMonthsParse[i]) {
	                    this._longMonthsParse[i] = new RegExp('^' + this.months(mom, '').replace('.', '') + '$', 'i');
	                    this._shortMonthsParse[i] = new RegExp('^' + this.monthsShort(mom, '').replace('.', '') + '$', 'i');
	                }
	                if (!strict && !this._monthsParse[i]) {
	                    regex = '^' + this.months(mom, '') + '|^' + this.monthsShort(mom, '');
	                    this._monthsParse[i] = new RegExp(regex.replace('.', ''), 'i');
	                }
	                // test the regex
	                if (strict && format === 'MMMM' && this._longMonthsParse[i].test(monthName)) {
	                    return i;
	                } else if (strict && format === 'MMM' && this._shortMonthsParse[i].test(monthName)) {
	                    return i;
	                } else if (!strict && this._monthsParse[i].test(monthName)) {
	                    return i;
	                }
	            }
	        },
	
	        _weekdays : 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_'),
	        weekdays : function (m) {
	            return this._weekdays[m.day()];
	        },
	
	        _weekdaysShort : 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_'),
	        weekdaysShort : function (m) {
	            return this._weekdaysShort[m.day()];
	        },
	
	        _weekdaysMin : 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_'),
	        weekdaysMin : function (m) {
	            return this._weekdaysMin[m.day()];
	        },
	
	        weekdaysParse : function (weekdayName) {
	            var i, mom, regex;
	
	            if (!this._weekdaysParse) {
	                this._weekdaysParse = [];
	            }
	
	            for (i = 0; i < 7; i++) {
	                // make the regex if we don't have it already
	                if (!this._weekdaysParse[i]) {
	                    mom = moment([2000, 1]).day(i);
	                    regex = '^' + this.weekdays(mom, '') + '|^' + this.weekdaysShort(mom, '') + '|^' + this.weekdaysMin(mom, '');
	                    this._weekdaysParse[i] = new RegExp(regex.replace('.', ''), 'i');
	                }
	                // test the regex
	                if (this._weekdaysParse[i].test(weekdayName)) {
	                    return i;
	                }
	            }
	        },
	
	        _longDateFormat : {
	            LTS : 'h:mm:ss A',
	            LT : 'h:mm A',
	            L : 'MM/DD/YYYY',
	            LL : 'MMMM D, YYYY',
	            LLL : 'MMMM D, YYYY LT',
	            LLLL : 'dddd, MMMM D, YYYY LT'
	        },
	        longDateFormat : function (key) {
	            var output = this._longDateFormat[key];
	            if (!output && this._longDateFormat[key.toUpperCase()]) {
	                output = this._longDateFormat[key.toUpperCase()].replace(/MMMM|MM|DD|dddd/g, function (val) {
	                    return val.slice(1);
	                });
	                this._longDateFormat[key] = output;
	            }
	            return output;
	        },
	
	        isPM : function (input) {
	            // IE8 Quirks Mode & IE7 Standards Mode do not allow accessing strings like arrays
	            // Using charAt should be more compatible.
	            return ((input + '').toLowerCase().charAt(0) === 'p');
	        },
	
	        _meridiemParse : /[ap]\.?m?\.?/i,
	        meridiem : function (hours, minutes, isLower) {
	            if (hours > 11) {
	                return isLower ? 'pm' : 'PM';
	            } else {
	                return isLower ? 'am' : 'AM';
	            }
	        },
	
	        _calendar : {
	            sameDay : '[Today at] LT',
	            nextDay : '[Tomorrow at] LT',
	            nextWeek : 'dddd [at] LT',
	            lastDay : '[Yesterday at] LT',
	            lastWeek : '[Last] dddd [at] LT',
	            sameElse : 'L'
	        },
	        calendar : function (key, mom, now) {
	            var output = this._calendar[key];
	            return typeof output === 'function' ? output.apply(mom, [now]) : output;
	        },
	
	        _relativeTime : {
	            future : 'in %s',
	            past : '%s ago',
	            s : 'a few seconds',
	            m : 'a minute',
	            mm : '%d minutes',
	            h : 'an hour',
	            hh : '%d hours',
	            d : 'a day',
	            dd : '%d days',
	            M : 'a month',
	            MM : '%d months',
	            y : 'a year',
	            yy : '%d years'
	        },
	
	        relativeTime : function (number, withoutSuffix, string, isFuture) {
	            var output = this._relativeTime[string];
	            return (typeof output === 'function') ?
	                output(number, withoutSuffix, string, isFuture) :
	                output.replace(/%d/i, number);
	        },
	
	        pastFuture : function (diff, output) {
	            var format = this._relativeTime[diff > 0 ? 'future' : 'past'];
	            return typeof format === 'function' ? format(output) : format.replace(/%s/i, output);
	        },
	
	        ordinal : function (number) {
	            return this._ordinal.replace('%d', number);
	        },
	        _ordinal : '%d',
	        _ordinalParse : /\d{1,2}/,
	
	        preparse : function (string) {
	            return string;
	        },
	
	        postformat : function (string) {
	            return string;
	        },
	
	        week : function (mom) {
	            return weekOfYear(mom, this._week.dow, this._week.doy).week;
	        },
	
	        _week : {
	            dow : 0, // Sunday is the first day of the week.
	            doy : 6  // The week that contains Jan 1st is the first week of the year.
	        },
	
	        _invalidDate: 'Invalid date',
	        invalidDate: function () {
	            return this._invalidDate;
	        }
	    });
	
	    /************************************
	        Formatting
	    ************************************/
	
	
	    function removeFormattingTokens(input) {
	        if (input.match(/\[[\s\S]/)) {
	            return input.replace(/^\[|\]$/g, '');
	        }
	        return input.replace(/\\/g, '');
	    }
	
	    function makeFormatFunction(format) {
	        var array = format.match(formattingTokens), i, length;
	
	        for (i = 0, length = array.length; i < length; i++) {
	            if (formatTokenFunctions[array[i]]) {
	                array[i] = formatTokenFunctions[array[i]];
	            } else {
	                array[i] = removeFormattingTokens(array[i]);
	            }
	        }
	
	        return function (mom) {
	            var output = '';
	            for (i = 0; i < length; i++) {
	                output += array[i] instanceof Function ? array[i].call(mom, format) : array[i];
	            }
	            return output;
	        };
	    }
	
	    // format date using native date object
	    function formatMoment(m, format) {
	        if (!m.isValid()) {
	            return m.localeData().invalidDate();
	        }
	
	        format = expandFormat(format, m.localeData());
	
	        if (!formatFunctions[format]) {
	            formatFunctions[format] = makeFormatFunction(format);
	        }
	
	        return formatFunctions[format](m);
	    }
	
	    function expandFormat(format, locale) {
	        var i = 5;
	
	        function replaceLongDateFormatTokens(input) {
	            return locale.longDateFormat(input) || input;
	        }
	
	        localFormattingTokens.lastIndex = 0;
	        while (i >= 0 && localFormattingTokens.test(format)) {
	            format = format.replace(localFormattingTokens, replaceLongDateFormatTokens);
	            localFormattingTokens.lastIndex = 0;
	            i -= 1;
	        }
	
	        return format;
	    }
	
	
	    /************************************
	        Parsing
	    ************************************/
	
	
	    // get the regex to find the next token
	    function getParseRegexForToken(token, config) {
	        var a, strict = config._strict;
	        switch (token) {
	        case 'Q':
	            return parseTokenOneDigit;
	        case 'DDDD':
	            return parseTokenThreeDigits;
	        case 'YYYY':
	        case 'GGGG':
	        case 'gggg':
	            return strict ? parseTokenFourDigits : parseTokenOneToFourDigits;
	        case 'Y':
	        case 'G':
	        case 'g':
	            return parseTokenSignedNumber;
	        case 'YYYYYY':
	        case 'YYYYY':
	        case 'GGGGG':
	        case 'ggggg':
	            return strict ? parseTokenSixDigits : parseTokenOneToSixDigits;
	        case 'S':
	            if (strict) {
	                return parseTokenOneDigit;
	            }
	            /* falls through */
	        case 'SS':
	            if (strict) {
	                return parseTokenTwoDigits;
	            }
	            /* falls through */
	        case 'SSS':
	            if (strict) {
	                return parseTokenThreeDigits;
	            }
	            /* falls through */
	        case 'DDD':
	            return parseTokenOneToThreeDigits;
	        case 'MMM':
	        case 'MMMM':
	        case 'dd':
	        case 'ddd':
	        case 'dddd':
	            return parseTokenWord;
	        case 'a':
	        case 'A':
	            return config._locale._meridiemParse;
	        case 'x':
	            return parseTokenOffsetMs;
	        case 'X':
	            return parseTokenTimestampMs;
	        case 'Z':
	        case 'ZZ':
	            return parseTokenTimezone;
	        case 'T':
	            return parseTokenT;
	        case 'SSSS':
	            return parseTokenDigits;
	        case 'MM':
	        case 'DD':
	        case 'YY':
	        case 'GG':
	        case 'gg':
	        case 'HH':
	        case 'hh':
	        case 'mm':
	        case 'ss':
	        case 'ww':
	        case 'WW':
	            return strict ? parseTokenTwoDigits : parseTokenOneOrTwoDigits;
	        case 'M':
	        case 'D':
	        case 'd':
	        case 'H':
	        case 'h':
	        case 'm':
	        case 's':
	        case 'w':
	        case 'W':
	        case 'e':
	        case 'E':
	            return parseTokenOneOrTwoDigits;
	        case 'Do':
	            return strict ? config._locale._ordinalParse : config._locale._ordinalParseLenient;
	        default :
	            a = new RegExp(regexpEscape(unescapeFormat(token.replace('\\', '')), 'i'));
	            return a;
	        }
	    }
	
	    function timezoneMinutesFromString(string) {
	        string = string || '';
	        var possibleTzMatches = (string.match(parseTokenTimezone) || []),
	            tzChunk = possibleTzMatches[possibleTzMatches.length - 1] || [],
	            parts = (tzChunk + '').match(parseTimezoneChunker) || ['-', 0, 0],
	            minutes = +(parts[1] * 60) + toInt(parts[2]);
	
	        return parts[0] === '+' ? -minutes : minutes;
	    }
	
	    // function to convert string input to date
	    function addTimeToArrayFromToken(token, input, config) {
	        var a, datePartArray = config._a;
	
	        switch (token) {
	        // QUARTER
	        case 'Q':
	            if (input != null) {
	                datePartArray[MONTH] = (toInt(input) - 1) * 3;
	            }
	            break;
	        // MONTH
	        case 'M' : // fall through to MM
	        case 'MM' :
	            if (input != null) {
	                datePartArray[MONTH] = toInt(input) - 1;
	            }
	            break;
	        case 'MMM' : // fall through to MMMM
	        case 'MMMM' :
	            a = config._locale.monthsParse(input, token, config._strict);
	            // if we didn't find a month name, mark the date as invalid.
	            if (a != null) {
	                datePartArray[MONTH] = a;
	            } else {
	                config._pf.invalidMonth = input;
	            }
	            break;
	        // DAY OF MONTH
	        case 'D' : // fall through to DD
	        case 'DD' :
	            if (input != null) {
	                datePartArray[DATE] = toInt(input);
	            }
	            break;
	        case 'Do' :
	            if (input != null) {
	                datePartArray[DATE] = toInt(parseInt(
	                            input.match(/\d{1,2}/)[0], 10));
	            }
	            break;
	        // DAY OF YEAR
	        case 'DDD' : // fall through to DDDD
	        case 'DDDD' :
	            if (input != null) {
	                config._dayOfYear = toInt(input);
	            }
	
	            break;
	        // YEAR
	        case 'YY' :
	            datePartArray[YEAR] = moment.parseTwoDigitYear(input);
	            break;
	        case 'YYYY' :
	        case 'YYYYY' :
	        case 'YYYYYY' :
	            datePartArray[YEAR] = toInt(input);
	            break;
	        // AM / PM
	        case 'a' : // fall through to A
	        case 'A' :
	            config._isPm = config._locale.isPM(input);
	            break;
	        // HOUR
	        case 'h' : // fall through to hh
	        case 'hh' :
	            config._pf.bigHour = true;
	            /* falls through */
	        case 'H' : // fall through to HH
	        case 'HH' :
	            datePartArray[HOUR] = toInt(input);
	            break;
	        // MINUTE
	        case 'm' : // fall through to mm
	        case 'mm' :
	            datePartArray[MINUTE] = toInt(input);
	            break;
	        // SECOND
	        case 's' : // fall through to ss
	        case 'ss' :
	            datePartArray[SECOND] = toInt(input);
	            break;
	        // MILLISECOND
	        case 'S' :
	        case 'SS' :
	        case 'SSS' :
	        case 'SSSS' :
	            datePartArray[MILLISECOND] = toInt(('0.' + input) * 1000);
	            break;
	        // UNIX OFFSET (MILLISECONDS)
	        case 'x':
	            config._d = new Date(toInt(input));
	            break;
	        // UNIX TIMESTAMP WITH MS
	        case 'X':
	            config._d = new Date(parseFloat(input) * 1000);
	            break;
	        // TIMEZONE
	        case 'Z' : // fall through to ZZ
	        case 'ZZ' :
	            config._useUTC = true;
	            config._tzm = timezoneMinutesFromString(input);
	            break;
	        // WEEKDAY - human
	        case 'dd':
	        case 'ddd':
	        case 'dddd':
	            a = config._locale.weekdaysParse(input);
	            // if we didn't get a weekday name, mark the date as invalid
	            if (a != null) {
	                config._w = config._w || {};
	                config._w['d'] = a;
	            } else {
	                config._pf.invalidWeekday = input;
	            }
	            break;
	        // WEEK, WEEK DAY - numeric
	        case 'w':
	        case 'ww':
	        case 'W':
	        case 'WW':
	        case 'd':
	        case 'e':
	        case 'E':
	            token = token.substr(0, 1);
	            /* falls through */
	        case 'gggg':
	        case 'GGGG':
	        case 'GGGGG':
	            token = token.substr(0, 2);
	            if (input) {
	                config._w = config._w || {};
	                config._w[token] = toInt(input);
	            }
	            break;
	        case 'gg':
	        case 'GG':
	            config._w = config._w || {};
	            config._w[token] = moment.parseTwoDigitYear(input);
	        }
	    }
	
	    function dayOfYearFromWeekInfo(config) {
	        var w, weekYear, week, weekday, dow, doy, temp;
	
	        w = config._w;
	        if (w.GG != null || w.W != null || w.E != null) {
	            dow = 1;
	            doy = 4;
	
	            // TODO: We need to take the current isoWeekYear, but that depends on
	            // how we interpret now (local, utc, fixed offset). So create
	            // a now version of current config (take local/utc/offset flags, and
	            // create now).
	            weekYear = dfl(w.GG, config._a[YEAR], weekOfYear(moment(), 1, 4).year);
	            week = dfl(w.W, 1);
	            weekday = dfl(w.E, 1);
	        } else {
	            dow = config._locale._week.dow;
	            doy = config._locale._week.doy;
	
	            weekYear = dfl(w.gg, config._a[YEAR], weekOfYear(moment(), dow, doy).year);
	            week = dfl(w.w, 1);
	
	            if (w.d != null) {
	                // weekday -- low day numbers are considered next week
	                weekday = w.d;
	                if (weekday < dow) {
	                    ++week;
	                }
	            } else if (w.e != null) {
	                // local weekday -- counting starts from begining of week
	                weekday = w.e + dow;
	            } else {
	                // default to begining of week
	                weekday = dow;
	            }
	        }
	        temp = dayOfYearFromWeeks(weekYear, week, weekday, doy, dow);
	
	        config._a[YEAR] = temp.year;
	        config._dayOfYear = temp.dayOfYear;
	    }
	
	    // convert an array to a date.
	    // the array should mirror the parameters below
	    // note: all values past the year are optional and will default to the lowest possible value.
	    // [year, month, day , hour, minute, second, millisecond]
	    function dateFromConfig(config) {
	        var i, date, input = [], currentDate, yearToUse;
	
	        if (config._d) {
	            return;
	        }
	
	        currentDate = currentDateArray(config);
	
	        //compute day of the year from weeks and weekdays
	        if (config._w && config._a[DATE] == null && config._a[MONTH] == null) {
	            dayOfYearFromWeekInfo(config);
	        }
	
	        //if the day of the year is set, figure out what it is
	        if (config._dayOfYear) {
	            yearToUse = dfl(config._a[YEAR], currentDate[YEAR]);
	
	            if (config._dayOfYear > daysInYear(yearToUse)) {
	                config._pf._overflowDayOfYear = true;
	            }
	
	            date = makeUTCDate(yearToUse, 0, config._dayOfYear);
	            config._a[MONTH] = date.getUTCMonth();
	            config._a[DATE] = date.getUTCDate();
	        }
	
	        // Default to current date.
	        // * if no year, month, day of month are given, default to today
	        // * if day of month is given, default month and year
	        // * if month is given, default only year
	        // * if year is given, don't default anything
	        for (i = 0; i < 3 && config._a[i] == null; ++i) {
	            config._a[i] = input[i] = currentDate[i];
	        }
	
	        // Zero out whatever was not defaulted, including time
	        for (; i < 7; i++) {
	            config._a[i] = input[i] = (config._a[i] == null) ? (i === 2 ? 1 : 0) : config._a[i];
	        }
	
	        // Check for 24:00:00.000
	        if (config._a[HOUR] === 24 &&
	                config._a[MINUTE] === 0 &&
	                config._a[SECOND] === 0 &&
	                config._a[MILLISECOND] === 0) {
	            config._nextDay = true;
	            config._a[HOUR] = 0;
	        }
	
	        config._d = (config._useUTC ? makeUTCDate : makeDate).apply(null, input);
	        // Apply timezone offset from input. The actual zone can be changed
	        // with parseZone.
	        if (config._tzm != null) {
	            config._d.setUTCMinutes(config._d.getUTCMinutes() + config._tzm);
	        }
	
	        if (config._nextDay) {
	            config._a[HOUR] = 24;
	        }
	    }
	
	    function dateFromObject(config) {
	        var normalizedInput;
	
	        if (config._d) {
	            return;
	        }
	
	        normalizedInput = normalizeObjectUnits(config._i);
	        config._a = [
	            normalizedInput.year,
	            normalizedInput.month,
	            normalizedInput.day || normalizedInput.date,
	            normalizedInput.hour,
	            normalizedInput.minute,
	            normalizedInput.second,
	            normalizedInput.millisecond
	        ];
	
	        dateFromConfig(config);
	    }
	
	    function currentDateArray(config) {
	        var now = new Date();
	        if (config._useUTC) {
	            return [
	                now.getUTCFullYear(),
	                now.getUTCMonth(),
	                now.getUTCDate()
	            ];
	        } else {
	            return [now.getFullYear(), now.getMonth(), now.getDate()];
	        }
	    }
	
	    // date from string and format string
	    function makeDateFromStringAndFormat(config) {
	        if (config._f === moment.ISO_8601) {
	            parseISO(config);
	            return;
	        }
	
	        config._a = [];
	        config._pf.empty = true;
	
	        // This array is used to make a Date, either with `new Date` or `Date.UTC`
	        var string = '' + config._i,
	            i, parsedInput, tokens, token, skipped,
	            stringLength = string.length,
	            totalParsedInputLength = 0;
	
	        tokens = expandFormat(config._f, config._locale).match(formattingTokens) || [];
	
	        for (i = 0; i < tokens.length; i++) {
	            token = tokens[i];
	            parsedInput = (string.match(getParseRegexForToken(token, config)) || [])[0];
	            if (parsedInput) {
	                skipped = string.substr(0, string.indexOf(parsedInput));
	                if (skipped.length > 0) {
	                    config._pf.unusedInput.push(skipped);
	                }
	                string = string.slice(string.indexOf(parsedInput) + parsedInput.length);
	                totalParsedInputLength += parsedInput.length;
	            }
	            // don't parse if it's not a known token
	            if (formatTokenFunctions[token]) {
	                if (parsedInput) {
	                    config._pf.empty = false;
	                }
	                else {
	                    config._pf.unusedTokens.push(token);
	                }
	                addTimeToArrayFromToken(token, parsedInput, config);
	            }
	            else if (config._strict && !parsedInput) {
	                config._pf.unusedTokens.push(token);
	            }
	        }
	
	        // add remaining unparsed input length to the string
	        config._pf.charsLeftOver = stringLength - totalParsedInputLength;
	        if (string.length > 0) {
	            config._pf.unusedInput.push(string);
	        }
	
	        // clear _12h flag if hour is <= 12
	        if (config._pf.bigHour === true && config._a[HOUR] <= 12) {
	            config._pf.bigHour = undefined;
	        }
	        // handle am pm
	        if (config._isPm && config._a[HOUR] < 12) {
	            config._a[HOUR] += 12;
	        }
	        // if is 12 am, change hours to 0
	        if (config._isPm === false && config._a[HOUR] === 12) {
	            config._a[HOUR] = 0;
	        }
	        dateFromConfig(config);
	        checkOverflow(config);
	    }
	
	    function unescapeFormat(s) {
	        return s.replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function (matched, p1, p2, p3, p4) {
	            return p1 || p2 || p3 || p4;
	        });
	    }
	
	    // Code from http://stackoverflow.com/questions/3561493/is-there-a-regexp-escape-function-in-javascript
	    function regexpEscape(s) {
	        return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
	    }
	
	    // date from string and array of format strings
	    function makeDateFromStringAndArray(config) {
	        var tempConfig,
	            bestMoment,
	
	            scoreToBeat,
	            i,
	            currentScore;
	
	        if (config._f.length === 0) {
	            config._pf.invalidFormat = true;
	            config._d = new Date(NaN);
	            return;
	        }
	
	        for (i = 0; i < config._f.length; i++) {
	            currentScore = 0;
	            tempConfig = copyConfig({}, config);
	            if (config._useUTC != null) {
	                tempConfig._useUTC = config._useUTC;
	            }
	            tempConfig._pf = defaultParsingFlags();
	            tempConfig._f = config._f[i];
	            makeDateFromStringAndFormat(tempConfig);
	
	            if (!isValid(tempConfig)) {
	                continue;
	            }
	
	            // if there is any input that was not parsed add a penalty for that format
	            currentScore += tempConfig._pf.charsLeftOver;
	
	            //or tokens
	            currentScore += tempConfig._pf.unusedTokens.length * 10;
	
	            tempConfig._pf.score = currentScore;
	
	            if (scoreToBeat == null || currentScore < scoreToBeat) {
	                scoreToBeat = currentScore;
	                bestMoment = tempConfig;
	            }
	        }
	
	        extend(config, bestMoment || tempConfig);
	    }
	
	    // date from iso format
	    function parseISO(config) {
	        var i, l,
	            string = config._i,
	            match = isoRegex.exec(string);
	
	        if (match) {
	            config._pf.iso = true;
	            for (i = 0, l = isoDates.length; i < l; i++) {
	                if (isoDates[i][1].exec(string)) {
	                    // match[5] should be 'T' or undefined
	                    config._f = isoDates[i][0] + (match[6] || ' ');
	                    break;
	                }
	            }
	            for (i = 0, l = isoTimes.length; i < l; i++) {
	                if (isoTimes[i][1].exec(string)) {
	                    config._f += isoTimes[i][0];
	                    break;
	                }
	            }
	            if (string.match(parseTokenTimezone)) {
	                config._f += 'Z';
	            }
	            makeDateFromStringAndFormat(config);
	        } else {
	            config._isValid = false;
	        }
	    }
	
	    // date from iso format or fallback
	    function makeDateFromString(config) {
	        parseISO(config);
	        if (config._isValid === false) {
	            delete config._isValid;
	            moment.createFromInputFallback(config);
	        }
	    }
	
	    function map(arr, fn) {
	        var res = [], i;
	        for (i = 0; i < arr.length; ++i) {
	            res.push(fn(arr[i], i));
	        }
	        return res;
	    }
	
	    function makeDateFromInput(config) {
	        var input = config._i, matched;
	        if (input === undefined) {
	            config._d = new Date();
	        } else if (isDate(input)) {
	            config._d = new Date(+input);
	        } else if ((matched = aspNetJsonRegex.exec(input)) !== null) {
	            config._d = new Date(+matched[1]);
	        } else if (typeof input === 'string') {
	            makeDateFromString(config);
	        } else if (isArray(input)) {
	            config._a = map(input.slice(0), function (obj) {
	                return parseInt(obj, 10);
	            });
	            dateFromConfig(config);
	        } else if (typeof(input) === 'object') {
	            dateFromObject(config);
	        } else if (typeof(input) === 'number') {
	            // from milliseconds
	            config._d = new Date(input);
	        } else {
	            moment.createFromInputFallback(config);
	        }
	    }
	
	    function makeDate(y, m, d, h, M, s, ms) {
	        //can't just apply() to create a date:
	        //http://stackoverflow.com/questions/181348/instantiating-a-javascript-object-by-calling-prototype-constructor-apply
	        var date = new Date(y, m, d, h, M, s, ms);
	
	        //the date constructor doesn't accept years < 1970
	        if (y < 1970) {
	            date.setFullYear(y);
	        }
	        return date;
	    }
	
	    function makeUTCDate(y) {
	        var date = new Date(Date.UTC.apply(null, arguments));
	        if (y < 1970) {
	            date.setUTCFullYear(y);
	        }
	        return date;
	    }
	
	    function parseWeekday(input, locale) {
	        if (typeof input === 'string') {
	            if (!isNaN(input)) {
	                input = parseInt(input, 10);
	            }
	            else {
	                input = locale.weekdaysParse(input);
	                if (typeof input !== 'number') {
	                    return null;
	                }
	            }
	        }
	        return input;
	    }
	
	    /************************************
	        Relative Time
	    ************************************/
	
	
	    // helper function for moment.fn.from, moment.fn.fromNow, and moment.duration.fn.humanize
	    function substituteTimeAgo(string, number, withoutSuffix, isFuture, locale) {
	        return locale.relativeTime(number || 1, !!withoutSuffix, string, isFuture);
	    }
	
	    function relativeTime(posNegDuration, withoutSuffix, locale) {
	        var duration = moment.duration(posNegDuration).abs(),
	            seconds = round(duration.as('s')),
	            minutes = round(duration.as('m')),
	            hours = round(duration.as('h')),
	            days = round(duration.as('d')),
	            months = round(duration.as('M')),
	            years = round(duration.as('y')),
	
	            args = seconds < relativeTimeThresholds.s && ['s', seconds] ||
	                minutes === 1 && ['m'] ||
	                minutes < relativeTimeThresholds.m && ['mm', minutes] ||
	                hours === 1 && ['h'] ||
	                hours < relativeTimeThresholds.h && ['hh', hours] ||
	                days === 1 && ['d'] ||
	                days < relativeTimeThresholds.d && ['dd', days] ||
	                months === 1 && ['M'] ||
	                months < relativeTimeThresholds.M && ['MM', months] ||
	                years === 1 && ['y'] || ['yy', years];
	
	        args[2] = withoutSuffix;
	        args[3] = +posNegDuration > 0;
	        args[4] = locale;
	        return substituteTimeAgo.apply({}, args);
	    }
	
	
	    /************************************
	        Week of Year
	    ************************************/
	
	
	    // firstDayOfWeek       0 = sun, 6 = sat
	    //                      the day of the week that starts the week
	    //                      (usually sunday or monday)
	    // firstDayOfWeekOfYear 0 = sun, 6 = sat
	    //                      the first week is the week that contains the first
	    //                      of this day of the week
	    //                      (eg. ISO weeks use thursday (4))
	    function weekOfYear(mom, firstDayOfWeek, firstDayOfWeekOfYear) {
	        var end = firstDayOfWeekOfYear - firstDayOfWeek,
	            daysToDayOfWeek = firstDayOfWeekOfYear - mom.day(),
	            adjustedMoment;
	
	
	        if (daysToDayOfWeek > end) {
	            daysToDayOfWeek -= 7;
	        }
	
	        if (daysToDayOfWeek < end - 7) {
	            daysToDayOfWeek += 7;
	        }
	
	        adjustedMoment = moment(mom).add(daysToDayOfWeek, 'd');
	        return {
	            week: Math.ceil(adjustedMoment.dayOfYear() / 7),
	            year: adjustedMoment.year()
	        };
	    }
	
	    //http://en.wikipedia.org/wiki/ISO_week_date#Calculating_a_date_given_the_year.2C_week_number_and_weekday
	    function dayOfYearFromWeeks(year, week, weekday, firstDayOfWeekOfYear, firstDayOfWeek) {
	        var d = makeUTCDate(year, 0, 1).getUTCDay(), daysToAdd, dayOfYear;
	
	        d = d === 0 ? 7 : d;
	        weekday = weekday != null ? weekday : firstDayOfWeek;
	        daysToAdd = firstDayOfWeek - d + (d > firstDayOfWeekOfYear ? 7 : 0) - (d < firstDayOfWeek ? 7 : 0);
	        dayOfYear = 7 * (week - 1) + (weekday - firstDayOfWeek) + daysToAdd + 1;
	
	        return {
	            year: dayOfYear > 0 ? year : year - 1,
	            dayOfYear: dayOfYear > 0 ?  dayOfYear : daysInYear(year - 1) + dayOfYear
	        };
	    }
	
	    /************************************
	        Top Level Functions
	    ************************************/
	
	    function makeMoment(config) {
	        var input = config._i,
	            format = config._f,
	            res;
	
	        config._locale = config._locale || moment.localeData(config._l);
	
	        if (input === null || (format === undefined && input === '')) {
	            return moment.invalid({nullInput: true});
	        }
	
	        if (typeof input === 'string') {
	            config._i = input = config._locale.preparse(input);
	        }
	
	        if (moment.isMoment(input)) {
	            return new Moment(input, true);
	        } else if (format) {
	            if (isArray(format)) {
	                makeDateFromStringAndArray(config);
	            } else {
	                makeDateFromStringAndFormat(config);
	            }
	        } else {
	            makeDateFromInput(config);
	        }
	
	        res = new Moment(config);
	        if (res._nextDay) {
	            // Adding is smart enough around DST
	            res.add(1, 'd');
	            res._nextDay = undefined;
	        }
	
	        return res;
	    }
	
	    moment = function (input, format, locale, strict) {
	        var c;
	
	        if (typeof(locale) === 'boolean') {
	            strict = locale;
	            locale = undefined;
	        }
	        // object construction must be done this way.
	        // https://github.com/moment/moment/issues/1423
	        c = {};
	        c._isAMomentObject = true;
	        c._i = input;
	        c._f = format;
	        c._l = locale;
	        c._strict = strict;
	        c._isUTC = false;
	        c._pf = defaultParsingFlags();
	
	        return makeMoment(c);
	    };
	
	    moment.suppressDeprecationWarnings = false;
	
	    moment.createFromInputFallback = deprecate(
	        'moment construction falls back to js Date. This is ' +
	        'discouraged and will be removed in upcoming major ' +
	        'release. Please refer to ' +
	        'https://github.com/moment/moment/issues/1407 for more info.',
	        function (config) {
	            config._d = new Date(config._i + (config._useUTC ? ' UTC' : ''));
	        }
	    );
	
	    // Pick a moment m from moments so that m[fn](other) is true for all
	    // other. This relies on the function fn to be transitive.
	    //
	    // moments should either be an array of moment objects or an array, whose
	    // first element is an array of moment objects.
	    function pickBy(fn, moments) {
	        var res, i;
	        if (moments.length === 1 && isArray(moments[0])) {
	            moments = moments[0];
	        }
	        if (!moments.length) {
	            return moment();
	        }
	        res = moments[0];
	        for (i = 1; i < moments.length; ++i) {
	            if (moments[i][fn](res)) {
	                res = moments[i];
	            }
	        }
	        return res;
	    }
	
	    moment.min = function () {
	        var args = [].slice.call(arguments, 0);
	
	        return pickBy('isBefore', args);
	    };
	
	    moment.max = function () {
	        var args = [].slice.call(arguments, 0);
	
	        return pickBy('isAfter', args);
	    };
	
	    // creating with utc
	    moment.utc = function (input, format, locale, strict) {
	        var c;
	
	        if (typeof(locale) === 'boolean') {
	            strict = locale;
	            locale = undefined;
	        }
	        // object construction must be done this way.
	        // https://github.com/moment/moment/issues/1423
	        c = {};
	        c._isAMomentObject = true;
	        c._useUTC = true;
	        c._isUTC = true;
	        c._l = locale;
	        c._i = input;
	        c._f = format;
	        c._strict = strict;
	        c._pf = defaultParsingFlags();
	
	        return makeMoment(c).utc();
	    };
	
	    // creating with unix timestamp (in seconds)
	    moment.unix = function (input) {
	        return moment(input * 1000);
	    };
	
	    // duration
	    moment.duration = function (input, key) {
	        var duration = input,
	            // matching against regexp is expensive, do it on demand
	            match = null,
	            sign,
	            ret,
	            parseIso,
	            diffRes;
	
	        if (moment.isDuration(input)) {
	            duration = {
	                ms: input._milliseconds,
	                d: input._days,
	                M: input._months
	            };
	        } else if (typeof input === 'number') {
	            duration = {};
	            if (key) {
	                duration[key] = input;
	            } else {
	                duration.milliseconds = input;
	            }
	        } else if (!!(match = aspNetTimeSpanJsonRegex.exec(input))) {
	            sign = (match[1] === '-') ? -1 : 1;
	            duration = {
	                y: 0,
	                d: toInt(match[DATE]) * sign,
	                h: toInt(match[HOUR]) * sign,
	                m: toInt(match[MINUTE]) * sign,
	                s: toInt(match[SECOND]) * sign,
	                ms: toInt(match[MILLISECOND]) * sign
	            };
	        } else if (!!(match = isoDurationRegex.exec(input))) {
	            sign = (match[1] === '-') ? -1 : 1;
	            parseIso = function (inp) {
	                // We'd normally use ~~inp for this, but unfortunately it also
	                // converts floats to ints.
	                // inp may be undefined, so careful calling replace on it.
	                var res = inp && parseFloat(inp.replace(',', '.'));
	                // apply sign while we're at it
	                return (isNaN(res) ? 0 : res) * sign;
	            };
	            duration = {
	                y: parseIso(match[2]),
	                M: parseIso(match[3]),
	                d: parseIso(match[4]),
	                h: parseIso(match[5]),
	                m: parseIso(match[6]),
	                s: parseIso(match[7]),
	                w: parseIso(match[8])
	            };
	        } else if (typeof duration === 'object' &&
	                ('from' in duration || 'to' in duration)) {
	            diffRes = momentsDifference(moment(duration.from), moment(duration.to));
	
	            duration = {};
	            duration.ms = diffRes.milliseconds;
	            duration.M = diffRes.months;
	        }
	
	        ret = new Duration(duration);
	
	        if (moment.isDuration(input) && hasOwnProp(input, '_locale')) {
	            ret._locale = input._locale;
	        }
	
	        return ret;
	    };
	
	    // version number
	    moment.version = VERSION;
	
	    // default format
	    moment.defaultFormat = isoFormat;
	
	    // constant that refers to the ISO standard
	    moment.ISO_8601 = function () {};
	
	    // Plugins that add properties should also add the key here (null value),
	    // so we can properly clone ourselves.
	    moment.momentProperties = momentProperties;
	
	    // This function will be called whenever a moment is mutated.
	    // It is intended to keep the offset in sync with the timezone.
	    moment.updateOffset = function () {};
	
	    // This function allows you to set a threshold for relative time strings
	    moment.relativeTimeThreshold = function (threshold, limit) {
	        if (relativeTimeThresholds[threshold] === undefined) {
	            return false;
	        }
	        if (limit === undefined) {
	            return relativeTimeThresholds[threshold];
	        }
	        relativeTimeThresholds[threshold] = limit;
	        return true;
	    };
	
	    moment.lang = deprecate(
	        'moment.lang is deprecated. Use moment.locale instead.',
	        function (key, value) {
	            return moment.locale(key, value);
	        }
	    );
	
	    // This function will load locale and then set the global locale.  If
	    // no arguments are passed in, it will simply return the current global
	    // locale key.
	    moment.locale = function (key, values) {
	        var data;
	        if (key) {
	            if (typeof(values) !== 'undefined') {
	                data = moment.defineLocale(key, values);
	            }
	            else {
	                data = moment.localeData(key);
	            }
	
	            if (data) {
	                moment.duration._locale = moment._locale = data;
	            }
	        }
	
	        return moment._locale._abbr;
	    };
	
	    moment.defineLocale = function (name, values) {
	        if (values !== null) {
	            values.abbr = name;
	            if (!locales[name]) {
	                locales[name] = new Locale();
	            }
	            locales[name].set(values);
	
	            // backwards compat for now: also set the locale
	            moment.locale(name);
	
	            return locales[name];
	        } else {
	            // useful for testing
	            delete locales[name];
	            return null;
	        }
	    };
	
	    moment.langData = deprecate(
	        'moment.langData is deprecated. Use moment.localeData instead.',
	        function (key) {
	            return moment.localeData(key);
	        }
	    );
	
	    // returns locale data
	    moment.localeData = function (key) {
	        var locale;
	
	        if (key && key._locale && key._locale._abbr) {
	            key = key._locale._abbr;
	        }
	
	        if (!key) {
	            return moment._locale;
	        }
	
	        if (!isArray(key)) {
	            //short-circuit everything else
	            locale = loadLocale(key);
	            if (locale) {
	                return locale;
	            }
	            key = [key];
	        }
	
	        return chooseLocale(key);
	    };
	
	    // compare moment object
	    moment.isMoment = function (obj) {
	        return obj instanceof Moment ||
	            (obj != null && hasOwnProp(obj, '_isAMomentObject'));
	    };
	
	    // for typechecking Duration objects
	    moment.isDuration = function (obj) {
	        return obj instanceof Duration;
	    };
	
	    for (i = lists.length - 1; i >= 0; --i) {
	        makeList(lists[i]);
	    }
	
	    moment.normalizeUnits = function (units) {
	        return normalizeUnits(units);
	    };
	
	    moment.invalid = function (flags) {
	        var m = moment.utc(NaN);
	        if (flags != null) {
	            extend(m._pf, flags);
	        }
	        else {
	            m._pf.userInvalidated = true;
	        }
	
	        return m;
	    };
	
	    moment.parseZone = function () {
	        return moment.apply(null, arguments).parseZone();
	    };
	
	    moment.parseTwoDigitYear = function (input) {
	        return toInt(input) + (toInt(input) > 68 ? 1900 : 2000);
	    };
	
	    /************************************
	        Moment Prototype
	    ************************************/
	
	
	    extend(moment.fn = Moment.prototype, {
	
	        clone : function () {
	            return moment(this);
	        },
	
	        valueOf : function () {
	            return +this._d + ((this._offset || 0) * 60000);
	        },
	
	        unix : function () {
	            return Math.floor(+this / 1000);
	        },
	
	        toString : function () {
	            return this.clone().locale('en').format('ddd MMM DD YYYY HH:mm:ss [GMT]ZZ');
	        },
	
	        toDate : function () {
	            return this._offset ? new Date(+this) : this._d;
	        },
	
	        toISOString : function () {
	            var m = moment(this).utc();
	            if (0 < m.year() && m.year() <= 9999) {
	                if ('function' === typeof Date.prototype.toISOString) {
	                    // native implementation is ~50x faster, use it when we can
	                    return this.toDate().toISOString();
	                } else {
	                    return formatMoment(m, 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
	                }
	            } else {
	                return formatMoment(m, 'YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
	            }
	        },
	
	        toArray : function () {
	            var m = this;
	            return [
	                m.year(),
	                m.month(),
	                m.date(),
	                m.hours(),
	                m.minutes(),
	                m.seconds(),
	                m.milliseconds()
	            ];
	        },
	
	        isValid : function () {
	            return isValid(this);
	        },
	
	        isDSTShifted : function () {
	            if (this._a) {
	                return this.isValid() && compareArrays(this._a, (this._isUTC ? moment.utc(this._a) : moment(this._a)).toArray()) > 0;
	            }
	
	            return false;
	        },
	
	        parsingFlags : function () {
	            return extend({}, this._pf);
	        },
	
	        invalidAt: function () {
	            return this._pf.overflow;
	        },
	
	        utc : function (keepLocalTime) {
	            return this.zone(0, keepLocalTime);
	        },
	
	        local : function (keepLocalTime) {
	            if (this._isUTC) {
	                this.zone(0, keepLocalTime);
	                this._isUTC = false;
	
	                if (keepLocalTime) {
	                    this.add(this._dateTzOffset(), 'm');
	                }
	            }
	            return this;
	        },
	
	        format : function (inputString) {
	            var output = formatMoment(this, inputString || moment.defaultFormat);
	            return this.localeData().postformat(output);
	        },
	
	        add : createAdder(1, 'add'),
	
	        subtract : createAdder(-1, 'subtract'),
	
	        diff : function (input, units, asFloat) {
	            var that = makeAs(input, this),
	                zoneDiff = (this.zone() - that.zone()) * 6e4,
	                diff, output, daysAdjust;
	
	            units = normalizeUnits(units);
	
	            if (units === 'year' || units === 'month') {
	                // average number of days in the months in the given dates
	                diff = (this.daysInMonth() + that.daysInMonth()) * 432e5; // 24 * 60 * 60 * 1000 / 2
	                // difference in months
	                output = ((this.year() - that.year()) * 12) + (this.month() - that.month());
	                // adjust by taking difference in days, average number of days
	                // and dst in the given months.
	                daysAdjust = (this - moment(this).startOf('month')) -
	                    (that - moment(that).startOf('month'));
	                // same as above but with zones, to negate all dst
	                daysAdjust -= ((this.zone() - moment(this).startOf('month').zone()) -
	                        (that.zone() - moment(that).startOf('month').zone())) * 6e4;
	                output += daysAdjust / diff;
	                if (units === 'year') {
	                    output = output / 12;
	                }
	            } else {
	                diff = (this - that);
	                output = units === 'second' ? diff / 1e3 : // 1000
	                    units === 'minute' ? diff / 6e4 : // 1000 * 60
	                    units === 'hour' ? diff / 36e5 : // 1000 * 60 * 60
	                    units === 'day' ? (diff - zoneDiff) / 864e5 : // 1000 * 60 * 60 * 24, negate dst
	                    units === 'week' ? (diff - zoneDiff) / 6048e5 : // 1000 * 60 * 60 * 24 * 7, negate dst
	                    diff;
	            }
	            return asFloat ? output : absRound(output);
	        },
	
	        from : function (time, withoutSuffix) {
	            return moment.duration({to: this, from: time}).locale(this.locale()).humanize(!withoutSuffix);
	        },
	
	        fromNow : function (withoutSuffix) {
	            return this.from(moment(), withoutSuffix);
	        },
	
	        calendar : function (time) {
	            // We want to compare the start of today, vs this.
	            // Getting start-of-today depends on whether we're zone'd or not.
	            var now = time || moment(),
	                sod = makeAs(now, this).startOf('day'),
	                diff = this.diff(sod, 'days', true),
	                format = diff < -6 ? 'sameElse' :
	                    diff < -1 ? 'lastWeek' :
	                    diff < 0 ? 'lastDay' :
	                    diff < 1 ? 'sameDay' :
	                    diff < 2 ? 'nextDay' :
	                    diff < 7 ? 'nextWeek' : 'sameElse';
	            return this.format(this.localeData().calendar(format, this, moment(now)));
	        },
	
	        isLeapYear : function () {
	            return isLeapYear(this.year());
	        },
	
	        isDST : function () {
	            return (this.zone() < this.clone().month(0).zone() ||
	                this.zone() < this.clone().month(5).zone());
	        },
	
	        day : function (input) {
	            var day = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
	            if (input != null) {
	                input = parseWeekday(input, this.localeData());
	                return this.add(input - day, 'd');
	            } else {
	                return day;
	            }
	        },
	
	        month : makeAccessor('Month', true),
	
	        startOf : function (units) {
	            units = normalizeUnits(units);
	            // the following switch intentionally omits break keywords
	            // to utilize falling through the cases.
	            switch (units) {
	            case 'year':
	                this.month(0);
	                /* falls through */
	            case 'quarter':
	            case 'month':
	                this.date(1);
	                /* falls through */
	            case 'week':
	            case 'isoWeek':
	            case 'day':
	                this.hours(0);
	                /* falls through */
	            case 'hour':
	                this.minutes(0);
	                /* falls through */
	            case 'minute':
	                this.seconds(0);
	                /* falls through */
	            case 'second':
	                this.milliseconds(0);
	                /* falls through */
	            }
	
	            // weeks are a special case
	            if (units === 'week') {
	                this.weekday(0);
	            } else if (units === 'isoWeek') {
	                this.isoWeekday(1);
	            }
	
	            // quarters are also special
	            if (units === 'quarter') {
	                this.month(Math.floor(this.month() / 3) * 3);
	            }
	
	            return this;
	        },
	
	        endOf: function (units) {
	            units = normalizeUnits(units);
	            if (units === undefined || units === 'millisecond') {
	                return this;
	            }
	            return this.startOf(units).add(1, (units === 'isoWeek' ? 'week' : units)).subtract(1, 'ms');
	        },
	
	        isAfter: function (input, units) {
	            var inputMs;
	            units = normalizeUnits(typeof units !== 'undefined' ? units : 'millisecond');
	            if (units === 'millisecond') {
	                input = moment.isMoment(input) ? input : moment(input);
	                return +this > +input;
	            } else {
	                inputMs = moment.isMoment(input) ? +input : +moment(input);
	                return inputMs < +this.clone().startOf(units);
	            }
	        },
	
	        isBefore: function (input, units) {
	            var inputMs;
	            units = normalizeUnits(typeof units !== 'undefined' ? units : 'millisecond');
	            if (units === 'millisecond') {
	                input = moment.isMoment(input) ? input : moment(input);
	                return +this < +input;
	            } else {
	                inputMs = moment.isMoment(input) ? +input : +moment(input);
	                return +this.clone().endOf(units) < inputMs;
	            }
	        },
	
	        isSame: function (input, units) {
	            var inputMs;
	            units = normalizeUnits(units || 'millisecond');
	            if (units === 'millisecond') {
	                input = moment.isMoment(input) ? input : moment(input);
	                return +this === +input;
	            } else {
	                inputMs = +moment(input);
	                return +(this.clone().startOf(units)) <= inputMs && inputMs <= +(this.clone().endOf(units));
	            }
	        },
	
	        min: deprecate(
	                 'moment().min is deprecated, use moment.min instead. https://github.com/moment/moment/issues/1548',
	                 function (other) {
	                     other = moment.apply(null, arguments);
	                     return other < this ? this : other;
	                 }
	         ),
	
	        max: deprecate(
	                'moment().max is deprecated, use moment.max instead. https://github.com/moment/moment/issues/1548',
	                function (other) {
	                    other = moment.apply(null, arguments);
	                    return other > this ? this : other;
	                }
	        ),
	
	        // keepLocalTime = true means only change the timezone, without
	        // affecting the local hour. So 5:31:26 +0300 --[zone(2, true)]-->
	        // 5:31:26 +0200 It is possible that 5:31:26 doesn't exist int zone
	        // +0200, so we adjust the time as needed, to be valid.
	        //
	        // Keeping the time actually adds/subtracts (one hour)
	        // from the actual represented time. That is why we call updateOffset
	        // a second time. In case it wants us to change the offset again
	        // _changeInProgress == true case, then we have to adjust, because
	        // there is no such time in the given timezone.
	        zone : function (input, keepLocalTime) {
	            var offset = this._offset || 0,
	                localAdjust;
	            if (input != null) {
	                if (typeof input === 'string') {
	                    input = timezoneMinutesFromString(input);
	                }
	                if (Math.abs(input) < 16) {
	                    input = input * 60;
	                }
	                if (!this._isUTC && keepLocalTime) {
	                    localAdjust = this._dateTzOffset();
	                }
	                this._offset = input;
	                this._isUTC = true;
	                if (localAdjust != null) {
	                    this.subtract(localAdjust, 'm');
	                }
	                if (offset !== input) {
	                    if (!keepLocalTime || this._changeInProgress) {
	                        addOrSubtractDurationFromMoment(this,
	                                moment.duration(offset - input, 'm'), 1, false);
	                    } else if (!this._changeInProgress) {
	                        this._changeInProgress = true;
	                        moment.updateOffset(this, true);
	                        this._changeInProgress = null;
	                    }
	                }
	            } else {
	                return this._isUTC ? offset : this._dateTzOffset();
	            }
	            return this;
	        },
	
	        zoneAbbr : function () {
	            return this._isUTC ? 'UTC' : '';
	        },
	
	        zoneName : function () {
	            return this._isUTC ? 'Coordinated Universal Time' : '';
	        },
	
	        parseZone : function () {
	            if (this._tzm) {
	                this.zone(this._tzm);
	            } else if (typeof this._i === 'string') {
	                this.zone(this._i);
	            }
	            return this;
	        },
	
	        hasAlignedHourOffset : function (input) {
	            if (!input) {
	                input = 0;
	            }
	            else {
	                input = moment(input).zone();
	            }
	
	            return (this.zone() - input) % 60 === 0;
	        },
	
	        daysInMonth : function () {
	            return daysInMonth(this.year(), this.month());
	        },
	
	        dayOfYear : function (input) {
	            var dayOfYear = round((moment(this).startOf('day') - moment(this).startOf('year')) / 864e5) + 1;
	            return input == null ? dayOfYear : this.add((input - dayOfYear), 'd');
	        },
	
	        quarter : function (input) {
	            return input == null ? Math.ceil((this.month() + 1) / 3) : this.month((input - 1) * 3 + this.month() % 3);
	        },
	
	        weekYear : function (input) {
	            var year = weekOfYear(this, this.localeData()._week.dow, this.localeData()._week.doy).year;
	            return input == null ? year : this.add((input - year), 'y');
	        },
	
	        isoWeekYear : function (input) {
	            var year = weekOfYear(this, 1, 4).year;
	            return input == null ? year : this.add((input - year), 'y');
	        },
	
	        week : function (input) {
	            var week = this.localeData().week(this);
	            return input == null ? week : this.add((input - week) * 7, 'd');
	        },
	
	        isoWeek : function (input) {
	            var week = weekOfYear(this, 1, 4).week;
	            return input == null ? week : this.add((input - week) * 7, 'd');
	        },
	
	        weekday : function (input) {
	            var weekday = (this.day() + 7 - this.localeData()._week.dow) % 7;
	            return input == null ? weekday : this.add(input - weekday, 'd');
	        },
	
	        isoWeekday : function (input) {
	            // behaves the same as moment#day except
	            // as a getter, returns 7 instead of 0 (1-7 range instead of 0-6)
	            // as a setter, sunday should belong to the previous week.
	            return input == null ? this.day() || 7 : this.day(this.day() % 7 ? input : input - 7);
	        },
	
	        isoWeeksInYear : function () {
	            return weeksInYear(this.year(), 1, 4);
	        },
	
	        weeksInYear : function () {
	            var weekInfo = this.localeData()._week;
	            return weeksInYear(this.year(), weekInfo.dow, weekInfo.doy);
	        },
	
	        get : function (units) {
	            units = normalizeUnits(units);
	            return this[units]();
	        },
	
	        set : function (units, value) {
	            units = normalizeUnits(units);
	            if (typeof this[units] === 'function') {
	                this[units](value);
	            }
	            return this;
	        },
	
	        // If passed a locale key, it will set the locale for this
	        // instance.  Otherwise, it will return the locale configuration
	        // variables for this instance.
	        locale : function (key) {
	            var newLocaleData;
	
	            if (key === undefined) {
	                return this._locale._abbr;
	            } else {
	                newLocaleData = moment.localeData(key);
	                if (newLocaleData != null) {
	                    this._locale = newLocaleData;
	                }
	                return this;
	            }
	        },
	
	        lang : deprecate(
	            'moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.',
	            function (key) {
	                if (key === undefined) {
	                    return this.localeData();
	                } else {
	                    return this.locale(key);
	                }
	            }
	        ),
	
	        localeData : function () {
	            return this._locale;
	        },
	
	        _dateTzOffset : function () {
	            // On Firefox.24 Date#getTimezoneOffset returns a floating point.
	            // https://github.com/moment/moment/pull/1871
	            return Math.round(this._d.getTimezoneOffset() / 15) * 15;
	        }
	    });
	
	    function rawMonthSetter(mom, value) {
	        var dayOfMonth;
	
	        // TODO: Move this out of here!
	        if (typeof value === 'string') {
	            value = mom.localeData().monthsParse(value);
	            // TODO: Another silent failure?
	            if (typeof value !== 'number') {
	                return mom;
	            }
	        }
	
	        dayOfMonth = Math.min(mom.date(),
	                daysInMonth(mom.year(), value));
	        mom._d['set' + (mom._isUTC ? 'UTC' : '') + 'Month'](value, dayOfMonth);
	        return mom;
	    }
	
	    function rawGetter(mom, unit) {
	        return mom._d['get' + (mom._isUTC ? 'UTC' : '') + unit]();
	    }
	
	    function rawSetter(mom, unit, value) {
	        if (unit === 'Month') {
	            return rawMonthSetter(mom, value);
	        } else {
	            return mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value);
	        }
	    }
	
	    function makeAccessor(unit, keepTime) {
	        return function (value) {
	            if (value != null) {
	                rawSetter(this, unit, value);
	                moment.updateOffset(this, keepTime);
	                return this;
	            } else {
	                return rawGetter(this, unit);
	            }
	        };
	    }
	
	    moment.fn.millisecond = moment.fn.milliseconds = makeAccessor('Milliseconds', false);
	    moment.fn.second = moment.fn.seconds = makeAccessor('Seconds', false);
	    moment.fn.minute = moment.fn.minutes = makeAccessor('Minutes', false);
	    // Setting the hour should keep the time, because the user explicitly
	    // specified which hour he wants. So trying to maintain the same hour (in
	    // a new timezone) makes sense. Adding/subtracting hours does not follow
	    // this rule.
	    moment.fn.hour = moment.fn.hours = makeAccessor('Hours', true);
	    // moment.fn.month is defined separately
	    moment.fn.date = makeAccessor('Date', true);
	    moment.fn.dates = deprecate('dates accessor is deprecated. Use date instead.', makeAccessor('Date', true));
	    moment.fn.year = makeAccessor('FullYear', true);
	    moment.fn.years = deprecate('years accessor is deprecated. Use year instead.', makeAccessor('FullYear', true));
	
	    // add plural methods
	    moment.fn.days = moment.fn.day;
	    moment.fn.months = moment.fn.month;
	    moment.fn.weeks = moment.fn.week;
	    moment.fn.isoWeeks = moment.fn.isoWeek;
	    moment.fn.quarters = moment.fn.quarter;
	
	    // add aliased format methods
	    moment.fn.toJSON = moment.fn.toISOString;
	
	    /************************************
	        Duration Prototype
	    ************************************/
	
	
	    function daysToYears (days) {
	        // 400 years have 146097 days (taking into account leap year rules)
	        return days * 400 / 146097;
	    }
	
	    function yearsToDays (years) {
	        // years * 365 + absRound(years / 4) -
	        //     absRound(years / 100) + absRound(years / 400);
	        return years * 146097 / 400;
	    }
	
	    extend(moment.duration.fn = Duration.prototype, {
	
	        _bubble : function () {
	            var milliseconds = this._milliseconds,
	                days = this._days,
	                months = this._months,
	                data = this._data,
	                seconds, minutes, hours, years = 0;
	
	            // The following code bubbles up values, see the tests for
	            // examples of what that means.
	            data.milliseconds = milliseconds % 1000;
	
	            seconds = absRound(milliseconds / 1000);
	            data.seconds = seconds % 60;
	
	            minutes = absRound(seconds / 60);
	            data.minutes = minutes % 60;
	
	            hours = absRound(minutes / 60);
	            data.hours = hours % 24;
	
	            days += absRound(hours / 24);
	
	            // Accurately convert days to years, assume start from year 0.
	            years = absRound(daysToYears(days));
	            days -= absRound(yearsToDays(years));
	
	            // 30 days to a month
	            // TODO (iskren): Use anchor date (like 1st Jan) to compute this.
	            months += absRound(days / 30);
	            days %= 30;
	
	            // 12 months -> 1 year
	            years += absRound(months / 12);
	            months %= 12;
	
	            data.days = days;
	            data.months = months;
	            data.years = years;
	        },
	
	        abs : function () {
	            this._milliseconds = Math.abs(this._milliseconds);
	            this._days = Math.abs(this._days);
	            this._months = Math.abs(this._months);
	
	            this._data.milliseconds = Math.abs(this._data.milliseconds);
	            this._data.seconds = Math.abs(this._data.seconds);
	            this._data.minutes = Math.abs(this._data.minutes);
	            this._data.hours = Math.abs(this._data.hours);
	            this._data.months = Math.abs(this._data.months);
	            this._data.years = Math.abs(this._data.years);
	
	            return this;
	        },
	
	        weeks : function () {
	            return absRound(this.days() / 7);
	        },
	
	        valueOf : function () {
	            return this._milliseconds +
	              this._days * 864e5 +
	              (this._months % 12) * 2592e6 +
	              toInt(this._months / 12) * 31536e6;
	        },
	
	        humanize : function (withSuffix) {
	            var output = relativeTime(this, !withSuffix, this.localeData());
	
	            if (withSuffix) {
	                output = this.localeData().pastFuture(+this, output);
	            }
	
	            return this.localeData().postformat(output);
	        },
	
	        add : function (input, val) {
	            // supports only 2.0-style add(1, 's') or add(moment)
	            var dur = moment.duration(input, val);
	
	            this._milliseconds += dur._milliseconds;
	            this._days += dur._days;
	            this._months += dur._months;
	
	            this._bubble();
	
	            return this;
	        },
	
	        subtract : function (input, val) {
	            var dur = moment.duration(input, val);
	
	            this._milliseconds -= dur._milliseconds;
	            this._days -= dur._days;
	            this._months -= dur._months;
	
	            this._bubble();
	
	            return this;
	        },
	
	        get : function (units) {
	            units = normalizeUnits(units);
	            return this[units.toLowerCase() + 's']();
	        },
	
	        as : function (units) {
	            var days, months;
	            units = normalizeUnits(units);
	
	            if (units === 'month' || units === 'year') {
	                days = this._days + this._milliseconds / 864e5;
	                months = this._months + daysToYears(days) * 12;
	                return units === 'month' ? months : months / 12;
	            } else {
	                // handle milliseconds separately because of floating point math errors (issue #1867)
	                days = this._days + Math.round(yearsToDays(this._months / 12));
	                switch (units) {
	                    case 'week': return days / 7 + this._milliseconds / 6048e5;
	                    case 'day': return days + this._milliseconds / 864e5;
	                    case 'hour': return days * 24 + this._milliseconds / 36e5;
	                    case 'minute': return days * 24 * 60 + this._milliseconds / 6e4;
	                    case 'second': return days * 24 * 60 * 60 + this._milliseconds / 1000;
	                    // Math.floor prevents floating point math errors here
	                    case 'millisecond': return Math.floor(days * 24 * 60 * 60 * 1000) + this._milliseconds;
	                    default: throw new Error('Unknown unit ' + units);
	                }
	            }
	        },
	
	        lang : moment.fn.lang,
	        locale : moment.fn.locale,
	
	        toIsoString : deprecate(
	            'toIsoString() is deprecated. Please use toISOString() instead ' +
	            '(notice the capitals)',
	            function () {
	                return this.toISOString();
	            }
	        ),
	
	        toISOString : function () {
	            // inspired by https://github.com/dordille/moment-isoduration/blob/master/moment.isoduration.js
	            var years = Math.abs(this.years()),
	                months = Math.abs(this.months()),
	                days = Math.abs(this.days()),
	                hours = Math.abs(this.hours()),
	                minutes = Math.abs(this.minutes()),
	                seconds = Math.abs(this.seconds() + this.milliseconds() / 1000);
	
	            if (!this.asSeconds()) {
	                // this is the same as C#'s (Noda) and python (isodate)...
	                // but not other JS (goog.date)
	                return 'P0D';
	            }
	
	            return (this.asSeconds() < 0 ? '-' : '') +
	                'P' +
	                (years ? years + 'Y' : '') +
	                (months ? months + 'M' : '') +
	                (days ? days + 'D' : '') +
	                ((hours || minutes || seconds) ? 'T' : '') +
	                (hours ? hours + 'H' : '') +
	                (minutes ? minutes + 'M' : '') +
	                (seconds ? seconds + 'S' : '');
	        },
	
	        localeData : function () {
	            return this._locale;
	        }
	    });
	
	    moment.duration.fn.toString = moment.duration.fn.toISOString;
	
	    function makeDurationGetter(name) {
	        moment.duration.fn[name] = function () {
	            return this._data[name];
	        };
	    }
	
	    for (i in unitMillisecondFactors) {
	        if (hasOwnProp(unitMillisecondFactors, i)) {
	            makeDurationGetter(i.toLowerCase());
	        }
	    }
	
	    moment.duration.fn.asMilliseconds = function () {
	        return this.as('ms');
	    };
	    moment.duration.fn.asSeconds = function () {
	        return this.as('s');
	    };
	    moment.duration.fn.asMinutes = function () {
	        return this.as('m');
	    };
	    moment.duration.fn.asHours = function () {
	        return this.as('h');
	    };
	    moment.duration.fn.asDays = function () {
	        return this.as('d');
	    };
	    moment.duration.fn.asWeeks = function () {
	        return this.as('weeks');
	    };
	    moment.duration.fn.asMonths = function () {
	        return this.as('M');
	    };
	    moment.duration.fn.asYears = function () {
	        return this.as('y');
	    };
	
	    /************************************
	        Default Locale
	    ************************************/
	
	
	    // Set default locale, other locale will inherit from English.
	    moment.locale('en', {
	        ordinalParse: /\d{1,2}(th|st|nd|rd)/,
	        ordinal : function (number) {
	            var b = number % 10,
	                output = (toInt(number % 100 / 10) === 1) ? 'th' :
	                (b === 1) ? 'st' :
	                (b === 2) ? 'nd' :
	                (b === 3) ? 'rd' : 'th';
	            return number + output;
	        }
	    });
	
	    /* EMBED_LOCALES */
	
	    /************************************
	        Exposing Moment
	    ************************************/
	
	    function makeGlobal(shouldDeprecate) {
	        /*global ender:false */
	        if (typeof ender !== 'undefined') {
	            return;
	        }
	        oldGlobalMoment = globalScope.moment;
	        if (shouldDeprecate) {
	            globalScope.moment = deprecate(
	                    'Accessing Moment through the global scope is ' +
	                    'deprecated, and will be removed in an upcoming ' +
	                    'release.',
	                    moment);
	        } else {
	            globalScope.moment = moment;
	        }
	    }
	
	    // CommonJS module is defined
	    if (hasModule) {
	        module.exports = moment;
	    } else if (true) {
	        !(__WEBPACK_AMD_DEFINE_RESULT__ = function (require, exports, module) {
	            if (module.config && module.config() && module.config().noGlobal === true) {
	                // release the global variable
	                globalScope.moment = oldGlobalMoment;
	            }
	
	            return moment;
	        }.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
	        makeGlobal(true);
	    } else {
	        makeGlobal();
	    }
	}).call(this);
	
	/* WEBPACK VAR INJECTION */}.call(exports, (function() { return this; }()), __webpack_require__(492)(module)))

/***/ }),

/***/ 1760:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	var $ = __webpack_require__(31),
	    dash = __webpack_require__(1);
	__webpack_require__(1367);
	__webpack_require__(1761);
	__webpack_require__(1704);
	
	dash.navigation = {
	    gotoNotes: function gotoNotes(key_measure_id) {
	        if (!key_measure_id) return;
	        $.blockUI();
	        var newWindowURL = 'viewNotes.php?key_measure_id=' + key_measure_id;
	
	        var theReq = $.ajax(newWindowURL);
	        theReq.done(function (data) {
	            var showModal = function showModal() {
	                var m = $(data).zbootstrapmodal({
	                    show: true
	                });
	
	                m.on('hidden.bs.modal', function () {
	                    m.remove();
	                });
	
	                $.unblockUI();
	            };
	            var existing = $('#keyMeasureNotesModal');
	
	            if (existing.length) {
	                existing.on("hidden.bs.modal", function (event) {
	                    showModal();
	                });
	                existing.zbootstrapmodal('hide');
	            } else {
	                showModal();
	            }
	        });
	        return false;
	    },
	    gotoShareItem: function gotoShareItem(tableName, tableId, extraTitle, filter, applyFiltersToExisting, shareDataOverride) {
	
	        if (!tableName || !tableId) return;
	        $.blockUI();
	
	        var newWindowURL = 'shareItem.php';
	        var params = { 'table_name': tableName, 'table_id': tableId, 'extra_title': extraTitle || '' };
	        if (filter != undefined) params['filter'] = filter;
	        if (shareDataOverride != undefined) params['shareDataOverride'] = JSON.stringify(shareDataOverride);
	        if (applyFiltersToExisting != undefined && applyFiltersToExisting != false) params['applyFiltersToExistingShares'] = 'true';
	
	        var existing = $('#shareItemModal');
	        if (existing.length) {
	            existing.modal('hide');
	        }
	        $.ajax({ url: newWindowURL,
	            type: 'POST',
	            data: params
	        }).done(function (data) {
	            var showModal = function showModal() {
	                var m = $(data).modal({
	                    show: true
	                });
	
	                m.on('hidden.bs.modal', function () {
	                    m.remove();
	                });
	            };
	            showModal();
	            $.unblockUI();
	        });
	
	        return false;
	    },
	    makeItemPublic: function makeItemPublic(tableName, tableID, isNowPublic, extraPostSuccessFunc, showAlert) {
	        var url = 'shareMakePublic.php';
	
	        if (isNowPublic == undefined || isNowPublic == null) isNowPublic = true;
	
	        if (showAlert === undefined || showAlert == null) showAlert = true;
	
	        $.blockUI();
	        var theReq = $.post(url, { 'table_name': tableName, 'table_id': tableID, 'is_public': !!isNowPublic ? 'true' : 'false' });
	        theReq.done(function (data) {
	            $.unblockUI();
	            if (!data) {
	                return alert('An unknown error occured.');
	            }
	            try {
	                data = eval('(' + data + ')');
	            } catch (e) {
	                alert('An unknown error occured');document.theData = data;return;
	            }
	            if (!data['message']) {
	                return alert('An unknown error occured.');
	            }
	            if (!!showAlert) dash.dialog.showAjaxSuccess(data['message']);
	            if (data['success']) {
	                if (!!extraPostSuccessFunc) extraPostSuccessFunc();
	            }
	        });
	    },
	    viewAlert: function viewAlert(alertID) {
	        var url = 'alert_description_modal.php';
	        $.blockUI();
	        var theReq = $.post(url, { 'id': alertID });
	        theReq.done(function (data) {
	            if (!data) {
	                alert('An unknown error occured.');
	                $.unblockUI();
	            }
	            $('#alertModal' + alertID).remove();
	            $(document.body).append(data);
	            $.unblockUI();
	            $('#alertModal' + alertID).zbootstrapmodal();
	        });
	    },
	    launchManagePassword: function launchManagePassword(event, userId) {
	        var url = 'managePassword.php';
	        $.blockUI();
	        var theReq = $.post(url, { 'id': userId });
	        theReq.done(function (data) {
	            $.unblockUI();
	            if (!data) return dash.dialog.showAjaxSuccess('There was an error.');
	            try {
	                $('#managePasswordModal').modal('hide');
	            } catch (e) {}
	            $('#managePasswordModal').remove();
	
	            $(document.body).append(data);
	        });
	    },
	    launchResetPassword: function launchResetPassword(event, userId) {
	        var url = 'views/modals/resetPasswordModal.php';
	        $.blockUI();
	        var theReq = $.post(url, { 'id': userId });
	        theReq.done(function (data) {
	            $.unblockUI();
	            if (!data) return dash.dialog.showAjaxSuccess('There was an error.');
	            try {
	                $('#resetPasswordModal').modal('hide');
	            } catch (e) {}
	            $('#resetPasswordModal').remove();
	
	            $(document.body).append(data);
	        });
	    },
	    launchForceChangePassword: function launchForceChangePassword(event, userId) {
	        var url = 'views/modals/forceChangePasswordModal.php';
	        $.blockUI();
	        var theReq = $.post(url, { 'id': userId });
	        theReq.done(function (data) {
	            $.unblockUI();
	            if (!data) return dash.dialog.showAjaxSuccess('There was an error.');
	            try {
	                $('#forceChangePasswordModal').modal('hide');
	            } catch (e) {}
	            $('#forceChangePasswordModal').remove();
	
	            $(document.body).append(data);
	        });
	    },
	    editUserSecurity: function editUserSecurity(event, userId) {
	        event.preventDefault();
	        var container = $('<div id="modal-user-security-edit-' + userId + '"></div>');
	        var options = {
	            okButton: {
	                text: 'Save'
	            }
	        };
	        container.zmodal(options);
	
	        var inner = $('<div id="user-security-edit-"' + userId + '"></div>');
	        container.find('.modal-body').first().append(inner).css('min-height', '250px');
	
	        options.modal = container;
	        options.userId = userId;
	        inner.userSecurityEditor(options);
	    },
	    editGroupSecurity: function editGroupSecurity(event, groupId) {
	        event.preventDefault();
	        var container = $('<div id="modal-group-security-edit-' + groupId + '"></div>');
	        var options = {
	            okButton: {
	                text: 'Save'
	            }
	        };
	        container.zmodal(options);
	
	        var inner = $('<div id="group-security-edit-"' + groupId + '"></div>');
	        container.find('.modal-body').first().append(inner).css('min-height', '250px');
	
	        options.modal = container;
	        options.groupId = groupId;
	        inner.userSecurityEditor(options);
	    }
	};
	
	module.exports = dash.navigation;

/***/ }),

/***/ 1761:
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;'use strict';
	
	/*!
	 * jQuery blockUI plugin
	 * Version 2.70.0-2014.11.23
	 * Requires jQuery v1.7 or later
	 *
	 * Examples at: http://malsup.com/jquery/block/
	 * Copyright (c) 2007-2013 M. Alsup
	 * Dual licensed under the MIT and GPL licenses:
	 * http://www.opensource.org/licenses/mit-license.php
	 * http://www.gnu.org/licenses/gpl.html
	 *
	 * Thanks to Amir-Hossein Sobhi for some excellent contributions!
	 */
	
	;(function () {
		/*jshint eqeqeq:false curly:false latedef:false */
		"use strict";
	
		function setup($) {
			$.fn._fadeIn = $.fn.fadeIn;
	
			var noOp = $.noop || function () {};
	
			// this bit is to ensure we don't call setExpression when we shouldn't (with extra muscle to handle
			// confusing userAgent strings on Vista)
			var msie = /MSIE/.test(navigator.userAgent);
			var ie6 = /MSIE 6.0/.test(navigator.userAgent) && !/MSIE 8.0/.test(navigator.userAgent);
			var mode = document.documentMode || 0;
			var setExpr = $.isFunction(document.createElement('div').style.setExpression);
	
			// global $ methods for blocking/unblocking the entire page
			$.blockUI = function (opts) {
				install(window, opts);
			};
			$.unblockUI = function (opts) {
				remove(window, opts);
			};
	
			// convenience method for quick growl-like notifications  (http://www.google.com/search?q=growl)
			$.growlUI = function (title, message, timeout, onClose) {
				var $m = $('<div class="growlUI"></div>');
				if (title) $m.append('<h1>' + title + '</h1>');
				if (message) $m.append('<h2>' + message + '</h2>');
				if (timeout === undefined) timeout = 3000;
	
				// Added by konapun: Set timeout to 30 seconds if this growl is moused over, like normal toast notifications
				var callBlock = function callBlock(opts) {
					opts = opts || {};
	
					$.blockUI({
						message: $m,
						fadeIn: typeof opts.fadeIn !== 'undefined' ? opts.fadeIn : 700,
						fadeOut: typeof opts.fadeOut !== 'undefined' ? opts.fadeOut : 1000,
						timeout: typeof opts.timeout !== 'undefined' ? opts.timeout : timeout,
						centerY: false,
						showOverlay: false,
						onUnblock: onClose,
						css: $.blockUI.defaults.growlCSS
					});
				};
	
				callBlock();
				var nonmousedOpacity = $m.css('opacity');
				$m.mouseover(function () {
					callBlock({
						fadeIn: 0,
						timeout: 30000
					});
	
					var displayBlock = $('.blockMsg');
					displayBlock.stop(); // cancel fadeout if it has started
					displayBlock.fadeTo(300, 1); // make it easier to read the message by removing transparency
				}).mouseout(function () {
					$('.blockMsg').fadeOut(1000);
				});
				// End konapun additions
			};
	
			// plugin method for blocking element content
			$.fn.block = function (opts) {
				if (this[0] === window) {
					$.blockUI(opts);
					return this;
				}
				var fullOpts = $.extend({}, $.blockUI.defaults, opts || {});
				this.each(function () {
					var $el = $(this);
					if (fullOpts.ignoreIfBlocked && $el.data('blockUI.isBlocked')) return;
					$el.unblock({ fadeOut: 0 });
				});
	
				return this.each(function () {
					if ($.css(this, 'position') == 'static') {
						this.style.position = 'relative';
						$(this).data('blockUI.static', true);
					}
					this.style.zoom = 1; // force 'hasLayout' in ie
					install(this, opts);
				});
			};
	
			// plugin method for unblocking element content
			$.fn.unblock = function (opts) {
				if (this[0] === window) {
					$.unblockUI(opts);
					return this;
				}
				return this.each(function () {
					remove(this, opts);
				});
			};
	
			$.blockUI.version = 2.70; // 2nd generation blocking at no extra cost!
	
			// override these in your code to change the default behavior and style
			$.blockUI.defaults = {
				// message displayed when blocking (use null for no message)
				message: '<h1 id="waiting">Please wait...</h1>',
	
				title: null, // title string; only used when theme == true
				draggable: true, // only used when theme == true (requires jquery-ui.js to be loaded)
	
				theme: false, // set to true to use with jQuery UI themes
	
				// styles for the message when blocking; if you wish to disable
				// these and use an external stylesheet then do this in your code:
				// $.blockUI.defaults.css = {};
				css: {
					padding: 0,
					margin: 0,
					width: '30%',
					top: '40%',
					left: '35%',
					textAlign: 'center',
					color: '#000',
					border: '3px solid #aaa',
					backgroundColor: '#fff',
					cursor: 'wait'
				},
	
				// minimal style set used when themes are used
				themedCSS: {
					width: '30%',
					top: '40%',
					left: '35%'
				},
	
				// styles for the overlay
				overlayCSS: {
					backgroundColor: '#000',
					opacity: 0.6,
					cursor: 'wait'
				},
	
				// style to replace wait cursor before unblocking to correct issue
				// of lingering wait cursor
				cursorReset: 'default',
	
				// styles applied when using $.growlUI
				growlCSS: {
					width: '350px',
					top: '10px',
					left: '',
					right: '10px',
					border: 'none',
					padding: '5px',
					opacity: 0.6,
					cursor: 'default',
					color: '#fff',
					backgroundColor: '#000',
					'-webkit-border-radius': '10px',
					'-moz-border-radius': '10px',
					'border-radius': '10px'
				},
	
				// IE issues: 'about:blank' fails on HTTPS and javascript:false is s-l-o-w
				// (hat tip to Jorge H. N. de Vasconcelos)
				/*jshint scripturl:true */
				iframeSrc: /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank',
	
				// force usage of iframe in non-IE browsers (handy for blocking applets)
				forceIframe: false,
	
				// z-index for the blocking overlay
				baseZ: 1000,
	
				// set these to true to have the message automatically centered
				centerX: true, // <-- only effects element blocking (page block controlled via css above)
				centerY: true,
	
				// allow body element to be stetched in ie6; this makes blocking look better
				// on "short" pages.  disable if you wish to prevent changes to the body height
				allowBodyStretch: true,
	
				// enable if you want key and mouse events to be disabled for content that is blocked
				bindEvents: true,
	
				// be default blockUI will supress tab navigation from leaving blocking content
				// (if bindEvents is true)
				constrainTabKey: true,
	
				// fadeIn time in millis; set to 0 to disable fadeIn on block
				fadeIn: 200,
	
				// fadeOut time in millis; set to 0 to disable fadeOut on unblock
				fadeOut: 400,
	
				// time in millis to wait before auto-unblocking; set to 0 to disable auto-unblock
				timeout: 0,
	
				// disable if you don't want to show the overlay
				showOverlay: true,
	
				// if true, focus will be placed in the first available input field when
				// page blocking
				focusInput: true,
	
				// elements that can receive focus
				focusableElements: ':input:enabled:visible',
	
				// suppresses the use of overlay styles on FF/Linux (due to performance issues with opacity)
				// no longer needed in 2012
				// applyPlatformOpacityRules: true,
	
				// callback method invoked when fadeIn has completed and blocking message is visible
				onBlock: null,
	
				// callback method invoked when unblocking has completed; the callback is
				// passed the element that has been unblocked (which is the window object for page
				// blocks) and the options that were passed to the unblock call:
				//	onUnblock(element, options)
				onUnblock: null,
	
				// callback method invoked when the overlay area is clicked.
				// setting this will turn the cursor to a pointer, otherwise cursor defined in overlayCss will be used.
				onOverlayClick: null,
	
				// don't ask; if you really must know: http://groups.google.com/group/jquery-en/browse_thread/thread/36640a8730503595/2f6a79a77a78e493#2f6a79a77a78e493
				quirksmodeOffsetHack: 4,
	
				// class name of the message block
				blockMsgClass: 'blockMsg',
	
				// if it is already blocked, then ignore it (don't unblock and reblock)
				ignoreIfBlocked: false
			};
	
			// private data and functions follow...
	
			var pageBlock = null;
			var pageBlockEls = [];
	
			function install(el, opts) {
				var css, themedCSS;
				var full = el == window;
				var msg = opts && opts.message !== undefined ? opts.message : undefined;
				opts = $.extend({}, $.blockUI.defaults, opts || {});
	
				if (opts.ignoreIfBlocked && $(el).data('blockUI.isBlocked')) return;
	
				opts.overlayCSS = $.extend({}, $.blockUI.defaults.overlayCSS, opts.overlayCSS || {});
				css = $.extend({}, $.blockUI.defaults.css, opts.css || {});
				if (opts.onOverlayClick) opts.overlayCSS.cursor = 'pointer';
	
				themedCSS = $.extend({}, $.blockUI.defaults.themedCSS, opts.themedCSS || {});
				msg = msg === undefined ? opts.message : msg;
	
				// remove the current block (if there is one)
				if (full && pageBlock) remove(window, { fadeOut: 0 });
	
				// if an existing element is being used as the blocking content then we capture
				// its current place in the DOM (and current display style) so we can restore
				// it when we unblock
				if (msg && typeof msg != 'string' && (msg.parentNode || msg.jquery)) {
					var node = msg.jquery ? msg[0] : msg;
					var data = {};
					$(el).data('blockUI.history', data);
					data.el = node;
					data.parent = node.parentNode;
					data.display = node.style.display;
					data.position = node.style.position;
					if (data.parent) data.parent.removeChild(node);
				}
	
				$(el).data('blockUI.onUnblock', opts.onUnblock);
				var z = opts.baseZ;
	
				// blockUI uses 3 layers for blocking, for simplicity they are all used on every platform;
				// layer1 is the iframe layer which is used to supress bleed through of underlying content
				// layer2 is the overlay layer which has opacity and a wait cursor (by default)
				// layer3 is the message content that is displayed while blocking
				var lyr1, lyr2, lyr3, s;
				if (msie || opts.forceIframe) lyr1 = $('<iframe class="blockUI" style="z-index:' + z++ + ';display:none;border:none;margin:0;padding:0;position:absolute;width:100%;height:100%;top:0;left:0" src="' + opts.iframeSrc + '"></iframe>');else lyr1 = $('<div class="blockUI" style="display:none"></div>');
	
				if (opts.theme) lyr2 = $('<div class="blockUI blockOverlay ui-widget-overlay" style="z-index:' + z++ + ';display:none"></div>');else lyr2 = $('<div class="blockUI blockOverlay" style="z-index:' + z++ + ';display:none;border:none;margin:0;padding:0;width:100%;height:100%;top:0;left:0"></div>');
	
				if (opts.theme && full) {
					s = '<div class="blockUI ' + opts.blockMsgClass + ' blockPage ui-dialog ui-widget ui-corner-all" style="z-index:' + (z + 10) + ';display:none;position:fixed">';
					if (opts.title) {
						s += '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">' + (opts.title || '&nbsp;') + '</div>';
					}
					s += '<div class="ui-widget-content ui-dialog-content"></div>';
					s += '</div>';
				} else if (opts.theme) {
					s = '<div class="blockUI ' + opts.blockMsgClass + ' blockElement ui-dialog ui-widget ui-corner-all" style="z-index:' + (z + 10) + ';display:none;position:absolute">';
					if (opts.title) {
						s += '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">' + (opts.title || '&nbsp;') + '</div>';
					}
					s += '<div class="ui-widget-content ui-dialog-content"></div>';
					s += '</div>';
				} else if (full) {
					s = '<div class="blockUI ' + opts.blockMsgClass + ' blockPage" style="z-index:' + (z + 10) + ';display:none;position:fixed"></div>';
				} else {
					s = '<div class="blockUI ' + opts.blockMsgClass + ' blockElement" style="z-index:' + (z + 10) + ';display:none;position:absolute"></div>';
				}
				lyr3 = $(s);
	
				// if we have a message, style it
				if (msg) {
					if (opts.theme) {
						lyr3.css(themedCSS);
						lyr3.addClass('ui-widget-content');
					} else lyr3.css(css);
				}
	
				// style the overlay
				if (!opts.theme /*&& (!opts.applyPlatformOpacityRules)*/) lyr2.css(opts.overlayCSS);
				lyr2.css('position', full ? 'fixed' : 'absolute');
	
				// make iframe layer transparent in IE
				if (msie || opts.forceIframe) lyr1.css('opacity', 0.0);
	
				//$([lyr1[0],lyr2[0],lyr3[0]]).appendTo(full ? 'body' : el);
				var layers = [lyr1, lyr2, lyr3],
				    $par = full ? $('body') : $(el);
				$.each(layers, function () {
					this.appendTo($par);
				});
	
				if (opts.theme && opts.draggable && $.fn.draggable) {
					lyr3.draggable({
						handle: '.ui-dialog-titlebar',
						cancel: 'li'
					});
				}
	
				// ie7 must use absolute positioning in quirks mode and to account for activex issues (when scrolling)
				var expr = setExpr && (!$.support.boxModel || $('object,embed', full ? null : el).length > 0);
				if (ie6 || expr) {
					// give body 100% height
					if (full && opts.allowBodyStretch && $.support.boxModel) $('html,body').css('height', '100%');
	
					// fix ie6 issue when blocked element has a border width
					if ((ie6 || !$.support.boxModel) && !full) {
						var t = sz(el, 'borderTopWidth'),
						    l = sz(el, 'borderLeftWidth');
						var fixT = t ? '(0 - ' + t + ')' : 0;
						var fixL = l ? '(0 - ' + l + ')' : 0;
					}
	
					// simulate fixed position
					$.each(layers, function (i, o) {
						var s = o[0].style;
						s.position = 'absolute';
						if (i < 2) {
							if (full) s.setExpression('height', 'Math.max(document.body.scrollHeight, document.body.offsetHeight) - (jQuery.support.boxModel?0:' + opts.quirksmodeOffsetHack + ') + "px"');else s.setExpression('height', 'this.parentNode.offsetHeight + "px"');
							if (full) s.setExpression('width', 'jQuery.support.boxModel && document.documentElement.clientWidth || document.body.clientWidth + "px"');else s.setExpression('width', 'this.parentNode.offsetWidth + "px"');
							if (fixL) s.setExpression('left', fixL);
							if (fixT) s.setExpression('top', fixT);
						} else if (opts.centerY) {
							if (full) s.setExpression('top', '(document.documentElement.clientHeight || document.body.clientHeight) / 2 - (this.offsetHeight / 2) + (blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"');
							s.marginTop = 0;
						} else if (!opts.centerY && full) {
							var top = opts.css && opts.css.top ? parseInt(opts.css.top, 10) : 0;
							var expression = '((document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + ' + top + ') + "px"';
							s.setExpression('top', expression);
						}
					});
				}
	
				// show the message
				if (msg) {
					if (opts.theme) lyr3.find('.ui-widget-content').append(msg);else lyr3.append(msg);
					if (msg.jquery || msg.nodeType) $(msg).show();
				}
	
				if ((msie || opts.forceIframe) && opts.showOverlay) lyr1.show(); // opacity is zero
				if (opts.fadeIn) {
					var cb = opts.onBlock ? opts.onBlock : noOp;
					var cb1 = opts.showOverlay && !msg ? cb : noOp;
					var cb2 = msg ? cb : noOp;
					if (opts.showOverlay) lyr2._fadeIn(opts.fadeIn, cb1);
					if (msg) lyr3._fadeIn(opts.fadeIn, cb2);
				} else {
					if (opts.showOverlay) lyr2.show();
					if (msg) lyr3.show();
					if (opts.onBlock) opts.onBlock.bind(lyr3)();
				}
	
				// bind key and mouse events
				bind(1, el, opts);
	
				if (full) {
					pageBlock = lyr3[0];
					pageBlockEls = $(opts.focusableElements, pageBlock);
					if (opts.focusInput) setTimeout(focus, 20);
				} else center(lyr3[0], opts.centerX, opts.centerY);
	
				if (opts.timeout) {
					// auto-unblock
					var to = setTimeout(function () {
						if (full) $.unblockUI(opts);else $(el).unblock(opts);
					}, opts.timeout);
					$(el).data('blockUI.timeout', to);
				}
			}
	
			// remove the block
			function remove(el, opts) {
				var count;
				var full = el == window;
				var $el = $(el);
				var data = $el.data('blockUI.history');
				var to = $el.data('blockUI.timeout');
				if (to) {
					clearTimeout(to);
					$el.removeData('blockUI.timeout');
				}
				opts = $.extend({}, $.blockUI.defaults, opts || {});
				bind(0, el, opts); // unbind events
	
				if (opts.onUnblock === null) {
					opts.onUnblock = $el.data('blockUI.onUnblock');
					$el.removeData('blockUI.onUnblock');
				}
	
				var els;
				if (full) // crazy selector to handle odd field errors in ie6/7
					els = $('body').children().filter('.blockUI').add('body > .blockUI');else els = $el.find('>.blockUI');
	
				// fix cursor issue
				if (opts.cursorReset) {
					if (els.length > 1) els[1].style.cursor = opts.cursorReset;
					if (els.length > 2) els[2].style.cursor = opts.cursorReset;
				}
	
				if (full) pageBlock = pageBlockEls = null;
	
				if (opts.fadeOut) {
					count = els.length;
					els.stop().fadeOut(opts.fadeOut, function () {
						if (--count === 0) reset(els, data, opts, el);
					});
				} else reset(els, data, opts, el);
			}
	
			// move blocking element back into the DOM where it started
			function reset(els, data, opts, el) {
				var $el = $(el);
				if ($el.data('blockUI.isBlocked')) return;
	
				els.each(function (i, o) {
					// remove via DOM calls so we don't lose event handlers
					if (this.parentNode) this.parentNode.removeChild(this);
				});
	
				if (data && data.el) {
					data.el.style.display = data.display;
					data.el.style.position = data.position;
					data.el.style.cursor = 'default'; // #59
					if (data.parent) data.parent.appendChild(data.el);
					$el.removeData('blockUI.history');
				}
	
				if ($el.data('blockUI.static')) {
					$el.css('position', 'static'); // #22
				}
	
				if (typeof opts.onUnblock == 'function') opts.onUnblock(el, opts);
	
				// fix issue in Safari 6 where block artifacts remain until reflow
				var body = $(document.body),
				    w = body.width(),
				    cssW = body[0].style.width;
				body.width(w - 1).width(w);
				body[0].style.width = cssW;
			}
	
			// bind/unbind the handler
			function bind(b, el, opts) {
				var full = el == window,
				    $el = $(el);
	
				// don't bother unbinding if there is nothing to unbind
				if (!b && (full && !pageBlock || !full && !$el.data('blockUI.isBlocked'))) return;
	
				$el.data('blockUI.isBlocked', b);
	
				// don't bind events when overlay is not in use or if bindEvents is false
				if (!full || !opts.bindEvents || b && !opts.showOverlay) return;
	
				// bind anchors and inputs for mouse and key events
				var events = 'mousedown mouseup keydown keypress keyup touchstart touchend touchmove';
				if (b) $(document).bind(events, opts, handler);else $(document).unbind(events, handler);
	
				// former impl...
				//		var $e = $('a,:input');
				//		b ? $e.bind(events, opts, handler) : $e.unbind(events, handler);
			}
	
			// event handler to suppress keyboard/mouse events when blocking
			function handler(e) {
				// allow tab navigation (conditionally)
				if (e.type === 'keydown' && e.keyCode && e.keyCode == 9) {
					if (pageBlock && e.data.constrainTabKey) {
						var els = pageBlockEls;
						var fwd = !e.shiftKey && e.target === els[els.length - 1];
						var back = e.shiftKey && e.target === els[0];
						if (fwd || back) {
							setTimeout(function () {
								focus(back);
							}, 10);
							return false;
						}
					}
				}
				var opts = e.data;
				var target = $(e.target);
				if (target.hasClass('blockOverlay') && opts.onOverlayClick) opts.onOverlayClick(e);
	
				// allow events within the message content
				if (target.parents('div.' + opts.blockMsgClass).length > 0) return true;
	
				// allow events for content that is not being blocked
				return target.parents().children().filter('div.blockUI').length === 0;
			}
	
			function focus(back) {
				if (!pageBlockEls) return;
				var e = pageBlockEls[back === true ? pageBlockEls.length - 1 : 0];
				if (e) e.focus();
			}
	
			function center(el, x, y) {
				var p = el.parentNode,
				    s = el.style;
				var l = (p.offsetWidth - el.offsetWidth) / 2 - sz(p, 'borderLeftWidth');
				var t = (p.offsetHeight - el.offsetHeight) / 2 - sz(p, 'borderTopWidth');
				if (x) s.left = l > 0 ? l + 'px' : '0';
				if (y) s.top = t > 0 ? t + 'px' : '0';
			}
	
			function sz(el, p) {
				return parseInt($.css(el, p), 10) || 0;
			}
		}
	
		/*global define:true */
		if ("function" === 'function' && __webpack_require__(1762) && __webpack_require__(1762).jQuery) {
			!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(31)], __WEBPACK_AMD_DEFINE_FACTORY__ = (setup), __WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ? (__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
		} else {
			setup(jQuery);
		}
	})();

/***/ }),

/***/ 1762:
/***/ (function(module, exports) {

	/* WEBPACK VAR INJECTION */(function(__webpack_amd_options__) {module.exports = __webpack_amd_options__;
	
	/* WEBPACK VAR INJECTION */}.call(exports, {}))

/***/ }),

/***/ 1763:
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_RESULT__;'use strict';
	
	!(__WEBPACK_AMD_DEFINE_RESULT__ = function (require) {
	    $ = __webpack_require__(31);
	    __webpack_require__(1367);
	    dash = __webpack_require__(1);
	
	    dash.zmodalAjax = function () {
	        $.blockUI();
	        $.ajax.apply(null, arguments).done(function (data) {
	            $.unblockUI();
	            $('<div></div>').zmodal({ content: data, okButton: { closeOnClick: true }, cancelButton: { disabled: true } });
	        }.bind(this));
	    };
	    return dash;
	}.call(exports, __webpack_require__, exports, module), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ }),

/***/ 1764:
/***/ (function(module, exports, __webpack_require__) {

	var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;'use strict';
	
	var _index = __webpack_require__(1765);
	
	var FormInputController = _interopRequireWildcard(_index);
	
	var _ResponseCache = __webpack_require__(536);
	
	var _ResponseCache2 = _interopRequireDefault(_ResponseCache);
	
	function _interopRequireDefault(obj) {
	    return obj && obj.__esModule ? obj : { default: obj };
	}
	
	function _interopRequireWildcard(obj) {
	    if (obj && obj.__esModule) {
	        return obj;
	    } else {
	        var newObj = {};if (obj != null) {
	            for (var key in obj) {
	                if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key];
	            }
	        }newObj.default = obj;return newObj;
	    }
	}
	
	!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(31)], __WEBPACK_AMD_DEFINE_RESULT__ = function ($) {
	
	    $.fn.showDoctrineGridForRelation = function (type, relationField, owningEntity, relationClass, targetMember, mappedProperty) {
	        if (type === 'normal') {
	            var where = [{
	                field: relationField,
	                operation: '=',
	                value: owningEntity
	            }];
	            $(this).popUpModalDoctrineGrid({
	                modal: { "class": "modal-grid" },
	                createEnabled: true,
	                "class": relationClass,
	                where: where
	            });
	        } else if (type === 'many') {
	            $(this).popUpModalDoctrineGrid({
	                modal: { "class": "modal-grid" },
	                createEnabled: true,
	                "class": relationClass,
	                relationFilter: {
	                    member: targetMember,
	                    mappedProperty: mappedProperty,
	                    id: owningEntity
	                }
	            });
	        }
	    };
	
	    $.fn.doctrineSelectRecord = function (context, caller, id, name) {
	        caller.trigger("recordSelected", [id, name]);
	        context.zbootstrapmodal("hide");
	        context.empty();
	    };
	
	    $.fn.doctrineSelectRecordGrid = function (classname, caller) {
	        var url = "grid.php";
	        var ctx = this;
	        $.post(url, {
	            gridOptions: {
	                'class': classname,
	                'behavior': 'select'
	            }
	        }, function (data, textStatus, XHR) {
	            if (typeof data.success !== 'undefined') {
	                if (data.success) {
	                    ctx.find(".body").html(data.html);
	                    ctx.find('.selectRecordButton').each(function () {
	                        $(this).data("ctx", ctx).data("caller", caller);
	                    });
	                    ctx.zbootstrapmodal();
	                } else {}
	            }
	        }, 'json');
	    };
	
	    function DoctrineForm(obj, options) {
	        if (obj[0] && $.isPlainObject(obj[0])) {
	            this.data = obj[0];
	        } else {
	            this.el = obj;
	            this.$el = $(obj);
	        }
	
	        var defaultOptions = {
	            'class': '',
	            id: 0,
	            behavior: 'create',
	            ajax: true,
	            uid: '',
	            modal: null,
	            shownPromise: null,
	            formUrl: 'form.php',
	            submitUrl: 'formProcessor.php',
	            extraOptions: {},
	            submitEnabled: true
	        };
	
	        this.errorDiv = null;
	        this.blocked = false;
	        this.options = $.extend({}, defaultOptions, options);
	        this.modal = this.options.modal;
	        this.shownPromise = this.options.shownPromise;
	        this.inputs = [];
	        this.init();
	
	        return this;
	    }
	
	    var fn = DoctrineForm.prototype;
	
	    fn.init = function () {
	        if (typeof this.options['class'] !== 'string' || this.options['class'].length === 0) throw "Doctrine Form class not specified.";
	        if (typeof this.options.eventTarget === 'undefined') {
	            this.options.eventTarget = this.$el;
	        }
	
	        switch (this.options.behavior) {
	            case 'create':
	                this.initCreateRecordForm();
	                break;
	            case 'update':
	                this.initUpdateRecordForm();
	                break;
	            case 'copy':
	                this.initCopyRecordForm();
	                break;
	            case 'delete':
	                this.initDeleteRecordForm();
	                break;
	            default:
	                throw "Doctrine form behavior not specified.";
	        }
	    };
	
	    fn.loadStart = function () {
	        this.$el.parent().parent().block({
	            message: '<h1>Loading Form...</h1>',
	            css: { marginLeft: '35%', marginTop: '10%' }
	        });
	    };
	
	    fn.submitStart = function () {
	        this.$el.parent().parent().block({
	            message: '<h1>Submitting Form...</h1>'
	        });
	        for (var i = 0; i < this.inputs.length; i++) {
	            this.inputs[i].preSubmit();
	        }
	    };
	
	    fn.ajaxStop = function () {
	        this.$el.parent().parent().unblock({
	            fadeOut: 80
	        });
	    };
	
	    fn.initCreateRecordForm = function () {
	        this.loadStart();
	        $.ajax(this.options.formUrl, {
	            data: $.extend({}, {
	                'class': this.options['class'],
	                behavior: this.options.behavior,
	                ajax: true
	            }, this.options.extraOptions),
	            type: 'post',
	            dataType: 'json',
	            success: this.doctrineFormSuccess,
	            error: this.doctrineFormError,
	            complete: this.ajaxStop,
	            context: this
	        });
	    };
	
	    fn.initUpdateRecordForm = function () {
	        if (typeof this.options.id === 'undefined' || this.options.id === 0) {
	            throw "Cannot create update form: no id specified.";
	        }
	
	        this.loadStart();
	        $.ajax(this.options.formUrl, {
	            data: $.extend({}, {
	                'class': this.options['class'],
	                id: this.options.id,
	                behavior: this.options.behavior,
	                ajax: true
	            }, this.options.extraOptions),
	            type: 'post',
	            dataType: 'json',
	            success: this.doctrineFormSuccess,
	            error: this.doctrineFormError,
	            complete: this.ajaxStop,
	            context: this
	        });
	    };
	
	    fn.initCopyRecordForm = function () {
	        if (typeof this.options.id === 'undefined' || this.options.id === 0) {
	            throw "Cannot create update form: no id specified.";
	        }
	
	        this.loadStart();
	        $.ajax(this.options.formUrl, {
	            data: $.extend({}, {
	                'class': this.options['class'],
	                id: this.options.id,
	                behavior: this.options.behavior,
	                ajax: true
	            }, this.options.extraOptions),
	            type: 'post',
	            dataType: 'json',
	            success: this.doctrineFormSuccess,
	            error: this.doctrineFormError,
	            complete: this.ajaxStop,
	            context: this
	        });
	    };
	
	    fn.initDeleteRecordForm = function () {
	        dash.dialog.ajaxConfirm("Are you sure you want to delete this record?", $.proxy(this.deleteRecordConfirm, this), $.proxy(function () {
	            this.destroyModal();
	        }, this));
	        //Todo delete
	    };
	
	    fn.deleteRecordConfirm = function () {
	        var url = "form.php";
	        $.ajax(url, {
	            data: {
	                'class': this.options['class'],
	                id: this.options.id,
	                behavior: 'delete',
	                ajax: true
	            },
	            type: 'post',
	            dataType: 'json',
	            context: this
	        }).done(this.doctrineDeleteRecordSuccess).fail(this.doctrineFormError);
	    };
	
	    fn.doctrineDeleteRecordSuccess = function (data, textStatus, xhr) {
	        if (typeof data.success !== 'undefined') {
	            if (data.success) {
	                dash.dialog.showAjaxSuccessPopup("Record deleted.", 1500);
	                this.doctrineRecordsChanged(data);
	            } else {
	                if (typeof data.html !== 'undefined') {
	                    $(this).find(".body").html(data.html);
	                } else if (typeof data.message !== 'undefined') {
	                    this.destroyModal();
	                    dash.dialog.showAjaxError(data.message);
	                } else {
	                    this.destroyModal();
	                    dash.dialog.showAjaxError("An unspecified error occurred while loading the form.");
	                }
	            }
	        } else {
	            this.destroyModal();
	            dash.dialog.showAjaxError("An unspecified error occurred while loading the form.");
	        }
	    };
	
	    fn.submit = function (event) {
	        if (!this.options.submitEnabled) {
	            dash.dialog.showAjaxError("You do not have permission to save this object.");
	            return;
	        }
	        this.submitStart();
	        // Todo validate element objects? (inputs arr)
	        if (event.preventDefault) event.preventDefault();else event.returnValue = false;
	        var url = this.options.submitUrl;
	        // TODO - convert everything to run through the Requester so we can properly remove the cache instead of wiping
	        // all health widget caches since no request id is specified
	        _ResponseCache2.default.clearWithId('getHealthWidgetChart');
	        var method = 'post';
	        $.ajax(url, {
	            data: this.$el.find('form').first().serializeArray(),
	            type: method,
	            dataType: 'json',
	            success: $.proxy(this.submitSuccess, this),
	            complete: this.ajaxStop,
	            context: this
	        });
	    };
	
	    fn.submitSuccess = function (data) {
	        var _this = this;
	
	        if (typeof data.success !== 'undefined') {
	            if (data.success === true) {
	                this.doctrineRecordsChanged(data);
	                this.$el.html('<div class="alert alert-success">' + data.message + '</div>');
	                // add timeout so we can see the success message...
	                setTimeout(function () {
	                    _this.destroyModal();
	                }, 1000);
	            } else {
	                this.repopForm(data);
	            }
	        }
	    };
	
	    fn.destroyModal = function (immediate) {
	        var timeout = 1200;
	        if (typeof immediate !== 'undefined' && immediate === true) timeout = 150;
	        if (this.options.modal !== null) {
	            var modal = this.options.modal;
	            modal.find('.modal-footer').hide();
	            setTimeout(function () {
	                modal.zmodal('destroy');
	            }, timeout);
	        }
	    };
	
	    fn.doctrineRecordsChanged = function (data) {
	        if (typeof data.data !== 'undefined') {
	            entityId = data.data.entityId;
	        } else {
	            entityId = this.options.id;
	        }
	        var target;
	        if (document.body.contains(this.options.eventTarget.get(0))) {
	            target = this.options.eventTarget;
	        } else {
	            target = $(document.body);
	        }
	        target.trigger('recordsChanged.doctrineForm', new DoctrineRecordsChangedEventArgs(this.options['class'], this.options.behavior, entityId));
	    };
	
	    fn.doctrineFormSuccess = function (data, textStatus, xhr) {
	        if (data !== null && typeof data.success !== 'undefined') {
	            if (data.success) {
	                this.repopForm(data);
	            } else {
	                if (typeof data.html !== 'undefined') {
	                    this.repopForm(data);
	                } else if (typeof data.message !== 'undefined') {
	                    this.destroyModal(true);
	                    dash.dialog.showAjaxError(data.message);
	                } else {
	                    this.destroyModal(true);
	                    dash.dialog.showAjaxError("An unspecified error occurred while loading the form.");
	                }
	            }
	        } else {
	            this.destroyModal(true);
	            dash.dialog.showAjaxError("An unspecified error occurred while loading the form.");
	        }
	    };
	
	    fn.repopForm = function (data) {
	        if (typeof data !== 'undefined') {
	            if (typeof data.html !== 'undefined') {
	                if (typeof data.submitEnabled !== 'undefined' && !data.submitEnabled) {
	                    this.disableSubmit();
	                }
	                this.$el.html(data.html);
	                this.$el.find('form').on("submit", $.proxy(this.submit, this));
	                this.$el.find('[data-element-class]').each(function (i, el) {
	                    var elementClass = FormInputController[$(el).data('elementClass')],
	                        elementClassInstance = new elementClass(el, this, {});
	                    this.inputs.push(elementClassInstance);
	                    this.shownPromise.done(function () {
	                        elementClassInstance.postShow(el);
	                    });
	                    $(el).data('elementClassInstance', elementClassInstance);
	                }.bind(this));
	                this.$el.initDoctrineDatepickers();
	
	                if (typeof data.message !== 'undefined') {
	                    this.displayFormError(data.message, true);
	                }
	
	                this.scrollToTop();
	            } else if (typeof data.message !== 'undefined') {
	                var message = "<strong>An error occurred while loading the form.</strong><br>";
	                if (typeof data.line !== 'undefined') {
	                    message += data.file + " line " + data.line + ":<br>";
	                }
	                message += data.message;
	                this.displayFormError(message, true);
	            } else {
	                this.displayFormError("The server did not return a valid response. Please try again. If the problem persists, please contact your system administrator for assistance.");
	            }
	        } else {
	            this.displayFormError("The server did not return a valid response. Please try again. If the problem persists, please contact your system administrator for assistance.");
	        }
	    };
	
	    /**
	     * disableSubmit
	     *
	     * Disables the save button and changes the cancel button to say "close"
	     */
	    fn.disableSubmit = function () {
	        this.options.modal.find('.modal-ok').hide();
	        this.options.modal.find('.modal-cancel').html("Close");
	        this.options.submitEnabled = false;
	    };
	
	    fn.displayFormError = function (message, permanent) {
	        permanent = permanent || false;
	        this.createErrorDiv();
	        this.errorDiv.html(message);
	        this.errorDiv.show();
	        this.scrollToTop();
	        if (!permanent) setTimeout($.proxy(this.removeFormError, this), 5000);
	    };
	
	    fn.removeFormError = function () {
	        if (this.errorDiv !== null) {
	            this.errorDiv.remove();
	            this.errorDiv = null;
	        }
	    };
	
	    fn.createErrorDiv = function () {
	        if (this.errorDiv !== null) {
	            this.errorDiv.remove();
	            this.errorDiv = null;
	        }
	        this.errorDiv = $('<div class="alert alert-danger"></div>');
	        this.errorDiv.hide();
	        var h2 = this.$el.find('h2').first();
	        if (h2.length) {
	            h2.after(this.errorDiv);
	        } else {
	            this.$el.prepend(this.errorDiv);
	        }
	    };
	
	    fn.doctrineFormError = function (xhr, textStatus, errorThrown) {
	        var errorMessage;
	        if (textStatus.length > 0) {
	            switch (textStatus) {
	                case 'timeout':
	                    errorMessage = "The request to the server took too long to complete.";
	                    break;
	                case 'parsererror':
	                    errorMessage = "The server returned a malformed response.";
	                    break;
	                case 'error': //Fallthrough is intended.
	                case 'notmodified':
	                case 'success':
	                    errorMessage = "An unspecified error occurred while loading the form.";
	                    break;
	                default:
	                    errorMessage = "An unspecified error occurred while loading the form.";
	                    break;
	            }
	        } else {
	            errorMessage = "An error occurred while communicating with the server.";
	        }
	
	        dash.dialog.showAjaxError("Form loading failed. " + errorMessage);
	    };
	
	    fn.scrollToTop = function () {
	        if (typeof this.modal !== 'undefined') {
	            this.modal.zbootstrapmodal('scrollToTop');
	        }
	    };
	
	    $.fn.doctrineForm = function (options) {
	        if (this.data('doctrine-form')) {
	            return this.data('doctrine-form');
	        }
	
	        var form = new DoctrineForm(this, arguments[0]);
	        this.data('doctrine-form', form);
	        return form;
	    };
	
	    /**
	     * popUpModalDoctrineForm
	     *
	     * This creates a modal form at the document level, having the object it is called on as the target of its events.
	     * Currently only the recordsChanged event is fired into the calling object.
	     * @param options
	     */
	    $.fn.popUpModalDoctrineForm = function (options) {
	        //Make sure options exists
	        if (typeof options === 'undefined') options = {};
	        //Set the event target as the dom object this func is called on
	        options.eventTarget = this;
	
	        // PLAT-1220 workaround for admin forms not displaying validations. Default is to auto-close the modal upon
	        // save, thus the modal is removed from the DOM and unable to display validation. Workaround is to set all non
	        // view/report paths to not autoclose on save.
	        var shouldClose = true;
	        if (window.location.pathname.indexOf('view') < 0 && window.location.pathname.indexOf('report') < 0) {
	            shouldClose = false;
	        }
	        //Defaults
	        var modalDefaultOptions = {
	            okButton: {
	                text: "Save",
	                closeOnClick: shouldClose
	            },
	            enterPressesOk: true
	        };
	        if (typeof options.modal === 'undefined') {
	            options.modal = modalDefaultOptions;
	        } else {
	            options.modal = $.extend({}, modalDefaultOptions, options.modal);
	        }
	
	        if (options.uid) $('#modal-doctrine-form-' + options.uid).remove(); // Cleanup potentially already-existing modal
	
	        //Build a container that will be populated with the modal stuff
	        var outerContainer = $('<div></div>');
	        outerContainer.attr('id', "doctrine-form-" + options.uid);
	        //Init the modal
	
	        var deferred = $.Deferred();
	        outerContainer.on('shown.zmodal', function () {
	            deferred.resolve();
	        });
	        if (typeof options.behavior !== 'undefined' && options.behavior === 'delete') options.modal.show = false;
	        if (typeof options.modal !== 'undefined') {
	            outerContainer.zmodal(options.modal);
	        } else {
	            outerContainer.zmodal();
	        }
	        //Show the modal
	        //outerContainer.zmodal("show");
	        outerContainer.attr('id', 'modal-' + outerContainer.attr('id'));
	        var container = $('<div class="doctrine-form-container"></div>');
	        outerContainer.find('.modal-body').first().append(container);
	        outerContainer.find('.modal-body').first().css('min-height', '250px');
	        options.modal = outerContainer;
	        options.shownPromise = deferred.promise();
	        //Init the form
	        var form = container.doctrineForm(options);
	        outerContainer.on('ok.zmodal', $.proxy(form.submit, form));
	    };
	
	    function AssociationMultiSelectElement(obj, options) {
	        if (obj[0] && $.isPlainObject(obj[0])) {
	            this.data = obj[0];
	        } else {
	            this.el = obj;
	            this.$el = $(obj);
	        }
	
	        var defaultOptions = {
	            id: ''
	        };
	
	        //Initialize vars
	        this.members = [];
	        this.membersInput = null;
	        this.select = null;
	        this.id = null;
	
	        this.options = $.extend({}, defaultOptions, options);
	        this.init();
	
	        return this;
	    }
	
	    fn = AssociationMultiSelectElement.prototype;
	
	    fn.init = function () {
	        this.id = this.options.id;
	        this.membersInput = this.$el.find('.members').first();
	        if (this.membersInput.val().length > 0) this.members = this.membersInput.val().split(',');
	        this.select = this.$el.find('select').first();
	        this.$el.find('#button_select_' + this.id).on('recordSelected.doctrineGrid', $.proxy(this.onRecordSelected, this));
	    };
	
	    fn.resetSelectSize = function () {
	        var options = this.select.find('option');
	        this.select.attr("size", Math.max(2, options.length));
	    };
	
	    fn.updateMembersInput = function () {
	        this.membersInput.val(this.members.join(','));
	    };
	
	    fn.onRecordSelected = function (event, entityId, name) {
	        event.stopPropagation();
	        entityId = entityId.toString();
	
	        var member = this.members.indexOf(entityId);
	        if (member > -1) {
	            //Already exists...
	            return;
	        }
	        this.members.push(entityId);
	        this.updateMembersInput();
	        this.select.append('<option value="' + entityId + '">' + name + '</option>');
	        this.resetSelectSize();
	    };
	
	    fn.onRecordRemoveButtonClick = function (event) {
	        event.preventDefault();
	
	        var selections = this.select.find(':selected');
	        var ctx = this;
	        selections.each(function (index, option) {
	            var id = $(option).val();
	            var member = ctx.members.indexOf(id);
	            if (member === -1) {
	                //Tried to remove something that doesn't exist...
	                return;
	            }
	            //If its in the member list
	            ctx.members.splice(member, 1);
	            ctx.updateMembersInput();
	            ctx.select.find('option[value=' + id + ']').remove();
	            ctx.membersInput.val(ctx.members.join(','));
	        });
	        this.resetSelectSize();
	    };
	
	    fn.moveSelectionUp = function (event) {
	        event.preventDefault();
	
	        var selection = this.select.find(':selected').first();
	        if (selection.length === 0) {
	            //Nothing selected
	            return;
	        }
	        var selectedId = selection.val();
	        var index = this.members.indexOf(selectedId);
	        if (index === -1) {
	            return; //NOT FOUND, this is bad!
	        }
	        if (index === 0) {
	            //If this is the first item in the list, we can't move it up!
	            return;
	        }
	
	        var previous = selection.prev();
	        selection.remove();
	        selection.insertBefore(previous);
	        this.members.splice(index - 1, 0, this.members.splice(index, 1)[0]);
	        this.updateMembersInput();
	    };
	
	    fn.moveSelectionDown = function (event) {
	        event.preventDefault();
	
	        var selection = this.select.find(':selected').first();
	        if (selection.length === 0) {
	            //Nothing selected
	            return;
	        }
	        var selectedId = selection.val();
	        var index = this.members.indexOf(selectedId);
	        if (index === -1) {
	            return; //NOT FOUND, this is bad!
	        }
	        if (index === this.members.length - 1) {
	            //If this is the last item in the list, we can't move it down!
	            return;
	        }
	
	        var next = selection.next();
	        selection.remove();
	        selection.insertAfter(next);
	        this.members.splice(index + 1, 0, this.members.splice(index, 1)[0]);
	        this.updateMembersInput();
	    };
	
	    fn.editSelection = function (event, entityClass) {
	        event.preventDefault();
	
	        var selection = this.select.find(':selected').first();
	        if (selection.length === 0) {
	            //Nothing selected
	            return;
	        }
	        var selectedId = selection.val();
	        var options = {
	            'class': entityClass,
	            id: selectedId,
	            behavior: 'update'
	        };
	        $(event.currentTarget).popUpModalDoctrineForm(options);
	    };
	
	    $.fn.associationMultiSelect = function (options) {
	        if (this.data('association-multi-select')) {
	            return this.data('association-multi-select');
	        }
	
	        var select = new AssociationMultiSelectElement(this, arguments[0]);
	        this.data('association-multi-select', select);
	        return select;
	    };
	
	    function AssociationSelectElement(obj, options) {
	        if (obj[0] && $.isPlainObject(obj[0])) {
	            this.data = obj[0];
	        } else {
	            this.el = obj;
	            this.$el = $(obj);
	        }
	
	        var defaultOptions = {};
	
	        //Initialize vars
	        this.select = null;
	        this.editButton = null;
	
	        this.options = $.extend({}, defaultOptions, options);
	        this.init();
	
	        return this;
	    }
	
	    fn = AssociationSelectElement.prototype;
	
	    fn.init = function () {
	        this.select = this.$el.find('select').first();
	        this.editButton = this.$el.find('.edit-button').first();
	        this.select.on("change", $.proxy(this.selectionChanged, this));
	        this.editButton.on('recordsChanged.doctrineForm', $.proxy(this.onRecordsChanged, this));
	        this.editButton.on('click', $.proxy(this.editSelection, this));
	        this.selectionChanged();
	    };
	
	    fn.onRecordsChanged = function () {};
	
	    fn.selectionChanged = function (event) {
	        var selected = this.select.find(':selected').first();
	        if (typeof selected === 'undefined') {
	            return;
	        }
	        if (selected.data("editable") === true) {
	            this.editButton.show();
	        } else {
	            this.editButton.hide();
	        }
	    };
	
	    fn.editSelection = function (event) {
	        event.preventDefault();
	
	        var selection = this.select.find(':selected').first();
	        if (selection.length === 0) {
	            //Nothing selected
	            return;
	        }
	        var selectedId = selection.val();
	        var editable = selection.data("editable") === true;
	        if (!editable) {
	            return;
	        }
	        var entityClass = $(event.currentTarget).data("entity-class");
	
	        var options = {
	            'class': entityClass,
	            id: selectedId,
	            behavior: 'update'
	        };
	        $(event.currentTarget).popUpModalDoctrineForm(options);
	    };
	
	    $.fn.associationSelect = function (options) {
	        if (this.data('association-select')) {
	            return this.data('association-select');
	        }
	
	        var select = new AssociationSelectElement(this, arguments[0]);
	        this.data('association-select', select);
	        return select;
	    };
	
	    window.showDoctrineGridForRelation = function (event) {
	        //TODO refactor this to event handler and util function
	        var that = event.currentTarget;
	        var select = $(that).parent().parent().find("select").first();
	        var selected = select.find(":selected").first();
	        var type = selected.data('target-type');
	        var relationField = select.val();
	        var owningEntity = $(that).data("entity-id");
	        var relationClass = selected.data("target-entity");
	        var targetMember = selected.data("target-member");
	        var mappedProperty = selected.data("target-mapped-property");
	        $(that).showDoctrineGridForRelation(type, relationField, owningEntity, relationClass, targetMember, mappedProperty);
	    };
	
	    window.DoctrineRecordsChangedEventArgs = function (entityClass, behavior, entityId) {
	        this.entityClass = entityClass;
	        this.behavior = behavior;
	        this.entityId = entityId;
	    };
	}.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__), __WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ }),

/***/ 1765:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	  value: true
	});
	
	var _alertFiltersInput = __webpack_require__(1766);
	
	Object.defineProperty(exports, 'alertFiltersInput', {
	  enumerable: true,
	  get: function get() {
	    return _interopRequireDefault(_alertFiltersInput).default;
	  }
	});
	
	var _defaultElement = __webpack_require__(1768);
	
	Object.defineProperty(exports, 'defaultElement', {
	  enumerable: true,
	  get: function get() {
	    return _interopRequireDefault(_defaultElement).default;
	  }
	});
	
	var _formInput = __webpack_require__(1767);
	
	Object.defineProperty(exports, 'formInput', {
	  enumerable: true,
	  get: function get() {
	    return _interopRequireDefault(_formInput).default;
	  }
	});
	
	var _markdownInput = __webpack_require__(1769);
	
	Object.defineProperty(exports, 'markdownInput', {
	  enumerable: true,
	  get: function get() {
	    return _interopRequireDefault(_markdownInput).default;
	  }
	});
	
	var _pivotTableInput = __webpack_require__(1773);
	
	Object.defineProperty(exports, 'pivotTableInput', {
	  enumerable: true,
	  get: function get() {
	    return _interopRequireDefault(_pivotTableInput).default;
	  }
	});

	function _interopRequireDefault(obj) {
	  return obj && obj.__esModule ? obj : { default: obj };
	}

/***/ }),

/***/ 1766:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	var _react = __webpack_require__(34);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _reactDom = __webpack_require__(77);
	
	var _reactDom2 = _interopRequireDefault(_reactDom);
	
	var _lodash = __webpack_require__(491);
	
	var _lodash2 = _interopRequireDefault(_lodash);
	
	var _jquery = __webpack_require__(31);
	
	var _jquery2 = _interopRequireDefault(_jquery);
	
	var _KeyMeasureDatasetFilters = __webpack_require__(1378);
	
	var _KeyMeasureDatasetFilters2 = _interopRequireDefault(_KeyMeasureDatasetFilters);
	
	var _KeyMeasure = __webpack_require__(935);
	
	var _KeyMeasure2 = _interopRequireDefault(_KeyMeasure);
	
	var _KeyMeasureDatasetFilter = __webpack_require__(915);
	
	var _KeyMeasureDatasetFilter2 = _interopRequireDefault(_KeyMeasureDatasetFilter);
	
	var _KeyMeasureDataset = __webpack_require__(939);
	
	var _KeyMeasureDataset2 = _interopRequireDefault(_KeyMeasureDataset);
	
	var _dashHelper = __webpack_require__(904);
	
	var _dashHelper2 = _interopRequireDefault(_dashHelper);
	
	function _interopRequireDefault(obj) {
	    return obj && obj.__esModule ? obj : { default: obj };
	}
	
	// import KeyMeasureModel from '~/models/KeyMeasureModel';
	var EntityHelper = _dashHelper2.default.helper.entity;
	
	// import KeyMeasureDatasetFilters from '~/dash/component/KeyMeasureDatasetFilters.old';
	
	
	var formInput = __webpack_require__(1767);
	var KMDM = __webpack_require__(940);
	
	module.exports = formInput.createFormInput({
	
	    postShow: function postShow() {
	        var _this = this;
	
	        this.formId = this.$element.data('formId');
	        this.kmId = parseInt(this.$element.parent().find('#' + this.formId + '_key_measure').val(), 10);
	        this.dataset = this.$element.data('keyMeasureDataset');
	        this.hiddenId = this.$element.data('hiddenId');
	        // console.log('formID: '+JSON.stringify(this.formId));
	        // console.log('kmId: '+JSON.stringify(this.kmId));
	        // console.log('dataset: '+JSON.stringify(this.dataset));
	        // console.log('hiddenId: '+JSON.stringify(this.hiddenId));
	
	        // Workaround for PLAT-977 and forms magic: Don't look up key measures without ids.
	        if (!this.kmId && !this.dataset) {
	            return;
	        }
	
	        if (_lodash2.default.isNull(this.dataset)) {
	            this.dataset = {
	                keyMeasure: this.kmId,
	                groupByDateRange: false
	            };
	        }
	        var filters = this.dataset.dimensionFilters;
	        this.dataset = new KMDM(this.dataset);
	
	        var currentDateRange = getPageData('currentDateRange');
	
	        this.dataset.setDateRanges([currentDateRange.id]);
	        this.keyMeasureDataset = {};
	        this.onUpdate = function (filter) {
	            // console.log(filter);
	            if (filter.countExclude() > 0) {
	                var excludeFilter = filter.clone();
	                excludeFilter.includeFilters = {};
	                this.keyMeasureDataset.dimensionFilters.setExcludeFilter(filter.dimensionId, undefined, excludeFilter);
	            } else {
	                delete this.keyMeasureDataset.dimensionFilters.excludeFilters[filter.dimensionId];
	            }
	            if (filter.countInclude() > 0) {
	                var includeFilter = filter.clone();
	                includeFilter.excludeFilters = {};
	                this.keyMeasureDataset.dimensionFilters.setIncludeFilter(filter.dimensionId, undefined, includeFilter);
	            } else {
	                delete this.keyMeasureDataset.dimensionFilters.includeFilters[filter.dimensionId];
	            }
	            // console.log(this.keyMeasureDataset);
	        };
	        this.onUpdate = this.onUpdate.bind(this);
	
	        // var filtersContainer = this.$element.find('.filters-container').first();
	
	        EntityHelper.getKeyMeasure(this.dataset.keyMeasure).done(function (kmData) {
	            _KeyMeasure2.default.fromServer(kmData).then(function (model) {
	                var filter = _KeyMeasureDatasetFilter2.default.fromServer(model, filters);
	                _KeyMeasureDataset2.default.factory({
	                    keyMeasure: model.id,
	                    dateRanges: [currentDateRange],
	                    dimensionFilters: filter
	                }, model).then(function (dataset) {
	                    // console.log(dataset);
	                    _this.keyMeasureDataset = dataset;
	                    var options = {
	                        keyMeasureDataset: dataset,
	                        onUpdate: _this.onUpdate
	                    };
	
	                    _reactDom2.default.render(_react2.default.createFactory(_KeyMeasureDatasetFilters2.default)(options), _this.$element.find('.filters-container')[0]);
	                });
	            });
	            // const options = {
	            //     model: this.dataset,
	            //     keyMeasure: new KeyMeasureModel(kmData),
	            // };
	            //
	            // ReactDOM.render(
	            //     React.createFactory(KeyMeasureDatasetFilters)(options),
	            //     this.$element.find('.filters-container')[0]
	            // );
	        });
	    },
	
	    preSubmit: function preSubmit() {
	        var payload = void 0;
	        if (this.keyMeasureDataset) {
	            payload = this.keyMeasureDataset.toServerJSON();
	        }
	        (0, _jquery2.default)('#' + this.hiddenId).val(JSON.stringify(payload));
	    }
	
	});

/***/ }),

/***/ 1767:
/***/ (function(module, exports) {

	'use strict';
	
	var FormInput = function FormInput(el, form, options) {
	    this.element = el;
	    this.$element = $(el);
	    this.form = form;
	    this.options = options;
	    this.init();
	    this.preShow(el);
	    this.attach(el);
	};
	
	var fn = FormInput.prototype;
	
	fn.init = function () {};
	
	fn.preShow = function (el) {};
	fn.attach = function (el) {};
	fn.postShow = function (el) {};
	
	fn.preSubmit = function () {};
	
	module.exports = {
	    createFormInput: function createFormInput(functions) {
	        var fun = function fun(el, form, options) {
	            FormInput.apply(this, arguments);
	        };
	        fun.prototype = Object.create(FormInput.prototype);
	
	        // Mixin
	        for (var f in functions) {
	            if (functions.hasOwnProperty(f) && typeof functions[f] === 'function') {
	                fun.prototype[f] = functions[f];
	            }
	        }
	        return fun;
	    }
	};

/***/ }),

/***/ 1768:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	/**
	 * Created by William Schaller on 7/23/2015.
	 */
	
	var formInput = __webpack_require__(1767);
	module.exports = formInput.createFormInput({});

/***/ }),

/***/ 1769:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	var formInput = __webpack_require__(1767),
	    Markdown = __webpack_require__(1770);
	
	__webpack_require__(1771);
	__webpack_require__(1772);
	
	module.exports = formInput.createFormInput({
	    postShow: function postShow() {
	        this.formId = this.$element.data('formId');
	        this.inputId = this.$element.data('inputId');
	        this.converter = new Markdown.Converter();
	        Markdown.Extra.init(this.converter);
	        this.editor = new Markdown.Editor(this.converter, '-' + this.inputId);
	        this.editor.run();
	    }
	});

/***/ }),

/***/ 1770:
/***/ (function(module, exports) {

	"use strict";
	
	var Markdown = {};
	module.exports = Markdown;
	
	// The following text is included for historical reasons, but should
	// be taken with a pinch of salt; it's not all true anymore.
	
	//
	// Wherever possible, Showdown is a straight, line-by-line port
	// of the Perl version of Markdown.
	//
	// This is not a normal parser design; it's basically just a
	// series of string substitutions.  It's hard to read and
	// maintain this way,  but keeping Showdown close to the original
	// design makes it easier to port new features.
	//
	// More importantly, Showdown behaves like markdown.pl in most
	// edge cases.  So web applications can do client-side preview
	// in Javascript, and then build identical HTML on the server.
	//
	// This port needs the new RegExp functionality of ECMA 262,
	// 3rd Edition (i.e. Javascript 1.5).  Most modern web browsers
	// should do fine.  Even with the new regular expression features,
	// We do a lot of work to emulate Perl's regex functionality.
	// The tricky changes in this file mostly have the "attacklab:"
	// label.  Major or self-explanatory changes don't.
	//
	// Smart diff tools like Araxis Merge will be able to match up
	// this file with markdown.pl in a useful way.  A little tweaking
	// helps: in a copy of markdown.pl, replace "#" with "//" and
	// replace "$text" with "text".  Be sure to ignore whitespace
	// and line endings.
	//
	
	
	//
	// Usage:
	//
	//   var text = "Markdown *rocks*.";
	//
	//   var converter = new Markdown.Converter();
	//   var html = converter.makeHtml(text);
	//
	//   alert(html);
	//
	// Note: move the sample code to the bottom of this
	// file before uncommenting it.
	//
	
	(function () {
	
	    function identity(x) {
	        return x;
	    }
	    function returnFalse(x) {
	        return false;
	    }
	
	    function HookCollection() {}
	
	    HookCollection.prototype = {
	
	        chain: function chain(hookname, func) {
	            var original = this[hookname];
	            if (!original) throw new Error("unknown hook " + hookname);
	
	            if (original === identity) this[hookname] = func;else this[hookname] = function (text) {
	                var args = Array.prototype.slice.call(arguments, 0);
	                args[0] = original.apply(null, args);
	                return func.apply(null, args);
	            };
	        },
	        set: function set(hookname, func) {
	            if (!this[hookname]) throw new Error("unknown hook " + hookname);
	            this[hookname] = func;
	        },
	        addNoop: function addNoop(hookname) {
	            this[hookname] = identity;
	        },
	        addFalse: function addFalse(hookname) {
	            this[hookname] = returnFalse;
	        }
	    };
	
	    Markdown.HookCollection = HookCollection;
	
	    // g_urls and g_titles allow arbitrary user-entered strings as keys. This
	    // caused an exception (and hence stopped the rendering) when the user entered
	    // e.g. [push] or [__proto__]. Adding a prefix to the actual key prevents this
	    // (since no builtin property starts with "s_"). See
	    // http://meta.stackoverflow.com/questions/64655/strange-wmd-bug
	    // (granted, switching from Array() to Object() alone would have left only __proto__
	    // to be a problem)
	    function SaveHash() {}
	    SaveHash.prototype = {
	        set: function set(key, value) {
	            this["s_" + key] = value;
	        },
	        get: function get(key) {
	            return this["s_" + key];
	        }
	    };
	
	    Markdown.Converter = function () {
	        var pluginHooks = this.hooks = new HookCollection();
	
	        // given a URL that was encountered by itself (without markup), should return the link text that's to be given to this link
	        pluginHooks.addNoop("plainLinkText");
	
	        // called with the orignal text as given to makeHtml. The result of this plugin hook is the actual markdown source that will be cooked
	        pluginHooks.addNoop("preConversion");
	
	        // called with the text once all normalizations have been completed (tabs to spaces, line endings, etc.), but before any conversions have
	        pluginHooks.addNoop("postNormalization");
	
	        // Called with the text before / after creating block elements like code blocks and lists. Note that this is called recursively
	        // with inner content, e.g. it's called with the full text, and then only with the content of a blockquote. The inner
	        // call will receive outdented text.
	        pluginHooks.addNoop("preBlockGamut");
	        pluginHooks.addNoop("postBlockGamut");
	
	        // called with the text of a single block element before / after the span-level conversions (bold, code spans, etc.) have been made
	        pluginHooks.addNoop("preSpanGamut");
	        pluginHooks.addNoop("postSpanGamut");
	
	        // called with the final cooked HTML code. The result of this plugin hook is the actual output of makeHtml
	        pluginHooks.addNoop("postConversion");
	
	        //
	        // Private state of the converter instance:
	        //
	
	        // Global hashes, used by various utility routines
	        var g_urls;
	        var g_titles;
	        var g_html_blocks;
	
	        // Used to track when we're inside an ordered or unordered list
	        // (see _ProcessListItems() for details):
	        var g_list_level;
	
	        this.makeHtml = function (text) {
	
	            //
	            // Main function. The order in which other subs are called here is
	            // essential. Link and image substitutions need to happen before
	            // _EscapeSpecialCharsWithinTagAttributes(), so that any *'s or _'s in the <a>
	            // and <img> tags get encoded.
	            //
	
	            // This will only happen if makeHtml on the same converter instance is called from a plugin hook.
	            // Don't do that.
	            if (g_urls) throw new Error("Recursive call to converter.makeHtml");
	
	            // Create the private state objects.
	            g_urls = new SaveHash();
	            g_titles = new SaveHash();
	            g_html_blocks = [];
	            g_list_level = 0;
	
	            text = pluginHooks.preConversion(text);
	
	            // attacklab: Replace ~ with ~T
	            // This lets us use tilde as an escape char to avoid md5 hashes
	            // The choice of character is arbitray; anything that isn't
	            // magic in Markdown will work.
	            text = text.replace(/~/g, "~T");
	
	            // attacklab: Replace $ with ~D
	            // RegExp interprets $ as a special character
	            // when it's in a replacement string
	            text = text.replace(/\$/g, "~D");
	
	            // Standardize line endings
	            text = text.replace(/\r\n/g, "\n"); // DOS to Unix
	            text = text.replace(/\r/g, "\n"); // Mac to Unix
	
	            // Make sure text begins and ends with a couple of newlines:
	            text = "\n\n" + text + "\n\n";
	
	            // Convert all tabs to spaces.
	            text = _Detab(text);
	
	            // Strip any lines consisting only of spaces and tabs.
	            // This makes subsequent regexen easier to write, because we can
	            // match consecutive blank lines with /\n+/ instead of something
	            // contorted like /[ \t]*\n+/ .
	            text = text.replace(/^[ \t]+$/mg, "");
	
	            text = pluginHooks.postNormalization(text);
	
	            // Turn block-level HTML blocks into hash entries
	            text = _HashHTMLBlocks(text);
	
	            // Strip link definitions, store in hashes.
	            text = _StripLinkDefinitions(text);
	
	            text = _RunBlockGamut(text);
	
	            text = _UnescapeSpecialChars(text);
	
	            // attacklab: Restore dollar signs
	            text = text.replace(/~D/g, "$$");
	
	            // attacklab: Restore tildes
	            text = text.replace(/~T/g, "~");
	
	            text = pluginHooks.postConversion(text);
	
	            g_html_blocks = g_titles = g_urls = null;
	
	            return text;
	        };
	
	        function _StripLinkDefinitions(text) {
	            //
	            // Strips link definitions from text, stores the URLs and titles in
	            // hash references.
	            //
	
	            // Link defs are in the form: ^[id]: url "optional title"
	
	            /*
	            text = text.replace(/
	                ^[ ]{0,3}\[(.+)\]:  // id = $1  attacklab: g_tab_width - 1
	                [ \t]*
	                \n?                 // maybe *one* newline
	                [ \t]*
	                <?(\S+?)>?          // url = $2
	                (?=\s|$)            // lookahead for whitespace instead of the lookbehind removed below
	                [ \t]*
	                \n?                 // maybe one newline
	                [ \t]*
	                (                   // (potential) title = $3
	                    (\n*)           // any lines skipped = $4 attacklab: lookbehind removed
	                    [ \t]+
	                    ["(]
	                    (.+?)           // title = $5
	                    [")]
	                    [ \t]*
	                )?                  // title is optional
	                (?:\n+|$)
	            /gm, function(){...});
	            */
	
	            text = text.replace(/^[ ]{0,3}\[(.+)\]:[ \t]*\n?[ \t]*<?(\S+?)>?(?=\s|$)[ \t]*\n?[ \t]*((\n*)["(](.+?)[")][ \t]*)?(?:\n+)/gm, function (wholeMatch, m1, m2, m3, m4, m5) {
	                m1 = m1.toLowerCase();
	                g_urls.set(m1, _EncodeAmpsAndAngles(m2)); // Link IDs are case-insensitive
	                if (m4) {
	                    // Oops, found blank lines, so it's not a title.
	                    // Put back the parenthetical statement we stole.
	                    return m3;
	                } else if (m5) {
	                    g_titles.set(m1, m5.replace(/"/g, "&quot;"));
	                }
	
	                // Completely remove the definition from the text
	                return "";
	            });
	
	            return text;
	        }
	
	        function _HashHTMLBlocks(text) {
	
	            // Hashify HTML blocks:
	            // We only want to do this for block-level HTML tags, such as headers,
	            // lists, and tables. That's because we still want to wrap <p>s around
	            // "paragraphs" that are wrapped in non-block-level tags, such as anchors,
	            // phrase emphasis, and spans. The list of tags we're looking for is
	            // hard-coded:
	            var block_tags_a = "p|div|h[1-6]|blockquote|pre|table|dl|ol|ul|script|noscript|form|fieldset|iframe|math|ins|del";
	            var block_tags_b = "p|div|h[1-6]|blockquote|pre|table|dl|ol|ul|script|noscript|form|fieldset|iframe|math";
	
	            // First, look for nested blocks, e.g.:
	            //   <div>
	            //     <div>
	            //     tags for inner block must be indented.
	            //     </div>
	            //   </div>
	            //
	            // The outermost tags must start at the left margin for this to match, and
	            // the inner nested divs must be indented.
	            // We need to do this before the next, more liberal match, because the next
	            // match will start at the first `<div>` and stop at the first `</div>`.
	
	            // attacklab: This regex can be expensive when it fails.
	
	            /*
	            text = text.replace(/
	                (                       // save in $1
	                    ^                   // start of line  (with /m)
	                    <($block_tags_a)    // start tag = $2
	                    \b                  // word break
	                                        // attacklab: hack around khtml/pcre bug...
	                    [^\r]*?\n           // any number of lines, minimally matching
	                    </\2>               // the matching end tag
	                    [ \t]*              // trailing spaces/tabs
	                    (?=\n+)             // followed by a newline
	                )                       // attacklab: there are sentinel newlines at end of document
	            /gm,function(){...}};
	            */
	            text = text.replace(/^(<(p|div|h[1-6]|blockquote|pre|table|dl|ol|ul|script|noscript|form|fieldset|iframe|math|ins|del)\b[^\r]*?\n<\/\2>[ \t]*(?=\n+))/gm, hashElement);
	
	            //
	            // Now match more liberally, simply from `\n<tag>` to `</tag>\n`
	            //
	
	            /*
	            text = text.replace(/
	                (                       // save in $1
	                    ^                   // start of line  (with /m)
	                    <($block_tags_b)    // start tag = $2
	                    \b                  // word break
	                                        // attacklab: hack around khtml/pcre bug...
	                    [^\r]*?             // any number of lines, minimally matching
	                    .*</\2>             // the matching end tag
	                    [ \t]*              // trailing spaces/tabs
	                    (?=\n+)             // followed by a newline
	                )                       // attacklab: there are sentinel newlines at end of document
	            /gm,function(){...}};
	            */
	            text = text.replace(/^(<(p|div|h[1-6]|blockquote|pre|table|dl|ol|ul|script|noscript|form|fieldset|iframe|math)\b[^\r]*?.*<\/\2>[ \t]*(?=\n+)\n)/gm, hashElement);
	
	            // Special case just for <hr />. It was easier to make a special case than
	            // to make the other regex more complicated.  
	
	            /*
	            text = text.replace(/
	                \n                  // Starting after a blank line
	                [ ]{0,3}
	                (                   // save in $1
	                    (<(hr)          // start tag = $2
	                        \b          // word break
	                        ([^<>])*?
	                    \/?>)           // the matching end tag
	                    [ \t]*
	                    (?=\n{2,})      // followed by a blank line
	                )
	            /g,hashElement);
	            */
	            text = text.replace(/\n[ ]{0,3}((<(hr)\b([^<>])*?\/?>)[ \t]*(?=\n{2,}))/g, hashElement);
	
	            // Special case for standalone HTML comments:
	
	            /*
	            text = text.replace(/
	                \n\n                                            // Starting after a blank line
	                [ ]{0,3}                                        // attacklab: g_tab_width - 1
	                (                                               // save in $1
	                    <!
	                    (--(?:|(?:[^>-]|-[^>])(?:[^-]|-[^-])*)--)   // see http://www.w3.org/TR/html-markup/syntax.html#comments and http://meta.stackoverflow.com/q/95256
	                    >
	                    [ \t]*
	                    (?=\n{2,})                                  // followed by a blank line
	                )
	            /g,hashElement);
	            */
	            text = text.replace(/\n\n[ ]{0,3}(<!(--(?:|(?:[^>-]|-[^>])(?:[^-]|-[^-])*)--)>[ \t]*(?=\n{2,}))/g, hashElement);
	
	            // PHP and ASP-style processor instructions (<?...?> and <%...%>)
	
	            /*
	            text = text.replace(/
	                (?:
	                    \n\n            // Starting after a blank line
	                )
	                (                   // save in $1
	                    [ ]{0,3}        // attacklab: g_tab_width - 1
	                    (?:
	                        <([?%])     // $2
	                        [^\r]*?
	                        \2>
	                    )
	                    [ \t]*
	                    (?=\n{2,})      // followed by a blank line
	                )
	            /g,hashElement);
	            */
	            text = text.replace(/(?:\n\n)([ ]{0,3}(?:<([?%])[^\r]*?\2>)[ \t]*(?=\n{2,}))/g, hashElement);
	
	            return text;
	        }
	
	        function hashElement(wholeMatch, m1) {
	            var blockText = m1;
	
	            // Undo double lines
	            blockText = blockText.replace(/^\n+/, "");
	
	            // strip trailing blank lines
	            blockText = blockText.replace(/\n+$/g, "");
	
	            // Replace the element text with a marker ("~KxK" where x is its key)
	            blockText = "\n\n~K" + (g_html_blocks.push(blockText) - 1) + "K\n\n";
	
	            return blockText;
	        }
	
	        var blockGamutHookCallback = function blockGamutHookCallback(t) {
	            return _RunBlockGamut(t);
	        };
	
	        function _RunBlockGamut(text, doNotUnhash) {
	            //
	            // These are all the transformations that form block-level
	            // tags like paragraphs, headers, and list items.
	            //
	
	            text = pluginHooks.preBlockGamut(text, blockGamutHookCallback);
	
	            text = _DoHeaders(text);
	
	            // Do Horizontal Rules:
	            var replacement = "<hr />\n";
	            text = text.replace(/^[ ]{0,2}([ ]?\*[ ]?){3,}[ \t]*$/gm, replacement);
	            text = text.replace(/^[ ]{0,2}([ ]?-[ ]?){3,}[ \t]*$/gm, replacement);
	            text = text.replace(/^[ ]{0,2}([ ]?_[ ]?){3,}[ \t]*$/gm, replacement);
	
	            text = _DoLists(text);
	            text = _DoCodeBlocks(text);
	            text = _DoBlockQuotes(text);
	
	            text = pluginHooks.postBlockGamut(text, blockGamutHookCallback);
	
	            // We already ran _HashHTMLBlocks() before, in Markdown(), but that
	            // was to escape raw HTML in the original Markdown source. This time,
	            // we're escaping the markup we've just created, so that we don't wrap
	            // <p> tags around block-level tags.
	            text = _HashHTMLBlocks(text);
	            text = _FormParagraphs(text, doNotUnhash);
	
	            return text;
	        }
	
	        function _RunSpanGamut(text) {
	            //
	            // These are all the transformations that occur *within* block-level
	            // tags like paragraphs, headers, and list items.
	            //
	
	            text = pluginHooks.preSpanGamut(text);
	
	            text = _DoCodeSpans(text);
	            text = _EscapeSpecialCharsWithinTagAttributes(text);
	            text = _EncodeBackslashEscapes(text);
	
	            // Process anchor and image tags. Images must come first,
	            // because ![foo][f] looks like an anchor.
	            text = _DoImages(text);
	            text = _DoAnchors(text);
	
	            // Make links out of things like `<http://example.com/>`
	            // Must come after _DoAnchors(), because you can use < and >
	            // delimiters in inline links like [this](<url>).
	            text = _DoAutoLinks(text);
	
	            text = text.replace(/~P/g, "://"); // put in place to prevent autolinking; reset now
	
	            text = _EncodeAmpsAndAngles(text);
	            text = _DoItalicsAndBold(text);
	
	            // Do hard breaks:
	            text = text.replace(/  +\n/g, " <br>\n");
	
	            text = pluginHooks.postSpanGamut(text);
	
	            return text;
	        }
	
	        function _EscapeSpecialCharsWithinTagAttributes(text) {
	            //
	            // Within tags -- meaning between < and > -- encode [\ ` * _] so they
	            // don't conflict with their use in Markdown for code, italics and strong.
	            //
	
	            // Build a regex to find HTML tags and comments.  See Friedl's 
	            // "Mastering Regular Expressions", 2nd Ed., pp. 200-201.
	
	            // SE: changed the comment part of the regex
	
	            var regex = /(<[a-z\/!$]("[^"]*"|'[^']*'|[^'">])*>|<!(--(?:|(?:[^>-]|-[^>])(?:[^-]|-[^-])*)--)>)/gi;
	
	            text = text.replace(regex, function (wholeMatch) {
	                var tag = wholeMatch.replace(/(.)<\/?code>(?=.)/g, "$1`");
	                tag = escapeCharacters(tag, wholeMatch.charAt(1) == "!" ? "\\`*_/" : "\\`*_"); // also escape slashes in comments to prevent autolinking there -- http://meta.stackoverflow.com/questions/95987
	                return tag;
	            });
	
	            return text;
	        }
	
	        function _DoAnchors(text) {
	            //
	            // Turn Markdown link shortcuts into XHTML <a> tags.
	            //
	            //
	            // First, handle reference-style links: [link text] [id]
	            //
	
	            /*
	            text = text.replace(/
	                (                           // wrap whole match in $1
	                    \[
	                    (
	                        (?:
	                            \[[^\]]*\]      // allow brackets nested one level
	                            |
	                            [^\[]           // or anything else
	                        )*
	                    )
	                    \]
	                     [ ]?                    // one optional space
	                    (?:\n[ ]*)?             // one optional newline followed by spaces
	                     \[
	                    (.*?)                   // id = $3
	                    \]
	                )
	                ()()()()                    // pad remaining backreferences
	            /g, writeAnchorTag);
	            */
	            text = text.replace(/(\[((?:\[[^\]]*\]|[^\[\]])*)\][ ]?(?:\n[ ]*)?\[(.*?)\])()()()()/g, writeAnchorTag);
	
	            //
	            // Next, inline-style links: [link text](url "optional title")
	            //
	
	            /*
	            text = text.replace(/
	                (                           // wrap whole match in $1
	                    \[
	                    (
	                        (?:
	                            \[[^\]]*\]      // allow brackets nested one level
	                            |
	                            [^\[\]]         // or anything else
	                        )*
	                    )
	                    \]
	                    \(                      // literal paren
	                    [ \t]*
	                    ()                      // no id, so leave $3 empty
	                    <?(                     // href = $4
	                        (?:
	                            \([^)]*\)       // allow one level of (correctly nested) parens (think MSDN)
	                            |
	                            [^()\s]
	                        )*?
	                    )>?                
	                    [ \t]*
	                    (                       // $5
	                        (['"])              // quote char = $6
	                        (.*?)               // Title = $7
	                        \6                  // matching quote
	                        [ \t]*              // ignore any spaces/tabs between closing quote and )
	                    )?                      // title is optional
	                    \)
	                )
	            /g, writeAnchorTag);
	            */
	
	            text = text.replace(/(\[((?:\[[^\]]*\]|[^\[\]])*)\]\([ \t]*()<?((?:\([^)]*\)|[^()\s])*?)>?[ \t]*((['"])(.*?)\6[ \t]*)?\))/g, writeAnchorTag);
	
	            //
	            // Last, handle reference-style shortcuts: [link text]
	            // These must come last in case you've also got [link test][1]
	            // or [link test](/foo)
	            //
	
	            /*
	            text = text.replace(/
	                (                   // wrap whole match in $1
	                    \[
	                    ([^\[\]]+)      // link text = $2; can't contain '[' or ']'
	                    \]
	                )
	                ()()()()()          // pad rest of backreferences
	            /g, writeAnchorTag);
	            */
	            text = text.replace(/(\[([^\[\]]+)\])()()()()()/g, writeAnchorTag);
	
	            return text;
	        }
	
	        function writeAnchorTag(wholeMatch, m1, m2, m3, m4, m5, m6, m7) {
	            if (m7 == undefined) m7 = "";
	            var whole_match = m1;
	            var link_text = m2.replace(/:\/\//g, "~P"); // to prevent auto-linking withing the link. will be converted back after the auto-linker runs
	            var link_id = m3.toLowerCase();
	            var url = m4;
	            var title = m7;
	
	            if (url == "") {
	                if (link_id == "") {
	                    // lower-case and turn embedded newlines into spaces
	                    link_id = link_text.toLowerCase().replace(/ ?\n/g, " ");
	                }
	                url = "#" + link_id;
	
	                if (g_urls.get(link_id) != undefined) {
	                    url = g_urls.get(link_id);
	                    if (g_titles.get(link_id) != undefined) {
	                        title = g_titles.get(link_id);
	                    }
	                } else {
	                    if (whole_match.search(/\(\s*\)$/m) > -1) {
	                        // Special case for explicit empty url
	                        url = "";
	                    } else {
	                        return whole_match;
	                    }
	                }
	            }
	            url = encodeProblemUrlChars(url);
	            url = escapeCharacters(url, "*_");
	            var result = "<a href=\"" + url + "\"";
	
	            if (title != "") {
	                title = attributeEncode(title);
	                title = escapeCharacters(title, "*_");
	                result += " title=\"" + title + "\"";
	            }
	
	            result += ">" + link_text + "</a>";
	
	            return result;
	        }
	
	        function _DoImages(text) {
	            //
	            // Turn Markdown image shortcuts into <img> tags.
	            //
	
	            //
	            // First, handle reference-style labeled images: ![alt text][id]
	            //
	
	            /*
	            text = text.replace(/
	                (                   // wrap whole match in $1
	                    !\[
	                    (.*?)           // alt text = $2
	                    \]
	                     [ ]?            // one optional space
	                    (?:\n[ ]*)?     // one optional newline followed by spaces
	                     \[
	                    (.*?)           // id = $3
	                    \]
	                )
	                ()()()()            // pad rest of backreferences
	            /g, writeImageTag);
	            */
	            text = text.replace(/(!\[(.*?)\][ ]?(?:\n[ ]*)?\[(.*?)\])()()()()/g, writeImageTag);
	
	            //
	            // Next, handle inline images:  ![alt text](url "optional title")
	            // Don't forget: encode * and _
	
	            /*
	            text = text.replace(/
	                (                   // wrap whole match in $1
	                    !\[
	                    (.*?)           // alt text = $2
	                    \]
	                    \s?             // One optional whitespace character
	                    \(              // literal paren
	                    [ \t]*
	                    ()              // no id, so leave $3 empty
	                    <?(\S+?)>?      // src url = $4
	                    [ \t]*
	                    (               // $5
	                        (['"])      // quote char = $6
	                        (.*?)       // title = $7
	                        \6          // matching quote
	                        [ \t]*
	                    )?              // title is optional
	                    \)
	                )
	            /g, writeImageTag);
	            */
	            text = text.replace(/(!\[(.*?)\]\s?\([ \t]*()<?(\S+?)>?[ \t]*((['"])(.*?)\6[ \t]*)?\))/g, writeImageTag);
	
	            return text;
	        }
	
	        function attributeEncode(text) {
	            // unconditionally replace angle brackets here -- what ends up in an attribute (e.g. alt or title)
	            // never makes sense to have verbatim HTML in it (and the sanitizer would totally break it)
	            return text.replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;");
	        }
	
	        function writeImageTag(wholeMatch, m1, m2, m3, m4, m5, m6, m7) {
	            var whole_match = m1;
	            var alt_text = m2;
	            var link_id = m3.toLowerCase();
	            var url = m4;
	            var title = m7;
	
	            if (!title) title = "";
	
	            if (url == "") {
	                if (link_id == "") {
	                    // lower-case and turn embedded newlines into spaces
	                    link_id = alt_text.toLowerCase().replace(/ ?\n/g, " ");
	                }
	                url = "#" + link_id;
	
	                if (g_urls.get(link_id) != undefined) {
	                    url = g_urls.get(link_id);
	                    if (g_titles.get(link_id) != undefined) {
	                        title = g_titles.get(link_id);
	                    }
	                } else {
	                    return whole_match;
	                }
	            }
	
	            alt_text = escapeCharacters(attributeEncode(alt_text), "*_[]()");
	            url = escapeCharacters(url, "*_");
	            var result = "<img src=\"" + url + "\" alt=\"" + alt_text + "\"";
	
	            // attacklab: Markdown.pl adds empty title attributes to images.
	            // Replicate this bug.
	
	            //if (title != "") {
	            title = attributeEncode(title);
	            title = escapeCharacters(title, "*_");
	            result += " title=\"" + title + "\"";
	            //}
	
	            result += " />";
	
	            return result;
	        }
	
	        function _DoHeaders(text) {
	
	            // Setext-style headers:
	            //  Header 1
	            //  ========
	            //  
	            //  Header 2
	            //  --------
	            //
	            text = text.replace(/^(.+)[ \t]*\n=+[ \t]*\n+/gm, function (wholeMatch, m1) {
	                return "<h1>" + _RunSpanGamut(m1) + "</h1>\n\n";
	            });
	
	            text = text.replace(/^(.+)[ \t]*\n-+[ \t]*\n+/gm, function (matchFound, m1) {
	                return "<h2>" + _RunSpanGamut(m1) + "</h2>\n\n";
	            });
	
	            // atx-style headers:
	            //  # Header 1
	            //  ## Header 2
	            //  ## Header 2 with closing hashes ##
	            //  ...
	            //  ###### Header 6
	            //
	
	            /*
	            text = text.replace(/
	                ^(\#{1,6})      // $1 = string of #'s
	                [ \t]*
	                (.+?)           // $2 = Header text
	                [ \t]*
	                \#*             // optional closing #'s (not counted)
	                \n+
	            /gm, function() {...});
	            */
	
	            text = text.replace(/^(\#{1,6})[ \t]*(.+?)[ \t]*\#*\n+/gm, function (wholeMatch, m1, m2) {
	                var h_level = m1.length;
	                return "<h" + h_level + ">" + _RunSpanGamut(m2) + "</h" + h_level + ">\n\n";
	            });
	
	            return text;
	        }
	
	        function _DoLists(text, isInsideParagraphlessListItem) {
	            //
	            // Form HTML ordered (numbered) and unordered (bulleted) lists.
	            //
	
	            // attacklab: add sentinel to hack around khtml/safari bug:
	            // http://bugs.webkit.org/show_bug.cgi?id=11231
	            text += "~0";
	
	            // Re-usable pattern to match any entirel ul or ol list:
	
	            /*
	            var whole_list = /
	                (                                   // $1 = whole list
	                    (                               // $2
	                        [ ]{0,3}                    // attacklab: g_tab_width - 1
	                        ([*+-]|\d+[.])              // $3 = first list item marker
	                        [ \t]+
	                    )
	                    [^\r]+?
	                    (                               // $4
	                        ~0                          // sentinel for workaround; should be $
	                        |
	                        \n{2,}
	                        (?=\S)
	                        (?!                         // Negative lookahead for another list item marker
	                            [ \t]*
	                            (?:[*+-]|\d+[.])[ \t]+
	                        )
	                    )
	                )
	            /g
	            */
	            var whole_list = /^(([ ]{0,3}([*+-]|\d+[.])[ \t]+)[^\r]+?(~0|\n{2,}(?=\S)(?![ \t]*(?:[*+-]|\d+[.])[ \t]+)))/gm;
	
	            if (g_list_level) {
	                text = text.replace(whole_list, function (wholeMatch, m1, m2) {
	                    var list = m1;
	                    var list_type = m2.search(/[*+-]/g) > -1 ? "ul" : "ol";
	
	                    var result = _ProcessListItems(list, list_type, isInsideParagraphlessListItem);
	
	                    // Trim any trailing whitespace, to put the closing `</$list_type>`
	                    // up on the preceding line, to get it past the current stupid
	                    // HTML block parser. This is a hack to work around the terrible
	                    // hack that is the HTML block parser.
	                    result = result.replace(/\s+$/, "");
	                    result = "<" + list_type + ">" + result + "</" + list_type + ">\n";
	                    return result;
	                });
	            } else {
	                whole_list = /(\n\n|^\n?)(([ ]{0,3}([*+-]|\d+[.])[ \t]+)[^\r]+?(~0|\n{2,}(?=\S)(?![ \t]*(?:[*+-]|\d+[.])[ \t]+)))/g;
	                text = text.replace(whole_list, function (wholeMatch, m1, m2, m3) {
	                    var runup = m1;
	                    var list = m2;
	
	                    var list_type = m3.search(/[*+-]/g) > -1 ? "ul" : "ol";
	                    var result = _ProcessListItems(list, list_type);
	                    result = runup + "<" + list_type + ">\n" + result + "</" + list_type + ">\n";
	                    return result;
	                });
	            }
	
	            // attacklab: strip sentinel
	            text = text.replace(/~0/, "");
	
	            return text;
	        }
	
	        var _listItemMarkers = { ol: "\\d+[.]", ul: "[*+-]" };
	
	        function _ProcessListItems(list_str, list_type, isInsideParagraphlessListItem) {
	            //
	            //  Process the contents of a single ordered or unordered list, splitting it
	            //  into individual list items.
	            //
	            //  list_type is either "ul" or "ol".
	
	            // The $g_list_level global keeps track of when we're inside a list.
	            // Each time we enter a list, we increment it; when we leave a list,
	            // we decrement. If it's zero, we're not in a list anymore.
	            //
	            // We do this because when we're not inside a list, we want to treat
	            // something like this:
	            //
	            //    I recommend upgrading to version
	            //    8. Oops, now this line is treated
	            //    as a sub-list.
	            //
	            // As a single paragraph, despite the fact that the second line starts
	            // with a digit-period-space sequence.
	            //
	            // Whereas when we're inside a list (or sub-list), that line will be
	            // treated as the start of a sub-list. What a kludge, huh? This is
	            // an aspect of Markdown's syntax that's hard to parse perfectly
	            // without resorting to mind-reading. Perhaps the solution is to
	            // change the syntax rules such that sub-lists must start with a
	            // starting cardinal number; e.g. "1." or "a.".
	
	            g_list_level++;
	
	            // trim trailing blank lines:
	            list_str = list_str.replace(/\n{2,}$/, "\n");
	
	            // attacklab: add sentinel to emulate \z
	            list_str += "~0";
	
	            // In the original attacklab showdown, list_type was not given to this function, and anything
	            // that matched /[*+-]|\d+[.]/ would just create the next <li>, causing this mismatch:
	            //
	            //  Markdown          rendered by WMD        rendered by MarkdownSharp
	            //  ------------------------------------------------------------------
	            //  1. first          1. first               1. first
	            //  2. second         2. second              2. second
	            //  - third           3. third                   * third
	            //
	            // We changed this to behave identical to MarkdownSharp. This is the constructed RegEx,
	            // with {MARKER} being one of \d+[.] or [*+-], depending on list_type:
	
	            /*
	            list_str = list_str.replace(/
	                (^[ \t]*)                       // leading whitespace = $1
	                ({MARKER}) [ \t]+               // list marker = $2
	                ([^\r]+?                        // list item text   = $3
	                    (\n+)
	                )
	                (?=
	                    (~0 | \2 ({MARKER}) [ \t]+)
	                )
	            /gm, function(){...});
	            */
	
	            var marker = _listItemMarkers[list_type];
	            var re = new RegExp("(^[ \\t]*)(" + marker + ")[ \\t]+([^\\r]+?(\\n+))(?=(~0|\\1(" + marker + ")[ \\t]+))", "gm");
	            var last_item_had_a_double_newline = false;
	            list_str = list_str.replace(re, function (wholeMatch, m1, m2, m3) {
	                var item = m3;
	                var leading_space = m1;
	                var ends_with_double_newline = /\n\n$/.test(item);
	                var contains_double_newline = ends_with_double_newline || item.search(/\n{2,}/) > -1;
	
	                if (contains_double_newline || last_item_had_a_double_newline) {
	                    item = _RunBlockGamut(_Outdent(item), /* doNotUnhash = */true);
	                } else {
	                    // Recursion for sub-lists:
	                    item = _DoLists(_Outdent(item), /* isInsideParagraphlessListItem= */true);
	                    item = item.replace(/\n$/, ""); // chomp(item)
	                    if (!isInsideParagraphlessListItem) // only the outer-most item should run this, otherwise it's run multiple times for the inner ones
	                        item = _RunSpanGamut(item);
	                }
	                last_item_had_a_double_newline = ends_with_double_newline;
	                return "<li>" + item + "</li>\n";
	            });
	
	            // attacklab: strip sentinel
	            list_str = list_str.replace(/~0/g, "");
	
	            g_list_level--;
	            return list_str;
	        }
	
	        function _DoCodeBlocks(text) {
	            //
	            //  Process Markdown `<pre><code>` blocks.
	            //  
	
	            /*
	            text = text.replace(/
	                (?:\n\n|^)
	                (                               // $1 = the code block -- one or more lines, starting with a space/tab
	                    (?:
	                        (?:[ ]{4}|\t)           // Lines must start with a tab or a tab-width of spaces - attacklab: g_tab_width
	                        .*\n+
	                    )+
	                )
	                (\n*[ ]{0,3}[^ \t\n]|(?=~0))    // attacklab: g_tab_width
	            /g ,function(){...});
	            */
	
	            // attacklab: sentinel workarounds for lack of \A and \Z, safari\khtml bug
	            text += "~0";
	
	            text = text.replace(/(?:\n\n|^\n?)((?:(?:[ ]{4}|\t).*\n+)+)(\n*[ ]{0,3}[^ \t\n]|(?=~0))/g, function (wholeMatch, m1, m2) {
	                var codeblock = m1;
	                var nextChar = m2;
	
	                codeblock = _EncodeCode(_Outdent(codeblock));
	                codeblock = _Detab(codeblock);
	                codeblock = codeblock.replace(/^\n+/g, ""); // trim leading newlines
	                codeblock = codeblock.replace(/\n+$/g, ""); // trim trailing whitespace
	
	                codeblock = "<pre><code>" + codeblock + "\n</code></pre>";
	
	                return "\n\n" + codeblock + "\n\n" + nextChar;
	            });
	
	            // attacklab: strip sentinel
	            text = text.replace(/~0/, "");
	
	            return text;
	        }
	
	        function hashBlock(text) {
	            text = text.replace(/(^\n+|\n+$)/g, "");
	            return "\n\n~K" + (g_html_blocks.push(text) - 1) + "K\n\n";
	        }
	
	        function _DoCodeSpans(text) {
	            //
	            // * Backtick quotes are used for <code></code> spans.
	            // 
	            // * You can use multiple backticks as the delimiters if you want to
	            //   include literal backticks in the code span. So, this input:
	            //     
	            //      Just type ``foo `bar` baz`` at the prompt.
	            //     
	            //   Will translate to:
	            //     
	            //      <p>Just type <code>foo `bar` baz</code> at the prompt.</p>
	            //     
	            //   There's no arbitrary limit to the number of backticks you
	            //   can use as delimters. If you need three consecutive backticks
	            //   in your code, use four for delimiters, etc.
	            //
	            // * You can use spaces to get literal backticks at the edges:
	            //     
	            //      ... type `` `bar` `` ...
	            //     
	            //   Turns to:
	            //     
	            //      ... type <code>`bar`</code> ...
	            //
	
	            /*
	            text = text.replace(/
	                (^|[^\\])       // Character before opening ` can't be a backslash
	                (`+)            // $2 = Opening run of `
	                (               // $3 = The code block
	                    [^\r]*?
	                    [^`]        // attacklab: work around lack of lookbehind
	                )
	                \2              // Matching closer
	                (?!`)
	            /gm, function(){...});
	            */
	
	            text = text.replace(/(^|[^\\])(`+)([^\r]*?[^`])\2(?!`)/gm, function (wholeMatch, m1, m2, m3, m4) {
	                var c = m3;
	                c = c.replace(/^([ \t]*)/g, ""); // leading whitespace
	                c = c.replace(/[ \t]*$/g, ""); // trailing whitespace
	                c = _EncodeCode(c);
	                c = c.replace(/:\/\//g, "~P"); // to prevent auto-linking. Not necessary in code *blocks*, but in code spans. Will be converted back after the auto-linker runs.
	                return m1 + "<code>" + c + "</code>";
	            });
	
	            return text;
	        }
	
	        function _EncodeCode(text) {
	            //
	            // Encode/escape certain characters inside Markdown code runs.
	            // The point is that in code, these characters are literals,
	            // and lose their special Markdown meanings.
	            //
	            // Encode all ampersands; HTML entities are not
	            // entities within a Markdown code span.
	            text = text.replace(/&/g, "&amp;");
	
	            // Do the angle bracket song and dance:
	            text = text.replace(/</g, "&lt;");
	            text = text.replace(/>/g, "&gt;");
	
	            // Now, escape characters that are magic in Markdown:
	            text = escapeCharacters(text, "\*_{}[]\\", false);
	
	            // jj the line above breaks this:
	            //---
	
	            //* Item
	
	            //   1. Subitem
	
	            //            special char: *
	            //---
	
	            return text;
	        }
	
	        function _DoItalicsAndBold(text) {
	
	            // <strong> must go first:
	            text = text.replace(/([\W_]|^)(\*\*|__)(?=\S)([^\r]*?\S[\*_]*)\2([\W_]|$)/g, "$1<strong>$3</strong>$4");
	
	            text = text.replace(/([\W_]|^)(\*|_)(?=\S)([^\r\*_]*?\S)\2([\W_]|$)/g, "$1<em>$3</em>$4");
	
	            return text;
	        }
	
	        function _DoBlockQuotes(text) {
	
	            /*
	            text = text.replace(/
	                (                           // Wrap whole match in $1
	                    (
	                        ^[ \t]*>[ \t]?      // '>' at the start of a line
	                        .+\n                // rest of the first line
	                        (.+\n)*             // subsequent consecutive lines
	                        \n*                 // blanks
	                    )+
	                )
	            /gm, function(){...});
	            */
	
	            text = text.replace(/((^[ \t]*>[ \t]?.+\n(.+\n)*\n*)+)/gm, function (wholeMatch, m1) {
	                var bq = m1;
	
	                // attacklab: hack around Konqueror 3.5.4 bug:
	                // "----------bug".replace(/^-/g,"") == "bug"
	
	                bq = bq.replace(/^[ \t]*>[ \t]?/gm, "~0"); // trim one level of quoting
	
	                // attacklab: clean up hack
	                bq = bq.replace(/~0/g, "");
	
	                bq = bq.replace(/^[ \t]+$/gm, ""); // trim whitespace-only lines
	                bq = _RunBlockGamut(bq); // recurse
	
	                bq = bq.replace(/(^|\n)/g, "$1  ");
	                // These leading spaces screw with <pre> content, so we need to fix that:
	                bq = bq.replace(/(\s*<pre>[^\r]+?<\/pre>)/gm, function (wholeMatch, m1) {
	                    var pre = m1;
	                    // attacklab: hack around Konqueror 3.5.4 bug:
	                    pre = pre.replace(/^  /mg, "~0");
	                    pre = pre.replace(/~0/g, "");
	                    return pre;
	                });
	
	                return hashBlock("<blockquote>\n" + bq + "\n</blockquote>");
	            });
	            return text;
	        }
	
	        function _FormParagraphs(text, doNotUnhash) {
	            //
	            //  Params:
	            //    $text - string to process with html <p> tags
	            //
	
	            // Strip leading and trailing lines:
	            text = text.replace(/^\n+/g, "");
	            text = text.replace(/\n+$/g, "");
	
	            var grafs = text.split(/\n{2,}/g);
	            var grafsOut = [];
	
	            var markerRe = /~K(\d+)K/;
	
	            //
	            // Wrap <p> tags.
	            //
	            var end = grafs.length;
	            for (var i = 0; i < end; i++) {
	                var str = grafs[i];
	
	                // if this is an HTML marker, copy it
	                if (markerRe.test(str)) {
	                    grafsOut.push(str);
	                } else if (/\S/.test(str)) {
	                    str = _RunSpanGamut(str);
	                    str = str.replace(/^([ \t]*)/g, "<p>");
	                    str += "</p>";
	                    grafsOut.push(str);
	                }
	            }
	            //
	            // Unhashify HTML blocks
	            //
	            if (!doNotUnhash) {
	                end = grafsOut.length;
	                for (var i = 0; i < end; i++) {
	                    var foundAny = true;
	                    while (foundAny) {
	                        // we may need several runs, since the data may be nested
	                        foundAny = false;
	                        grafsOut[i] = grafsOut[i].replace(/~K(\d+)K/g, function (wholeMatch, id) {
	                            foundAny = true;
	                            return g_html_blocks[id];
	                        });
	                    }
	                }
	            }
	            return grafsOut.join("\n\n");
	        }
	
	        function _EncodeAmpsAndAngles(text) {
	            // Smart processing for ampersands and angle brackets that need to be encoded.
	
	            // Ampersand-encoding based entirely on Nat Irons's Amputator MT plugin:
	            //   http://bumppo.net/projects/amputator/
	            text = text.replace(/&(?!#?[xX]?(?:[0-9a-fA-F]+|\w+);)/g, "&amp;");
	
	            // Encode naked <'s
	            text = text.replace(/<(?![a-z\/?!]|~D)/gi, "&lt;");
	
	            return text;
	        }
	
	        function _EncodeBackslashEscapes(text) {
	            //
	            //   Parameter:  String.
	            //   Returns:    The string, with after processing the following backslash
	            //               escape sequences.
	            //
	
	            // attacklab: The polite way to do this is with the new
	            // escapeCharacters() function:
	            //
	            //     text = escapeCharacters(text,"\\",true);
	            //     text = escapeCharacters(text,"`*_{}[]()>#+-.!",true);
	            //
	            // ...but we're sidestepping its use of the (slow) RegExp constructor
	            // as an optimization for Firefox.  This function gets called a LOT.
	
	            text = text.replace(/\\(\\)/g, escapeCharacters_callback);
	            text = text.replace(/\\([`*_{}\[\]()>#+-.!])/g, escapeCharacters_callback);
	            return text;
	        }
	
	        var charInsideUrl = "[-A-Z0-9+&@#/%?=~_|[\\]()!:,.;]",
	            charEndingUrl = "[-A-Z0-9+&@#/%=~_|[\\])]",
	            autoLinkRegex = new RegExp("(=\"|<)?\\b(https?|ftp)(://" + charInsideUrl + "*" + charEndingUrl + ")(?=$|\\W)", "gi"),
	            endCharRegex = new RegExp(charEndingUrl, "i");
	
	        function handleTrailingParens(wholeMatch, lookbehind, protocol, link) {
	            if (lookbehind) return wholeMatch;
	            if (link.charAt(link.length - 1) !== ")") return "<" + protocol + link + ">";
	            var parens = link.match(/[()]/g);
	            var level = 0;
	            for (var i = 0; i < parens.length; i++) {
	                if (parens[i] === "(") {
	                    if (level <= 0) level = 1;else level++;
	                } else {
	                    level--;
	                }
	            }
	            var tail = "";
	            if (level < 0) {
	                var re = new RegExp("\\){1," + -level + "}$");
	                link = link.replace(re, function (trailingParens) {
	                    tail = trailingParens;
	                    return "";
	                });
	            }
	            if (tail) {
	                var lastChar = link.charAt(link.length - 1);
	                if (!endCharRegex.test(lastChar)) {
	                    tail = lastChar + tail;
	                    link = link.substr(0, link.length - 1);
	                }
	            }
	            return "<" + protocol + link + ">" + tail;
	        }
	
	        function _DoAutoLinks(text) {
	
	            // note that at this point, all other URL in the text are already hyperlinked as <a href=""></a>
	            // *except* for the <http://www.foo.com> case
	
	            // automatically add < and > around unadorned raw hyperlinks
	            // must be preceded by a non-word character (and not by =" or <) and followed by non-word/EOF character
	            // simulating the lookbehind in a consuming way is okay here, since a URL can neither and with a " nor
	            // with a <, so there is no risk of overlapping matches.
	            text = text.replace(autoLinkRegex, handleTrailingParens);
	
	            //  autolink anything like <http://example.com>
	
	            var replacer = function replacer(wholematch, m1) {
	                return "<a href=\"" + m1 + "\">" + pluginHooks.plainLinkText(m1) + "</a>";
	            };
	            text = text.replace(/<((https?|ftp):[^'">\s]+)>/gi, replacer);
	
	            // Email addresses: <address@domain.foo>
	            /*
	            text = text.replace(/
	                <
	                (?:mailto:)?
	                (
	                    [-.\w]+
	                    \@
	                    [-a-z0-9]+(\.[-a-z0-9]+)*\.[a-z]+
	                )
	                >
	            /gi, _DoAutoLinks_callback());
	            */
	
	            /* disabling email autolinking, since we don't do that on the server, either
	            text = text.replace(/<(?:mailto:)?([-.\w]+\@[-a-z0-9]+(\.[-a-z0-9]+)*\.[a-z]+)>/gi,
	                function(wholeMatch,m1) {
	                    return _EncodeEmailAddress( _UnescapeSpecialChars(m1) );
	                }
	            );
	            */
	            return text;
	        }
	
	        function _UnescapeSpecialChars(text) {
	            //
	            // Swap back in all the special characters we've hidden.
	            //
	            text = text.replace(/~E(\d+)E/g, function (wholeMatch, m1) {
	                var charCodeToReplace = parseInt(m1);
	                return String.fromCharCode(charCodeToReplace);
	            });
	            return text;
	        }
	
	        function _Outdent(text) {
	            //
	            // Remove one level of line-leading tabs or spaces
	            //
	
	            // attacklab: hack around Konqueror 3.5.4 bug:
	            // "----------bug".replace(/^-/g,"") == "bug"
	
	            text = text.replace(/^(\t|[ ]{1,4})/gm, "~0"); // attacklab: g_tab_width
	
	            // attacklab: clean up hack
	            text = text.replace(/~0/g, "");
	
	            return text;
	        }
	
	        function _Detab(text) {
	            if (!/\t/.test(text)) return text;
	
	            var spaces = ["    ", "   ", "  ", " "],
	                skew = 0,
	                v;
	
	            return text.replace(/[\n\t]/g, function (match, offset) {
	                if (match === "\n") {
	                    skew = offset + 1;
	                    return match;
	                }
	                v = (offset - skew) % 4;
	                skew = offset + 1;
	                return spaces[v];
	            });
	        }
	
	        //
	        //  attacklab: Utility functions
	        //
	
	        var _problemUrlChars = /(?:["'*()[\]:]|~D)/g;
	
	        // hex-encodes some unusual "problem" chars in URLs to avoid URL detection problems 
	        function encodeProblemUrlChars(url) {
	            if (!url) return "";
	
	            var len = url.length;
	
	            return url.replace(_problemUrlChars, function (match, offset) {
	                if (match == "~D") // escape for dollar
	                    return "%24";
	                if (match == ":") {
	                    if (offset == len - 1 || /[0-9\/]/.test(url.charAt(offset + 1))) return ":";
	                }
	                return "%" + match.charCodeAt(0).toString(16);
	            });
	        }
	
	        function escapeCharacters(text, charsToEscape, afterBackslash) {
	            // First we have to escape the escape characters so that
	            // we can build a character class out of them
	            var regexString = "([" + charsToEscape.replace(/([\[\]\\])/g, "\\$1") + "])";
	
	            if (afterBackslash) {
	                regexString = "\\\\" + regexString;
	            }
	
	            var regex = new RegExp(regexString, "g");
	            text = text.replace(regex, escapeCharacters_callback);
	
	            return text;
	        }
	
	        function escapeCharacters_callback(wholeMatch, m1) {
	            var charCodeToEscape = m1.charCodeAt(0);
	            return "~E" + charCodeToEscape + "E";
	        }
	    }; // end of the Markdown.Converter constructor
	})();

/***/ }),

/***/ 1771:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	var Markdown = __webpack_require__(1770);
	(function () {
	  // A quick way to make sure we're only keeping span-level tags when we need to.
	  // This isn't supposed to be foolproof. It's just a quick way to make sure we
	  // keep all span-level tags returned by a pagedown converter. It should allow
	  // all span-level tags through, with or without attributes.
	  var inlineTags = new RegExp(['^(<\\/?(a|abbr|acronym|applet|area|b|basefont|', 'bdo|big|button|cite|code|del|dfn|em|figcaption|', 'font|i|iframe|img|input|ins|kbd|label|map|', 'mark|meter|object|param|progress|q|ruby|rp|rt|s|', 'samp|script|select|small|span|strike|strong|', 'sub|sup|textarea|time|tt|u|var|wbr)[^>]*>|', '<(br)\\s?\\/?>)$'].join(''), 'i');
	
	  /******************************************************************
	   * Utility Functions                                              *
	   *****************************************************************/
	
	  // patch for ie7
	  if (!Array.indexOf) {
	    Array.prototype.indexOf = function (obj) {
	      for (var i = 0; i < this.length; i++) {
	        if (this[i] == obj) {
	          return i;
	        }
	      }
	      return -1;
	    };
	  }
	
	  function trim(str) {
	    return str.replace(/^\s+|\s+$/g, '');
	  }
	
	  function rtrim(str) {
	    return str.replace(/\s+$/g, '');
	  }
	
	  // Remove one level of indentation from text. Indent is 4 spaces.
	  function outdent(text) {
	    return text.replace(new RegExp('^(\\t|[ ]{1,4})', 'gm'), '');
	  }
	
	  function contains(str, substr) {
	    return str.indexOf(substr) != -1;
	  }
	
	  // Sanitize html, removing tags that aren't in the whitelist
	  function sanitizeHtml(html, whitelist) {
	    return html.replace(/<[^>]*>?/gi, function (tag) {
	      return tag.match(whitelist) ? tag : '';
	    });
	  }
	
	  // Merge two arrays, keeping only unique elements.
	  function union(x, y) {
	    var obj = {};
	    for (var i = 0; i < x.length; i++) {
	      obj[x[i]] = x[i];
	    }for (i = 0; i < y.length; i++) {
	      obj[y[i]] = y[i];
	    }var res = [];
	    for (var k in obj) {
	      if (obj.hasOwnProperty(k)) res.push(obj[k]);
	    }
	    return res;
	  }
	
	  // JS regexes don't support \A or \Z, so we add sentinels, as Pagedown
	  // does. In this case, we add the ascii codes for start of text (STX) and
	  // end of text (ETX), an idea borrowed from:
	  // https://github.com/tanakahisateru/js-markdown-extra
	  function addAnchors(text) {
	    if (text.charAt(0) != '\x02') text = '\x02' + text;
	    if (text.charAt(text.length - 1) != '\x03') text = text + '\x03';
	    return text;
	  }
	
	  // Remove STX and ETX sentinels.
	  function removeAnchors(text) {
	    if (text.charAt(0) == '\x02') text = text.substr(1);
	    if (text.charAt(text.length - 1) == '\x03') text = text.substr(0, text.length - 1);
	    return text;
	  }
	
	  // Convert markdown within an element, retaining only span-level tags
	  function convertSpans(text, extra) {
	    return sanitizeHtml(convertAll(text, extra), inlineTags);
	  }
	
	  // Convert internal markdown using the stock pagedown converter
	  function convertAll(text, extra) {
	    var result = extra.blockGamutHookCallback(text);
	    // We need to perform these operations since we skip the steps in the converter
	    result = unescapeSpecialChars(result);
	    result = result.replace(/~D/g, "$$").replace(/~T/g, "~");
	    result = extra.previousPostConversion(result);
	    return result;
	  }
	
	  // Convert escaped special characters
	  function processEscapesStep1(text) {
	    // Markdown extra adds two escapable characters, `:` and `|`
	    return text.replace(/\\\|/g, '~I').replace(/\\:/g, '~i');
	  }
	  function processEscapesStep2(text) {
	    return text.replace(/~I/g, '|').replace(/~i/g, ':');
	  }
	
	  // Duplicated from PageDown converter
	  function unescapeSpecialChars(text) {
	    // Swap back in all the special characters we've hidden.
	    text = text.replace(/~E(\d+)E/g, function (wholeMatch, m1) {
	      var charCodeToReplace = parseInt(m1);
	      return String.fromCharCode(charCodeToReplace);
	    });
	    return text;
	  }
	
	  function slugify(text) {
	    return text.toLowerCase().replace(/\s+/g, '-') // Replace spaces with -
	    .replace(/[^\w\-]+/g, '') // Remove all non-word chars
	    .replace(/\-\-+/g, '-') // Replace multiple - with single -
	    .replace(/^-+/, '') // Trim - from start of text
	    .replace(/-+$/, ''); // Trim - from end of text
	  }
	
	  /*****************************************************************************
	   * Markdown.Extra *
	   ****************************************************************************/
	
	  Markdown.Extra = function () {
	    // For converting internal markdown (in tables for instance).
	    // This is necessary since these methods are meant to be called as
	    // preConversion hooks, and the Markdown converter passed to init()
	    // won't convert any markdown contained in the html tags we return.
	    this.converter = null;
	
	    // Stores html blocks we generate in hooks so that
	    // they're not destroyed if the user is using a sanitizing converter
	    this.hashBlocks = [];
	
	    // Stores footnotes
	    this.footnotes = {};
	    this.usedFootnotes = [];
	
	    // Special attribute blocks for fenced code blocks and headers enabled.
	    this.attributeBlocks = false;
	
	    // Fenced code block options
	    this.googleCodePrettify = false;
	    this.highlightJs = false;
	
	    // Table options
	    this.tableClass = '';
	
	    this.tabWidth = 4;
	  };
	
	  Markdown.Extra.init = function (converter, options) {
	    // Each call to init creates a new instance of Markdown.Extra so it's
	    // safe to have multiple converters, with different options, on a single page
	    var extra = new Markdown.Extra();
	    var postNormalizationTransformations = [];
	    var preBlockGamutTransformations = [];
	    var postSpanGamutTransformations = [];
	    var postConversionTransformations = ["unHashExtraBlocks"];
	
	    options = options || {};
	    options.extensions = options.extensions || ["all"];
	    if (contains(options.extensions, "all")) {
	      options.extensions = ["tables", "fenced_code_gfm", "def_list", "attr_list", "footnotes", "smartypants", "strikethrough", "newlines"];
	    }
	    preBlockGamutTransformations.push("wrapHeaders");
	    if (contains(options.extensions, "attr_list")) {
	      postNormalizationTransformations.push("hashFcbAttributeBlocks");
	      preBlockGamutTransformations.push("hashHeaderAttributeBlocks");
	      postConversionTransformations.push("applyAttributeBlocks");
	      extra.attributeBlocks = true;
	    }
	    if (contains(options.extensions, "fenced_code_gfm")) {
	      // This step will convert fcb inside list items and blockquotes
	      preBlockGamutTransformations.push("fencedCodeBlocks");
	      // This extra step is to prevent html blocks hashing and link definition/footnotes stripping inside fcb
	      postNormalizationTransformations.push("fencedCodeBlocks");
	    }
	    if (contains(options.extensions, "tables")) {
	      preBlockGamutTransformations.push("tables");
	    }
	    if (contains(options.extensions, "def_list")) {
	      preBlockGamutTransformations.push("definitionLists");
	    }
	    if (contains(options.extensions, "footnotes")) {
	      postNormalizationTransformations.push("stripFootnoteDefinitions");
	      preBlockGamutTransformations.push("doFootnotes");
	      postConversionTransformations.push("printFootnotes");
	    }
	    if (contains(options.extensions, "smartypants")) {
	      postConversionTransformations.push("runSmartyPants");
	    }
	    if (contains(options.extensions, "strikethrough")) {
	      postSpanGamutTransformations.push("strikethrough");
	    }
	    if (contains(options.extensions, "newlines")) {
	      postSpanGamutTransformations.push("newlines");
	    }
	
	    converter.hooks.chain("postNormalization", function (text) {
	      return extra.doTransform(postNormalizationTransformations, text) + '\n';
	    });
	
	    converter.hooks.chain("preBlockGamut", function (text, blockGamutHookCallback) {
	      // Keep a reference to the block gamut callback to run recursively
	      extra.blockGamutHookCallback = blockGamutHookCallback;
	      text = processEscapesStep1(text);
	      text = extra.doTransform(preBlockGamutTransformations, text) + '\n';
	      text = processEscapesStep2(text);
	      return text;
	    });
	
	    converter.hooks.chain("postSpanGamut", function (text) {
	      return extra.doTransform(postSpanGamutTransformations, text);
	    });
	
	    // Keep a reference to the hook chain running before doPostConversion to apply on hashed extra blocks
	    extra.previousPostConversion = converter.hooks.postConversion;
	    converter.hooks.chain("postConversion", function (text) {
	      text = extra.doTransform(postConversionTransformations, text);
	      // Clear state vars that may use unnecessary memory
	      extra.hashBlocks = [];
	      extra.footnotes = {};
	      extra.usedFootnotes = [];
	      return text;
	    });
	
	    if ("highlighter" in options) {
	      extra.googleCodePrettify = options.highlighter === 'prettify';
	      extra.highlightJs = options.highlighter === 'highlight';
	    }
	
	    if ("table_class" in options) {
	      extra.tableClass = options.table_class;
	    }
	
	    extra.converter = converter;
	
	    // Caller usually won't need this, but it's handy for testing.
	    return extra;
	  };
	
	  // Do transformations
	  Markdown.Extra.prototype.doTransform = function (transformations, text) {
	    for (var i = 0; i < transformations.length; i++) {
	      text = this[transformations[i]](text);
	    }return text;
	  };
	
	  // Return a placeholder containing a key, which is the block's index in the
	  // hashBlocks array. We wrap our output in a <p> tag here so Pagedown won't.
	  Markdown.Extra.prototype.hashExtraBlock = function (block) {
	    return '\n<p>~X' + (this.hashBlocks.push(block) - 1) + 'X</p>\n';
	  };
	  Markdown.Extra.prototype.hashExtraInline = function (block) {
	    return '~X' + (this.hashBlocks.push(block) - 1) + 'X';
	  };
	
	  // Replace placeholder blocks in `text` with their corresponding
	  // html blocks in the hashBlocks array.
	  Markdown.Extra.prototype.unHashExtraBlocks = function (text) {
	    var self = this;
	    function recursiveUnHash() {
	      var hasHash = false;
	      text = text.replace(/(?:<p>)?~X(\d+)X(?:<\/p>)?/g, function (wholeMatch, m1) {
	        hasHash = true;
	        var key = parseInt(m1, 10);
	        return self.hashBlocks[key];
	      });
	      if (hasHash === true) {
	        recursiveUnHash();
	      }
	    }
	    recursiveUnHash();
	    return text;
	  };
	
	  // Wrap headers to make sure they won't be in def lists
	  Markdown.Extra.prototype.wrapHeaders = function (text) {
	    function wrap(text) {
	      return '\n' + text + '\n';
	    }
	    text = text.replace(/^.+[ \t]*\n=+[ \t]*\n+/gm, wrap);
	    text = text.replace(/^.+[ \t]*\n-+[ \t]*\n+/gm, wrap);
	    text = text.replace(/^\#{1,6}[ \t]*.+?[ \t]*\#*\n+/gm, wrap);
	    return text;
	  };
	
	  /******************************************************************
	   * Attribute Blocks                                               *
	   *****************************************************************/
	
	  // TODO: use sentinels. Should we just add/remove them in doConversion?
	  // TODO: better matches for id / class attributes
	  var attrBlock = "\\{[ \\t]*((?:[#.][-_:a-zA-Z0-9]+[ \\t]*)+)\\}";
	  var hdrAttributesA = new RegExp("^(#{1,6}.*#{0,6})[ \\t]+" + attrBlock + "[ \\t]*(?:\\n|0x03)", "gm");
	  var hdrAttributesB = new RegExp("^(.*)[ \\t]+" + attrBlock + "[ \\t]*\\n" + "(?=[\\-|=]+\\s*(?:\\n|0x03))", "gm"); // underline lookahead
	  var fcbAttributes = new RegExp("^(```[ \\t]*[^{\\s]*)[ \\t]+" + attrBlock + "[ \\t]*\\n" + "(?=([\\s\\S]*?)\\n```[ \\t]*(\\n|0x03))", "gm");
	
	  // Extract headers attribute blocks, move them above the element they will be
	  // applied to, and hash them for later.
	  Markdown.Extra.prototype.hashHeaderAttributeBlocks = function (text) {
	
	    var self = this;
	    function attributeCallback(wholeMatch, pre, attr) {
	      return '<p>~XX' + (self.hashBlocks.push(attr) - 1) + 'XX</p>\n' + pre + "\n";
	    }
	
	    text = text.replace(hdrAttributesA, attributeCallback); // ## headers
	    text = text.replace(hdrAttributesB, attributeCallback); // underline headers
	    return text;
	  };
	
	  // Extract FCB attribute blocks, move them above the element they will be
	  // applied to, and hash them for later.
	  Markdown.Extra.prototype.hashFcbAttributeBlocks = function (text) {
	    // TODO: use sentinels. Should we just add/remove them in doConversion?
	    // TODO: better matches for id / class attributes
	
	    var self = this;
	    function attributeCallback(wholeMatch, pre, attr) {
	      return '<p>~XX' + (self.hashBlocks.push(attr) - 1) + 'XX</p>\n' + pre + "\n";
	    }
	
	    return text.replace(fcbAttributes, attributeCallback);
	  };
	
	  Markdown.Extra.prototype.applyAttributeBlocks = function (text) {
	    var self = this;
	    var blockRe = new RegExp('<p>~XX(\\d+)XX</p>[\\s]*' + '(?:<(h[1-6]|pre)(?: +class="(\\S+)")?(>[\\s\\S]*?</\\2>))', "gm");
	    text = text.replace(blockRe, function (wholeMatch, k, tag, cls, rest) {
	      if (!tag) // no following header or fenced code block.
	        return '';
	
	      // get attributes list from hash
	      var key = parseInt(k, 10);
	      var attributes = self.hashBlocks[key];
	
	      // get id
	      var id = attributes.match(/#[^\s#.]+/g) || [];
	      var idStr = id[0] ? ' id="' + id[0].substr(1, id[0].length - 1) + '"' : '';
	
	      // get classes and merge with existing classes
	      var classes = attributes.match(/\.[^\s#.]+/g) || [];
	      for (var i = 0; i < classes.length; i++) {
	        // Remove leading dot
	        classes[i] = classes[i].substr(1, classes[i].length - 1);
	      }var classStr = '';
	      if (cls) classes = union(classes, [cls]);
	
	      if (classes.length > 0) classStr = ' class="' + classes.join(' ') + '"';
	
	      return "<" + tag + idStr + classStr + rest;
	    });
	
	    return text;
	  };
	
	  /******************************************************************
	   * Tables                                                         *
	   *****************************************************************/
	
	  // Find and convert Markdown Extra tables into html.
	  Markdown.Extra.prototype.tables = function (text) {
	    var self = this;
	
	    var leadingPipe = new RegExp(['^', '[ ]{0,3}', // Allowed whitespace
	    '[|]', // Initial pipe
	    '(.+)\\n', // $1: Header Row
	
	    '[ ]{0,3}', // Allowed whitespace
	    '[|]([ ]*[-:]+[-| :]*)\\n', // $2: Separator
	
	    '(', // $3: Table Body
	    '(?:[ ]*[|].*\\n?)*', // Table rows
	    ')', '(?:\\n|$)' // Stop at final newline
	    ].join(''), 'gm');
	
	    var noLeadingPipe = new RegExp(['^', '[ ]{0,3}', // Allowed whitespace
	    '(\\S.*[|].*)\\n', // $1: Header Row
	
	    '[ ]{0,3}', // Allowed whitespace
	    '([-:]+[ ]*[|][-| :]*)\\n', // $2: Separator
	
	    '(', // $3: Table Body
	    '(?:.*[|].*\\n?)*', // Table rows
	    ')', '(?:\\n|$)' // Stop at final newline
	    ].join(''), 'gm');
	
	    text = text.replace(leadingPipe, doTable);
	    text = text.replace(noLeadingPipe, doTable);
	
	    // $1 = header, $2 = separator, $3 = body
	    function doTable(match, header, separator, body, offset, string) {
	      // remove any leading pipes and whitespace
	      header = header.replace(/^ *[|]/m, '');
	      separator = separator.replace(/^ *[|]/m, '');
	      body = body.replace(/^ *[|]/gm, '');
	
	      // remove trailing pipes and whitespace
	      header = header.replace(/[|] *$/m, '');
	      separator = separator.replace(/[|] *$/m, '');
	      body = body.replace(/[|] *$/gm, '');
	
	      // determine column alignments
	      alignspecs = separator.split(/ *[|] */);
	      align = [];
	      for (var i = 0; i < alignspecs.length; i++) {
	        var spec = alignspecs[i];
	        if (spec.match(/^ *-+: *$/m)) align[i] = ' style="text-align:right;"';else if (spec.match(/^ *:-+: *$/m)) align[i] = ' style="text-align:center;"';else if (spec.match(/^ *:-+ *$/m)) align[i] = ' style="text-align:left;"';else align[i] = '';
	      }
	
	      // TODO: parse spans in header and rows before splitting, so that pipes
	      // inside of tags are not interpreted as separators
	      var headers = header.split(/ *[|] */);
	      var colCount = headers.length;
	
	      // build html
	      var cls = self.tableClass ? ' class="' + self.tableClass + '"' : '';
	      var html = ['<table', cls, '>\n', '<thead>\n', '<tr>\n'].join('');
	
	      // build column headers.
	      for (i = 0; i < colCount; i++) {
	        var headerHtml = convertSpans(trim(headers[i]), self);
	        html += ["  <th", align[i], ">", headerHtml, "</th>\n"].join('');
	      }
	      html += "</tr>\n</thead>\n";
	
	      // build rows
	      var rows = body.split('\n');
	      for (i = 0; i < rows.length; i++) {
	        if (rows[i].match(/^\s*$/)) // can apply to final row
	          continue;
	
	        // ensure number of rowCells matches colCount
	        var rowCells = rows[i].split(/ *[|] */);
	        var lenDiff = colCount - rowCells.length;
	        for (var j = 0; j < lenDiff; j++) {
	          rowCells.push('');
	        }html += "<tr>\n";
	        for (j = 0; j < colCount; j++) {
	          var colHtml = convertSpans(trim(rowCells[j]), self);
	          html += ["  <td", align[j], ">", colHtml, "</td>\n"].join('');
	        }
	        html += "</tr>\n";
	      }
	
	      html += "</table>\n";
	
	      // replace html with placeholder until postConversion step
	      return self.hashExtraBlock(html);
	    }
	
	    return text;
	  };
	
	  /******************************************************************
	   * Footnotes                                                      *
	   *****************************************************************/
	
	  // Strip footnote, store in hashes.
	  Markdown.Extra.prototype.stripFootnoteDefinitions = function (text) {
	    var self = this;
	
	    text = text.replace(/\n[ ]{0,3}\[\^(.+?)\]\:[ \t]*\n?([\s\S]*?)\n{1,2}((?=\n[ ]{0,3}\S)|$)/g, function (wholeMatch, m1, m2) {
	      m1 = slugify(m1);
	      m2 += "\n";
	      m2 = m2.replace(/^[ ]{0,3}/g, "");
	      self.footnotes[m1] = m2;
	      return "\n";
	    });
	
	    return text;
	  };
	
	  // Find and convert footnotes references.
	  Markdown.Extra.prototype.doFootnotes = function (text) {
	    var self = this;
	    if (self.isConvertingFootnote === true) {
	      return text;
	    }
	
	    var footnoteCounter = 0;
	    text = text.replace(/\[\^(.+?)\]/g, function (wholeMatch, m1) {
	      var id = slugify(m1);
	      var footnote = self.footnotes[id];
	      if (footnote === undefined) {
	        return wholeMatch;
	      }
	      footnoteCounter++;
	      self.usedFootnotes.push(id);
	      var html = '<a href="#fn:' + id + '" id="fnref:' + id + '" title="See footnote" class="footnote">' + footnoteCounter + '</a>';
	      return self.hashExtraInline(html);
	    });
	
	    return text;
	  };
	
	  // Print footnotes at the end of the document
	  Markdown.Extra.prototype.printFootnotes = function (text) {
	    var self = this;
	
	    if (self.usedFootnotes.length === 0) {
	      return text;
	    }
	
	    text += '\n\n<div class="footnotes">\n<hr>\n<ol>\n\n';
	    for (var i = 0; i < self.usedFootnotes.length; i++) {
	      var id = self.usedFootnotes[i];
	      var footnote = self.footnotes[id];
	      self.isConvertingFootnote = true;
	      var formattedfootnote = convertSpans(footnote, self);
	      delete self.isConvertingFootnote;
	      text += '<li id="fn:' + id + '">' + formattedfootnote + ' <a href="#fnref:' + id + '" title="Return to article" class="reversefootnote">&#8617;</a></li>\n\n';
	    }
	    text += '</ol>\n</div>';
	    return text;
	  };
	
	  /******************************************************************
	  * Fenced Code Blocks  (gfm)                                       *
	  ******************************************************************/
	
	  // Find and convert gfm-inspired fenced code blocks into html.
	  Markdown.Extra.prototype.fencedCodeBlocks = function (text) {
	    function encodeCode(code) {
	      code = code.replace(/&/g, "&amp;");
	      code = code.replace(/</g, "&lt;");
	      code = code.replace(/>/g, "&gt;");
	      // These were escaped by PageDown before postNormalization 
	      code = code.replace(/~D/g, "$$");
	      code = code.replace(/~T/g, "~");
	      return code;
	    }
	
	    var self = this;
	    text = text.replace(/(?:^|\n)```[ \t]*(\S*)[ \t]*\n([\s\S]*?)\n```[ \t]*(?=\n)/g, function (match, m1, m2) {
	      var language = m1,
	          codeblock = m2;
	
	      // adhere to specified options
	      var preclass = self.googleCodePrettify ? ' class="prettyprint"' : '';
	      var codeclass = '';
	      if (language) {
	        if (self.googleCodePrettify || self.highlightJs) {
	          // use html5 language- class names. supported by both prettify and highlight.js
	          codeclass = ' class="language-' + language + '"';
	        } else {
	          codeclass = ' class="' + language + '"';
	        }
	      }
	
	      var html = ['<pre', preclass, '><code', codeclass, '>', encodeCode(codeblock), '</code></pre>'].join('');
	
	      // replace codeblock with placeholder until postConversion step
	      return self.hashExtraBlock(html);
	    });
	
	    return text;
	  };
	
	  /******************************************************************
	  * SmartyPants                                                     *
	  ******************************************************************/
	
	  Markdown.Extra.prototype.educatePants = function (text) {
	    var self = this;
	    var result = '';
	    var blockOffset = 0;
	    // Here we parse HTML in a very bad manner
	    text.replace(/(?:<!--[\s\S]*?-->)|(<)([a-zA-Z1-6]+)([^\n]*?>)([\s\S]*?)(<\/\2>)/g, function (wholeMatch, m1, m2, m3, m4, m5, offset) {
	      var token = text.substring(blockOffset, offset);
	      result += self.applyPants(token);
	      self.smartyPantsLastChar = result.substring(result.length - 1);
	      blockOffset = offset + wholeMatch.length;
	      if (!m1) {
	        // Skip commentary
	        result += wholeMatch;
	        return;
	      }
	      // Skip special tags
	      if (!/code|kbd|pre|script|noscript|iframe|math|ins|del|pre/i.test(m2)) {
	        m4 = self.educatePants(m4);
	      } else {
	        self.smartyPantsLastChar = m4.substring(m4.length - 1);
	      }
	      result += m1 + m2 + m3 + m4 + m5;
	    });
	    var lastToken = text.substring(blockOffset);
	    result += self.applyPants(lastToken);
	    self.smartyPantsLastChar = result.substring(result.length - 1);
	    return result;
	  };
	
	  function revertPants(wholeMatch, m1) {
	    var blockText = m1;
	    blockText = blockText.replace(/&\#8220;/g, "\"");
	    blockText = blockText.replace(/&\#8221;/g, "\"");
	    blockText = blockText.replace(/&\#8216;/g, "'");
	    blockText = blockText.replace(/&\#8217;/g, "'");
	    blockText = blockText.replace(/&\#8212;/g, "---");
	    blockText = blockText.replace(/&\#8211;/g, "--");
	    blockText = blockText.replace(/&\#8230;/g, "...");
	    return blockText;
	  }
	
	  Markdown.Extra.prototype.applyPants = function (text) {
	    // Dashes
	    text = text.replace(/---/g, "&#8212;").replace(/--/g, "&#8211;");
	    // Ellipses
	    text = text.replace(/\.\.\./g, "&#8230;").replace(/\.\s\.\s\./g, "&#8230;");
	    // Backticks
	    text = text.replace(/``/g, "&#8220;").replace(/''/g, "&#8221;");
	
	    if (/^'$/.test(text)) {
	      // Special case: single-character ' token
	      if (/\S/.test(this.smartyPantsLastChar)) {
	        return "&#8217;";
	      }
	      return "&#8216;";
	    }
	    if (/^"$/.test(text)) {
	      // Special case: single-character " token
	      if (/\S/.test(this.smartyPantsLastChar)) {
	        return "&#8221;";
	      }
	      return "&#8220;";
	    }
	
	    // Special case if the very first character is a quote
	    // followed by punctuation at a non-word-break. Close the quotes by brute force:
	    text = text.replace(/^'(?=[!"#\$\%'()*+,\-.\/:;<=>?\@\[\\]\^_`{|}~]\B)/, "&#8217;");
	    text = text.replace(/^"(?=[!"#\$\%'()*+,\-.\/:;<=>?\@\[\\]\^_`{|}~]\B)/, "&#8221;");
	
	    // Special case for double sets of quotes, e.g.:
	    //   <p>He said, "'Quoted' words in a larger quote."</p>
	    text = text.replace(/"'(?=\w)/g, "&#8220;&#8216;");
	    text = text.replace(/'"(?=\w)/g, "&#8216;&#8220;");
	
	    // Special case for decade abbreviations (the '80s):
	    text = text.replace(/'(?=\d{2}s)/g, "&#8217;");
	
	    // Get most opening single quotes:
	    text = text.replace(/(\s|&nbsp;|--|&[mn]dash;|&\#8211;|&\#8212;|&\#x201[34];)'(?=\w)/g, "$1&#8216;");
	
	    // Single closing quotes:
	    text = text.replace(/([^\s\[\{\(\-])'/g, "$1&#8217;");
	    text = text.replace(/'(?=\s|s\b)/g, "&#8217;");
	
	    // Any remaining single quotes should be opening ones:
	    text = text.replace(/'/g, "&#8216;");
	
	    // Get most opening double quotes:
	    text = text.replace(/(\s|&nbsp;|--|&[mn]dash;|&\#8211;|&\#8212;|&\#x201[34];)"(?=\w)/g, "$1&#8220;");
	
	    // Double closing quotes:
	    text = text.replace(/([^\s\[\{\(\-])"/g, "$1&#8221;");
	    text = text.replace(/"(?=\s)/g, "&#8221;");
	
	    // Any remaining quotes should be opening ones.
	    text = text.replace(/"/ig, "&#8220;");
	    return text;
	  };
	
	  // Find and convert markdown extra definition lists into html.
	  Markdown.Extra.prototype.runSmartyPants = function (text) {
	    this.smartyPantsLastChar = '';
	    text = this.educatePants(text);
	    // Clean everything inside html tags (some of them may have been converted due to our rough html parsing)
	    text = text.replace(/(<([a-zA-Z1-6]+)\b([^\n>]*?)(\/)?>)/g, revertPants);
	    return text;
	  };
	
	  /******************************************************************
	  * Definition Lists                                                *
	  ******************************************************************/
	
	  // Find and convert markdown extra definition lists into html.
	  Markdown.Extra.prototype.definitionLists = function (text) {
	    var wholeList = new RegExp(['(\\x02\\n?|\\n\\n)', '(?:', '(', // $1 = whole list
	    '(', // $2
	    '[ ]{0,3}', '((?:[ \\t]*\\S.*\\n)+)', // $3 = defined term
	    '\\n?', '[ ]{0,3}:[ ]+', // colon starting definition
	    ')', '([\\s\\S]+?)', '(', // $4
	    '(?=\\0x03)', // \z
	    '|', '(?=', '\\n{2,}', '(?=\\S)', '(?!', // Negative lookahead for another term
	    '[ ]{0,3}', '(?:\\S.*\\n)+?', // defined term
	    '\\n?', '[ ]{0,3}:[ ]+', // colon starting definition
	    ')', '(?!', // Negative lookahead for another definition
	    '[ ]{0,3}:[ ]+', // colon starting definition
	    ')', ')', ')', ')', ')'].join(''), 'gm');
	
	    var self = this;
	    text = addAnchors(text);
	
	    text = text.replace(wholeList, function (match, pre, list) {
	      var result = trim(self.processDefListItems(list));
	      result = "<dl>\n" + result + "\n</dl>";
	      return pre + self.hashExtraBlock(result) + "\n\n";
	    });
	
	    return removeAnchors(text);
	  };
	
	  // Process the contents of a single definition list, splitting it
	  // into individual term and definition list items.
	  Markdown.Extra.prototype.processDefListItems = function (listStr) {
	    var self = this;
	
	    var dt = new RegExp(['(\\x02\\n?|\\n\\n+)', // leading line
	    '(', // definition terms = $1
	    '[ ]{0,3}', // leading whitespace
	    '(?![:][ ]|[ ])', // negative lookahead for a definition
	    //   mark (colon) or more whitespace
	    '(?:\\S.*\\n)+?', // actual term (not whitespace)
	    ')', '(?=\\n?[ ]{0,3}:[ ])' // lookahead for following line feed
	    ].join(''), //   with a definition mark
	    'gm');
	
	    var dd = new RegExp(['\\n(\\n+)?', // leading line = $1
	    '(', // marker space = $2
	    '[ ]{0,3}', // whitespace before colon
	    '[:][ ]+', // definition mark (colon)
	    ')', '([\\s\\S]+?)', // definition text = $3
	    '(?=\\n*', // stop at next definition mark,
	    '(?:', // next term or end of text
	    '\\n[ ]{0,3}[:][ ]|', '<dt>|\\x03', // \z
	    ')', ')'].join(''), 'gm');
	
	    listStr = addAnchors(listStr);
	    // trim trailing blank lines:
	    listStr = listStr.replace(/\n{2,}(?=\\x03)/, "\n");
	
	    // Process definition terms.
	    listStr = listStr.replace(dt, function (match, pre, termsStr) {
	      var terms = trim(termsStr).split("\n");
	      var text = '';
	      for (var i = 0; i < terms.length; i++) {
	        var term = terms[i];
	        // process spans inside dt
	        term = convertSpans(trim(term), self);
	        text += "\n<dt>" + term + "</dt>";
	      }
	      return text + "\n";
	    });
	
	    // Process actual definitions.
	    listStr = listStr.replace(dd, function (match, leadingLine, markerSpace, def) {
	      if (leadingLine || def.match(/\n{2,}/)) {
	        // replace marker with the appropriate whitespace indentation
	        def = Array(markerSpace.length + 1).join(' ') + def;
	        // process markdown inside definition
	        // TODO?: currently doesn't apply extensions
	        def = outdent(def) + "\n\n";
	        def = "\n" + convertAll(def, self) + "\n";
	      } else {
	        // convert span-level markdown inside definition
	        def = rtrim(def);
	        def = convertSpans(outdent(def), self);
	      }
	
	      return "\n<dd>" + def + "</dd>\n";
	    });
	
	    return removeAnchors(listStr);
	  };
	
	  /***********************************************************
	  * Strikethrough                                            *
	  ************************************************************/
	
	  Markdown.Extra.prototype.strikethrough = function (text) {
	    // Pretty much duplicated from _DoItalicsAndBold
	    return text.replace(/([\W_]|^)~T~T(?=\S)([^\r]*?\S[\*_]*)~T~T([\W_]|$)/g, "$1<del>$2</del>$3");
	  };
	
	  /***********************************************************
	  * New lines                                                *
	  ************************************************************/
	
	  Markdown.Extra.prototype.newlines = function (text) {
	    // We have to ignore already converted newlines and line breaks in sub-list items
	    return text.replace(/(<(?:br|\/li)>)?\n/g, function (wholeMatch, previousTag) {
	      return previousTag ? wholeMatch : " <br>\n";
	    });
	  };
	})();

/***/ }),

/***/ 1772:
/***/ (function(module, exports, __webpack_require__) {

	"use strict";
	
	var Markdown = __webpack_require__(1770);
	
	(function () {
	
	    var util = {},
	        position = {},
	        ui = {},
	        doc = window.document,
	        re = window.RegExp,
	        nav = window.navigator,
	        SETTINGS = { lineLength: 72 },
	
	
	    // Used to work around some browser bugs where we can't use feature testing.
	    uaSniffed = {
	        isIE: /msie/.test(nav.userAgent.toLowerCase()),
	        isIE_5or6: /msie 6/.test(nav.userAgent.toLowerCase()) || /msie 5/.test(nav.userAgent.toLowerCase()),
	        isOpera: /opera/.test(nav.userAgent.toLowerCase())
	    };
	
	    var defaultsStrings = {
	        bold: "Strong <strong> Ctrl+B",
	        boldexample: "strong text",
	
	        italic: "Emphasis <em> Ctrl+I",
	        italicexample: "emphasized text",
	
	        link: "Hyperlink <a> Ctrl+L",
	        linkdescription: "enter link description here",
	        linkdialog: "<p><b>Insert Hyperlink</b></p><p>http://example.com/ \"optional title\"</p>",
	
	        quote: "Blockquote <blockquote> Ctrl+Q",
	        quoteexample: "Blockquote",
	
	        code: "Code Sample <pre><code> Ctrl+K",
	        codeexample: "enter code here",
	
	        image: "Image <img> Ctrl+G",
	        imagedescription: "enter image description here",
	        imagedialog: "<p><b>Insert Image</b></p><p>http://example.com/images/diagram.jpg \"optional title\"<br><br>Need <a href='http://www.google.com/search?q=free+image+hosting' target='_blank'>free image hosting?</a></p>",
	
	        olist: "Numbered List <ol> Ctrl+O",
	        ulist: "Bulleted List <ul> Ctrl+U",
	        litem: "List item",
	
	        heading: "Heading <h1>/<h2> Ctrl+H",
	        headingexample: "Heading",
	
	        hr: "Horizontal Rule <hr> Ctrl+R",
	
	        undo: "Undo - Ctrl+Z",
	        redo: "Redo - Ctrl+Y",
	        redomac: "Redo - Ctrl+Shift+Z",
	
	        help: "Markdown Editing Help"
	    };
	
	    // -------------------------------------------------------------------
	    //  YOUR CHANGES GO HERE
	    //
	    // I've tried to localize the things you are likely to change to
	    // this area.
	    // -------------------------------------------------------------------
	
	    // The default text that appears in the dialog input box when entering
	    // links.
	    var imageDefaultText = "http://";
	    var linkDefaultText = "http://";
	
	    // -------------------------------------------------------------------
	    //  END OF YOUR CHANGES
	    // -------------------------------------------------------------------
	
	    // options, if given, can have the following properties:
	    //   options.helpButton = { handler: yourEventHandler }
	    //   options.strings = { italicexample: "slanted text" }
	    // `yourEventHandler` is the click handler for the help button.
	    // If `options.helpButton` isn't given, not help button is created.
	    // `options.strings` can have any or all of the same properties as
	    // `defaultStrings` above, so you can just override some string displayed
	    // to the user on a case-by-case basis, or translate all strings to
	    // a different language.
	    //
	    // For backwards compatibility reasons, the `options` argument can also
	    // be just the `helpButton` object, and `strings.help` can also be set via
	    // `helpButton.title`. This should be considered legacy.
	    //
	    // The constructed editor object has the methods:
	    // - getConverter() returns the markdown converter object that was passed to the constructor
	    // - run() actually starts the editor; should be called after all necessary plugins are registered. Calling this more than once is a no-op.
	    // - refreshPreview() forces the preview to be updated. This method is only available after run() was called.
	    Markdown.Editor = function (markdownConverter, idPostfix, options) {
	
	        options = options || {};
	
	        if (typeof options.handler === "function") {
	            //backwards compatible behavior
	            options = { helpButton: options };
	        }
	        options.strings = options.strings || {};
	        if (options.helpButton) {
	            options.strings.help = options.strings.help || options.helpButton.title;
	        }
	        var getString = function getString(identifier) {
	            return options.strings[identifier] || defaultsStrings[identifier];
	        };
	
	        idPostfix = idPostfix || "";
	
	        var hooks = this.hooks = new Markdown.HookCollection();
	        hooks.addNoop("onPreviewRefresh"); // called with no arguments after the preview has been refreshed
	        hooks.addNoop("postBlockquoteCreation"); // called with the user's selection *after* the blockquote was created; should return the actual to-be-inserted text
	        hooks.addFalse("insertImageDialog"); /* called with one parameter: a callback to be called with the URL of the image. If the application creates
	                                              * its own image insertion dialog, this hook should return true, and the callback should be called with the chosen
	                                              * image url (or null if the user cancelled). If this hook returns false, the default dialog will be used.
	                                              */
	
	        this.getConverter = function () {
	            return markdownConverter;
	        };
	
	        var that = this,
	            panels;
	
	        this.run = function () {
	            if (panels) return; // already initialized
	
	            panels = new PanelCollection(idPostfix);
	            var commandManager = new CommandManager(hooks, getString);
	            var previewManager = new PreviewManager(markdownConverter, panels, function () {
	                hooks.onPreviewRefresh();
	            });
	            var undoManager, uiManager;
	
	            if (!/\?noundo/.test(doc.location.href)) {
	                undoManager = new UndoManager(function () {
	                    previewManager.refresh();
	                    if (uiManager) // not available on the first call
	                        uiManager.setUndoRedoButtonStates();
	                }, panels);
	                this.textOperation = function (f) {
	                    undoManager.setCommandMode();
	                    f();
	                    that.refreshPreview();
	                };
	            }
	
	            uiManager = new UIManager(idPostfix, panels, undoManager, previewManager, commandManager, options.helpButton, getString);
	            uiManager.setUndoRedoButtonStates();
	
	            var forceRefresh = that.refreshPreview = function () {
	                previewManager.refresh(true);
	            };
	
	            forceRefresh();
	        };
	    };
	
	    // before: contains all the text in the input box BEFORE the selection.
	    // after: contains all the text in the input box AFTER the selection.
	    function Chunks() {}
	
	    // startRegex: a regular expression to find the start tag
	    // endRegex: a regular expresssion to find the end tag
	    Chunks.prototype.findTags = function (startRegex, endRegex) {
	
	        var chunkObj = this;
	        var regex;
	
	        if (startRegex) {
	
	            regex = util.extendRegExp(startRegex, "", "$");
	
	            this.before = this.before.replace(regex, function (match) {
	                chunkObj.startTag = chunkObj.startTag + match;
	                return "";
	            });
	
	            regex = util.extendRegExp(startRegex, "^", "");
	
	            this.selection = this.selection.replace(regex, function (match) {
	                chunkObj.startTag = chunkObj.startTag + match;
	                return "";
	            });
	        }
	
	        if (endRegex) {
	
	            regex = util.extendRegExp(endRegex, "", "$");
	
	            this.selection = this.selection.replace(regex, function (match) {
	                chunkObj.endTag = match + chunkObj.endTag;
	                return "";
	            });
	
	            regex = util.extendRegExp(endRegex, "^", "");
	
	            this.after = this.after.replace(regex, function (match) {
	                chunkObj.endTag = match + chunkObj.endTag;
	                return "";
	            });
	        }
	    };
	
	    // If remove is false, the whitespace is transferred
	    // to the before/after regions.
	    //
	    // If remove is true, the whitespace disappears.
	    Chunks.prototype.trimWhitespace = function (remove) {
	        var beforeReplacer,
	            afterReplacer,
	            that = this;
	        if (remove) {
	            beforeReplacer = afterReplacer = "";
	        } else {
	            beforeReplacer = function beforeReplacer(s) {
	                that.before += s;return "";
	            };
	            afterReplacer = function afterReplacer(s) {
	                that.after = s + that.after;return "";
	            };
	        }
	
	        this.selection = this.selection.replace(/^(\s*)/, beforeReplacer).replace(/(\s*)$/, afterReplacer);
	    };
	
	    Chunks.prototype.skipLines = function (nLinesBefore, nLinesAfter, findExtraNewlines) {
	
	        if (nLinesBefore === undefined) {
	            nLinesBefore = 1;
	        }
	
	        if (nLinesAfter === undefined) {
	            nLinesAfter = 1;
	        }
	
	        nLinesBefore++;
	        nLinesAfter++;
	
	        var regexText;
	        var replacementText;
	
	        // chrome bug ... documented at: http://meta.stackoverflow.com/questions/63307/blockquote-glitch-in-editor-in-chrome-6-and-7/65985#65985
	        if (navigator.userAgent.match(/Chrome/)) {
	            "X".match(/()./);
	        }
	
	        this.selection = this.selection.replace(/(^\n*)/, "");
	
	        this.startTag = this.startTag + re.$1;
	
	        this.selection = this.selection.replace(/(\n*$)/, "");
	        this.endTag = this.endTag + re.$1;
	        this.startTag = this.startTag.replace(/(^\n*)/, "");
	        this.before = this.before + re.$1;
	        this.endTag = this.endTag.replace(/(\n*$)/, "");
	        this.after = this.after + re.$1;
	
	        if (this.before) {
	
	            regexText = replacementText = "";
	
	            while (nLinesBefore--) {
	                regexText += "\\n?";
	                replacementText += "\n";
	            }
	
	            if (findExtraNewlines) {
	                regexText = "\\n*";
	            }
	            this.before = this.before.replace(new re(regexText + "$", ""), replacementText);
	        }
	
	        if (this.after) {
	
	            regexText = replacementText = "";
	
	            while (nLinesAfter--) {
	                regexText += "\\n?";
	                replacementText += "\n";
	            }
	            if (findExtraNewlines) {
	                regexText = "\\n*";
	            }
	
	            this.after = this.after.replace(new re(regexText, ""), replacementText);
	        }
	    };
	
	    // end of Chunks
	
	    // A collection of the important regions on the page.
	    // Cached so we don't have to keep traversing the DOM.
	    // Also holds ieCachedRange and ieCachedScrollTop, where necessary; working around
	    // this issue:
	    // Internet explorer has problems with CSS sprite buttons that use HTML
	    // lists.  When you click on the background image "button", IE will
	    // select the non-existent link text and discard the selection in the
	    // textarea.  The solution to this is to cache the textarea selection
	    // on the button's mousedown event and set a flag.  In the part of the
	    // code where we need to grab the selection, we check for the flag
	    // and, if it's set, use the cached area instead of querying the
	    // textarea.
	    //
	    // This ONLY affects Internet Explorer (tested on versions 6, 7
	    // and 8) and ONLY on button clicks.  Keyboard shortcuts work
	    // normally since the focus never leaves the textarea.
	    function PanelCollection(postfix) {
	        this.buttonBar = doc.getElementById("wmd-button-bar" + postfix);
	        this.preview = doc.getElementById("wmd-preview" + postfix);
	        this.input = doc.getElementById("wmd-input" + postfix);
	    };
	
	    // Returns true if the DOM element is visible, false if it's hidden.
	    // Checks if display is anything other than none.
	    util.isVisible = function (elem) {
	
	        if (window.getComputedStyle) {
	            // Most browsers
	            return window.getComputedStyle(elem, null).getPropertyValue("display") !== "none";
	        } else if (elem.currentStyle) {
	            // IE
	            return elem.currentStyle["display"] !== "none";
	        }
	    };
	
	    // Adds a listener callback to a DOM element which is fired on a specified
	    // event.
	    util.addEvent = function (elem, event, listener) {
	        if (elem.attachEvent) {
	            // IE only.  The "on" is mandatory.
	            elem.attachEvent("on" + event, listener);
	        } else {
	            // Other browsers.
	            elem.addEventListener(event, listener, false);
	        }
	    };
	
	    // Removes a listener callback from a DOM element which is fired on a specified
	    // event.
	    util.removeEvent = function (elem, event, listener) {
	        if (elem.detachEvent) {
	            // IE only.  The "on" is mandatory.
	            elem.detachEvent("on" + event, listener);
	        } else {
	            // Other browsers.
	            elem.removeEventListener(event, listener, false);
	        }
	    };
	
	    // Converts \r\n and \r to \n.
	    util.fixEolChars = function (text) {
	        text = text.replace(/\r\n/g, "\n");
	        text = text.replace(/\r/g, "\n");
	        return text;
	    };
	
	    // Extends a regular expression.  Returns a new RegExp
	    // using pre + regex + post as the expression.
	    // Used in a few functions where we have a base
	    // expression and we want to pre- or append some
	    // conditions to it (e.g. adding "$" to the end).
	    // The flags are unchanged.
	    //
	    // regex is a RegExp, pre and post are strings.
	    util.extendRegExp = function (regex, pre, post) {
	
	        if (pre === null || pre === undefined) {
	            pre = "";
	        }
	        if (post === null || post === undefined) {
	            post = "";
	        }
	
	        var pattern = regex.toString();
	        var flags;
	
	        // Replace the flags with empty space and store them.
	        pattern = pattern.replace(/\/([gim]*)$/, function (wholeMatch, flagsPart) {
	            flags = flagsPart;
	            return "";
	        });
	
	        // Remove the slash delimiters on the regular expression.
	        pattern = pattern.replace(/(^\/|\/$)/g, "");
	        pattern = pre + pattern + post;
	
	        return new re(pattern, flags);
	    };
	
	    // UNFINISHED
	    // The assignment in the while loop makes jslint cranky.
	    // I'll change it to a better loop later.
	    position.getTop = function (elem, isInner) {
	        var result = elem.offsetTop;
	        if (!isInner) {
	            while (elem = elem.offsetParent) {
	                result += elem.offsetTop;
	            }
	        }
	        return result;
	    };
	
	    position.getHeight = function (elem) {
	        return elem.offsetHeight || elem.scrollHeight;
	    };
	
	    position.getWidth = function (elem) {
	        return elem.offsetWidth || elem.scrollWidth;
	    };
	
	    position.getPageSize = function () {
	
	        var scrollWidth, scrollHeight;
	        var innerWidth, innerHeight;
	
	        // It's not very clear which blocks work with which browsers.
	        if (self.innerHeight && self.scrollMaxY) {
	            scrollWidth = doc.body.scrollWidth;
	            scrollHeight = self.innerHeight + self.scrollMaxY;
	        } else if (doc.body.scrollHeight > doc.body.offsetHeight) {
	            scrollWidth = doc.body.scrollWidth;
	            scrollHeight = doc.body.scrollHeight;
	        } else {
	            scrollWidth = doc.body.offsetWidth;
	            scrollHeight = doc.body.offsetHeight;
	        }
	
	        if (self.innerHeight) {
	            // Non-IE browser
	            innerWidth = self.innerWidth;
	            innerHeight = self.innerHeight;
	        } else if (doc.documentElement && doc.documentElement.clientHeight) {
	            // Some versions of IE (IE 6 w/ a DOCTYPE declaration)
	            innerWidth = doc.documentElement.clientWidth;
	            innerHeight = doc.documentElement.clientHeight;
	        } else if (doc.body) {
	            // Other versions of IE
	            innerWidth = doc.body.clientWidth;
	            innerHeight = doc.body.clientHeight;
	        }
	
	        var maxWidth = Math.max(scrollWidth, innerWidth);
	        var maxHeight = Math.max(scrollHeight, innerHeight);
	        return [maxWidth, maxHeight, innerWidth, innerHeight];
	    };
	
	    // Handles pushing and popping TextareaStates for undo/redo commands.
	    // I should rename the stack variables to list.
	    function UndoManager(callback, panels) {
	
	        var undoObj = this;
	        var undoStack = []; // A stack of undo states
	        var stackPtr = 0; // The index of the current state
	        var mode = "none";
	        var lastState; // The last state
	        var timer; // The setTimeout handle for cancelling the timer
	        var inputStateObj;
	
	        // Set the mode for later logic steps.
	        var setMode = function setMode(newMode, noSave) {
	            if (mode != newMode) {
	                mode = newMode;
	                if (!noSave) {
	                    saveState();
	                }
	            }
	
	            if (!uaSniffed.isIE || mode != "moving") {
	                timer = setTimeout(refreshState, 1);
	            } else {
	                inputStateObj = null;
	            }
	        };
	
	        var refreshState = function refreshState(isInitialState) {
	            inputStateObj = new TextareaState(panels, isInitialState);
	            timer = undefined;
	        };
	
	        this.setCommandMode = function () {
	            mode = "command";
	            saveState();
	            timer = setTimeout(refreshState, 0);
	        };
	
	        this.canUndo = function () {
	            return stackPtr > 1;
	        };
	
	        this.canRedo = function () {
	            if (undoStack[stackPtr + 1]) {
	                return true;
	            }
	            return false;
	        };
	
	        // Removes the last state and restores it.
	        this.undo = function () {
	
	            if (undoObj.canUndo()) {
	                if (lastState) {
	                    // What about setting state -1 to null or checking for undefined?
	                    lastState.restore();
	                    lastState = null;
	                } else {
	                    undoStack[stackPtr] = new TextareaState(panels);
	                    undoStack[--stackPtr].restore();
	
	                    if (callback) {
	                        callback();
	                    }
	                }
	            }
	
	            mode = "none";
	            panels.input.focus();
	            refreshState();
	        };
	
	        // Redo an action.
	        this.redo = function () {
	
	            if (undoObj.canRedo()) {
	
	                undoStack[++stackPtr].restore();
	
	                if (callback) {
	                    callback();
	                }
	            }
	
	            mode = "none";
	            panels.input.focus();
	            refreshState();
	        };
	
	        // Push the input area state to the stack.
	        var saveState = function saveState() {
	            var currState = inputStateObj || new TextareaState(panels);
	
	            if (!currState) {
	                return false;
	            }
	            if (mode == "moving") {
	                if (!lastState) {
	                    lastState = currState;
	                }
	                return;
	            }
	            if (lastState) {
	                if (undoStack[stackPtr - 1].text != lastState.text) {
	                    undoStack[stackPtr++] = lastState;
	                }
	                lastState = null;
	            }
	            undoStack[stackPtr++] = currState;
	            undoStack[stackPtr + 1] = null;
	            if (callback) {
	                callback();
	            }
	        };
	
	        var handleCtrlYZ = function handleCtrlYZ(event) {
	
	            var handled = false;
	
	            if ((event.ctrlKey || event.metaKey) && !event.altKey) {
	
	                // IE and Opera do not support charCode.
	                var keyCode = event.charCode || event.keyCode;
	                var keyCodeChar = String.fromCharCode(keyCode);
	
	                switch (keyCodeChar.toLowerCase()) {
	
	                    case "y":
	                        undoObj.redo();
	                        handled = true;
	                        break;
	
	                    case "z":
	                        if (!event.shiftKey) {
	                            undoObj.undo();
	                        } else {
	                            undoObj.redo();
	                        }
	                        handled = true;
	                        break;
	                }
	            }
	
	            if (handled) {
	                if (event.preventDefault) {
	                    event.preventDefault();
	                }
	                if (window.event) {
	                    window.event.returnValue = false;
	                }
	                return;
	            }
	        };
	
	        // Set the mode depending on what is going on in the input area.
	        var handleModeChange = function handleModeChange(event) {
	
	            if (!event.ctrlKey && !event.metaKey) {
	
	                var keyCode = event.keyCode;
	
	                if (keyCode >= 33 && keyCode <= 40 || keyCode >= 63232 && keyCode <= 63235) {
	                    // 33 - 40: page up/dn and arrow keys
	                    // 63232 - 63235: page up/dn and arrow keys on safari
	                    setMode("moving");
	                } else if (keyCode == 8 || keyCode == 46 || keyCode == 127) {
	                    // 8: backspace
	                    // 46: delete
	                    // 127: delete
	                    setMode("deleting");
	                } else if (keyCode == 13) {
	                    // 13: Enter
	                    setMode("newlines");
	                } else if (keyCode == 27) {
	                    // 27: escape
	                    setMode("escape");
	                } else if ((keyCode < 16 || keyCode > 20) && keyCode != 91) {
	                    // 16-20 are shift, etc.
	                    // 91: left window key
	                    // I think this might be a little messed up since there are
	                    // a lot of nonprinting keys above 20.
	                    setMode("typing");
	                }
	            }
	        };
	
	        var setEventHandlers = function setEventHandlers() {
	            util.addEvent(panels.input, "keypress", function (event) {
	                // keyCode 89: y
	                // keyCode 90: z
	                if ((event.ctrlKey || event.metaKey) && !event.altKey && (event.keyCode == 89 || event.keyCode == 90)) {
	                    event.preventDefault();
	                }
	            });
	
	            var handlePaste = function handlePaste() {
	                if (uaSniffed.isIE || inputStateObj && inputStateObj.text != panels.input.value) {
	                    if (timer == undefined) {
	                        mode = "paste";
	                        saveState();
	                        refreshState();
	                    }
	                }
	            };
	
	            util.addEvent(panels.input, "keydown", handleCtrlYZ);
	            util.addEvent(panels.input, "keydown", handleModeChange);
	            util.addEvent(panels.input, "mousedown", function () {
	                setMode("moving");
	            });
	
	            panels.input.onpaste = handlePaste;
	            panels.input.ondrop = handlePaste;
	        };
	
	        var init = function init() {
	            setEventHandlers();
	            refreshState(true);
	            saveState();
	        };
	
	        init();
	    }
	
	    // end of UndoManager
	
	    // The input textarea state/contents.
	    // This is used to implement undo/redo by the undo manager.
	    function TextareaState(panels, isInitialState) {
	
	        // Aliases
	        var stateObj = this;
	        var inputArea = panels.input;
	        this.init = function () {
	            if (!util.isVisible(inputArea)) {
	                return;
	            }
	            if (!isInitialState && doc.activeElement && doc.activeElement !== inputArea) {
	                // this happens when tabbing out of the input box
	                return;
	            }
	
	            this.setInputAreaSelectionStartEnd();
	            this.scrollTop = inputArea.scrollTop;
	            if (!this.text && inputArea.selectionStart || inputArea.selectionStart === 0) {
	                this.text = inputArea.value;
	            }
	        };
	
	        // Sets the selected text in the input box after we've performed an
	        // operation.
	        this.setInputAreaSelection = function () {
	
	            if (!util.isVisible(inputArea)) {
	                return;
	            }
	
	            if (inputArea.selectionStart !== undefined && !uaSniffed.isOpera) {
	
	                inputArea.focus();
	                inputArea.selectionStart = stateObj.start;
	                inputArea.selectionEnd = stateObj.end;
	                inputArea.scrollTop = stateObj.scrollTop;
	            } else if (doc.selection) {
	
	                if (doc.activeElement && doc.activeElement !== inputArea) {
	                    return;
	                }
	
	                inputArea.focus();
	                var range = inputArea.createTextRange();
	                range.moveStart("character", -inputArea.value.length);
	                range.moveEnd("character", -inputArea.value.length);
	                range.moveEnd("character", stateObj.end);
	                range.moveStart("character", stateObj.start);
	                range.select();
	            }
	        };
	
	        this.setInputAreaSelectionStartEnd = function () {
	
	            if (!panels.ieCachedRange && (inputArea.selectionStart || inputArea.selectionStart === 0)) {
	
	                stateObj.start = inputArea.selectionStart;
	                stateObj.end = inputArea.selectionEnd;
	            } else if (doc.selection) {
	
	                stateObj.text = util.fixEolChars(inputArea.value);
	
	                // IE loses the selection in the textarea when buttons are
	                // clicked.  On IE we cache the selection. Here, if something is cached,
	                // we take it.
	                var range = panels.ieCachedRange || doc.selection.createRange();
	
	                var fixedRange = util.fixEolChars(range.text);
	                var marker = "\x07";
	                var markedRange = marker + fixedRange + marker;
	                range.text = markedRange;
	                var inputText = util.fixEolChars(inputArea.value);
	
	                range.moveStart("character", -markedRange.length);
	                range.text = fixedRange;
	
	                stateObj.start = inputText.indexOf(marker);
	                stateObj.end = inputText.lastIndexOf(marker) - marker.length;
	
	                var len = stateObj.text.length - util.fixEolChars(inputArea.value).length;
	
	                if (len) {
	                    range.moveStart("character", -fixedRange.length);
	                    while (len--) {
	                        fixedRange += "\n";
	                        stateObj.end += 1;
	                    }
	                    range.text = fixedRange;
	                }
	
	                if (panels.ieCachedRange) stateObj.scrollTop = panels.ieCachedScrollTop; // this is set alongside with ieCachedRange
	
	                panels.ieCachedRange = null;
	
	                this.setInputAreaSelection();
	            }
	        };
	
	        // Restore this state into the input area.
	        this.restore = function () {
	
	            if (stateObj.text != undefined && stateObj.text != inputArea.value) {
	                inputArea.value = stateObj.text;
	            }
	            this.setInputAreaSelection();
	            inputArea.scrollTop = stateObj.scrollTop;
	        };
	
	        // Gets a collection of HTML chunks from the inptut textarea.
	        this.getChunks = function () {
	
	            var chunk = new Chunks();
	            chunk.before = util.fixEolChars(stateObj.text.substring(0, stateObj.start));
	            chunk.startTag = "";
	            chunk.selection = util.fixEolChars(stateObj.text.substring(stateObj.start, stateObj.end));
	            chunk.endTag = "";
	            chunk.after = util.fixEolChars(stateObj.text.substring(stateObj.end));
	            chunk.scrollTop = stateObj.scrollTop;
	
	            return chunk;
	        };
	
	        // Sets the TextareaState properties given a chunk of markdown.
	        this.setChunks = function (chunk) {
	
	            chunk.before = chunk.before + chunk.startTag;
	            chunk.after = chunk.endTag + chunk.after;
	
	            this.start = chunk.before.length;
	            this.end = chunk.before.length + chunk.selection.length;
	            this.text = chunk.before + chunk.selection + chunk.after;
	            this.scrollTop = chunk.scrollTop;
	        };
	        this.init();
	    };
	
	    function PreviewManager(converter, panels, previewRefreshCallback) {
	
	        var managerObj = this;
	        var timeout;
	        var elapsedTime;
	        var oldInputText;
	        var maxDelay = 3000;
	        var startType = "delayed"; // The other legal value is "manual"
	
	        // Adds event listeners to elements
	        var setupEvents = function setupEvents(inputElem, listener) {
	
	            util.addEvent(inputElem, "input", listener);
	            inputElem.onpaste = listener;
	            inputElem.ondrop = listener;
	
	            util.addEvent(inputElem, "keypress", listener);
	            util.addEvent(inputElem, "keydown", listener);
	        };
	
	        var getDocScrollTop = function getDocScrollTop() {
	
	            var result = 0;
	
	            if (window.innerHeight) {
	                result = window.pageYOffset;
	            } else if (doc.documentElement && doc.documentElement.scrollTop) {
	                result = doc.documentElement.scrollTop;
	            } else if (doc.body) {
	                result = doc.body.scrollTop;
	            }
	
	            return result;
	        };
	
	        var makePreviewHtml = function makePreviewHtml() {
	
	            // If there is no registered preview panel
	            // there is nothing to do.
	            if (!panels.preview) return;
	
	            var text = panels.input.value;
	            if (text && text == oldInputText) {
	                return; // Input text hasn't changed.
	            } else {
	                oldInputText = text;
	            }
	
	            var prevTime = new Date().getTime();
	
	            text = converter.makeHtml(text);
	
	            // Calculate the processing time of the HTML creation.
	            // It's used as the delay time in the event listener.
	            var currTime = new Date().getTime();
	            elapsedTime = currTime - prevTime;
	
	            pushPreviewHtml(text);
	        };
	
	        // setTimeout is already used.  Used as an event listener.
	        var applyTimeout = function applyTimeout() {
	
	            if (timeout) {
	                clearTimeout(timeout);
	                timeout = undefined;
	            }
	
	            if (startType !== "manual") {
	
	                var delay = 0;
	
	                if (startType === "delayed") {
	                    delay = elapsedTime;
	                }
	
	                if (delay > maxDelay) {
	                    delay = maxDelay;
	                }
	                timeout = setTimeout(makePreviewHtml, delay);
	            }
	        };
	
	        var getScaleFactor = function getScaleFactor(panel) {
	            if (panel.scrollHeight <= panel.clientHeight) {
	                return 1;
	            }
	            return panel.scrollTop / (panel.scrollHeight - panel.clientHeight);
	        };
	
	        var setPanelScrollTops = function setPanelScrollTops() {
	            if (panels.preview) {
	                panels.preview.scrollTop = (panels.preview.scrollHeight - panels.preview.clientHeight) * getScaleFactor(panels.preview);
	            }
	        };
	
	        this.refresh = function (requiresRefresh) {
	
	            if (requiresRefresh) {
	                oldInputText = "";
	                makePreviewHtml();
	            } else {
	                applyTimeout();
	            }
	        };
	
	        this.processingTime = function () {
	            return elapsedTime;
	        };
	
	        var isFirstTimeFilled = true;
	
	        // IE doesn't let you use innerHTML if the element is contained somewhere in a table
	        // (which is the case for inline editing) -- in that case, detach the element, set the
	        // value, and reattach. Yes, that *is* ridiculous.
	        var ieSafePreviewSet = function ieSafePreviewSet(text) {
	            var preview = panels.preview;
	            var parent = preview.parentNode;
	            var sibling = preview.nextSibling;
	            parent.removeChild(preview);
	            preview.innerHTML = text;
	            if (!sibling) parent.appendChild(preview);else parent.insertBefore(preview, sibling);
	        };
	
	        var nonSuckyBrowserPreviewSet = function nonSuckyBrowserPreviewSet(text) {
	            panels.preview.innerHTML = text;
	        };
	
	        var previewSetter;
	
	        var previewSet = function previewSet(text) {
	            if (previewSetter) return previewSetter(text);
	
	            try {
	                nonSuckyBrowserPreviewSet(text);
	                previewSetter = nonSuckyBrowserPreviewSet;
	            } catch (e) {
	                previewSetter = ieSafePreviewSet;
	                previewSetter(text);
	            }
	        };
	
	        var pushPreviewHtml = function pushPreviewHtml(text) {
	
	            var emptyTop = position.getTop(panels.input) - getDocScrollTop();
	
	            if (panels.preview) {
	                previewSet(text);
	                previewRefreshCallback();
	            }
	
	            setPanelScrollTops();
	
	            if (isFirstTimeFilled) {
	                isFirstTimeFilled = false;
	                return;
	            }
	
	            var fullTop = position.getTop(panels.input) - getDocScrollTop();
	
	            if (uaSniffed.isIE) {
	                setTimeout(function () {
	                    window.scrollBy(0, fullTop - emptyTop);
	                }, 0);
	            } else {
	                window.scrollBy(0, fullTop - emptyTop);
	            }
	        };
	
	        var init = function init() {
	
	            setupEvents(panels.input, applyTimeout);
	            makePreviewHtml();
	
	            if (panels.preview) {
	                panels.preview.scrollTop = 0;
	            }
	        };
	
	        init();
	    };
	
	    // Creates the background behind the hyperlink text entry box.
	    // And download dialog
	    // Most of this has been moved to CSS but the div creation and
	    // browser-specific hacks remain here.
	    ui.createBackground = function () {
	
	        var background = doc.createElement("div"),
	            style = background.style;
	
	        background.className = "wmd-prompt-background";
	
	        style.position = "absolute";
	        style.top = "0";
	
	        style.zIndex = "1000";
	
	        if (uaSniffed.isIE) {
	            style.filter = "alpha(opacity=50)";
	        } else {
	            style.opacity = "0.5";
	        }
	
	        var pageSize = position.getPageSize();
	        style.height = pageSize[1] + "px";
	
	        if (uaSniffed.isIE) {
	            style.left = doc.documentElement.scrollLeft;
	            style.width = doc.documentElement.clientWidth;
	        } else {
	            style.left = "0";
	            style.width = "100%";
	        }
	
	        doc.body.appendChild(background);
	        return background;
	    };
	
	    // This simulates a modal dialog box and asks for the URL when you
	    // click the hyperlink or image buttons.
	    //
	    // text: The html for the input box.
	    // defaultInputText: The default value that appears in the input box.
	    // callback: The function which is executed when the prompt is dismissed, either via OK or Cancel.
	    //      It receives a single argument; either the entered text (if OK was chosen) or null (if Cancel
	    //      was chosen).
	    ui.prompt = function (text, defaultInputText, callback) {
	
	        // These variables need to be declared at this level since they are used
	        // in multiple functions.
	        var dialog; // The dialog box.
	        var input; // The text box where you enter the hyperlink.
	
	
	        if (defaultInputText === undefined) {
	            defaultInputText = "";
	        }
	
	        // Used as a keydown event handler. Esc dismisses the prompt.
	        // Key code 27 is ESC.
	        var checkEscape = function checkEscape(key) {
	            var code = key.charCode || key.keyCode;
	            if (code === 27) {
	                close(true);
	            }
	        };
	
	        // Dismisses the hyperlink input box.
	        // isCancel is true if we don't care about the input text.
	        // isCancel is false if we are going to keep the text.
	        var close = function close(isCancel) {
	            util.removeEvent(doc.body, "keydown", checkEscape);
	            var text = input.value;
	
	            if (isCancel) {
	                text = null;
	            } else {
	                // Fixes common pasting errors.
	                text = text.replace(/^http:\/\/(https?|ftp):\/\//, '$1://');
	                if (!/^(?:https?|ftp):\/\//.test(text)) text = 'http://' + text;
	            }
	
	            dialog.parentNode.removeChild(dialog);
	
	            callback(text);
	            return false;
	        };
	
	        // Create the text input box form/window.
	        var createDialog = function createDialog() {
	
	            // The main dialog box.
	            dialog = doc.createElement("div");
	            dialog.className = "wmd-prompt-dialog";
	            dialog.style.padding = "10px;";
	            dialog.style.position = "fixed";
	            dialog.style.width = "400px";
	            dialog.style.zIndex = "1001";
	
	            // The dialog text.
	            var question = doc.createElement("div");
	            question.innerHTML = text;
	            question.style.padding = "5px";
	            dialog.appendChild(question);
	
	            // The web form container for the text box and buttons.
	            var form = doc.createElement("form"),
	                style = form.style;
	            form.onsubmit = function () {
	                return close(false);
	            };
	            style.padding = "0";
	            style.margin = "0";
	            style.cssFloat = "left";
	            style.width = "100%";
	            style.textAlign = "center";
	            style.position = "relative";
	            dialog.appendChild(form);
	
	            // The input text box
	            input = doc.createElement("input");
	            input.type = "text";
	            input.value = defaultInputText;
	            style = input.style;
	            style.display = "block";
	            style.width = "80%";
	            style.marginLeft = style.marginRight = "auto";
	            form.appendChild(input);
	
	            // The ok button
	            var okButton = doc.createElement("input");
	            okButton.type = "button";
	            okButton.onclick = function () {
	                return close(false);
	            };
	            okButton.value = "OK";
	            style = okButton.style;
	            style.margin = "10px";
	            style.display = "inline";
	            style.width = "7em";
	
	            // The cancel button
	            var cancelButton = doc.createElement("input");
	            cancelButton.type = "button";
	            cancelButton.onclick = function () {
	                return close(true);
	            };
	            cancelButton.value = "Cancel";
	            style = cancelButton.style;
	            style.margin = "10px";
	            style.display = "inline";
	            style.width = "7em";
	
	            form.appendChild(okButton);
	            form.appendChild(cancelButton);
	
	            util.addEvent(doc.body, "keydown", checkEscape);
	            dialog.style.top = "50%";
	            dialog.style.left = "50%";
	            dialog.style.display = "block";
	            if (uaSniffed.isIE_5or6) {
	                dialog.style.position = "absolute";
	                dialog.style.top = doc.documentElement.scrollTop + 200 + "px";
	                dialog.style.left = "50%";
	            }
	            doc.body.appendChild(dialog);
	
	            // This has to be done AFTER adding the dialog to the form if you
	            // want it to be centered.
	            dialog.style.marginTop = -(position.getHeight(dialog) / 2) + "px";
	            dialog.style.marginLeft = -(position.getWidth(dialog) / 2) + "px";
	        };
	
	        // Why is this in a zero-length timeout?
	        // Is it working around a browser bug?
	        setTimeout(function () {
	
	            createDialog();
	
	            var defTextLen = defaultInputText.length;
	            if (input.selectionStart !== undefined) {
	                input.selectionStart = 0;
	                input.selectionEnd = defTextLen;
	            } else if (input.createTextRange) {
	                var range = input.createTextRange();
	                range.collapse(false);
	                range.moveStart("character", -defTextLen);
	                range.moveEnd("character", defTextLen);
	                range.select();
	            }
	
	            input.focus();
	        }, 0);
	    };
	
	    function UIManager(postfix, panels, undoManager, previewManager, commandManager, helpOptions, getString) {
	
	        var inputBox = panels.input,
	            buttons = {}; // buttons.undo, buttons.link, etc. The actual DOM elements.
	
	        makeSpritedButtonRow();
	
	        var keyEvent = "keydown";
	        if (uaSniffed.isOpera) {
	            keyEvent = "keypress";
	        }
	
	        util.addEvent(inputBox, keyEvent, function (key) {
	
	            // Check to see if we have a button key and, if so execute the callback.
	            if ((key.ctrlKey || key.metaKey) && !key.altKey && !key.shiftKey) {
	
	                var keyCode = key.charCode || key.keyCode;
	                var keyCodeStr = String.fromCharCode(keyCode).toLowerCase();
	
	                switch (keyCodeStr) {
	                    case "b":
	                        doClick(buttons.bold);
	                        break;
	                    case "i":
	                        doClick(buttons.italic);
	                        break;
	                    case "l":
	                        doClick(buttons.link);
	                        break;
	                    case "q":
	                        doClick(buttons.quote);
	                        break;
	                    case "k":
	                        doClick(buttons.code);
	                        break;
	                    case "g":
	                        doClick(buttons.image);
	                        break;
	                    case "o":
	                        doClick(buttons.olist);
	                        break;
	                    case "u":
	                        doClick(buttons.ulist);
	                        break;
	                    case "h":
	                        doClick(buttons.heading);
	                        break;
	                    case "r":
	                        doClick(buttons.hr);
	                        break;
	                    case "y":
	                        doClick(buttons.redo);
	                        break;
	                    case "z":
	                        if (key.shiftKey) {
	                            doClick(buttons.redo);
	                        } else {
	                            doClick(buttons.undo);
	                        }
	                        break;
	                    default:
	                        return;
	                }
	
	                if (key.preventDefault) {
	                    key.preventDefault();
	                }
	
	                if (window.event) {
	                    window.event.returnValue = false;
	                }
	            }
	        });
	
	        // Auto-indent on shift-enter
	        util.addEvent(inputBox, "keyup", function (key) {
	            if (key.shiftKey && !key.ctrlKey && !key.metaKey) {
	                var keyCode = key.charCode || key.keyCode;
	                // Character 13 is Enter
	                if (keyCode === 13) {
	                    var fakeButton = {};
	                    fakeButton.textOp = bindCommand("doAutoindent");
	                    doClick(fakeButton);
	                }
	            }
	        });
	
	        // special handler because IE clears the context of the textbox on ESC
	        if (uaSniffed.isIE) {
	            util.addEvent(inputBox, "keydown", function (key) {
	                var code = key.keyCode;
	                if (code === 27) {
	                    return false;
	                }
	            });
	        }
	
	        // Perform the button's action.
	        function doClick(button) {
	
	            inputBox.focus();
	
	            if (button.textOp) {
	
	                if (undoManager) {
	                    undoManager.setCommandMode();
	                }
	
	                var state = new TextareaState(panels);
	
	                if (!state) {
	                    return;
	                }
	
	                var chunks = state.getChunks();
	
	                // Some commands launch a "modal" prompt dialog.  Javascript
	                // can't really make a modal dialog box and the WMD code
	                // will continue to execute while the dialog is displayed.
	                // This prevents the dialog pattern I'm used to and means
	                // I can't do something like this:
	                //
	                // var link = CreateLinkDialog();
	                // makeMarkdownLink(link);
	                //
	                // Instead of this straightforward method of handling a
	                // dialog I have to pass any code which would execute
	                // after the dialog is dismissed (e.g. link creation)
	                // in a function parameter.
	                //
	                // Yes this is awkward and I think it sucks, but there's
	                // no real workaround.  Only the image and link code
	                // create dialogs and require the function pointers.
	                var fixupInputArea = function fixupInputArea() {
	
	                    inputBox.focus();
	
	                    if (chunks) {
	                        state.setChunks(chunks);
	                    }
	
	                    state.restore();
	                    previewManager.refresh();
	                };
	
	                var noCleanup = button.textOp(chunks, fixupInputArea);
	
	                if (!noCleanup) {
	                    fixupInputArea();
	                }
	            }
	
	            if (button.execute) {
	                button.execute(undoManager);
	            }
	        };
	
	        function setupButton(button, isEnabled) {
	
	            var normalYShift = "0px";
	            var disabledYShift = "-20px";
	            var highlightYShift = "-40px";
	            var image = button.getElementsByTagName("span")[0];
	            if (isEnabled) {
	                image.style.backgroundPosition = button.XShift + " " + normalYShift;
	                button.onmouseover = function () {
	                    image.style.backgroundPosition = this.XShift + " " + highlightYShift;
	                };
	
	                button.onmouseout = function () {
	                    image.style.backgroundPosition = this.XShift + " " + normalYShift;
	                };
	
	                // IE tries to select the background image "button" text (it's
	                // implemented in a list item) so we have to cache the selection
	                // on mousedown.
	                if (uaSniffed.isIE) {
	                    button.onmousedown = function () {
	                        if (doc.activeElement && doc.activeElement !== panels.input) {
	                            // we're not even in the input box, so there's no selection
	                            return;
	                        }
	                        panels.ieCachedRange = document.selection.createRange();
	                        panels.ieCachedScrollTop = panels.input.scrollTop;
	                    };
	                }
	
	                if (!button.isHelp) {
	                    button.onclick = function () {
	                        if (this.onmouseout) {
	                            this.onmouseout();
	                        }
	                        doClick(this);
	                        return false;
	                    };
	                }
	            } else {
	                image.style.backgroundPosition = button.XShift + " " + disabledYShift;
	                button.onmouseover = button.onmouseout = button.onclick = function () {};
	            }
	        }
	
	        function bindCommand(method) {
	            if (typeof method === "string") method = commandManager[method];
	            return function () {
	                method.apply(commandManager, arguments);
	            };
	        }
	
	        function makeSpritedButtonRow() {
	
	            var buttonBar = panels.buttonBar;
	
	            var normalYShift = "0px";
	            var disabledYShift = "-20px";
	            var highlightYShift = "-40px";
	
	            var buttonRow = document.createElement("ul");
	            buttonRow.id = "wmd-button-row" + postfix;
	            buttonRow.className = 'wmd-button-row';
	            buttonRow = buttonBar.appendChild(buttonRow);
	            var xPosition = 0;
	            var makeButton = function makeButton(id, title, XShift, textOp) {
	                var button = document.createElement("li");
	                button.className = "wmd-button";
	                button.style.left = xPosition + "px";
	                xPosition += 25;
	                var buttonImage = document.createElement("span");
	                button.id = id + postfix;
	                button.appendChild(buttonImage);
	                button.title = title;
	                button.XShift = XShift;
	                if (textOp) button.textOp = textOp;
	                setupButton(button, true);
	                buttonRow.appendChild(button);
	                return button;
	            };
	            var makeSpacer = function makeSpacer(num) {
	                var spacer = document.createElement("li");
	                spacer.className = "wmd-spacer wmd-spacer" + num;
	                spacer.id = "wmd-spacer" + num + postfix;
	                buttonRow.appendChild(spacer);
	                xPosition += 25;
	            };
	
	            buttons.bold = makeButton("wmd-bold-button", getString("bold"), "0px", bindCommand("doBold"));
	            buttons.italic = makeButton("wmd-italic-button", getString("italic"), "-20px", bindCommand("doItalic"));
	            makeSpacer(1);
	            buttons.link = makeButton("wmd-link-button", getString("link"), "-40px", bindCommand(function (chunk, postProcessing) {
	                return this.doLinkOrImage(chunk, postProcessing, false);
	            }));
	            buttons.quote = makeButton("wmd-quote-button", getString("quote"), "-60px", bindCommand("doBlockquote"));
	            buttons.code = makeButton("wmd-code-button", getString("code"), "-80px", bindCommand("doCode"));
	            buttons.image = makeButton("wmd-image-button", getString("image"), "-100px", bindCommand(function (chunk, postProcessing) {
	                return this.doLinkOrImage(chunk, postProcessing, true);
	            }));
	            makeSpacer(2);
	            buttons.olist = makeButton("wmd-olist-button", getString("olist"), "-120px", bindCommand(function (chunk, postProcessing) {
	                this.doList(chunk, postProcessing, true);
	            }));
	            buttons.ulist = makeButton("wmd-ulist-button", getString("ulist"), "-140px", bindCommand(function (chunk, postProcessing) {
	                this.doList(chunk, postProcessing, false);
	            }));
	            buttons.heading = makeButton("wmd-heading-button", getString("heading"), "-160px", bindCommand("doHeading"));
	            buttons.hr = makeButton("wmd-hr-button", getString("hr"), "-180px", bindCommand("doHorizontalRule"));
	            makeSpacer(3);
	            buttons.undo = makeButton("wmd-undo-button", getString("undo"), "-200px", null);
	            buttons.undo.execute = function (manager) {
	                if (manager) manager.undo();
	            };
	
	            var redoTitle = /win/.test(nav.platform.toLowerCase()) ? getString("redo") : getString("redomac"); // mac and other non-Windows platforms
	
	            buttons.redo = makeButton("wmd-redo-button", redoTitle, "-220px", null);
	            buttons.redo.execute = function (manager) {
	                if (manager) manager.redo();
	            };
	
	            if (helpOptions) {
	                var helpButton = document.createElement("li");
	                var helpButtonImage = document.createElement("span");
	                helpButton.appendChild(helpButtonImage);
	                helpButton.className = "wmd-button wmd-help-button";
	                helpButton.id = "wmd-help-button" + postfix;
	                helpButton.XShift = "-240px";
	                helpButton.isHelp = true;
	                helpButton.style.right = "0px";
	                helpButton.title = getString("help");
	                helpButton.onclick = helpOptions.handler;
	
	                setupButton(helpButton, true);
	                buttonRow.appendChild(helpButton);
	                buttons.help = helpButton;
	            }
	
	            setUndoRedoButtonStates();
	        }
	
	        function setUndoRedoButtonStates() {
	            if (undoManager) {
	                setupButton(buttons.undo, undoManager.canUndo());
	                setupButton(buttons.redo, undoManager.canRedo());
	            }
	        };
	
	        this.setUndoRedoButtonStates = setUndoRedoButtonStates;
	    }
	
	    function CommandManager(pluginHooks, getString) {
	        this.hooks = pluginHooks;
	        this.getString = getString;
	    }
	
	    var commandProto = CommandManager.prototype;
	
	    // The markdown symbols - 4 spaces = code, > = blockquote, etc.
	    commandProto.prefixes = "(?:\\s{4,}|\\s*>|\\s*-\\s+|\\s*\\d+\\.|=|\\+|-|_|\\*|#|\\s*\\[[^\n]]+\\]:)";
	
	    // Remove markdown symbols from the chunk selection.
	    commandProto.unwrap = function (chunk) {
	        var txt = new re("([^\\n])\\n(?!(\\n|" + this.prefixes + "))", "g");
	        chunk.selection = chunk.selection.replace(txt, "$1 $2");
	    };
	
	    commandProto.wrap = function (chunk, len) {
	        this.unwrap(chunk);
	        var regex = new re("(.{1," + len + "})( +|$\\n?)", "gm"),
	            that = this;
	
	        chunk.selection = chunk.selection.replace(regex, function (line, marked) {
	            if (new re("^" + that.prefixes, "").test(line)) {
	                return line;
	            }
	            return marked + "\n";
	        });
	
	        chunk.selection = chunk.selection.replace(/\s+$/, "");
	    };
	
	    commandProto.doBold = function (chunk, postProcessing) {
	        return this.doBorI(chunk, postProcessing, 2, this.getString("boldexample"));
	    };
	
	    commandProto.doItalic = function (chunk, postProcessing) {
	        return this.doBorI(chunk, postProcessing, 1, this.getString("italicexample"));
	    };
	
	    // chunk: The selected region that will be enclosed with */**
	    // nStars: 1 for italics, 2 for bold
	    // insertText: If you just click the button without highlighting text, this gets inserted
	    commandProto.doBorI = function (chunk, postProcessing, nStars, insertText) {
	
	        // Get rid of whitespace and fixup newlines.
	        chunk.trimWhitespace();
	        chunk.selection = chunk.selection.replace(/\n{2,}/g, "\n");
	
	        // Look for stars before and after.  Is the chunk already marked up?
	        // note that these regex matches cannot fail
	        var starsBefore = /(\**$)/.exec(chunk.before)[0];
	        var starsAfter = /(^\**)/.exec(chunk.after)[0];
	
	        var prevStars = Math.min(starsBefore.length, starsAfter.length);
	
	        // Remove stars if we have to since the button acts as a toggle.
	        if (prevStars >= nStars && (prevStars != 2 || nStars != 1)) {
	            chunk.before = chunk.before.replace(re("[*]{" + nStars + "}$", ""), "");
	            chunk.after = chunk.after.replace(re("^[*]{" + nStars + "}", ""), "");
	        } else if (!chunk.selection && starsAfter) {
	            // It's not really clear why this code is necessary.  It just moves
	            // some arbitrary stuff around.
	            chunk.after = chunk.after.replace(/^([*_]*)/, "");
	            chunk.before = chunk.before.replace(/(\s?)$/, "");
	            var whitespace = re.$1;
	            chunk.before = chunk.before + starsAfter + whitespace;
	        } else {
	
	            // In most cases, if you don't have any selected text and click the button
	            // you'll get a selected, marked up region with the default text inserted.
	            if (!chunk.selection && !starsAfter) {
	                chunk.selection = insertText;
	            }
	
	            // Add the true markup.
	            var markup = nStars <= 1 ? "*" : "**"; // shouldn't the test be = ?
	            chunk.before = chunk.before + markup;
	            chunk.after = markup + chunk.after;
	        }
	
	        return;
	    };
	
	    commandProto.stripLinkDefs = function (text, defsToAdd) {
	
	        text = text.replace(/^[ ]{0,3}\[(\d+)\]:[ \t]*\n?[ \t]*<?(\S+?)>?[ \t]*\n?[ \t]*(?:(\n*)["(](.+?)[")][ \t]*)?(?:\n+|$)/gm, function (totalMatch, id, link, newlines, title) {
	            defsToAdd[id] = totalMatch.replace(/\s*$/, "");
	            if (newlines) {
	                // Strip the title and return that separately.
	                defsToAdd[id] = totalMatch.replace(/["(](.+?)[")]$/, "");
	                return newlines + title;
	            }
	            return "";
	        });
	
	        return text;
	    };
	
	    commandProto.addLinkDef = function (chunk, linkDef) {
	
	        var refNumber = 0; // The current reference number
	        var defsToAdd = {}; //
	        // Start with a clean slate by removing all previous link definitions.
	        chunk.before = this.stripLinkDefs(chunk.before, defsToAdd);
	        chunk.selection = this.stripLinkDefs(chunk.selection, defsToAdd);
	        chunk.after = this.stripLinkDefs(chunk.after, defsToAdd);
	
	        var defs = "";
	        var regex = /(\[)((?:\[[^\]]*\]|[^\[\]])*)(\][ ]?(?:\n[ ]*)?\[)(\d+)(\])/g;
	
	        var addDefNumber = function addDefNumber(def) {
	            refNumber++;
	            def = def.replace(/^[ ]{0,3}\[(\d+)\]:/, "  [" + refNumber + "]:");
	            defs += "\n" + def;
	        };
	
	        // note that
	        // a) the recursive call to getLink cannot go infinite, because by definition
	        //    of regex, inner is always a proper substring of wholeMatch, and
	        // b) more than one level of nesting is neither supported by the regex
	        //    nor making a lot of sense (the only use case for nesting is a linked image)
	        var getLink = function getLink(wholeMatch, before, inner, afterInner, id, end) {
	            inner = inner.replace(regex, getLink);
	            if (defsToAdd[id]) {
	                addDefNumber(defsToAdd[id]);
	                return before + inner + afterInner + refNumber + end;
	            }
	            return wholeMatch;
	        };
	
	        chunk.before = chunk.before.replace(regex, getLink);
	
	        if (linkDef) {
	            addDefNumber(linkDef);
	        } else {
	            chunk.selection = chunk.selection.replace(regex, getLink);
	        }
	
	        var refOut = refNumber;
	
	        chunk.after = chunk.after.replace(regex, getLink);
	
	        if (chunk.after) {
	            chunk.after = chunk.after.replace(/\n*$/, "");
	        }
	        if (!chunk.after) {
	            chunk.selection = chunk.selection.replace(/\n*$/, "");
	        }
	
	        chunk.after += "\n\n" + defs;
	
	        return refOut;
	    };
	
	    // takes the line as entered into the add link/as image dialog and makes
	    // sure the URL and the optinal title are "nice".
	    function properlyEncoded(linkdef) {
	        return linkdef.replace(/^\s*(.*?)(?:\s+"(.+)")?\s*$/, function (wholematch, link, title) {
	            link = link.replace(/\?.*$/, function (querypart) {
	                return querypart.replace(/\+/g, " "); // in the query string, a plus and a space are identical
	            });
	            link = decodeURIComponent(link); // unencode first, to prevent double encoding
	            link = encodeURI(link).replace(/'/g, '%27').replace(/\(/g, '%28').replace(/\)/g, '%29');
	            link = link.replace(/\?.*$/, function (querypart) {
	                return querypart.replace(/\+/g, "%2b"); // since we replaced plus with spaces in the query part, all pluses that now appear where originally encoded
	            });
	            if (title) {
	                title = title.trim ? title.trim() : title.replace(/^\s*/, "").replace(/\s*$/, "");
	                title = title.replace(/"/g, "quot;").replace(/\(/g, "&#40;").replace(/\)/g, "&#41;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
	            }
	            return title ? link + ' "' + title + '"' : link;
	        });
	    }
	
	    commandProto.doLinkOrImage = function (chunk, postProcessing, isImage) {
	
	        chunk.trimWhitespace();
	        chunk.findTags(/\s*!?\[/, /\][ ]?(?:\n[ ]*)?(\[.*?\])?/);
	        var background;
	
	        if (chunk.endTag.length > 1 && chunk.startTag.length > 0) {
	
	            chunk.startTag = chunk.startTag.replace(/!?\[/, "");
	            chunk.endTag = "";
	            this.addLinkDef(chunk, null);
	        } else {
	
	            // We're moving start and end tag back into the selection, since (as we're in the else block) we're not
	            // *removing* a link, but *adding* one, so whatever findTags() found is now back to being part of the
	            // link text. linkEnteredCallback takes care of escaping any brackets.
	            chunk.selection = chunk.startTag + chunk.selection + chunk.endTag;
	            chunk.startTag = chunk.endTag = "";
	
	            if (/\n\n/.test(chunk.selection)) {
	                this.addLinkDef(chunk, null);
	                return;
	            }
	            var that = this;
	            // The function to be executed when you enter a link and press OK or Cancel.
	            // Marks up the link and adds the ref.
	            var linkEnteredCallback = function linkEnteredCallback(link) {
	
	                background.parentNode.removeChild(background);
	
	                if (link !== null) {
	                    // (                          $1
	                    //     [^\\]                  anything that's not a backslash
	                    //     (?:\\\\)*              an even number (this includes zero) of backslashes
	                    // )
	                    // (?=                        followed by
	                    //     [[\]]                  an opening or closing bracket
	                    // )
	                    //
	                    // In other words, a non-escaped bracket. These have to be escaped now to make sure they
	                    // don't count as the end of the link or similar.
	                    // Note that the actual bracket has to be a lookahead, because (in case of to subsequent brackets),
	                    // the bracket in one match may be the "not a backslash" character in the next match, so it
	                    // should not be consumed by the first match.
	                    // The "prepend a space and finally remove it" steps makes sure there is a "not a backslash" at the
	                    // start of the string, so this also works if the selection begins with a bracket. We cannot solve
	                    // this by anchoring with ^, because in the case that the selection starts with two brackets, this
	                    // would mean a zero-width match at the start. Since zero-width matches advance the string position,
	                    // the first bracket could then not act as the "not a backslash" for the second.
	                    chunk.selection = (" " + chunk.selection).replace(/([^\\](?:\\\\)*)(?=[[\]])/g, "$1\\").substr(1);
	
	                    var linkDef = " [999]: " + properlyEncoded(link);
	
	                    var num = that.addLinkDef(chunk, linkDef);
	                    chunk.startTag = isImage ? "![" : "[";
	                    chunk.endTag = "][" + num + "]";
	
	                    if (!chunk.selection) {
	                        if (isImage) {
	                            chunk.selection = that.getString("imagedescription");
	                        } else {
	                            chunk.selection = that.getString("linkdescription");
	                        }
	                    }
	                }
	                postProcessing();
	            };
	
	            background = ui.createBackground();
	
	            if (isImage) {
	                if (!this.hooks.insertImageDialog(linkEnteredCallback)) ui.prompt(this.getString("imagedialog"), imageDefaultText, linkEnteredCallback);
	            } else {
	                ui.prompt(this.getString("linkdialog"), linkDefaultText, linkEnteredCallback);
	            }
	            return true;
	        }
	    };
	
	    // When making a list, hitting shift-enter will put your cursor on the next line
	    // at the current indent level.
	    commandProto.doAutoindent = function (chunk, postProcessing) {
	
	        var commandMgr = this,
	            fakeSelection = false;
	
	        chunk.before = chunk.before.replace(/(\n|^)[ ]{0,3}([*+-]|\d+[.])[ \t]*\n$/, "\n\n");
	        chunk.before = chunk.before.replace(/(\n|^)[ ]{0,3}>[ \t]*\n$/, "\n\n");
	        chunk.before = chunk.before.replace(/(\n|^)[ \t]+\n$/, "\n\n");
	
	        // There's no selection, end the cursor wasn't at the end of the line:
	        // The user wants to split the current list item / code line / blockquote line
	        // (for the latter it doesn't really matter) in two. Temporarily select the
	        // (rest of the) line to achieve this.
	        if (!chunk.selection && !/^[ \t]*(?:\n|$)/.test(chunk.after)) {
	            chunk.after = chunk.after.replace(/^[^\n]*/, function (wholeMatch) {
	                chunk.selection = wholeMatch;
	                return "";
	            });
	            fakeSelection = true;
	        }
	
	        if (/(\n|^)[ ]{0,3}([*+-]|\d+[.])[ \t]+.*\n$/.test(chunk.before)) {
	            if (commandMgr.doList) {
	                commandMgr.doList(chunk);
	            }
	        }
	        if (/(\n|^)[ ]{0,3}>[ \t]+.*\n$/.test(chunk.before)) {
	            if (commandMgr.doBlockquote) {
	                commandMgr.doBlockquote(chunk);
	            }
	        }
	        if (/(\n|^)(\t|[ ]{4,}).*\n$/.test(chunk.before)) {
	            if (commandMgr.doCode) {
	                commandMgr.doCode(chunk);
	            }
	        }
	
	        if (fakeSelection) {
	            chunk.after = chunk.selection + chunk.after;
	            chunk.selection = "";
	        }
	    };
	
	    commandProto.doBlockquote = function (chunk, postProcessing) {
	
	        chunk.selection = chunk.selection.replace(/^(\n*)([^\r]+?)(\n*)$/, function (totalMatch, newlinesBefore, text, newlinesAfter) {
	            chunk.before += newlinesBefore;
	            chunk.after = newlinesAfter + chunk.after;
	            return text;
	        });
	
	        chunk.before = chunk.before.replace(/(>[ \t]*)$/, function (totalMatch, blankLine) {
	            chunk.selection = blankLine + chunk.selection;
	            return "";
	        });
	
	        chunk.selection = chunk.selection.replace(/^(\s|>)+$/, "");
	        chunk.selection = chunk.selection || this.getString("quoteexample");
	
	        // The original code uses a regular expression to find out how much of the
	        // text *directly before* the selection already was a blockquote:
	
	        /*
	        if (chunk.before) {
	        chunk.before = chunk.before.replace(/\n?$/, "\n");
	        }
	        chunk.before = chunk.before.replace(/(((\n|^)(\n[ \t]*)*>(.+\n)*.*)+(\n[ \t]*)*$)/,
	        function (totalMatch) {
	        chunk.startTag = totalMatch;
	        return "";
	        });
	        */
	
	        // This comes down to:
	        // Go backwards as many lines a possible, such that each line
	        //  a) starts with ">", or
	        //  b) is almost empty, except for whitespace, or
	        //  c) is preceeded by an unbroken chain of non-empty lines
	        //     leading up to a line that starts with ">" and at least one more character
	        // and in addition
	        //  d) at least one line fulfills a)
	        //
	        // Since this is essentially a backwards-moving regex, it's susceptible to
	        // catstrophic backtracking and can cause the browser to hang;
	        // see e.g. http://meta.stackoverflow.com/questions/9807.
	        //
	        // Hence we replaced this by a simple state machine that just goes through the
	        // lines and checks for a), b), and c).
	
	        var match = "",
	            leftOver = "",
	            line;
	        if (chunk.before) {
	            var lines = chunk.before.replace(/\n$/, "").split("\n");
	            var inChain = false;
	            for (var i = 0; i < lines.length; i++) {
	                var good = false;
	                line = lines[i];
	                inChain = inChain && line.length > 0; // c) any non-empty line continues the chain
	                if (/^>/.test(line)) {
	                    // a)
	                    good = true;
	                    if (!inChain && line.length > 1) // c) any line that starts with ">" and has at least one more character starts the chain
	                        inChain = true;
	                } else if (/^[ \t]*$/.test(line)) {
	                    // b)
	                    good = true;
	                } else {
	                    good = inChain; // c) the line is not empty and does not start with ">", so it matches if and only if we're in the chain
	                }
	                if (good) {
	                    match += line + "\n";
	                } else {
	                    leftOver += match + line;
	                    match = "\n";
	                }
	            }
	            if (!/(^|\n)>/.test(match)) {
	                // d)
	                leftOver += match;
	                match = "";
	            }
	        }
	
	        chunk.startTag = match;
	        chunk.before = leftOver;
	
	        // end of change
	
	        if (chunk.after) {
	            chunk.after = chunk.after.replace(/^\n?/, "\n");
	        }
	
	        chunk.after = chunk.after.replace(/^(((\n|^)(\n[ \t]*)*>(.+\n)*.*)+(\n[ \t]*)*)/, function (totalMatch) {
	            chunk.endTag = totalMatch;
	            return "";
	        });
	
	        var replaceBlanksInTags = function replaceBlanksInTags(useBracket) {
	
	            var replacement = useBracket ? "> " : "";
	
	            if (chunk.startTag) {
	                chunk.startTag = chunk.startTag.replace(/\n((>|\s)*)\n$/, function (totalMatch, markdown) {
	                    return "\n" + markdown.replace(/^[ ]{0,3}>?[ \t]*$/gm, replacement) + "\n";
	                });
	            }
	            if (chunk.endTag) {
	                chunk.endTag = chunk.endTag.replace(/^\n((>|\s)*)\n/, function (totalMatch, markdown) {
	                    return "\n" + markdown.replace(/^[ ]{0,3}>?[ \t]*$/gm, replacement) + "\n";
	                });
	            }
	        };
	
	        if (/^(?![ ]{0,3}>)/m.test(chunk.selection)) {
	            this.wrap(chunk, SETTINGS.lineLength - 2);
	            chunk.selection = chunk.selection.replace(/^/gm, "> ");
	            replaceBlanksInTags(true);
	            chunk.skipLines();
	        } else {
	            chunk.selection = chunk.selection.replace(/^[ ]{0,3}> ?/gm, "");
	            this.unwrap(chunk);
	            replaceBlanksInTags(false);
	
	            if (!/^(\n|^)[ ]{0,3}>/.test(chunk.selection) && chunk.startTag) {
	                chunk.startTag = chunk.startTag.replace(/\n{0,2}$/, "\n\n");
	            }
	
	            if (!/(\n|^)[ ]{0,3}>.*$/.test(chunk.selection) && chunk.endTag) {
	                chunk.endTag = chunk.endTag.replace(/^\n{0,2}/, "\n\n");
	            }
	        }
	
	        chunk.selection = this.hooks.postBlockquoteCreation(chunk.selection);
	
	        if (!/\n/.test(chunk.selection)) {
	            chunk.selection = chunk.selection.replace(/^(> *)/, function (wholeMatch, blanks) {
	                chunk.startTag += blanks;
	                return "";
	            });
	        }
	    };
	
	    commandProto.doCode = function (chunk, postProcessing) {
	
	        var hasTextBefore = /\S[ ]*$/.test(chunk.before);
	        var hasTextAfter = /^[ ]*\S/.test(chunk.after);
	
	        // Use 'four space' markdown if the selection is on its own
	        // line or is multiline.
	        if (!hasTextAfter && !hasTextBefore || /\n/.test(chunk.selection)) {
	
	            chunk.before = chunk.before.replace(/[ ]{4}$/, function (totalMatch) {
	                chunk.selection = totalMatch + chunk.selection;
	                return "";
	            });
	
	            var nLinesBack = 1;
	            var nLinesForward = 1;
	
	            if (/(\n|^)(\t|[ ]{4,}).*\n$/.test(chunk.before)) {
	                nLinesBack = 0;
	            }
	            if (/^\n(\t|[ ]{4,})/.test(chunk.after)) {
	                nLinesForward = 0;
	            }
	
	            chunk.skipLines(nLinesBack, nLinesForward);
	
	            if (!chunk.selection) {
	                chunk.startTag = "    ";
	                chunk.selection = this.getString("codeexample");
	            } else {
	                if (/^[ ]{0,3}\S/m.test(chunk.selection)) {
	                    if (/\n/.test(chunk.selection)) chunk.selection = chunk.selection.replace(/^/gm, "    ");else // if it's not multiline, do not select the four added spaces; this is more consistent with the doList behavior
	                        chunk.before += "    ";
	                } else {
	                    chunk.selection = chunk.selection.replace(/^(?:[ ]{4}|[ ]{0,3}\t)/gm, "");
	                }
	            }
	        } else {
	            // Use backticks (`) to delimit the code block.
	
	            chunk.trimWhitespace();
	            chunk.findTags(/`/, /`/);
	
	            if (!chunk.startTag && !chunk.endTag) {
	                chunk.startTag = chunk.endTag = "`";
	                if (!chunk.selection) {
	                    chunk.selection = this.getString("codeexample");
	                }
	            } else if (chunk.endTag && !chunk.startTag) {
	                chunk.before += chunk.endTag;
	                chunk.endTag = "";
	            } else {
	                chunk.startTag = chunk.endTag = "";
	            }
	        }
	    };
	
	    commandProto.doList = function (chunk, postProcessing, isNumberedList) {
	
	        // These are identical except at the very beginning and end.
	        // Should probably use the regex extension function to make this clearer.
	        var previousItemsRegex = /(\n|^)(([ ]{0,3}([*+-]|\d+[.])[ \t]+.*)(\n.+|\n{2,}([*+-].*|\d+[.])[ \t]+.*|\n{2,}[ \t]+\S.*)*)\n*$/;
	        var nextItemsRegex = /^\n*(([ ]{0,3}([*+-]|\d+[.])[ \t]+.*)(\n.+|\n{2,}([*+-].*|\d+[.])[ \t]+.*|\n{2,}[ \t]+\S.*)*)\n*/;
	
	        // The default bullet is a dash but others are possible.
	        // This has nothing to do with the particular HTML bullet,
	        // it's just a markdown bullet.
	        var bullet = "-";
	
	        // The number in a numbered list.
	        var num = 1;
	
	        // Get the item prefix - e.g. " 1. " for a numbered list, " - " for a bulleted list.
	        var getItemPrefix = function getItemPrefix() {
	            var prefix;
	            if (isNumberedList) {
	                prefix = " " + num + ". ";
	                num++;
	            } else {
	                prefix = " " + bullet + " ";
	            }
	            return prefix;
	        };
	
	        // Fixes the prefixes of the other list items.
	        var getPrefixedItem = function getPrefixedItem(itemText) {
	
	            // The numbering flag is unset when called by autoindent.
	            if (isNumberedList === undefined) {
	                isNumberedList = /^\s*\d/.test(itemText);
	            }
	
	            // Renumber/bullet the list element.
	            itemText = itemText.replace(/^[ ]{0,3}([*+-]|\d+[.])\s/gm, function (_) {
	                return getItemPrefix();
	            });
	
	            return itemText;
	        };
	
	        chunk.findTags(/(\n|^)*[ ]{0,3}([*+-]|\d+[.])\s+/, null);
	
	        if (chunk.before && !/\n$/.test(chunk.before) && !/^\n/.test(chunk.startTag)) {
	            chunk.before += chunk.startTag;
	            chunk.startTag = "";
	        }
	
	        if (chunk.startTag) {
	
	            var hasDigits = /\d+[.]/.test(chunk.startTag);
	            chunk.startTag = "";
	            chunk.selection = chunk.selection.replace(/\n[ ]{4}/g, "\n");
	            this.unwrap(chunk);
	            chunk.skipLines();
	
	            if (hasDigits) {
	                // Have to renumber the bullet points if this is a numbered list.
	                chunk.after = chunk.after.replace(nextItemsRegex, getPrefixedItem);
	            }
	            if (isNumberedList == hasDigits) {
	                return;
	            }
	        }
	
	        var nLinesUp = 1;
	
	        chunk.before = chunk.before.replace(previousItemsRegex, function (itemText) {
	            if (/^\s*([*+-])/.test(itemText)) {
	                bullet = re.$1;
	            }
	            nLinesUp = /[^\n]\n\n[^\n]/.test(itemText) ? 1 : 0;
	            return getPrefixedItem(itemText);
	        });
	
	        if (!chunk.selection) {
	            chunk.selection = this.getString("litem");
	        }
	
	        var prefix = getItemPrefix();
	
	        var nLinesDown = 1;
	
	        chunk.after = chunk.after.replace(nextItemsRegex, function (itemText) {
	            nLinesDown = /[^\n]\n\n[^\n]/.test(itemText) ? 1 : 0;
	            return getPrefixedItem(itemText);
	        });
	
	        chunk.trimWhitespace(true);
	        chunk.skipLines(nLinesUp, nLinesDown, true);
	        chunk.startTag = prefix;
	        var spaces = prefix.replace(/./g, " ");
	        this.wrap(chunk, SETTINGS.lineLength - spaces.length);
	        chunk.selection = chunk.selection.replace(/\n/g, "\n" + spaces);
	    };
	
	    commandProto.doHeading = function (chunk, postProcessing) {
	
	        // Remove leading/trailing whitespace and reduce internal spaces to single spaces.
	        chunk.selection = chunk.selection.replace(/\s+/g, " ");
	        chunk.selection = chunk.selection.replace(/(^\s+|\s+$)/g, "");
	
	        // If we clicked the button with no selected text, we just
	        // make a level 2 hash header around some default text.
	        if (!chunk.selection) {
	            chunk.startTag = "## ";
	            chunk.selection = this.getString("headingexample");
	            chunk.endTag = " ##";
	            return;
	        }
	
	        var headerLevel = 0; // The existing header level of the selected text.
	
	        // Remove any existing hash heading markdown and save the header level.
	        chunk.findTags(/#+[ ]*/, /[ ]*#+/);
	        if (/#+/.test(chunk.startTag)) {
	            headerLevel = re.lastMatch.length;
	        }
	        chunk.startTag = chunk.endTag = "";
	
	        // Try to get the current header level by looking for - and = in the line
	        // below the selection.
	        chunk.findTags(null, /\s?(-+|=+)/);
	        if (/=+/.test(chunk.endTag)) {
	            headerLevel = 1;
	        }
	        if (/-+/.test(chunk.endTag)) {
	            headerLevel = 2;
	        }
	
	        // Skip to the next line so we can create the header markdown.
	        chunk.startTag = chunk.endTag = "";
	        chunk.skipLines(1, 1);
	
	        // We make a level 2 header if there is no current header.
	        // If there is a header level, we substract one from the header level.
	        // If it's already a level 1 header, it's removed.
	        var headerLevelToCreate = headerLevel == 0 ? 2 : headerLevel - 1;
	
	        if (headerLevelToCreate > 0) {
	
	            // The button only creates level 1 and 2 underline headers.
	            // Why not have it iterate over hash header levels?  Wouldn't that be easier and cleaner?
	            var headerChar = headerLevelToCreate >= 2 ? "-" : "=";
	            var len = chunk.selection.length;
	            if (len > SETTINGS.lineLength) {
	                len = SETTINGS.lineLength;
	            }
	            chunk.endTag = "\n";
	            while (len--) {
	                chunk.endTag += headerChar;
	            }
	        }
	    };
	
	    commandProto.doHorizontalRule = function (chunk, postProcessing) {
	        chunk.startTag = "----------\n";
	        chunk.selection = "";
	        chunk.skipLines(2, 1, true);
	    };
	})();

/***/ }),

/***/ 1773:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	var _react = __webpack_require__(34);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _reactDom = __webpack_require__(77);
	
	var _reactDom2 = _interopRequireDefault(_reactDom);
	
	var _formInput = __webpack_require__(1767);
	
	var _formInput2 = _interopRequireDefault(_formInput);
	
	var _lodash = __webpack_require__(491);
	
	var _lodash2 = _interopRequireDefault(_lodash);
	
	var _PivotDefinitionButton = __webpack_require__(1774);
	
	var _PivotDefinitionButton2 = _interopRequireDefault(_PivotDefinitionButton);
	
	function _interopRequireDefault(obj) {
	    return obj && obj.__esModule ? obj : { default: obj };
	}
	
	exports.default = _formInput2.default.createFormInput({
	    onOk: function onOk(pivotData) {
	        if (_lodash2.default.isObject(pivotData)) {
	            this.setPivotTableDefinition(pivotData);
	        }
	        this.render();
	    },
	
	    render: function render() {
	        var pivotTableDefinition = this.pivotTableDefinition;
	        var pivotProps = {};
	        if (pivotTableDefinition && pivotTableDefinition.pivotItemLists.data !== null) {
	            pivotProps.savedPivot = pivotTableDefinition;
	        }
	        pivotProps.onOk = this.onOk;
	        _reactDom2.default.render(_react2.default.createFactory(_PivotDefinitionButton2.default)(pivotProps, null), this.target);
	    },
	
	    postShow: function postShow() {
	        this.target = this.$element.find('.input-container').get(0);
	        this.hiddenInput = $('#' + this.$element.data('hiddenId'));
	        this.onOk = this.onOk.bind(this);
	        this.setPivotTableDefinition(this.$element.data('pivotTableDefinition'));
	        this.render();
	    },
	
	    setPivotTableDefinition: function setPivotTableDefinition(def) {
	        this.$element.data('pivotTableDefinition', def);
	        this.pivotTableDefinition = this.$element.data('pivotTableDefinition');
	    },
	
	    preSubmit: function preSubmit() {
	        this.hiddenInput.val(JSON.stringify(this.pivotTableDefinition));
	    }
	});

/***/ }),

/***/ 1774:
/***/ (function(module, exports, __webpack_require__) {

	'use strict';
	
	Object.defineProperty(exports, "__esModule", {
	    value: true
	});
	
	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();
	
	var _react = __webpack_require__(34);
	
	var _react2 = _interopRequireDefault(_react);
	
	var _reactBootstrap = __webpack_require__(233);
	
	var _lodash = __webpack_require__(491);
	
	var _lodash2 = _interopRequireDefault(_lodash);
	
	var _jquery = __webpack_require__(31);
	
	var _jquery2 = _interopRequireDefault(_jquery);
	
	__webpack_require__(1371);
	
	var _PivotTableActions = __webpack_require__(1712);
	
	var _PivotTableActions2 = _interopRequireDefault(_PivotTableActions);
	
	var _PivotUi = __webpack_require__(1444);
	
	var _PivotUi2 = _interopRequireDefault(_PivotUi);
	
	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
	
	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
	
	function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }
	
	function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }
	
	var PivotDefinitionButton = function (_React$Component) {
	    _inherits(PivotDefinitionButton, _React$Component);
	
	    function PivotDefinitionButton(props) {
	        _classCallCheck(this, PivotDefinitionButton);
	
	        var _this = _possibleConstructorReturn(this, (PivotDefinitionButton.__proto__ || Object.getPrototypeOf(PivotDefinitionButton)).call(this, props));
	
	        _this.state = {
	            open: false,
	            dateControl: '',
	            pivotId: _this.loadPivot(props.savedPivot)
	        };
	
	        _this.refreshPivot = _this.refreshPivot.bind(_this);
	        return _this;
	    }
	
	    _createClass(PivotDefinitionButton, [{
	        key: 'componentDidMount',
	        value: function componentDidMount() {
	            dash.dr.getControl('pivot').then(this.handleDateControlHtml.bind(this));
	
	            (0, _jquery2.default)(window).on('date-range:pivot:date-changed', this.refreshPivot);
	        }
	    }, {
	        key: 'componentWillReceiveProps',
	        value: function componentWillReceiveProps(nextProps) {
	            if (!_lodash2.default.isEqual(this.props.savedPivot, nextProps.savedPivot)) {
	                this.setState({ pivotId: this.loadPivot(nextProps.savedPivot) });
	            }
	        }
	    }, {
	        key: 'componentWillUnmount',
	        value: function componentWillUnmount() {
	            (0, _jquery2.default)(window).off('date-range:pivot:date-changed', this.refreshPivot);
	        }
	    }, {
	        key: 'onConfigurePressed',
	        value: function onConfigurePressed() {
	            this.setState({ open: true });
	        }
	    }, {
	        key: 'onOkPressed',
	        value: function onOkPressed() {
	            this.props.onOk(this.pivotUi.getPivotDataToSave());
	            this.closeModal();
	        }
	    }, {
	        key: 'onCancelPressed',
	        value: function onCancelPressed() {
	            this.closeModal();
	        }
	    }, {
	        key: 'getPivotUi',
	        value: function getPivotUi() {
	            var _this2 = this;
	
	            return _react2.default.createElement(_PivotUi2.default, {
	                ref: function ref(r) {
	                    return _this2.pivotUi = r === null ? r : r.refs.child;
	                },
	                pivotId: this.state.pivotId,
	                dateRangeSource: 'pivot'
	            });
	        }
	    }, {
	        key: 'getModal',
	        value: function getModal() {
	            var _this3 = this;
	
	            var dateRangeControlStyle = {
	                marginBottom: '6px',
	                backgroundColor: 'white',
	                padding: '6px',
	                boxShadow: '1px 1px #c8c8c8'
	            };
	
	            var dateRangeControlHeadingStyle = {
	                borderBottom: '1px solid #eee',
	                width: '100%',
	                marginBottom: '6px'
	            };
	
	            var dateRangeControlInnerStyle = {
	                position: 'relative',
	                marginLeft: '18px'
	            };
	
	            return _react2.default.createElement(
	                _reactBootstrap.Modal,
	                {
	                    show: this.state.open,
	                    bsSize: 'lg',
	                    onHide: function onHide() {
	                        return _this3.closeModal();
	                    }
	                },
	                _react2.default.createElement(
	                    _reactBootstrap.Modal.Header,
	                    { closeButton: true },
	                    _react2.default.createElement(
	                        _reactBootstrap.Modal.Title,
	                        null,
	                        'Configure Pivot Table'
	                    )
	                ),
	                _react2.default.createElement(
	                    _reactBootstrap.Modal.Body,
	                    null,
	                    _react2.default.createElement(
	                        'div',
	                        null,
	                        _react2.default.createElement(
	                            'div',
	                            { style: dateRangeControlStyle, className: 'col-md-3' },
	                            _react2.default.createElement(
	                                'div',
	                                { style: dateRangeControlHeadingStyle },
	                                'Date Range'
	                            ),
	                            _react2.default.createElement('div', {
	                                ref: 'dateControl',
	                                style: dateRangeControlInnerStyle,
	                                dangerouslySetInnerHTML: { __html: this.state.dateControl }
	                            })
	                        )
	                    ),
	                    _react2.default.createElement('div', { className: 'clearfix' }),
	                    this.state.open && this.getPivotUi()
	                ),
	                _react2.default.createElement(
	                    _reactBootstrap.Modal.Footer,
	                    null,
	                    _react2.default.createElement(
	                        _reactBootstrap.Button,
	                        { className: 'btn-secondary', onClick: function onClick() {
	                                return _this3.onCancelPressed();
	                            } },
	                        'Cancel'
	                    ),
	                    _react2.default.createElement(
	                        _reactBootstrap.Button,
	                        { className: 'btn-primary', onClick: function onClick() {
	                                return _this3.onOkPressed();
	                            } },
	                        'Save'
	                    )
	                )
	            );
	        }
	    }, {
	        key: 'refreshPivot',
	        value: function refreshPivot() {
	            if (this.pivotUi) {
	                this.pivotUi.refreshPivot();
	            }
	        }
	    }, {
	        key: 'handleDateControlHtml',
	        value: function handleDateControlHtml(data) {
	            this.setState({ dateControl: data.control });
	        }
	    }, {
	        key: 'loadPivot',
	        value: function loadPivot(pivot) {
	            return typeof pivot !== 'undefined' ? _PivotTableActions2.default.hydrate(pivot) : _PivotTableActions2.default.newPivot();
	        }
	    }, {
	        key: 'closeModal',
	        value: function closeModal() {
	            this.setState({ open: false });
	        }
	    }, {
	        key: 'render',
	        value: function render() {
	            var _this4 = this;
	
	            var savedPivot = this.props.savedPivot;
	
	
	            return _react2.default.createElement(
	                'div',
	                null,
	                _react2.default.createElement(
	                    _reactBootstrap.Button,
	                    {
	                        bsStyle: _lodash2.default.isObject(savedPivot) ? 'success' : 'warning',
	                        onClick: function onClick() {
	                            return _this4.onConfigurePressed();
	                        }
	                    },
	                    _lodash2.default.isObject(savedPivot) ? 'Configured!' : 'Configure'
	                ),
	                this.state.open && this.getModal()
	            );
	        }
	    }]);
	
	    return PivotDefinitionButton;
	}(_react2.default.Component);
	
	PivotDefinitionButton.propTypes = {
	    onOk: _react2.default.PropTypes.func.isRequired,
	    onCancel: _react2.default.PropTypes.func,
	    savedPivot: _react2.default.PropTypes.object
	};
	exports.default = PivotDefinitionButton;

/***/ }),

/***/ 1775:
/***/ (function(module, exports) {

	'use strict';
	
	var _typeof2 = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };
	
	var _typeof = typeof Symbol === "function" && _typeof2(Symbol.iterator) === "symbol" ? function (obj) {
	    return typeof obj === "undefined" ? "undefined" : _typeof2(obj);
	} : function (obj) {
	    return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj === "undefined" ? "undefined" : _typeof2(obj);
	};
	
	var doctrineGridUID = 0;
	function DoctrineGrid(obj, options) {
	    if (obj[0] && $.isPlainObject(obj[0])) {
	        this.data = obj[0];
	    } else {
	        this.el = obj;
	        this.$el = $(obj);
	    }
	
	    if (typeof options.eventTarget !== 'undefined') {
	        this.eventTarget = options.eventTarget;
	        delete options.eventTarget;
	        //TODO make a separate "gridOptions" obj
	    } else {
	        this.eventTarget = this.$el;
	    }
	
	    if (typeof options.modal !== 'undefined') {
	        this.modal = options.modal;
	        delete options.modal;
	    } else {
	        this.modal = false;
	    }
	
	    var defaultOptions = {
	        filters: {},
	        sort: []
	    };
	
	    this.onRefreshDone = this._onRefreshDone;
	
	    this.options = $.extend({}, defaultOptions, options);
	    this.init();
	
	    return this;
	}
	
	var fn = DoctrineGrid.prototype;
	
	fn.init = function () {
	    this.loadGrid();
	};
	
	fn._onRefreshDone = function () {};
	
	fn.refreshDone = function () {
	    this.currentPage = this.$el.data("current-page");
	    this.pages = this.$el.data("pages");
	    this.options.page = this.currentPage;
	    window.scrollTo(0, 0);
	    if (typeof this.onRefreshDone === 'function') this.onRefreshDone();
	    this.onRefreshDone = this._onRefreshDone;
	};
	
	fn.loadGrid = function () {
	    if (typeof this.$el !== 'undefined') this.$el.block();
	
	    var url = "grid.php";
	    var ctx = this;
	    $.post(url, { gridOptions: this.options }).done(function (data, textstatus, XHR) {
	        data = $.parseJSON(data);
	        ctx.showGrid(data);
	    });
	};
	
	fn.showGrid = function (data) {
	    if (data.success) {
	        var parent = this.$el.parent();
	        this.$el.remove();
	        this.$el = $(data.html);
	        this.$el.data('doctrine-grid', this);
	        this.el = this.$el[0];
	        parent.append(this.$el);
	        this.initEvents();
	        this.refreshDone();
	    } else {
	        var alert = $('<div class="alert alert-danger">' + data.message + '</div>');
	        this.$el.empty().append(alert);
	        alert.alert();
	    }
	};
	
	fn.initEvents = function () {
	    this.$el.on('recordsChanged.doctrineForm', $.proxy(this.triggerRefreshGrid, this));
	    if (this.modal) {
	        //this.modal.on('dismissed.zmodal', function(event){console.log(event)});
	    }
	};
	
	fn.triggerRefreshGrid = function (event) {
	    this.refreshGrid();
	};
	
	fn.filterGridOnKeyDown = function (event, target) {
	    target = $(target);
	    if (event.keyCode === 9 || event.keyCode === 13) {
	        $('#filter-button-' + target.data('field')).popover("hide");
	        this.filterGrid(target.data('field'), target.val());
	        this.refreshGrid();
	    }
	};
	
	fn.filterGrid = function (field, value) {
	    this.options.filters[field] = { field: field, value: value };
	};
	
	fn.removeFilter = function (field) {
	    delete this.options.filters[field];
	    this.refreshGrid();
	};
	
	fn.refreshGrid = function (callback) {
	    if ((typeof callback === 'undefined' ? 'undefined' : _typeof(callback)) !== undefined) {
	        this.onRefreshDone = callback;
	    }
	    this.loadGrid();
	};
	
	fn.sortGrid = function (event, field) {
	    var foundIndex = null;
	    if (this.options.sort.length > 0) {
	        $.each(this.options.sort, function (index, object) {
	            if (object.field === field) {
	                foundIndex = index;
	                return false;
	            }
	        });
	    }
	    if (foundIndex !== null) {
	        var sort = this.options.sort.splice(foundIndex, 1);
	        sort = sort[0];
	        if (sort.direction === "ASC") {
	            sort.direction = "DESC";
	        } else {
	            sort.direction = "ASC";
	        }
	
	        this.options.sort.unshift(sort);
	    } else {
	        this.options.sort.unshift({
	            field: field,
	            direction: 'ASC'
	        });
	    }
	
	    this.refreshGrid();
	};
	
	fn.unSortGrid = function (event, field) {
	    if (typeof this.options.sort[field] != "undefined") delete this.options.sort[field];
	
	    this.refreshGrid();
	};
	
	fn.nextPage = function (event) {
	    this.options.page++;
	    this.refreshGrid();
	};
	
	fn.previousPage = function (event) {
	    this.options.page--;
	    this.refreshGrid();
	};
	
	fn.goToPage = function (event, page) {
	    this.options.page = page;
	    this.refreshGrid();
	};
	
	fn.selectRecord = function (event, entityId, name) {
	    this.eventTarget.trigger('recordSelected.doctrineGrid', [entityId, name]);
	    if (this.modal !== false) {
	        this.destroy();
	    }
	};
	
	fn.destroy = function () {
	    this.destroyModal();
	    this.$el.remove();
	};
	
	fn.destroyModal = function () {
	    if (this.modal !== false) {
	        this.modal.find('.modal-footer').hide();
	        setTimeout($.proxy(function () {
	            this.modal.zmodal('destroy');
	        }, this), 150);
	    }
	};
	
	$.fn.doctrineGrid = function (options) {
	    if (this.data('doctrine-grid')) {
	        return this.data('doctrine-grid');
	    }
	    var grid = new DoctrineGrid(this, arguments[0]);
	    this.data('doctrine-grid', grid);
	    return grid;
	};
	
	/**
	 * popUpModalDoctrineGrid
	 *
	 * This creates a modal grid at the document level, having the object it is called on as the target of its events.
	 * Currently only the selectRecord event is fired into the calling object.
	 * @param options
	 */
	$.fn.popUpModalDoctrineGrid = function (options) {
	    //Make sure options exists
	    if (typeof options === 'undefined') options = {};
	    //Set the event target as the dom object this func is called on
	    options.eventTarget = this;
	
	    //Defaults
	    var modalDefaultOptions = {
	        okButton: { disabled: true }
	    };
	    if (typeof options.modal === 'undefined') {
	        options.modal = modalDefaultOptions;
	    } else {
	        options.modal = $.extend({}, modalDefaultOptions, options.modal);
	    }
	
	    //Build a container that will be populated with the modal stuff
	    var outerContainer = $('<div></div>');
	    if (typeof options.uid === 'undefined') {
	        options.uid = doctrineGridUID++;
	    }
	
	    outerContainer.attr('id', "doctrine-grid-" + options.uid);
	    //Init the modal
	    if (typeof options.modal !== 'undefined') {
	        outerContainer.zmodal(options.modal);
	    } else {
	        outerContainer.zmodal();
	    }
	    outerContainer.attr('id', 'modal-' + outerContainer.attr('id'));
	    var container = $('<div class="doctrine-grid-container"></div>');
	    outerContainer.find('.modal-body').first().append(container);
	    outerContainer.find('.modal-body').first().css('min-height', '250px');
	    options.modal = outerContainer;
	    //Init the form
	    container.doctrineGrid(options);
	};
	
	function UserSecurityEditor(obj, options) {
	    if (obj[0] && $.isPlainObject(obj[0])) {
	        this.data = obj[0];
	    } else {
	        this.el = obj;
	        this.$el = $(obj);
	    }
	
	    if (typeof options.modal !== 'undefined') {
	        this.modal = options.modal;
	        delete options.modal;
	    } else {
	        this.modal = false;
	    }
	
	    var defaultOptions = {};
	
	    this.userCompositeRoles = [];
	    this.selectedUserCompositeRoles = [];
	    this.sysCompositeRoles = [];
	    this.selectedSysCompositeRoles = [];
	    this.options = $.extend({}, defaultOptions, options);
	
	    this.setMode();
	    if (typeof this.mode !== 'string') {
	        throw 'Mode not set. Cannot initialize user security editor.';
	    }
	
	    this.init();
	
	    return this;
	}
	
	fn = UserSecurityEditor.prototype;
	
	fn.setMode = function () {
	    var userId = this.options.userId || 0,
	        groupId = this.options.groupId || 0;
	
	    if (userId === 0 && groupId === 0) {
	        throw 'Cannot initialize user security editor - neither group nor user id provided.';
	    }
	
	    if (userId) {
	        this.mode = 'user';
	        return;
	    }
	
	    if (groupId) {
	        this.mode = 'group';
	    }
	};
	
	fn.init = function () {
	    this.loadEditor();
	    this.modal.on('ok.zmodal', $.proxy(this.doSubmit, this));
	};
	
	fn.loadEditor = function () {
	    this.userCompositeRoles.length = 0;
	    this.selectedUserCompositeRoles.length = 0;
	    this.sysCompositeRoles.length = 0;
	    this.selectedSysCompositeRoles.length = 0;
	    if (typeof this.$el !== 'undefined') this.$el.block();
	    var url = "tools/manage_user_roles_ajax.php",
	        ctx = this,
	        baseOpts;
	
	    if (this.mode == 'user') {
	        baseOpts = { id_user: this.options.userId };
	    } else {
	        baseOpts = { id_group: this.options.groupId };
	    }
	
	    $.post(url, baseOpts).done(function (data, textstatus, XHR) {
	        data = $.parseJSON(data);
	        ctx.showEditor(data);
	    });
	};
	
	fn.showEditor = function (data) {
	    if (data.success || false) {
	        var parent = this.$el.parent();
	        this.$el.remove();
	        this.$el = $(data.html);
	        this.$el.data('user-security-editor', this);
	        this.el = this.$el[0];
	        parent.append(this.$el);
	        this.initEvents();
	        //this.refreshDone();
	    } else {
	        dash.dialog.showAjaxError(data.message);
	        this.destroy();
	    }
	
	    if (typeof data.userCompositeRoles !== 'undefined') {
	        var roles = data.userCompositeRoles;
	        this.loadUserCompositeRoles(roles);
	    }
	
	    if (typeof data.sysCompositeRoles !== 'undefined') {
	        var sysRoles = data.sysCompositeRoles;
	        this.loadSysCompositeRoles(sysRoles);
	    }
	};
	
	fn.initEvents = function () {
	    var ctx = this;
	    $.each(this.$el.find('.composite-role-checkbox'), function (i, el) {
	        $(el).on("click", $.proxy(ctx.handleCompositeRoleChanged, ctx));
	    });
	    $.each(this.$el.find('.sys-composite-role-checkbox'), function (i, el) {
	        $(el).on("click", $.proxy(ctx.handleCompositeSysRoleChanged, ctx));
	    });
	    this.$el.find('.submit').on("click", $.proxy(ctx.doSubmit, ctx));
	};
	
	fn.loadUserCompositeRoles = function (compositeRoles) {
	    var role;
	    for (var i in compositeRoles) {
	        role = compositeRoles[i];
	        this.addUserCompositeRoleToList(role.id, role.bitValue);
	        this.setUserCompositeRoleSelected(role.id, role.active);
	    }
	};
	
	fn.addUserCompositeRoleToList = function (id, bitField) {
	    this.userCompositeRoles[id] = bitField;
	};
	
	fn.setUserCompositeRoleSelected = function (id, isSelected) {
	    if (isSelected) this.selectedUserCompositeRoles.push(id);else {
	        var newLst = [];
	        for (var i = 0; i < this.selectedUserCompositeRoles.length; i++) {
	            var val = this.selectedUserCompositeRoles[i];
	            if (val != id) newLst.push(val);
	        }
	        this.selectedUserCompositeRoles = newLst;
	    }
	};
	
	fn.handleCompositeRoleChanged = function (event) {
	    var em = event.currentTarget;
	    var id = $(em).data('id');
	    if (em.checked) {
	        // Adding - use fastpath
	        this.setUserCompositeRoleSelected(id, true);
	        var bitfieldValue = this.userCompositeRoles[id];
	        for (var bit = 1; bit < 4294967296; bit *= 2) {
	            if (bitfieldValue & bit) {
	                var securityCheckbox = this.$el.find('#security_role_' + bit)[0];
	                if (!!securityCheckbox) {
	                    securityCheckbox.checked = true;
	                    securityCheckbox.disabled = 'disabled';
	                }
	                // Note, we could break if this is false, however this assumes we have no gaps (or disabled permissions) in the model, so let's go all the way to 32-bit cap.
	            }
	        }
	    } else {
	        // Removing -- must iterate through every group and assert perms.
	        // Two options here -- we can re-enable the permission above, or we can uncheck it. Assuming removal of group means they want more fine-grained, so uncheck.
	        this.setUserCompositeRoleSelected(id, false);
	        var totalBitfield = 0;
	        for (var i = 0; i < this.selectedUserCompositeRoles.length; i++) {
	            var thisBitfield = this.userCompositeRoles[this.selectedUserCompositeRoles[i]];
	            totalBitfield |= thisBitfield;
	        }
	
	        for (var bit = 1; bit < 4294967296; bit *= 2) {
	            if (!(totalBitfield & bit)) {
	                var securityCheckbox = this.$el.find('#security_role_' + bit)[0];
	                if (!!securityCheckbox) {
	                    securityCheckbox.disabled = '';
	                }
	            }
	        }
	    }
	};
	
	fn.loadSysCompositeRoles = function (compositeRoles) {
	    var role;
	    for (var i in compositeRoles) {
	        role = compositeRoles[i];
	        this.addSysCompositeRoleToList(role.id, role.bitField);
	        this.setSysCompositeRoleSelected(role.id, role.active);
	    }
	};
	
	fn.addSysCompositeRoleToList = function (id, bitField) {
	    this.sysCompositeRoles[id] = bitField;
	};
	
	fn.setSysCompositeRoleSelected = function (id, isSelected) {
	    if (isSelected) this.selectedSysCompositeRoles.push(id);else {
	        var newLst = [];
	        for (var i = 0; i < this.selectedSysCompositeRoles.length; i++) {
	            var val = this.selectedSysCompositeRoles[i];
	            if (val != id) newLst.push(val);
	        }
	        this.selectedSysCompositeRoles = newLst;
	    }
	};
	
	fn.handleCompositeSysRoleChanged = function (event) {
	    var em = event.currentTarget;
	    var id = $(em).data('id');
	    if (em.checked) {
	        // Adding - use fastpath
	        this.setSysCompositeRoleSelected(id, true);
	        var bitfieldValue = this.sysCompositeRoles[id];
	        for (var bit = 1; bit < 4294967296; bit *= 2) {
	            if (bitfieldValue & bit) {
	                var securityCheckbox = this.$el.find('#security_sys_role_' + bit)[0];
	                if (!!securityCheckbox) {
	                    securityCheckbox.checked = true;
	                    securityCheckbox.disabled = 'disabled';
	                }
	                // Note, we could break if this is false, however this assumes we have no gaps (or disabled permissions) in the model, so let's go all the way to 32-bit cap.
	            }
	        }
	    } else {
	        // Removing -- must iterate through every group and assert perms.
	        // Two options here -- we can re-enable the permission above, or we can uncheck it. Assuming removal of group means they want more fine-grained, so uncheck.
	        this.setSysCompositeRoleSelected(id, false);
	        var totalBitfield = 0;
	        for (var i = 0; i < this.selectedSysCompositeRoles.length; i++) {
	            var thisBitfield = this.sysCompositeRoles[document.selectedSysCompositeRoles[i]];
	            totalBitfield |= thisBitfield;
	        }
	
	        for (var bit = 1; bit < 4294967296; bit *= 2) {
	            if (!(totalBitfield & bit)) {
	                var securityCheckbox = this.$el.find('#security_sys_role_' + bit)[0];
	                if (!!securityCheckbox) {
	                    securityCheckbox.disabled = '';
	                }
	            }
	        }
	    }
	};
	
	fn.gatherRoleParams = function () {
	    // Roles, passed as meta_roles
	    var securityMetaRolesEms = this.$el.find('[name="security_meta_roles"]').toArray();
	
	    // Individual permissions, passed as base_roles
	    var baseRolesEms = this.$el.find('[name="security_roles"]').toArray();
	
	    // Sys roles, passed as meta_sys_roles
	    var securityMetaSysRolesEms = this.$el.find('[name="security_meta_sys_roles"]').toArray();
	
	    // Individual sys permissions, passed as base_sys_roles
	    var baseSysRolesEms = this.$el.find('[name="security_sys_roles"]').toArray();
	    var selectedMetaRoles = [];
	    var selectedBaseRoles = [];
	    var selectedMetaSysRoles = [];
	    var selectedBaseSysRoles = [];
	
	    for (var i = 0; i < securityMetaRolesEms.length; i++) {
	        var thisEm = securityMetaRolesEms[i];
	        if (thisEm.checked) selectedMetaRoles.push(thisEm.id.replace('security_meta_role_', ''));
	    }
	
	    for (var i = 0; i < baseRolesEms.length; i++) {
	        var thisEm = baseRolesEms[i];
	        if (thisEm.checked && !thisEm.disabled) {
	            selectedBaseRoles.push(thisEm.id.replace('security_role_', ''));
	        }
	    }
	
	    for (var i = 0; i < securityMetaSysRolesEms.length; i++) {
	        var thisEm = securityMetaSysRolesEms[i];
	        if (thisEm.checked) selectedMetaSysRoles.push(thisEm.id.replace('security_meta_sys_role_', ''));
	    }
	
	    for (var i = 0; i < baseSysRolesEms.length; i++) {
	        var thisEm = baseSysRolesEms[i];
	        if (thisEm.checked && !thisEm.disabled) {
	            selectedBaseSysRoles.push(thisEm.id.replace('security_sys_role_', ''));
	        }
	    }
	
	    return [selectedBaseRoles, selectedMetaRoles, selectedBaseSysRoles, selectedMetaSysRoles];
	};
	
	fn.doSubmit = function () {
	    // TODO debug this
	    var roleParams = this.gatherRoleParams();
	    var url = "tools/save_user_roles_ajax.php?id_" + this.mode + "=" + this.$el.find('#hidden_' + this.mode + '_id').val();
	
	    if (this.$el.find('#hidden_user_perm').val() == '1') {
	        url += '&base_roles=' + roleParams[0].join(',') + '&meta_roles=' + roleParams[1].join(',');
	    }
	    if (this.$el.find('#hidden_sys_perm').val() == '1') url += '&base_sys_roles=' + roleParams[2].join(',') + '&meta_sys_roles=' + roleParams[3].join(',');
	
	    if (typeof this.$el !== 'undefined') this.$el.block();
	    var ctx = this;
	
	    $.get(url).done(function (data, textstatus, XHR) {
	        data = $.parseJSON(data);
	        ctx.submitSuccess(data);
	    });
	};
	
	fn.submitSuccess = function (data) {
	    if (data.success || false) {
	        this.$el.html(data.html);
	        this.destroy();
	    } else {
	        dash.dialog.showAjaxError("An unspecified error occurred while loading. If the problem persists, please contact your system administrator.");
	    }
	};
	
	fn.destroy = function () {
	    this.destroyModal();
	    this.$el.remove();
	};
	
	fn.destroyModal = function () {
	    if (this.modal !== false) {
	        this.modal.find('.modal-footer').hide();
	        setTimeout($.proxy(function () {
	            this.modal.zmodal('destroy');
	        }, this), 500);
	    }
	};
	
	$.fn.userSecurityEditor = function (options) {
	    if (this.data('user-security-editor')) {
	        return this.data('user-security-editor');
	    }
	    var editor = new UserSecurityEditor(this, arguments[0]);
	    this.data('user-security-editor', editor);
	    return editor;
	};

/***/ })

});
//# sourceMappingURL=layout.bundle.js.map