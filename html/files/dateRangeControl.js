/*
* Documentation in Classes\UI\DateRangeControl
*/
document.dateRangeFadeoutTime = 750;
document.date_range_updateSessionOnChange = {};

function date_range_recalculateScrollHeight(baseID)
{
    if(document.getElementById(baseID + '_select_date_range_box').style.display === 'none')
        return;

    var fromBottom = $(window).height() * .171;
    var fromTop = $('#' + baseID + '_current_date_range_box').offset()['top'] - $(window).scrollTop();

    var bottomBoxEm = document.getElementById(baseID + '_date_range_bottom_box');
    var origHeight = bottomBoxEm.scrollHeight;
    var maxHeight = parseInt(($(window).height() - fromTop - fromBottom));
    bottomBoxEm.style.minHeight = '100px';
    bottomBoxEm.style.maxHeight = maxHeight + 'px';
    bottomBoxEm.style.overflowY = 'auto';
    if(maxHeight < origHeight) // If scrollbar drawn, expand box
    {
        if(bottomBoxEm.getAttribute('expandedWidthForScroll') != 'true' && !bottomBoxEm.style.width)
        {
            bottomBoxEm.style.width = (bottomBoxEm.offsetWidth + 16) + 'px';
            bottomBoxEm.setAttribute('expandedWidthForScroll', 'true');
        }
    }
    else
    {
        bottomBoxEm.style.width = '';
        bottomBoxEm.setAttribute('expandedWidthForScroll', 'false');
    }
}

function date_range_showSelectDate(baseID)
{
    var selectDateRangeBoxEm = document.getElementById(baseID + '_select_date_range_box');
    if(selectDateRangeBoxEm.getAttribute('isFading') === 'true')
        return;
    selectDateRangeBoxEm.setAttribute('isFading', 'true');
    selectDateRangeBoxEm.style.display = '';
    $(selectDateRangeBoxEm).animate({opacity : 1}, document.dateRangeFadeoutTime);

    var bottomBoxEm = document.getElementById(baseID + '_date_range_bottom_box');

    var bottomBoxFullHeight = bottomBoxEm.offsetHeight;

    bottomBoxEm.style.height = '20px';
    bottomBoxEm.style.overflowY = 'hidden';


    $(bottomBoxEm).animate({'height' : bottomBoxFullHeight + 'px'}, document.dateRangeFadeoutTime - 100);
    setTimeout(function() { bottomBoxEm.style.height = 'auto'; date_range_recalculateScrollHeight(baseID); }, document.dateRangeFadeoutTime);

    setTimeout(function() { selectDateRangeBoxEm.setAttribute('isFading', 'false'); }, document.dateRangeFadeoutTime + 50);
}


// TODO: Latch this onto some sort of blur event or whatnot
function date_range_hideSelectDate(baseID)
{
    var selectDateRangeBoxEm = document.getElementById(baseID + '_select_date_range_box');
    if(selectDateRangeBoxEm && selectDateRangeBoxEm.getAttribute('isFading') === 'true')
        return;
    selectDateRangeBoxEm.setAttribute('isFading', 'true');
    selectDateRangeBoxEm.style.height = selectDateRangeBoxEm.offsetHeight + 'px';
    $(selectDateRangeBoxEm).animate({opacity : .01, height : '20px'}, document.dateRangeFadeoutTime);
    //$(selectDateRangeBoxEm).animate({height : '20px'}, document.dateRangeFadeoutTime - 100);
    setTimeout(function() { selectDateRangeBoxEm.style.display = 'none'; selectDateRangeBoxEm.setAttribute('isFading', 'false'); selectDateRangeBoxEm.style.height = 'auto'; }, document.dateRangeFadeoutTime + 50);
}

function date_range_showLoading(baseID)
{
    var currentDateRangeBoxEm = document.getElementById(baseID + '_current_date_range_box');
    currentDateRangeBoxEm.innerHTML = '<br />Loading...';
    currentDateRangeBoxEm.onclick = null;
    currentDateRangeBoxEm.style.cursor = 'progress';
}


function date_range_getUpdateSessionOnChange(baseID)
{
    return document.date_range_updateSessionOnChange[baseID] ? 1 : 0;
}

function _date_range_get_hidden_name(baseID)
{
    var em = document.getElementById(baseID + '_date_range_hidden_id');
    if(!!em)
        return em.name;
    return null;
}

function _date_range_get_change_period_disabled(baseID)
{
    var em = document.getElementById(baseID + '_date_range_change_period_disabled');
    if(!!em)
        return parseInt(em.value);
    return 0;
}

function date_range_changeDate(baseID, newDatetime, showSelectDateAfter)
{
    //console.log('date_range_changeDate() re-generating date range list for ['+baseID+']');
    var dfd = new $.Deferred();

    if(typeof showSelectDateAfter === 'undefined')
        showSelectDateAfter = true;

    var updateSession;
    if(showSelectDateAfter)
        updateSession = 0;
    else
        updateSession = date_range_getUpdateSessionOnChange(baseID);

    date_range_showLoading(baseID);
    var decache = (new Date).getTime();
    var theReq = $.post(absUrl('ajax.php?'+decache), {
                            'module' : 'date_range_control',
                            'action' : 'getControl',
                            'controlID' : baseID,
                            'hiddenName' : _date_range_get_hidden_name(baseID),
                            'changePeriodDisabled' : _date_range_get_change_period_disabled(baseID),
                            'newSelectedDate' : newDatetime,
                            'periodCode' : getCurrentPeriodCode(baseID),
                            'updateSession' : updateSession,
                            'nextUpdateSession' : date_range_getUpdateSessionOnChange(baseID),
                            'origInfo' : (showSelectDateAfter ? _date_range_getOrigTimestampInfo(baseID) : [0, 0])
                        });
    date_range_hideSelectDate(baseID);
    theReq.done(function(data)
    {
        if(!data) return alert('Failed.');
        data = eval('(' + data + ')');
        if(!data['success']) return alert('Failed 2.');

        $('#' + baseID + '_date_range_control').replaceWith(data['control']);

        if(showSelectDateAfter)
            date_range_showSelectDate(baseID)

        dfd.resolve(data);
    } );

    return dfd.promise();
}

function date_range_selectDate(boxEm, dateRangeID)
{
    var dfd = new $.Deferred();
    var baseID = boxEm.parentNode.id.replace('_date_range_bottom_box', '');
    var selectDateRangeBoxEm = document.getElementById(baseID + '_select_date_range_box');

    if(selectDateRangeBoxEm.getAttribute('isFading') === 'true')
        return;

    var newRangeText = $(boxEm).find('.date-range-text')[0].innerHTML.trim();
    var newRangeTitle = $(boxEm).find('.date-range-title')[0].innerHTML.trim();

    document.getElementById(baseID + '_range_text').innerHTML = newRangeText;
    document.getElementById(baseID + '_range_title').innerHTML = newRangeTitle;

    var currentDateRangeBoxEm = document.getElementById(baseID + '_current_date_range_box');
    currentDateRangeBoxEm.onclick = null;
    currentDateRangeBoxEm.style.cursor = 'progress';

    var newSelectedDate = $(boxEm).find('.date-range-button-timestamp').first().val();
    //var newDateRangeId = $(boxEm).find('.date-range-button-id').first().val();
    var newDateRangeId = dateRangeID;

    date_range_showLoading(baseID);
    date_range_hideSelectDate(baseID);
    set_selected_range_period(baseID+'||||').done( function() {
        var decache = (new Date).getTime();
        var theReq = $.post(absUrl('ajax.php?'+decache), {
                                'module' : 'date_range_control',
                                'action' : 'getControl',
                                'controlID' : baseID,
                                'hiddenName' : _date_range_get_hidden_name(baseID),
                                'changePeriodDisabled' : _date_range_get_change_period_disabled(baseID),
                                'newSelectedDate' : newSelectedDate,
                                'dateRangeId' : newDateRangeId,
                                'periodCode' : getCurrentPeriodCode(baseID),
                                'updateSession' : date_range_getUpdateSessionOnChange(baseID),
                                'nextUpdateSession' : date_range_getUpdateSessionOnChange(baseID)
                            });
        theReq.done(function(data) {
            if(!data) return alert('Failed.');
            data = eval('(' + data + ')');
            if(!data['success'])
                return alert('Failed 2.');

            $('#' + baseID + '_date_range_control').replaceWith(data['control']);
            dfd.resolve(data);
        });
    });
    return dfd.promise();
}

function date_range_showChangePeriod(baseID) {
    var dateRangeChooseRangeBox = document.getElementById(baseID + '_date_range_choose_range_box');
    if (dateRangeChooseRangeBox != null) { // Hidden for IE8 Users, dont attempt to hide the hidden
        dateRangeChooseRangeBox.setAttribute('hidden', 'true');
    }

    var selectDateRangeBoxEm = document.getElementById(baseID + '_select_date_range_box');
    if(selectDateRangeBoxEm.getAttribute('isFading') === 'true')
        return;

    var bottomBoxEm = document.getElementById(baseID + '_date_range_bottom_box');
    bottomBoxEm.style.height = bottomBoxEm.offsetHeight + 'px';

    $(bottomBoxEm).animate({opacity : .01, height: '20px'}, document.dateRangeFadeoutTime/2);
    setTimeout(function() { bottomBoxEm.style.display = 'none'; }, document.dateRangeFadeoutTime/2 + 25);

    var periodOptionsEm = document.getElementById(baseID + '_date_range_change_period_box');
    periodOptionsEm.style.display = '';
}

function date_range_selectPeriod(baseID, periodCode, showSelectDateAfter)
{
    var dfd = new $.Deferred();
    if(typeof showSelectDateAfter == 'undefined')
        showSelectDateAfter = true;

    var shouldUpdateSession;
    if(showSelectDateAfter)
        shouldUpdateSession = 0;
    else
        shouldUpdateSession = date_range_getUpdateSessionOnChange(baseID);

    date_range_showLoading(baseID);
    var decache = (new Date).getTime();
    var theReq = $.post(absUrl('ajax.php?'+decache), {
                            'module' : 'date_range_control',
                            'action' : 'getControl',
                            'controlID' : baseID,
                            'hiddenName' : _date_range_get_hidden_name(baseID),
                            'changePeriodDisabled' : _date_range_get_change_period_disabled(baseID),
                            'selectedDate' : document.getElementById(baseID + '_date_range_timestamp').value,
                            'newPeriodCode' : periodCode,
                            'updateSession' : shouldUpdateSession,
                            'nextUpdateSession': date_range_getUpdateSessionOnChange(baseID),
                            'origInfo' : _date_range_getOrigTimestampInfo(baseID)
                        });
    date_range_hideSelectDate(baseID);
    theReq.done(function(data)
    {
        document.theData = data;
        if(!data)
            return alert('Failed.');
        data = eval('(' + data + ')');
        if(!data['success'])
            return alert('Failed 2.');

        $('#' + baseID + '_date_range_control').replaceWith(data['control']);

        if(showSelectDateAfter) {
            date_range_showSelectDate(baseID);
        }

        dfd.resolve(data);
    }
    );
    return dfd.promise();
}


function _date_range_getOrigTimestampInfo(baseID)
{
    return [document.getElementById(baseID + '_date_range_orig_timestamp').value,
            document.getElementById(baseID + '_date_range_orig_period_code').value
            ];
}

function date_range_undoChanges(baseID)
{
    date_range_showLoading(baseID);
    date_range_hideSelectDate(baseID);

    var origArr = _date_range_getOrigTimestampInfo(baseID);

    var origTimestamp = origArr[0];
    var origPeriodCode = origArr[1];
    var decache = (new Date).getTime();
    var theReq = $.post(absUrl('ajax.php?'+decache), {
                            'module' : 'date_range_control',
                            'action' : 'getControl',
                            'controlID' : baseID,
                            'hiddenName' : _date_range_get_hidden_name(baseID),
                            'changePeriodDisabled' : _date_range_get_change_period_disabled(baseID),
                            'newSelectedDate' : origTimestamp,
                            'newPeriodCode' : origPeriodCode,
        'nextUpdateSession' : date_range_getUpdateSessionOnChange(baseID),
        'updateSession' : date_range_getUpdateSessionOnChange(baseID)

    });
    theReq.done(function(data)
    {
        document.theData = data;
        if(!data)
            return alert('Failed.');
        data = eval('(' + data + ')');
        if(!data['success'])
            return alert('Failed 2.');
        $('#' + baseID + '_date_range_control').replaceWith(data['control']);
    });
}


function displayDateRangePickerModal(baseID, selectedPeriodForRange, selectedPeriodCode)
{
    //console.log('displayDateRangePickerModal() baseID='+baseID+' selectedPeriodForRange='+selectedPeriodForRange+' selectedPeriodCode='+selectedPeriodCode);
    switch (selectedPeriodForRange) {

        case "Daily Range":
            var daysprior = new Date();
            daysprior.setDate(daysprior.getDate() - 1);
            var today = new Date();
            var startDate = ('0' + (daysprior.getMonth() + 1)).slice(-2)+"/"+('0' + daysprior.getDate()).slice(-2)+"/"+daysprior.getFullYear();
            var endDate =  ('0' + (today.getMonth() + 1)).slice(-2)+"/"+('0' + today.getDate()).slice(-2)+"/"+today.getFullYear();
            pickerFormStart = "<td><input id='"+baseID+"_day_start_date' type='text' value='"+startDate+"'><input type='hidden' id='start_date_id'></td>";
            pickerFormEnd = "<td><input id='"+baseID+"_day_end_date' type='text' value='"+endDate+"'><input type='hidden' id='end_date_id'></td>";
            break;

        case "Weekly Range":
        case "Monthly Range":
        case "Quarterly Range":
        case "Annual Range":
            pickerFormStart = "<td><select id='"+baseID+"_start_date' onChange='handleStartDateSelectionChange(\""+baseID+"\")'><option selected disabled hidden style='display: none' value=''>Loading...</option></select></td>";
            pickerFormEnd = "<td><select id='"+baseID+"_end_date' onchange='handleEndDateSelectionChange(\""+baseID+"\")'><option selected disabled hidden style='display: none' value=''>Loading...</option></select></td>";

            break;

        default:
            pickerFormStart = "<tr><td colspan='2'>Undefined Range</td></tr>";
            pickerFormEnd = pickerFormStart;
            break;
    }

    pickerForm = "<form id='picker'><table width='100%'>";
    pickerForm += "<input id='"+baseID+"_selected_period_for_range' name='selected_range' type='hidden' value=''>";
    pickerForm += "<tr>";
    pickerForm += "<td><b>Start</b>&nbsp;&nbsp;</td>";
    pickerForm += pickerFormStart;
    pickerForm += "</tr><tr><td>&nbsp;</td><td><div id='"+baseID+"_modal_start_date_changed_msg'>&nbsp;</div></td></tr><tr>";
    pickerForm += "<td><b>End</b>&nbsp;&nbsp;</td>";
    pickerForm += pickerFormEnd;
    pickerForm += "</tr>";
    pickerForm += "</tr><tr><td>&nbsp;</td><td><div id='"+baseID+"_modal_end_date_changed_msg'>&nbsp;</div></td></tr><tr>";
    pickerForm += "</table><input type='hidden' id='"+baseID+"_modal_selected_period_code' name='"+baseID+"_modal_selected_period_code' value='"+selectedPeriodCode+"'></form>";
    pickerForm += "<script>setDateRangePickerSelectors('"+baseID+"','"+selectedPeriodForRange+"');</script>"; // since there is no 'onLoad' for zmodal..

    $('<div id="#'+baseID+'_datepicker_zmodal"></div>').zmodal({
        title: $("<b>"+selectedPeriodForRange+"</b>").html(),
        content: $("<div>"+pickerForm+"</div>").html(),
        cancelButton: { disabled:false },
        okButton: { disabled:false, closeOnClick:true },
        width:'340px'
    });

    $('body').unbind('ok.zmodal');
    $('body').bind('ok.zmodal', function () {
        if (selectedPeriodForRange === "Daily Range") {
            if (typeof $('input#'+baseID+'_modal_selected_period_code').val() === 'undefined') {
                return; // PLAT-1052 to handle cases where we dont have a datepicker to select from.
            }
            selectedPeriodCode = $('input#'+baseID+'_modal_selected_period_code').val();
            var sd_value = document.getElementById(baseID+'_day_start_date').value;
            var sd = sd_value.split('/');
            var ed_value = document.getElementById(baseID+'_day_end_date').value;
            var ed = ed_value.split('/');
            //console.log('DEBUG: baseID='+baseID+' start date='+sd_value+' end date='+ed_value+' sd='+JSON.stringify(sd)+' ed='+JSON.stringify(ed));
            //console.log('selectedPeriodCode='+selectedPeriodCode);
            date_range_showLoading(baseID);
            $.when( get_date_range_by_date(sd[2]+"-"+sd[0]+"-"+sd[1]) )
                .done( function (start_range) {
                    start_date_id = start_range['data']['id'];
                    //console.log(baseID+'start_date_id='+start_date_id);
                    $.when( get_date_range_by_date(ed[2]+"-"+ed[0]+"-"+ed[1]) )
                        .done( function (end_range) {
                            end_date_id = end_range['data']['id'];
                            //console.log(baseID+'end_date_id='+end_date_id);
                            $.when( setSelectionPersistence( baseID, selectedPeriodForRange, selectedPeriodCode, start_date_id, end_date_id ) )
                                .done( function () {
                                    console.log('   +-> date_range_selection_special_range_selected()');
                                    date_range_selection_special_range_selected(baseID, null, false);
                                });
                        }
                    );
                }
            );
        } else {
            if (typeof $('input#'+baseID+'_modal_selected_period_code').val() === 'undefined') {
                return; // PLAT-1052 to handle cases where we dont have a datepicker to select from.
            }
            selectedPeriodCode = $('input#'+baseID+'_modal_selected_period_code').val();
            start_date_id = $('select#'+baseID+'_start_date').val();
            end_date_id = $('select#'+baseID+'_end_date').val();
            //console.log('selectedPeriodCode='+selectedPeriodCode);
            //console.log(baseID+'start_date_id='+start_date_id);
            //console.log(baseID+'end_date_id='+end_date_id);
            $.when(setSelectionPersistence( baseID, selectedPeriodForRange, selectedPeriodCode, start_date_id, end_date_id ) )
                .done( function () {
                    //console.log('   +-> date_range_selection_special_range_selected()');
                    date_range_selection_special_range_selected(baseID, null, false);
                });
        }
    });

    // Roll back the date range selection box navigation - DO NOT REMOVE THIS.
    //console.log(' zModal date_range_undoChanges');
    date_range_undoChanges(baseID);
}

function setSelectionPersistence( baseID, selectedPeriodForRange, selectedPeriodCode, start_date_id, end_date_id ) {
    var dfd = new $.Deferred();
    //console.log('setSelectionPersistence(baseID='+baseID+' selectedPeriodForRange='+selectedPeriodForRange+' selectedPeriodCode='+selectedPeriodCode+' start_date_id='+start_date_id+' end_date_id='+end_date_id+')');
    //console.log(' +-> set_selected_range_period()');
    $.when( set_selected_range_period(baseID+'|'+selectedPeriodForRange+'|'+selectedPeriodCode+'|'+start_date_id+'|'+end_date_id) )
        .done( function () {
            //console.log('  +-> setCurrentPeriodCode()');
            setCurrentPeriodCode(baseID, selectedPeriodCode);
            dfd.resolve();
        });
    return dfd.promise();
}

// Disable function
jQuery.fn.extend({
    disable: function(state) {
        return this.each(function() {
            this.disabled = state;
        });
    }
});

function setDateRangePickerSelectors(baseID, selectedPeriodForRange)
{
    //console.log('setDateRangePickerSelectors() selectedPeriodForRange='+selectedPeriodForRange);
    var selected_range_id = $('#'+baseID+'_date_range_hidden_id').val();
    var selected_start_date_id = $('#'+baseID+'_start_date_id').val();
    var selected_end_date_id = $('#'+baseID+'_end_date_id').val();
    var custom_range = selected_range_id.split(",");
    //console.log(' - custom_range.length='+custom_range.length);
    if (custom_range.length > 1) {
        //console.log(' ~ custom date set, overriding selected start and end date ids in order to get it to work..');
        selected_start_date_id = custom_range[0];
        selected_end_date_id = custom_range[1];
    } else {
        //console.log(' ~ individual date range, overriding selected start and end date ids in order to get it to work..');
        selected_start_date_id = selected_range_id;
        selected_end_date_id = selected_range_id;

    }

    document.getElementById(baseID+'_selected_period_for_range').value = selectedPeriodForRange;

    //console.log(' - selected_range_id='+selected_range_id);
    //console.log(' - selected_start_date_id='+selected_start_date_id);
    //console.log(' - selected_end_date_id='+selected_end_date_id);

    switch (selectedPeriodForRange) {

        case "Daily Range":
            var nowTemp = new Date();
            var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
            var divStartDate = "#"+baseID+"_day_start_date";
            var divEndDate = "#"+baseID+"_day_end_date";

            // get existing dates if we have them set..
            if (selected_start_date_id != 0 && selected_start_date_id != 'undefined' && selected_end_date_id != 0 && selected_end_date_id != 'undefined') {

                document.getElementById(baseID+'_day_start_date').value = 'Loading...';
                document.getElementById(baseID+'_day_end_date').value = 'Loading...';

                $.when(getDateRangeById(selected_start_date_id))
                    .done( function (existing_start_date_data) {
                        //console.log('existing_start_date_data='+JSON.stringify(existing_start_date_data));
                        rsd = existing_start_date_data['data']['begin_date']['date'];
                        // TODO: Handle user-preference for date format
                        document.getElementById(baseID+'_day_start_date').value = rsd.substring(5,7)+'/'+rsd.substring(8,10)+'/'+rsd.substring(0,4);
                        $(divStartDate).datepicker('update');
                        $.when(getDateRangeById(selected_end_date_id))
                            .done( function (existing_end_date_data) {
                                //console.log('existing_end_date_data='+JSON.stringify(existing_end_date_data));
                                red = existing_end_date_data['data']['end_date']['date'];
                                // TODO: Handle user-preference for date format
                                document.getElementById(baseID+'_day_end_date').value = red.substring(5,7)+'/'+red.substring(8,10)+'/'+red.substring(0,4);
                                $(divEndDate).datepicker('update');
                        });
                    });
            }

            var startDate = $(divStartDate).datepicker({
                onRender: function(date) {
                return date.valueOf() >= now.valueOf() ? 'disabled' : '';
                }
            }).on('changeDate', function(ev) {
                if (ev.date.valueOf() > endDate.date.valueOf()) {
                    var newDate = new Date(ev.date)
                    newDate.setDate(newDate.getDate() + 1);
                    endDate.setValue(newDate);
                }
                startDate.hide();
                $(divEndDate)[0].focus();
                $(divEndDate).datepicker('update');
            }).on('hide', function(ev) {
                 ev.stopPropagation(); // prevent 'hide' event from clobbering modal 'ok' button 'hide' event.
             }).data('datepicker');

            var endDate = $(divEndDate).datepicker({
                onRender: function(date) {
                    return (date.valueOf() <= startDate.date.valueOf() ? 'disabled' : '' || date.valueOf() >= nowTemp.valueOf() ? 'disabled' : '');
                }
            }).on('changeDate', function(ev) {
                endDate.hide();
            }).on('hide', function(ev) {
                ev.stopPropagation(); // prevent 'hide' event from clobbering modal 'ok' button 'hide' event.
            }).data('datepicker');

            return; // exit early hack
            break;

        case "Weekly Range":
            range_spread = 780;
            range_type = "WK";
            break;

        case "Monthly Range":
            range_spread = 180;
            range_type = "MTH";
            break;

        case "Quarterly Range":
            range_spread = 60;
            range_type = "QTR";
            break;

        case "Annual Range":
            range_spread = 15;
            range_type = "YR";
            break;

        default:
            range_spread = 1;
            range_type = "";
            break;
    }

    //console.log(' - range_spread='+range_spread+' range_type='+range_type);

    start_range_options = "";
    load_date_range_list(selected_range_id, (range_spread), (range_spread), range_type, false).then( function( start_list ) {
        start_range = start_list['data'];
        selected_flag = false;
        for (var i in start_range) {
            selected="";
            if (parseInt(start_range[i]['id']) === parseInt(selected_start_date_id)) {
                selected = "selected=\"selected\"";
                selected_flag = true;
            } else if ((parseInt(i) === parseInt(start_range.length - 1)) && (selected_flag === false)) {
                selected = "selected=\"selected\"";
            }
            start_range_options += '<option sequence="'+start_range[i]['sequence']+'" value="'+ start_range[i]['id'] + '" '+selected+'>' + start_range[i]['title'] + '</option>';
        }
        $('select#'+baseID+'_start_date').html(start_range_options);
        //console.log(' - start date selected');

        end_range_options = "";
        load_date_range_list(selected_range_id, (range_spread), (range_spread), range_type, false).then( function( end_list ) {
            end_range = end_list['data'];
            selected_flag = false;
            for (var i in end_range) {
                selected="";
                if (parseInt(end_range[i]['id']) === parseInt(selected_end_date_id)) {
                    selected = "selected=\"selected\"";
                    selected_flag = true;
                } else if ((parseInt(i) === parseInt(end_range.length - 1)) && (selected_flag === false)) {
                    selected = "selected=\"selected\"";
                }
                end_range_options += '<option sequence="'+end_range[i]['sequence']+'" value="'+ end_range[i]['id'] + '" '+selected+'>' + end_range[i]['title'] + '</option>';
            }
            $('select#'+baseID+'_end_date').html(end_range_options);
            //console.log(' - end date selected');
            determineAssociatedDateRanges(baseID, selectedPeriodForRange);
        });
    });
}

function determineAssociatedDateRanges(baseID, selectedPeriodForRange) {

    //console.log('determineAssociatedDateRanges()');

    selected_range_id = $('#'+baseID+'_date_range_hidden_id').val();
    selected_start_date_id = $('#'+baseID+'_start_date_id').val();
    selected_end_date_id = $('#'+baseID+'_end_date_id').val();

    switch (selectedPeriodForRange) {
        case "Daily Range":
            periodCode = "DAY";
            break;

        case "Weekly Range":
            periodCode = "WK";
            break;

        case "Monthly Range":
            periodCode = "MTH";
            break;

        case "Quarterly Range":
            periodCode = "QTR";
            break;

        case "Annual Range":
            periodCode = "YR";
            break;

    }

    // Custom Range
    if (selected_start_date_id != 0 && selected_start_date_id != 'undefined' && selected_end_date_id != 0 && selected_end_date_id != 'undefined') {
        $.when(get_associated_range_by_date_range_id_and_period(selected_start_date_id, periodCode)
            .done(function (corresponding_start_date) {
                $.when(get_associated_range_by_date_range_id_and_period(selected_end_date_id, periodCode))
                    .done(function (corresponding_end_date) {
                        //console.log('Custom Range - corresponding_start_date=' + corresponding_start_date['data']['id']);
                        //console.log('Custom Range - corresponding_end_date=' + corresponding_end_date['data']['id']);
                        $('#' + baseID + '_start_date option[value="' + corresponding_start_date['data']['id'] + '"]').prop("selected", true);
                        $('#' + baseID + '_end_date option[value="' + corresponding_end_date['data']['id'] + '"]').prop("selected", true);
                    });
            }));
    } else { // Single Range
        $.when(get_associated_range_by_date_range_id_and_period(selected_range_id, periodCode))
            .done(function (corresponding_start_date) {
                //console.log('Single Range - corresponding_start_date=' + corresponding_start_date['data']['id']);
                $('#' + baseID + '_start_date option[value="' + corresponding_start_date['data']['id'] + '"]').prop("selected", true);
                $('#' + baseID + '_end_date option[value="' + corresponding_start_date['data']['id'] + '"]').prop("selected", true);
            });
    }
}

function handleStartDateSelectionChange(baseID)
{
    //console.log('\nhandleStartDateSelectionChange()');
    selected_start_date_id = $('select#'+baseID+'_start_date option:selected').val();
    selected_start_date_seq = $('select#'+baseID+'_start_date option:selected').attr('sequence');
    selected_end_date_id = $('select#'+baseID+'_end_date option:selected').val();
    selected_end_date_seq = $('select#'+baseID+'_end_date option:selected').attr('sequence');

    //console.log('selected_start_date_id='+selected_start_date_id);
    //console.log('selected_start_date_seq='+selected_start_date_seq);
    //console.log('selected_end_date_id='+selected_end_date_id);
    //console.log('selected_end_date_seq='+selected_end_date_seq);

    if(+selected_start_date_seq > +selected_end_date_seq) {
        //console.log('selected_start_date_seq > selected_end_date_seq');
        $('#'+baseID+'_end_date option[value="'+selected_start_date_id+'"]').prop("selected", true);
        document.getElementById(baseID+'_modal_start_date_changed_msg').innerHTML = "";
        document.getElementById(baseID+'_modal_end_date_changed_msg').innerHTML = "<small>Note: End date has been adjusted to prevent overlap.</small>";
    } else if (pathMatches('/wsi_report.php')) {
        enforceFYTDStartEndDateLimitations(baseID, "start");
    }
}

function handleEndDateSelectionChange(baseID)
{
    //console.log('\nhandleEndDateSelectionChange()');
    selected_start_date_id = $('select#'+baseID+'_start_date option:selected').val();
    selected_start_date_seq = $('select#'+baseID+'_start_date option:selected').attr('sequence');
    selected_end_date_id = $('select#'+baseID+'_end_date option:selected').val();
    selected_end_date_seq = $('select#'+baseID+'_end_date option:selected').attr('sequence');

    //console.log('selected_start_date_id='+selected_start_date_id);
    //console.log('selected_start_date_seq='+selected_start_date_seq);
    //console.log('selected_end_date_id='+selected_end_date_id);
    //console.log('selected_end_date_seq='+selected_end_date_seq);

    if(+selected_end_date_seq < +selected_start_date_seq) {
        //console.log('selected_end_date_seq < selected_start_date_seq');
        $('#'+baseID+'_start_date option[value="'+selected_end_date_id+'"]').prop("selected", true);
        document.getElementById(baseID+'_modal_start_date_changed_msg').innerHTML = "<small>Note: Start date has been adjusted to prevent overlap.</small>";
        document.getElementById(baseID+'_modal_end_date_changed_msg').innerHTML = "";
    } else if (pathMatches('/wsi_report.php')) {
        enforceFYTDStartEndDateLimitations(baseID, "end");
    }
}

function pathMatches(pathname) {
    //console.log('pathMatches() path='+window.location.pathname+' = '+pathname+' ?');
    if (window.location.pathname === pathname) { return true; }
    return false;
}

function enforceFYTDStartEndDateLimitations(baseID, whatDateChanged)
{
    $('input[type="submit"], input[type="button"], button').disable(true);
    selected_start_date_id = $('select#' + baseID + '_start_date option:selected').val();
    selected_end_date_id = $('select#' + baseID + '_end_date option:selected').val();

    if (whatDateChanged === "start") {
        get_fytd_range_by_date_range_id(selected_start_date_id).then( function( fytd_range ) {
            fytd_start_date_range_id = fytd_range['data']['start']['id'];
            fytd_end_date_range_id = fytd_range['data']['end']['id'];
            if (+selected_end_date_id > +fytd_end_date_range_id) {
                $('#'+baseID+'_end_date option[value="'+ fytd_end_date_range_id +'"]').prop("selected", true);
                document.getElementById(baseID+'_modal_end_date_changed_msg').innerHTML = "<small>Note: End date has been adjusted to ensure the range is within a Fiscal Year.</small>";
                document.getElementById(baseID+'_modal_start_date_changed_msg').innerHTML = "";
            } else {
                document.getElementById(baseID+'_modal_start_date_changed_msg').innerHTML = "";
                document.getElementById(baseID+'_modal_end_date_changed_msg').innerHTML = "";
            }
            $('input[type="submit"], input[type="button"], button').disable(false);
        });

    } else {
        get_fytd_range_by_date_range_id(selected_end_date_id).then( function( fytd_range ) {
            fytd_start_date_range_id = fytd_range['data']['start']['id'];
            fytd_end_date_range_id = fytd_range['data']['end']['id'];
            if (+selected_start_date_id < +fytd_start_date_range_id) {
                $('#' + baseID + '_start_date option[value="' + fytd_start_date_range_id + '"]').prop("selected", true);
                document.getElementById(baseID + '_modal_start_date_changed_msg').innerHTML = "<small>Note: Start date has been adjusted to ensure the range is within a Fiscal Year.</small>";
                document.getElementById(baseID + '_modal_end_date_changed_msg').innerHTML = "";
            } else {
                document.getElementById(baseID+'_modal_start_date_changed_msg').innerHTML = "";
                document.getElementById(baseID+'_modal_end_date_changed_msg').innerHTML = "";
            }
            $('input[type="submit"], input[type="button"], button').disable(false);
        });

    }
}

function getCurrentTimestamp(baseID)
{
    if(baseID == undefined)
        baseID = 'main';
    return document.getElementById(baseID + '_date_range_timestamp').value;
}

function getCurrentPeriodID(baseID)
{
    if(baseID == undefined)
        baseID = 'main';
    return document.getElementById(baseID + '_date_range_period_id').value;
}

function getCurrentPeriodCode(baseID)
{
    if(baseID == undefined)
        baseID = 'main';
    return document.getElementById(baseID + '_date_range_period_code').value;
}

function setCurrentPeriodCode(baseID, periodCode)
{
    if(baseID == undefined)
        baseID = 'main';
    document.getElementById(baseID + '_date_range_period_code').value = periodCode;
}

function getCurrentDateRangeId(baseID)
{
    if(baseID == undefined) {
        baseID = 'main';
    }
    var theInput = document.getElementById(baseID + '_date_range_hidden_id');
    if(theInput)
        return theInput.value;
    var id = getPageData('currentDateRangeId');
    return id ? id : undefined;
}

function date_range_closeAll()
{
    var ems = $('.date-range-control');
    for(var i=0; i < ems.length; i++)
    {
        var baseID = ems[i].id.replace('_date_range_control', '');
        var selectEm = document.getElementById(baseID + '_select_date_range_box');
        if(selectEm.style.display === 'none')
            continue;
        date_range_undoChanges(baseID);
    }
}

function load_date_range_list(dateRangeId, historyPeriods, futurePeriods, range_type, realizedDatesOnly )
{
    var dfd = new $.Deferred();
    var decache = (new Date).getTime();
    var theReq = $.post(absUrl('ajax.php?'+decache), {
        'module' : 'dateRange',
        'action' : 'getHistoryAndFuturePeriods',
        'dateRangeId' : dateRangeId,
        'historyPeriods' : historyPeriods,
        'futurePeriods' : futurePeriods,
        'periodCode' : range_type,
        'realizedDates' : realizedDatesOnly
    });

    theReq.done(function(data)
    {
        if(!data) return alert('Failed.');
        data = eval('(' + data + ')');
        if(!data['success']) return alert('Failed 2.');
        dfd.resolve(data);
    } );
    return dfd.promise();
}

function get_date_range_by_date(date)
{
    var dfd = new $.Deferred();
    var decache = (new Date).getTime();
    var theReq = $.post(absUrl('ajax.php?'+decache), {
        'module' : 'dateRange',
        'action' : 'getDateRangeByDate',
        'date' : date
    });

    theReq.done(function(data)
    {
        if(!data) return alert('Failed.');
        data = eval('(' + data + ')');
        if(!data['success']) return alert('Failed 2.');
        dfd.resolve(data);
    } );
    return dfd.promise();
}

function set_selected_range_period(period_token)
{
    var dfd = new $.Deferred();
    var decache = (new Date).getTime();
    var theReq = $.post(absUrl('ajax.php?'+decache), {
        'module' : 'dateRange',
        'action' : 'setSelectedRangePeriod',
        'periodToken' : period_token
    });

    theReq.done(function(data)
    {
        if(!data) return alert('Failed.');
        data = eval('(' + data + ')');
        if(!data['success']) return alert('Failed 2.');
        dfd.resolve(data);
    } );
    return dfd.promise();
}

function date_range_selection_special_range_selected(baseID, newDatetime, showSelectDateAfter)
{
    //console.log('### Selection made: Special Range selected. ###');
    //console.log(' - baseID='+baseID);
    //console.log(' - newDatetime='+newDatetime);
    //console.log(' - showSelectDateAfter='+showSelectDateAfter);
    date_range_changeDate(baseID, newDatetime, showSelectDateAfter).done( function () {
        triggerDateChangedEvent(baseID);
    });
    //console.log('###############################################');
}

function date_range_selection_period_chosen(baseID, periodCode)
{
    //console.log('### Selection made: Period Chosen. ###');
    //console.log(' - baseID='+baseID);
    //console.log(' - periodCode='+periodCode);
    date_range_selectPeriod(baseID, periodCode);
    //console.log('######################################');
}

function date_range_selection_individual_range_chosen(baseID, dateRangeElementSelected, dateRangeID)
{
    //console.log('### Selection made: Individual Range Chosen. ###');
    //console.log(' - baseID='+baseID);
    //console.log(' - dateRangeElementSelected='+dateRangeElementSelected);
    //console.log(' - dateRangeID='+dateRangeID);
    $('#'+baseID+'_date_range_id').val(dateRangeID);
    date_range_selectDate(dateRangeElementSelected, dateRangeID).done( function () {
        triggerDateChangedEvent(baseID);
    });
    //console.log('################################################');
}

function triggerDateChangedEvent(baseID)
{
	//console.log('triggerDateChangedEvent() baseID: '+baseID);
	if (((window.location.pathname === '/scorecard.php')
		|| (window.location.pathname === '/objective_health.php')
		|| (window.location.pathname === '/objective_dashboard.php')
		|| (window.location.pathname === '/goal_health.php')
		|| (window.location.pathname === '/goal_gauges.php')
		|| (window.location.pathname === '/task_health.php')
		) && baseID === 'main') {
        //console.log(' - hard reload');
        setTimeout(function() {
            $(window).trigger('date-range:' + baseID + ':date-changed-reload');
        }, 1500); // Pause before triggering reload
    } else {
        //console.log(' - soft reload');
        $(window).trigger('date-range:' + baseID + ':date-changed');
    }
}

function wsiProductivityReportSubmitHandler()
{
    //console.log('wsiProductivityReportSubmitHandler()');
    if (document.getElementById('theDateRange_selected_range_period').value !="") {
        //alert(document.getElementById('theDateRange_selected_range_period').value);
        start_date_id = document.getElementById('theDateRange_start_date_id').value;
        end_date_id = document.getElementById('theDateRange_end_date_id').value;
        document.getElementById('theDateRange_date_range_hidden_id').value = start_date_id+","+end_date_id;
    }

    var store = document.getElementsByName('store')[0].options[document.getElementsByName('store')[0].selectedIndex].value;

    if (store == -1) {
        wsiProductivityReportSubmitJobQueue();
        window.location.href = 'documents.php';
        return false;
    }
    else {
        $.blockUI();
        return true;
    }
}

function wsiProductivityReportSubmitPDFHandler()
{
    //console.log('wsiProductivityReportSubmitPDFHandler()');
    if (document.getElementById('theDateRange_selected_range_period').value !="") {
        //alert(document.getElementById('theDateRange_selected_range_period').value);
        start_date_id = document.getElementById('theDateRange_start_date_id').value;
        end_date_id = document.getElementById('theDateRange_end_date_id').value;
        document.getElementById('theDateRange_date_range_hidden_id').value = start_date_id+","+end_date_id;
    }
    var dateRangeID = document.getElementById('theDateRange_date_range_hidden_id').value;
    //console.log('date range id='+dateRangeID+' store='+store);

    var brand = document.getElementsByName('brand')[0].options[document.getElementsByName('brand')[0].selectedIndex].value;
    var region = document.getElementsByName('region')[0].options[document.getElementsByName('region')[0].selectedIndex].value;
    var district = document.getElementsByName('district')[0].options[document.getElementsByName('district')[0].selectedIndex].value;
    var store = document.getElementsByName('store')[0].options[document.getElementsByName('store')[0].selectedIndex].value;

    if (store > -1) {
        post_to_url('/postPdf.php', {url:'/wsi_report.php', dpi: 105, orientation: 'landscape', brand: brand, region: region, district: district, store: store, date_range: dateRangeID}, 'post', true);
    } else {
        alert('Only store level reports can be submitted interactivly to PDF');
    }
}

function wsiProductivityReportSubmitJobQueue()
{
    if (document.getElementById('theDateRange_selected_range_period').value !="") {
        //alert(document.getElementById('theDateRange_selected_range_period').value);
        start_date_id = document.getElementById('theDateRange_start_date_id').value;
        end_date_id = document.getElementById('theDateRange_end_date_id').value;
        document.getElementById('theDateRange_date_range_hidden_id').value = start_date_id+","+end_date_id;
    }
    var dateRangeID = document.getElementById('theDateRange_date_range_hidden_id').value;
    var brand = document.getElementsByName('brand')[0].options[document.getElementsByName('brand')[0].selectedIndex].value;
    var region = document.getElementsByName('region')[0].options[document.getElementsByName('region')[0].selectedIndex].value;
    var district = document.getElementsByName('district')[0].options[document.getElementsByName('district')[0].selectedIndex].value;
    var store = document.getElementsByName('store')[0].options[document.getElementsByName('store')[0].selectedIndex].value;

    var dfd = new $.Deferred();
    var decache = (new Date).getTime();

    var theReq = $.post(absUrl('ajax.php?'+decache), {
            module: 'report',
            action: 'queueWSIReport',
            dateRange: dateRangeID,
            brand: brand,
            region: region,
            district: district,
            store: store
        },
        true
    );

    alert('The requested report will be available for download shortly in your documents area.');

    theReq.done(function(data)
    {
        if(!data) return alert('Failed.');
        data = eval('(' + data + ')');
        if(!data['success']) return alert('Failed 2.');

        dfd.resolve(data);
    });

    return dfd.promise();
}

function getDateRangeById(dateRangeID)
{
    var dfd = new $.Deferred();
    var decache = (new Date).getTime();
    var theReq = $.post(absUrl('ajax.php?'+decache), {
        module: 'dateRange',
        action: 'getDateRangeById',
        dateRangeId: dateRangeID},
        true
    );

    theReq.done(function(data)
    {
        if(!data) return alert('Failed.');
        data = eval('(' + data + ')');
        if(!data['success']) return alert('Failed 2.');

        dfd.resolve(data);
    });

    return dfd.promise();
}

function get_fytd_range_by_date_range_id(dateRangeID)
{
    var dfd = new $.Deferred();
    var decache = (new Date).getTime();
    var theReq = $.post(absUrl('ajax.php?'+decache), {
            module: 'dateRange',
            action: 'getFYTDDateRangesByDateRangeId',
            dateRangeId: dateRangeID},
        true
    );

    theReq.done(function(data)
    {
        if(!data) return alert('Failed.');
        data = eval('(' + data + ')');
        if(!data['success']) return alert('Failed 2.');

        dfd.resolve(data);
    });

    return dfd.promise();
}

function get_associated_range_by_date_range_id_and_period(dateRangeID, periodCode)
{
    var dfd = new $.Deferred();
    var decache = (new Date).getTime();
    var theReq = $.post(absUrl('ajax.php?'+decache), {
            module: 'dateRange',
            action: 'getAssociatedDateRangeByDateRangeIdAndPeriod',
            dateRangeId: dateRangeID,
            periodCode: periodCode},
        true
    );

    theReq.done(function(data)
    {
        if(!data) return alert('Failed.');
        data = eval('(' + data + ')');
        if(!data['success']) return alert('Failed 2.');

        dfd.resolve(data);
    });

    return dfd.promise();
}

$(function() {
    $(window).keyup(function(e) {
                            var ev = e || window.event;
                            var whichKey = ev.which || ev.keyCode;
                            if(whichKey === 27) //escape
                            {
                                date_range_closeAll();
                            }
                            });


    $(window).mouseup(function(e) {
                            var ev = e || window.event;
                            // If within a daterange control, do not close, otherwise close.
                            if(ev.which != 1)
                                return;

                            var targetEm = ev.target || ev.srcElement;
                            if(!targetEm)
                                return;

                            if(!!($(targetEm).parents('.date-range-control')[0]))
                                return;


                            var whichKey = ev.which;
                            if(whichKey === 1)
                            {
                                date_range_closeAll();
                            }
                        });
});
