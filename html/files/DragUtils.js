// DRAG AND DROP STUFFFFF. 

	

// This returns the callback function for resizing.
// THIS IS USED FOR THE MAIN SIDEBAR DIVIDER
// For time sake, this is currently content-left implicit, and horizontal-scale implicit.
function _getResizeFunction(resizerEm, resizableContent, limitSizeMax, onMouseUpCallback)
{
	// "Closure" is a funny word a guy told me one time for scope-aware code.
	return (function(passedEvent1) 
		{
			document._smoothDragTimeout = 0;
			try
			{
				document.body.style.setProperty('cursor', 'col-resize', 'important');
			}
			catch(e)
			{
				document.body.style.cursor = 'col-resize';
			}
			var oldResizerCursor = resizerEm.style.cursor;
			resizerEm.style.cursor = 'inherit';
			resizableContent.style.cursor = 'inherit';

			var _resizerMidWidth = resizerEm.offsetWidth / 2;
			var _applyLeftAndWidth = (function (value)
			{
				resizerEm.style.left = value + 'px';
				resizableContent.style.width = value + 'px';
				if(value <= _resizerMidWidth / 2)
					resizableContent.style.display = 'none';
				else
					resizableContent.style.display = '';
			});
			window._applyLeftAndWidth = _applyLeftAndWidth; // Move function to global scope for smooth-drag effect.
			document.onmouseover = function() { return false; };

			document.onmousemove = (function(passedEvent2) 
			{
				$(resizerEm).css("pointer-events", "none");
				var theEvent = passedEvent2 || window.event;
				var newSize;

				clearTimeout(document._smoothDragTimeout);

				if(limitSizeMax !== undefined)
					newSize = Math.min(theEvent.clientX - _resizerMidWidth, limitSizeMax);
				else
					newSize = theEvent.clientX - _resizerMidWidth;

				document._smoothDragTimeout = setTimeout('_applyLeftAndWidth(' + newSize + ');', 5); // 5ms cap for responsiveness
				return false;
			});
			document.onmouseup = (function(e)
				{
					document.onmousemove = null; 
					document.body.style.cursor = ''; 
					resizerEm.style.cursor = oldResizerCursor;
					$(resizerEm).css("pointer-events", "auto");
					resizableContent.style.cursor = '';
					if(!!onMouseUpCallback) 
					{ 
						try 
						{ 
							onMouseUpCallback();
						} 
						catch(e) { } 
					} 
					setTimeout('document.onmouseup = null;', 1); 
				});
			return false;
		});
}


// For time sake, this is currently content-left implicit, and horizontal-scale implicit.
function addResizeDrag(resizerEm, resizableContent, limitSizeMax, onMouseUpCallback)
{
	resizerEm = $('#' + resizerEm).first()[0];
	resizableContent = $('#' + resizableContent).first()[0];
	$(resizerEm).mousedown(_getResizeFunction(resizerEm, resizableContent, limitSizeMax, onMouseUpCallback));
}


document.dragMovingElement = null;
document.inDraggingFunction = false;

// On a timeout to let first-pass of events through (like clicks of buttons on the handle)
document.startDraggingDelayTimeout = null;
function startDragAndDropHandle(ev, handleEvent, movingElement)
{
	var theEvent = ev || window.event;
	var whichKey = theEvent.button || theEvent.which;
	if(whichKey != 1)
		return;
	$(document.body).addClass('nohighlight');
	clearTimeout(document.startDraggingDelayTimeout);
	var origClientX = ev.clientX; // Hack for IE which won't pass event
	document.onmouseup = (function() { if(!document.inDraggingFunction) clearTimeout(document.startDraggingDelayTimeout); });
	document.startDraggingDelayTimeout = setTimeout((function() { _startDragAndDropHandle(origClientX, handleEvent, movingElement); }), 200);
}

function _startDragAndDropHandle(origClientX, handleElement, movingElement)
{
	document.inDraggingFunction = true;
	$(document.body).addClass('nohighlight');
	handleElement.style.cursor = 'move';
	document.body.style.cursor = 'move';
	document.dragMovingElement = movingElement;
	movingElement.style.left = movingElement.offsetLeft + 'px';
	movingElement.style.top = (movingElement.offsetTop - 1.5*handleElement.offsetHeight) + 'px';
	movingElement.style.position = 'fixed';

	var origLeftOffset = (origClientX - movingElement.offsetLeft);

	document.onmousemove = (function(ev2) 
			{
				var theEvent = ev2 || window.event;

				// Don't allow to be dragged past where it can be dragged back. Quarter width must remain on alll sides (except top, all must remain)
				var minLeft = 0 - movingElement.offsetWidth * .75;
				var maxLeft = $(window).width() - (movingElement.offsetWidth * .25);
				var minTop = 0;
				var maxTop = $(window).height() - (movingElement.offsetHeight * .35);

				var newLeft = (theEvent.clientX - origLeftOffset);
				if(newLeft < minLeft)
					newLeft = minLeft;
				if(newLeft > maxLeft)
					newLeft = maxLeft;

				var newTop = (theEvent.clientY - 2 * handleElement.offsetHeight);
				if(newTop < minTop)
					newTop = minTop;
				if(newTop > maxTop)
					newTop = maxTop;

				movingElement.style.left = newLeft + 'px';
				movingElement.style.top =  newTop  + 'px';
				return false;
			});

	document.onmouseup = (function(e)
			{
				document.onmousemove = null;
				$(document.body).removeClass('nohighlight');
				movingElement.style.position = 'absolute';
				document.body.style.cursor = '';
				
			});
	document.inDraggingFunction = false;
}

		
